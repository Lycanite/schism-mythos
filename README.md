# Schism Mythos

A Doom Total Conversion mod designed for GZDoom (uses ZScript).

This brings the creatures and major characters of the Schism Universe to Doom.

Alone this mod adds playable classes and random monster spawners for use with classic Doom and Doom engine games as well as the many Doom mods out there.


## Maps

Map mods are also available such as the base `schism-mythos-maps` mod (separate git repo) which adds the main story maps (and a lot of music).

Other projects may also freely make use of this mod as a base.
