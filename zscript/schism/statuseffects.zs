class SchismStatusEffect : Powerup {
	mixin SpawnParticleMixin;
	
	bool detrimental; // If true, this effect should be considered entirely negative or harmful.

	Actor effectInflictor; // The actor to set as the overall creator of the effect.
	Actor effectSource; // The actor to set as the direct source of the effect, this could be a projectile, etc.
	SchismFollowEffect followEffect; // An invisible actor that follows the afflicted for additional effects.
	int effectInitialTime; // The initial duration of the effect in tics.
	int effectLifetime; // The lifetime (how long this effect should last) in tics.
	bool effectInvalid; // If true, something has happened to the afflicted to mark this effect as invalid changing some logic when expiring, depends on the effect.
	
	int effectInterval; // The interval in tics that this effect should apply periodic effects such as damage.
	Class<SchismStatusEffect> effectSpread; // The effect to spread to enemies within this effect's radius.
	int effectDamage;
	int effectRadius;
	string effectDamageType;
	bool effectPeriodicDamage; // True if this effect causes damage over time.
	double effectSpeedFactor; // If true, the follow effect actor will deal periodic damage to the afflicted.
	
	float offsetLat;
	float offsetLon;
	float offsetVert;
	
	string particleName; // The name of the particle to spawn from this effect's follow projectile, an empty string for no particles.
	float particleScale; // The scale of the particle to spawn from this effect's follow projectile.
	float particleSpeed; // The speed of the particle to spawn from this effect's follow projectile.
	int particleColor; // The color of the particle to spawn from this effect's follow projectile.
	int particleCount; // The number of particles to spawn from this effect's follow projectile.
	int particleInterval; // The frequency in tics to spawn particles from this effect's follow projectile.
	int particleDuration; // The duration that particles spawned from this effect's follow projectile last in tics.
	float particleOffsetZ; // A z offset from the center of this effect's follow projectile to spawn particles from.
	float particleOffsetRandom; // A random offset from the center of this effect's follow projectile to spawn particles from, 0 for no randomness.
	
	property Detrimental: detrimental;

	property EffectInterval: effectInterval;
	property EffectSpread: effectSpread;
	property EffectDamage: effectDamage;
	property EffectRadius: effectRadius;
	property EffectDamageType: effectDamageType;
	property EffectPeriodicDamage: effectPeriodicDamage;
	property EffectSpeedFactor: effectSpeedFactor;

	property OffsetLat: offsetLat;
	property OffsetLon: offsetLon;
	property OffsetVert: offsetVert;
	
	property ParticleName: particleName;
	property ParticleScale: particleScale;
	property ParticleSpeed: particleSpeed;
	property ParticleColor: particleColor;
	property ParticleCount: particleCount;
	property ParticleInterval: particleInterval;
	property ParticleDuration: particleDuration;
	property ParticleOffsetZ: particleOffsetZ;
	property ParticleOffsetRandom: particleOffsetRandom;
	
	Default {
		Inventory.Amount 1;
		Inventory.MaxAmount 1;
		Powerup.Duration -20;
		Powerup.Strength 1; // Effects vary, ignored by default.
		
		SchismStatusEffect.Detrimental false;

		SchismStatusEffect.EffectInterval TICRATE;
		SchismStatusEffect.EffectSpread "";
		SchismStatusEffect.EffectDamage 0;
		SchismStatusEffect.EffectRadius 0;
		SchismStatusEffect.EffectDamageType "None";
		SchismStatusEffect.EffectPeriodicDamage false;
		SchismStatusEffect.EffectSpeedFactor 1.0;

		SchismStatusEffect.ParticleName "";
		SchismStatusEffect.ParticleScale 1.0;
		SchismStatusEffect.ParticleSpeed 2;
		SchismStatusEffect.ParticleColor 0xF7000000;
		SchismStatusEffect.ParticleCount 1;
		SchismStatusEffect.ParticleInterval 8;
		SchismStatusEffect.ParticleDuration TICRATE;
		SchismStatusEffect.particleOffsetZ 0;
		SchismStatusEffect.particleOffsetRandom 0;

		+Inventory.ALWAYSPICKUP;
		-Inventory.ADDITIVETIME;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.effectLifetime = self.effectInterval;
	}
	
	override void DoEffect() {
		super.DoEffect();
		
		self.effectLifetime++;

		// Update Follow Effect:
		self.UpdateFollowEffect();

		// Stop if this effect has no traceable source.
		if (!self.effectSource) {
			return;
		}

		// Periodic Damage:
		if (self.effectPeriodicDamage && self.owner && self.effectLifetime % self.effectInterval == 0) {
			self.owner.DamageMobj(self.effectInflictor, self.effectSource, Floor(self.effectDamage * self.strength), self.effectDamageType, DMG_NO_ARMOR|DMG_NO_PAIN|DMG_THRUSTLESS);
		}
		
		// Effect Spreading:
		if (self.effectSpread && self.effectLifetime && self.effectLifetime % TICRATE == 0) {
			int spreadRadius = 100;
			if (self.effectRadius > 0) {
				spreadRadius = self.effectRadius;
			}
			BlockThingsIterator iter = BlockThingsIterator.Create(self.effectSource, spreadRadius, false);
			while (iter.Next()) {
				Actor target = iter.thing;
				if (self.owner && !self.owner.CheckSight(target)) {
					continue;
				}
				if (SchismInteraction.CanHarmTarget(self.effectInflictor, target)) {
					SchismStatusEffects.InflictEffect(self.effectSpread, self.effectInflictor, self.effectSource, target, self.effectInitialTime, "", self.strength);
				}
			}
		}
	}
	
	override void InitEffect() {
		super.InitEffect();
		self.effectInflictor = self.owner;
		self.effectSource = self.owner;
		self.tracer = self.owner;
	}
	
	/** 
	 * Called externally and after InitEffect to apply this effect fully, sets additional information such as the source of the effect, etc.
	 * @param effectInflictor The actor to set as the overall creator of this effect.
	 * @param effectSource The actor to set as the direct source of this effect, this could be a projectile, etc.
	 * @param effectTime The time in tics that this effect should last.
	 * @param spread An optional status effect to spread to enemies within this effect's radius.
	 * @param strength The strength of the effect, varies depending on the type of effect, ignored by most effects. Defaults to 1.
	 */
	virtual void ApplyEffect(Actor effectInflictor, Actor effectSource, int effectTime = 5 * TICRATE, Class<SchismStatusEffect> spread = "", int strength = 1) {
		self.RefreshEffect(effectInflictor, effectSource, effectTime, spread, strength);
	}
	
	/** 
	 * Called when this effect is already active and needs to be refreshed and on initial application also. Can be used to replace the inflictor/source, etc also.
	 * @param effectInflictor The actor to set as the overall creator of this effect. If null it will be unchanged.
	 * @param effectSource The actor to set as the direct source of this effect, this could be a projectile, etc. If null it will be unchanged.
	 * @param effectTime The time in tics that this effect should be reset to, will update the initial time also. If 0 it will be reset to the initial time.
	 * @param spread An optional status effect to spread to enemies within this effect's radius.
	 * @param strength The strength of the effect, varies depending on the type of effect, ignored by most effects. Defaults to 1.
	 */
	virtual void RefreshEffect(Actor effectInflictor = null, Actor effectSource = null, int effectTime = 0, Class<SchismStatusEffect> spread = "", int strength = 1) {
		if (effectInflictor) {
			self.effectInflictor = effectInflictor;
		}
		if (effectSource) {
			self.effectSource = effectSource;
		}
		self.tracer = self.owner;
		if (effectTime > 0) {
			self.effectInitialTime = effectTime;
		} else {
			effectTime = self.effectInitialTime;
		}
		self.effectTics = effectTime; // Native to Powerup
		self.effectSpread = spread;
		self.strength = strength;
		self.UpdateFollowEffect();
	}
	
	override void EndEffect() {
		super.EndEffect();
	}
	
	/**
	 * Gets the speed factor of this effect, only affects player movement.
	 */
	override double GetSpeedFactor() {
		return self.effectSpeedFactor;
	}

	/** 
	 * Updates the follow effect actor and spawns it if it doesn't already exist.
	 * @return Returns the spawned or existing follow effect actor.
	 */
	virtual SchismFollowEffect UpdateFollowEffect() {
		if (self.followEffect) {
			return self.followEffect;
		}

		// Spawn New Follow Effect:
		vector3 offset = self.GetEffectOffset();
		bool followEffectSpawned;
		Actor spawnedFollowEffect;
		[followEffectSpawned, spawnedFollowEffect] = A_SpawnItemEx(
			self.GetFollowEffectClass(),
			offset.y, offset.x, offset.z,
			0, 0, 0,
			GetEffectAngle(), SXF_NOCHECKPOSITION
		);
		if (spawnedFollowEffect) {
			self.followEffect = SchismFollowEffect(spawnedFollowEffect);
			if (self.followEffect) {
				self.followEffect.UpdateStatusEffect(self);
			}
			return self.followEffect;
		}
		return null;
	}

	/** 
	 * Gets the origin offset for all effects played by this status effect.
	 * @return The effect origin offset. As latitude, longitude and verticality, swap x and y for A_SpawnItemEx.
	 */
	virtual vector3 GetEffectOffset() {
		Actor effectTarget = self.owner;
		float offsetLat = self.offsetLat;
		float offsetLon = self.offsetLon;
		float offsetVert = self.offsetVert;
		float offsetAngle = 0;
		if (effectTarget == self.effectInflictor) {
			offsetLon += 20;
			offsetAngle = 180;
		}
		return (offsetLat, offsetLon, offsetVert);
	}

	/** 
	 * Gets the origin angle for all effects played by this status effect.
	 * @return The effect origin angle.
	 */
	virtual float GetEffectAngle() {
		if (self.owner == self.effectInflictor) {
			return 180;
		}
		return 0;
	}
	
	/** 
	 * Checks if the provided actor was one the one that inflcited this effect.
	 * @param testActor The actor to check.
	 */
	virtual bool InflictedBy(Actor testActor) {
		return testActor == self.effectInflictor;
	}
	
	virtual string GetFollowEffectClass() {
		return "SchismFollowEffect";
	}
	
	virtual void RemoveEffect() {
		if (self.owner) {
			self.owner.TakeInventory(self.GetClass(), 1);
		}
	}
	
	/** 
	 * Called (by a schism event handler) when the owner of this effect is damaged.
	 */
	virtual void OnOwnerDamaged(int damage, string damageType, actor inflictor) {}
}


#include "zscript/schism/statuseffects/followeffect.zs"

// ========== Debuffs ==========
#include "zscript/schism/statuseffects/immolate.zs"
#include "zscript/schism/statuseffects/soak.zs"
#include "zscript/schism/statuseffects/chill.zs"
#include "zscript/schism/statuseffects/twirl.zs"
#include "zscript/schism/statuseffects/float.zs"
#include "zscript/schism/statuseffects/fear.zs"
#include "zscript/schism/statuseffects/bleed.zs"
#include "zscript/schism/statuseffects/poison.zs"
#include "zscript/schism/statuseffects/plague.zs"
#include "zscript/schism/statuseffects/stun.zs"
#include "zscript/schism/statuseffects/delay.zs"
#include "zscript/schism/statuseffects/weakness.zs"
#include "zscript/schism/statuseffects/bound.zs"
#include "zscript/schism/statuseffects/charm.zs"
#include "zscript/schism/statuseffects/astonish.zs"
#include "zscript/schism/statuseffects/pandemic.zs"

// ========== Buffs ==========
#include "zscript/schism/statuseffects/haste.zs"
#include "zscript/schism/statuseffects/flight.zs"
#include "zscript/schism/statuseffects/anima.zs"
#include "zscript/schism/statuseffects/radiance.zs"

// ========== Neutral ==========
#include "zscript/schism/statuseffects/entropic.zs"
#include "zscript/schism/statuseffects/cloak.zs"
#include "zscript/schism/statuseffects/ghost.zs"
