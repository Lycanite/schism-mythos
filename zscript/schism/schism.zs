const TAU = 6.283185307179586476925286766559;

// ========== Core ==========
#include "zscript/schism/ambient.zs"
#include "zscript/schism/splashes.zs"
#include "zscript/schism/particles.zs"
#include "zscript/schism/sfx.zs"
#include "zscript/schism/statuseffects.zs"
#include "zscript/schism/utility.zs"
#include "zscript/schism/projectiles.zs"
#include "zscript/schism/spawners.zs"


// ========== Globals ==========
#include "zscript/schism/globals/utility.zs"
#include "zscript/schism/globals/interaction.zs"
#include "zscript/schism/globals/attacks.zs"
#include "zscript/schism/globals/statuseffects.zs"
#include "zscript/schism/globals/level.zs"
#include "zscript/schism/globals/playerinfo.zs"
#include "zscript/schism/globals/ammospawndirectory.zs"
#include "zscript/schism/globals/weaponspawndirectory.zs"
#include "zscript/schism/globals/creaturespawndirectory.zs"


// ========== Mixins ==========
#include "zscript/schism/mixins/queryplayerinfomixin.zs"
#include "zscript/schism/mixins/spawnparticle.zs"


// ========== Event Handlers ==========
#include "zscript/schism/eventhandlers/main.zs"
#include "zscript/schism/eventhandlers/interface.zs"
#include "zscript/schism/eventhandlers/ammospawnhandler.zs"
#include "zscript/schism/eventhandlers/weaponspawnhandler.zs"
#include "zscript/schism/eventhandlers/creaturespawnhandler.zs"


// ========== Classes ==========
#include "zscript/schism/classes/schismplayerpawn.zs"
#include "zscript/schism/classes/reilodos.zs"
#include "zscript/schism/classes/undulateoak.zs"
#include "zscript/schism/classes/mogdread.zs"
#include "zscript/schism/classes/chozo.zs"
#include "zscript/schism/classes/haruhi.zs"
#include "zscript/schism/classes/vess.zs"


// ========== Ammo ==========
#include "zscript/schism/ammo/spawners.zs"
#include "zscript/schism/ammo/reilodos.zs"
#include "zscript/schism/ammo/undulateoak.zs"
#include "zscript/schism/ammo/mogdread.zs"
#include "zscript/schism/ammo/chozo.zs"
#include "zscript/schism/ammo/haruhi.zs"
#include "zscript/schism/ammo/vess.zs"


// ========== Weapons ==========
#include "zscript/schism/weapons/schismweapon.zs"
#include "zscript/schism/weapons/weaponupgrade.zs"
#include "zscript/schism/weapons/weaponultimate.zs"
#include "zscript/schism/weapons/weaponpassive.zs"
#include "zscript/schism/weapons/dualhand.zs"

// Reilodos:
#include "zscript/schism/weapons/reilodos/reilodosspell.zs"
#include "zscript/schism/weapons/reilodos/foulmenagerie.zs"
#include "zscript/schism/weapons/reilodos/eboncommand.zs"
#include "zscript/schism/weapons/reilodos/voidmanipulation.zs"
#include "zscript/schism/weapons/reilodos/baronsclaw.zs"
#include "zscript/schism/weapons/reilodos/chaosshards.zs"
#include "zscript/schism/weapons/reilodos/arcofchaos.zs"
#include "zscript/schism/weapons/reilodos/chaossphere.zs"

// Undulate Oak:
#include "zscript/schism/weapons/undulateoak/lightningstrike.zs"
#include "zscript/schism/weapons/undulateoak/ectoplasmprojectile.zs"
#include "zscript/schism/weapons/undulateoak/primordialstaff.zs"
#include "zscript/schism/weapons/undulateoak/druidbow.zs"
#include "zscript/schism/weapons/undulateoak/raijinsbattleaxe.zs"
#include "zscript/schism/weapons/undulateoak/dimensionalfragment.zs"

// Mogdread:
#include "zscript/schism/weapons/mogdread/firestonecane.zs"
#include "zscript/schism/weapons/mogdread/lavacannon.zs"
#include "zscript/schism/weapons/mogdread/alchemicalraygun.zs"
#include "zscript/schism/weapons/mogdread/starstone.zs"

// Chozo:
#include "zscript/schism/weapons/chozo/soulessenceprojectile.zs"
#include "zscript/schism/weapons/chozo/grimscythe.zs"
#include "zscript/schism/weapons/chozo/necronomicon.zs"
#include "zscript/schism/weapons/chozo/lichscepter.zs"
#include "zscript/schism/weapons/chozo/lichtotem.zs"

// Haruhi:
#include "zscript/schism/weapons/haruhi/swiftdaggers.zs"
#include "zscript/schism/weapons/haruhi/glacialnodachi.zs"
#include "zscript/schism/weapons/haruhi/celestialkunai.zs"
#include "zscript/schism/weapons/haruhi/tempestrelic.zs"

// Vess:
#include "zscript/schism/weapons/vess/vessspell.zs"
#include "zscript/schism/weapons/vess/bloodglobe.zs"
#include "zscript/schism/weapons/vess/wickedcavort.zs"
#include "zscript/schism/weapons/vess/maladyquiver.zs"
#include "zscript/schism/weapons/vess/greatdrakescale.zs"


// ========== Creatures ==========
#include "zscript/schism/creatures.zs"


// ========== Pickups ==========
#include "zscript/schism/pickups.zs"


// ========== Decorations ==========
#include "zscript/schism/decorations.zs"