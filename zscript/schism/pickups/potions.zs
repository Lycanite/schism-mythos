class HealthPotion : HealthPickup {
	Default {
		Tag "Lesser Health Potion";
		Health 10;
		Inventory.PickupMessage "Obtained a Lesser Health Potion.";
		Inventory.Amount 1;
		Inventory.MaxAmount 5;
		Inventory.Icon "POTNA0";
		HealthPickup.AutoUse 0; // No Autouse

		Inventory.PickupSound "Item/Potion/Pickup";
		Inventory.UseSound "Item/Potion/Health/Use";
	}
	
	States {
		Spawn:
			POTN A -1;
			Stop;
	}
}

class HealthPotionGreater : HealthPotion {
	Default {
		Tag "Greater Health Potion";
		Health 25;
		Inventory.PickupMessage "Obtained a Greater Health Potion.";
		Inventory.MaxAmount 3;
		Inventory.Icon "POTNB0";
	}
	
	States {
		Spawn:
			POTN B -1;
			Stop;
	}
}

class HealthPotionSupreme : HealthPotion {
	Default {
		Tag "Supreme Health Potion";
		Health 50;
		Inventory.PickupMessage "Obtained a Supreme Health Potion.";
		Inventory.MaxAmount 2;
		Inventory.Icon "POTNC0";
	}
	
	States {
		Spawn:
			POTN C -1;
			Stop;
	}
}

class HealthPotionPerfect : HealthPotion {
	Default {
		Tag "Perfect Health Potion";
		Health 100;
		Inventory.PickupMessage "Obtained a Perfect Health Potion.";
		Inventory.MaxAmount 1;
		Inventory.Icon "POTND0";
	}
	
	States {
		Spawn:
			POTN D -1;
			Stop;
	}
}

class RejuvenationPotion : HealthPotion {
	Default {
		Tag "Rejuvenation Potion";
		Health 200;
		Inventory.PickupMessage "Obtained a Rejuvenation Potion.";
		Inventory.MaxAmount 1;
		HealthPickup.AutoUse 2; // Mystic Urn
		Inventory.Icon "POTNE0";
	}

	override bool Use(bool pickup) {
		super.Use(pickup);
		if (self.owner) {
			self.owner.GiveInventory("Soulsphere", self.health);
		}
		return true;
	}
	
	States {
		Spawn:
			POTN E -1;
			Stop;
	}
}

class DefensePotion : BasicArmorBonus {
	Default {
		Tag "Lesser Defense Potion";
		Inventory.PickupMessage "Obtained a Lesser Defense Potion.";
		Inventory.Amount 1;
		Inventory.MaxAmount 5;
		Inventory.Icon "POTNF0";

		Inventory.PickupSound "Item/Potion/Pickup";
		Inventory.UseSound "Item/Potion/Defense/Use";

		Armor.SavePercent 10; // Default % damage absorbed.
		Armor.SaveAmount 10; // Armor gained.
		Armor.MaxSaveAmount 100; // Max armor gained from this item.
		Armor.MaxFullAbsorb 50; // The max damage amount that can be absorbed.

		+COUNTITEM;
		-INVENTORY.AUTOACTIVATE;
		-INVENTORY.ALWAYSPICKUP;
		+INVENTORY.INVBAR;
	}
	
	States {
		Spawn:
			POTN F -1;
			Stop;
	}
}

class DefensePotionGreater : DefensePotion {
	Default {
		Tag "Greater Defense Potion";
		Inventory.PickupMessage "Obtained a Greater Defense Potion.";
		Inventory.MaxAmount 3;
		Inventory.Icon "POTNG0";

		Armor.SaveAmount 20; // Armor gained.
	}
	
	States {
		Spawn:
			POTN G -1;
			Stop;
	}
}

class DefensePotionSupreme : DefensePotion {
	Default {
		Tag "Supreme Defense Potion";
		Inventory.PickupMessage "Obtained a Supreme Defense Potion.";
		Inventory.MaxAmount 2;
		Inventory.Icon "POTNH0";

		Armor.SaveAmount 50; // Armor gained.
	}
	
	States {
		Spawn:
			POTN H -1;
			Stop;
	}
}

class DefensePotionPerfect : DefensePotion {
	Default {
		Tag "Perfect Defense Potion";
		Inventory.PickupMessage "Obtained a Perfect Defense Potion.";
		Inventory.MaxAmount 1;
		Inventory.Icon "POTNI0";

		Armor.SaveAmount 100; // Armor gained.
	}
	
	States {
		Spawn:
			POTN I -1;
			Stop;
	}
}

class BulwarkPotion : DefensePotion {
	Default {
		Tag "Bulwark Potion";
		Inventory.PickupMessage "Obtained a Bulwark Potion.";
		Inventory.MaxAmount 1;
		Inventory.Icon "POTNJ0";

		Armor.SaveAmount 200; // Armor gained.
		Armor.MaxSaveAmount 200; // Max armor gained from this item.
	}
	
	States {
		Spawn:
			POTN J -1;
			Stop;
	}
}