class ReilodosHavocSerpent : PlayerPawn {
	Default {
		Speed 1;
		Health 500;
		Radius 16;
		Height 64;
		Mass 220;
		
		Scale 0.7;
		PainChance 50;
		RadiusDamageFactor 0.125;
		
		Player.DisplayName "Reilodos";
		Player.SoundClass "ReilodosHavocSerpent";
		Player.CrouchSprite "REIM";
		Player.ScoreIcon "REILFACE";
		Player.ColorRange 0, 0;
		Player.DamageScreenColor "00 ff 66";
		Player.ViewHeight 58.0;
		Player.ViewBob 1.5;
		
		Player.ForwardMove 1.25, 1.25;
		Player.SideMove 0.875, 0.925;
		Player.JumpZ 10.0;
		Player.HexenArmor 25, 25, 30, 40, 50;
		Player.InvulnerabilityMode "Reflective";
		Player.HealRadiusType "Mana";
		Player.MaxHealth 250;
		
		Player.FlechetteType "ArtiPoisonBag2";
		
		Player.MorphWeapon "WeaponHavocSerpent";
		
		-PLAYERPAWN.CANSUPERMORPH;
		-PLAYERPAWN.CROUCHABLEMORPH;
		+NOSKIN;
	}
	
	States
    {
		Spawn:
			REIM A -1;
			Loop;
		See:
			REIM ABCD 4;
			Loop;
		Missile:
			REIM F 8;
			Goto Spawn;
		Melee:
			REIM G 8;
			Goto Spawn;
		Pain:
			REIM E 4;
			REIM E 4 A_Pain;
			Goto Spawn;
		Death:
			REIM H 6;
			REIM I 6 A_Scream;
			REIM JK 6;
			REIM L 6 A_NoBlocking;
			REIM MNO 6;
			REIM P -1;
			Stop;
		XDeath:
		Burn:
		Ice:
		Disintegrate:
			REIM H 6;
			REIM I 6 A_XScream;
			REIM JK 6;
			REIM L 6 A_NoBlocking;
			REIM MNO 6;
			REIM P -1;
			Stop;
    }
}