class EntropicPortal : Actor {
	mixin SpawnParticleMixin;

	Default {
		Radius 16;
		Height 80;
		Scale 1;
		RenderStyle "Add";
		
		-SOLID
		+NOGRAVITY
	}
	
	override bool Used(Actor user) {
		bool isUsed = super.Used(user);
		if (self.master && user == self.master) {
			self.SetStateLabel("Death");
		}
		return isUsed;
	}
	
	action state A_EntropicPortal() {
		invoker.A_StartSound("EntropicState/Active", CHAN_BODY, CHANF_LOOPING, 1.0);
		invoker.SpawnParticle("ChaosSpark", 8, 3, (FRandom(-32, 32), FRandom(-32, 32), FRandom(-8, 88)), Random(0, 359), Random(-60, -10));

		if (!invoker.master || invoker.master.health <= 0) {
			return ResolveState("Death");
		}
		
		Reilodos reilodosMaster = Reilodos(invoker.master);
		if (reilodosMaster && reilodosMaster.currentEntropicPortal != self) {
			return ResolveState("Death");
		}
		return null;
	}
	
	States {
		Spawn:
			REIP ABCD 2 bright {
				return A_EntropicPortal();
			}
			Loop;
			
		Death:
			REIP AABBCCDD 1 A_FadeOut(0.025);
			Stop;
	}
}