class Reilodos : SchismPlayerPawn {
	EntropicPortal currentEntropicPortal;
	bool user_disableHollowGuide;
	
	Default {
		Speed 1;
		Health 100;
		Radius 16;
		Height 56;
		Mass 100;
		Scale 0.7;
		PainChance 255;
		RadiusDamageFactor 0.25;
		BloodColor "00 ff 66";
		
		Player.DisplayName "Reilodos";
		Player.SoundClass "Reilodos";
		Player.CrouchSprite "REIC";
		Player.ScoreIcon "REILFACE";
		Player.ColorRange 0, 0;
		Player.DamageScreenColor "00 ff 66";
		Player.ViewBob 0.5;
		
		Player.MaxHealth 100;
		Player.ForwardMove 1, 1;
		Player.SideMove 1, 1;
		Player.JumpZ 8.0;
		Player.HexenArmor 5, 5, 15, 10, 25;
		Player.InvulnerabilityMode "Reflective";
		Player.HealRadiusType "Mana";
		
		Player.FlechetteType "ArtiPoisonBag2";
		Player.StartItem "FoulMenagerie";
		Player.StartItem "ChaosEnergy", 100;
		
		Player.WeaponSlot 1, "Chainsaw", "Gauntlets"; // Melee
		Player.WeaponSlot 2, "FoulMenagerie"; // Primary
		Player.WeaponSlot 3, "Shotgun", "SuperShotgun", "Crossbow", "StrifeCrossbow", "ChaosShards", "EbonCommand"; // Secondary
		Player.WeaponSlot 4, "Chaingun", "Blaster", "AssaultGun", "ArcOfChaos", "VoidManipulation"; // Tertiary
		Player.WeaponSlot 5, "RocketLauncher", "Mace", "MWeapBloodscourge", "MiniMissileLauncher", "BaronsClaw"; // Powerful
		Player.WeaponSlot 6, "PlasmaRifle", "SkullRod", "FlameThrower", "Mauler"; // Special
		Player.WeaponSlot 7, "BFG9000", "PhoenixRod", "StrifeGrenadeLauncher"; // Ultimate

		SchismPlayerPawn.BaseName "Reilodos";
		SchismPlayerPawn.HexenClass "Mage";
		SchismPlayerPawn.AbilityCooldownMax TICRATE;
		SchismPlayerPawn.AbilityBCooldownMax TICRATE;
	}
	
	override void Tick() {
		// Voodoo Doll:
		if (!self.player || !self.player.mo || self.player.mo != self) {
			super.Tick();
			return;
		}
		
		super.Tick();
		
		// Clear Teleport Abilities:
		if (self.user_clearTeleportAbilities && self.currentEntropicPortal) {
			self.currentEntropicPortal.Destroy();
			self.user_clearTeleportAbilities = false;
		}
		
		self.HollowGuide();
	}
	
	virtual void HollowGuide() {
		// Disabled:
		if (!self.passiveEnabled || self.user_disableHollowGuide) {
			return;
		}
		
		// Low Health:
		if (self.UpdateTic %(6 * 35) == 0 && self.health <= 25) {
			self.A_StartSound("Reilodos/Guide/Weak", CHAN_5);
		}
		
		// Item Finding:
		double closestKey = 9999;
		BlockThingsIterator iter = BlockThingsIterator.Create(self, 500, false);
		while (iter.Next()) {
			Actor target = iter.thing;
			double distance = self.Distance3D(target);
			
			// Keys:
			if (Key(target) || PuzzleItem(target)) {
				String className = target.GetClassName();
				if (self.CountInv(className)) {
					continue;
				}
				
				if (self.UpdateTic %(3 * 35) == 0) {
					self.A_StartSound("Reilodos/Guide/Key", CHAN_6);
				}
				
				// Key Colors:
				if (self.UpdateTic %(4 * 35) == 0 && distance < closestKey) {
					closestKey = distance;
					if (className.IndexOf("Red") >= 0) {
						self.A_StartSound("Reilodos/Guide/Key/Red", CHAN_7);
						continue;
					}
					if (className.IndexOf("Blue") >= 0) {
						self.A_StartSound("Reilodos/Guide/Key/Blue", CHAN_7);
						continue;
					}
					if (className.IndexOf("Yellow") >= 0) {
						self.A_StartSound("Reilodos/Guide/Key/Yellow", CHAN_7);
						continue;
					}
					if (className.IndexOf("Green") >= 0) {
						self.A_StartSound("Reilodos/Guide/Key/Green", CHAN_7);
						continue;
					}
					if (className.IndexOf("Cyan") >= 0) {
						self.A_StartSound("Reilodos/Guide/Key/Cyan", CHAN_7);
						continue;
					}
					if (className.IndexOf("Magenta") >= 0 || className.IndexOf("purple") >= 0) {
						self.A_StartSound("Reilodos/Guide/Key/Magenta", CHAN_7);
						continue;
					}
				}
			}
			
			// Powerups:
			if (self.UpdateTic %(6 * 35) == 0 && PowerupGiver(target)) {
				self.A_StartSound("Reilodos/Guide/Powerup", CHAN_5);
			}
		}
	}
	
	override int DamageMobj(Actor inflictor, Actor source, int damage, Name damageType, int flags, double angle) {
		// Reverse Telefrag:
		if (damageType == "Telefrag" && !Reilodos(inflictor)) {
			inflictor.DamageMobj(self, source, damage, damageType, flags, angle);
			return 0;
		}

		return super.DamageMobj(inflictor, source, damage, damageType, flags, angle);
	}
	
	override string GetAmmoClass(int category, int size) {
		string sizeString = "";
		if (size == 1) {
			sizeString = "Large";
		}

		switch(category) {
			case -1:
				return "HavocEnergy" .. sizeString;
			case 0:
				return "ChaosEnergy" .. sizeString;
			case 1:
				return "ShadowEnergy" .. sizeString;
			case 2:
				return "VoidEnergy" .. sizeString;
			default:
				return super.GetAmmoClass(category, size);
		}
	}
	
	override class<Inventory> GetWeaponClass(int weaponCategory, int index) {
		switch(weaponCategory) {
			case 0:
				switch(index) {
					case 0:
						return "FoulMenagerie";
					case 1:
						return "ArgusScroll";
					case 2:
						return "EntropicBoltScroll";
					case 3:
						return "BedlamFiendScroll";
					case 4:
						return "WarpWadjetsScroll";
					case 5:
						return "ChaoticChupacabraScroll";
					default:
						return "";
				}

			case 1:
				switch(index) {
					case 0:
						return "EbonCommand";
					case 1:
						return "GrueScroll";
					case 2:
						return "HowlScroll";
					case 3:
						return "DarkDoppelgangerScroll";
					case 4:
						return "DarkDoppelgangerScroll"; // Doubles as ShadowGamesScroll.
					case 5:
						return "ShadowCloakScroll";
					default:
						return "";
				}

			case 2:
				switch(index) {
					case 0:
						return "VoidManipulation";
					case 1:
						return "GapingRiftScroll";
					case 2:
						return "HungeringVengeanceScroll";
					case 3:
						return "ReverseRiftScroll";
					case 4:
						return "NixianRiftScroll";
					case 5:
						return "RiftboltScroll";
					default:
						return "";
				}

			case 3:
				switch(index) {
					case 0:
						return "BaronsClaw";
					case 1:
						return "ChaosShards";
					case 2:
						return "ArcOfChaos";
					default:
						return "";
				}

			default:
				return "";
		}
	}

	override class<Inventory> GetSigilClass() {
		return "ChaosSphere";
	}
	
	override int GetWeaponPassiveLevel(int category) {
		switch (category) {
			case 0:
				return min(CountInv("ChaoticChupacabraScroll"), 3);
			case 1:
				return min(CountInv("ShadowCloakScroll"), 3);
			case 2:
				return min(CountInv("RiftboltScroll"), 3);
			default:
				return super.GetWeaponPassiveLevel(category);
		}
	}
	
	override bool SchismSpecial(int specialId) {
		if (!super.SchismSpecial(specialId)) {
			return false;
		}
		
		if (specialId == 1) {
			self.EntropicStateTeleport();
		}
		else if (specialId == 2) {
			self.EntropicStateSummonPortal();
		}
		
		return true;
	}
	
	virtual void EntropicStateTeleport() {
		if (!self.currentEntropicPortal) {
			self.A_Print("\caFirst create an Entropic Portal to teleport to with Special Ability 2.");
			return;
		}
		
		self.abilityBCooldown = self.abilityBCooldownMax; // Set EntropicStateSummonPortal cooldown on teleport to prevent exploits.
		self.tracer = self.currentEntropicPortal;
		self.A_Warp(AAPTR_TRACER);
		self.A_StartSound("*teleport", CHAN_5);
	}
	
	virtual void EntropicStateSummonPortal() {
		if (self.currentEntropicPortal) {
			self.currentEntropicPortal.Destroy();
		}
		
		bool portalSpawned;
		Actor portalActor;
		[portalSpawned, portalActor] = self.A_SpawnItemEx("EntropicPortal", 0, 0, 0, 0, 0, 0, 0, SXF_SETMASTER|SXF_NOPOINTERS);
		self.currentEntropicPortal = EntropicPortal(portalActor);
		self.A_StartSound("EntropicState/Summon", CHAN_5);
	}
	
	States {
		Spawn:
			REIL A -1;
			Loop;
			
		See:
			REIL ABCD 4;
			Loop;
			
		Missile:
			REIL E 8;
			Goto Spawn;
			
		Melee:
			REIL F 8;
			Goto Spawn;
			
		Pain:
			REIL G 4;
			REIL G 4 A_Pain();
			Goto Spawn;
			
		Death:
			REIL H 6;
			REIL I 6 A_PlayerScream();
			REIL JK 6;
			REIL L 6 A_NoBlocking();
			REIL MNO 6;
			REIL P -1;
			Stop;
			
		XDeath:
		Burn:
		Ice:
		Disintegrate:
			REIL Q 5 A_XScream();
			REIL R 0 A_NoBlocking();
			REIL R 5 A_SkullPop();
			REIL STUVWX 5;
			REIL Y -1;
			Stop;
    }
}


#include "zscript/schism/classes/reilodos/entropicportal.zs"
#include "zscript/schism/classes/reilodos/havocserpent.zs"
