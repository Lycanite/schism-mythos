class Chozo : SchismPlayerPawn {
	ChozoPhylactery phylactery;
	int phylacteryProtectionTime;
	
	Default {
		Speed 1;
		Health 150;
		Radius 16;
		Height 56;
		Mass 100;
		Scale 0.85;
		PainChance 255;
		RadiusDamageFactor 0.25;
		
		Player.DisplayName "Chozo";
		Player.SoundClass "Chozo";
		//Player.CrouchSprite "CHZC";
		//Player.ScoreIcon "CHZFACE";
		Player.ColorRange 0, 0;
		Player.DamageScreenColor "99 00 00";
		
		Player.MaxHealth 150;
		Player.ForwardMove 1, 1;
		Player.SideMove 1, 1;
		Player.JumpZ 8.0;
		Player.HexenArmor 20, 20, 50, 10, 40;
		Player.InvulnerabilityMode "Ghost";
		Player.HealRadiusType "Armor";
		
		Player.FlechetteType "ArtiPoisonBag1";
		Player.StartItem "GrimScythe";
		Player.StartItem "NecroticRune", 100;
		
		Player.WeaponSlot 1, "Chainsaw", "Gauntlets"; // Melee
		Player.WeaponSlot 2, "GrimScythe"; // Primary
		Player.WeaponSlot 3, "Shotgun", "SuperShotgun", "Crossbow", "StrifeCrossbow", "FWeapAxe", "Necronomicon"; // Secondary
		Player.WeaponSlot 4, "Chaingun", "Blaster", "AssaultGun", "FWeapHammer", "LichScepter"; // Tertiary
		Player.WeaponSlot 5, "RocketLauncher", "Mace", "FWeapQuietus", "MiniMissileLauncher"; // Powerful
		Player.WeaponSlot 6, "PlasmaRifle", "SkullRod", "FlameThrower", "Mauler"; // Special
		Player.WeaponSlot 7, "BFG9000", "PhoenixRod", "StrifeGrenadeLauncher"; // Ultimate

		SchismPlayerPawn.BaseName "Chozo";
		SchismPlayerPawn.HexenClass "Fighter";
		SchismPlayerPawn.AbilityCooldownMax 3 * TICRATE;
		SchismPlayerPawn.AbilityBCooldownMax TICRATE; // Cooldown for placing/moving Phylactery.
	}
	
	override void Tick() {
		// Voodoo Doll:
		if (!self.player || !self.player.mo || self.player.mo != self) {
			super.Tick();
			return;
		}
		
		super.Tick();
		
		// Phylactery Protection:
		if (self.phylacteryProtectionTime > 0) {
			self.phylacteryProtectionTime--;
		}
	}
	
	override int DamageMobj(Actor inflictor, Actor source, int damage, Name mod, int flags, double angle) {
		double damageTaken = super.DamageMobj(inflictor, source, damage, mod, flags, angle);

		// Half Damage Always:
		int reducedDamage = Ceil(damageTaken / 2);
		
		// Phylactery:
		if (self.phylacteryProtectionTime > 0) {
			return 0;
		}
		if (self.health - reducedDamage <= 0 && self.phylactery) {
			tracer = self.phylactery;
			A_Warp(AAPTR_TRACER);
			A_StartSound("*teleport", CHAN_5);
			self.abilityBCooldown = 120 * self.abilityBCooldownMax; // Cooldown for Phylactery recharge.
			self.phylactery.Destroy();
			self.phylacteryProtectionTime = 3 * TICRATE;
			self.A_DamageSelf(-self.SpawnHealth(), "None", DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
			self.A_SetHealth(self.SpawnHealth());
			return 0;
		}
		
		return reducedDamage;
	}
	
	override string GetAmmoClass(int category, int size) {
		string sizeString = "";
		if (size == 1) {
			sizeString = "Large";
		}

		switch(category) {
			case -1:
				return "SoulEssence" .. sizeString;
			case 0:
				return "GrimRune" .. sizeString;
			case 1:
				return "NecroticRune" .. sizeString;
			case 2:
				return "DeathRune" .. sizeString;
			default:
				return super.GetAmmoClass(category, size);
		}
	}
	
	override class<Inventory> GetWeaponClass(int weaponCategory, int index) {
		switch(weaponCategory) {
			case 0:
				switch(index) {
					case 0:
						return "GrimScythe";
					case 1:
						return "CorpseExplosionSigil";
					case 2:
						return "SoulGraspSigil";
					case 3:
						return "DreadWallSigil";
					case 4:
						return "DreadValleySigil";
					case 5:
						return "BoneArmorSigil";
					default:
						return "";
				}

			case 1:
				switch(index) {
					case 0:
						return "Necronomicon";
					case 1:
						return "Necronomicon"; // Initial
					case 2:
						return "NecronomiconSoulsPage";
					case 3:
						return "NecronomiconFrostPage";
					case 4:
						return "NecronomiconBalancePage";
					case 5:
						return "NecronomiconMemoirPage";
					default:
						return "";
				}

			case 2:
				switch(index) {
					case 0:
						return "LichScepter";
					case 1:
						return "LichScepterOssifexEffigy";
					case 2:
						return "LichScepterReaverEffigy";
					case 3:
						return "LichScepterGhoulEffigy";
					case 4:
						return "LichScepterDraugrEffigy";
					case 5:
						return "LichScepterHarrowEffigy";
					default:
						return "";
				}

			default:
				return "";
		}
	}

	override class<Inventory> GetSigilClass() {
		return "LichTotem";
	}
	
	override int GetWeaponPassiveLevel(int category) {
		switch (category) {
			case 0:
				return Min(CountInv("BoneArmorSigil"), 3);
			case 1:
				return Min(CountInv("NecronomiconMemoirPage"), 3);
			case 2:
				return Min(CountInv("LichScepterHarrowEffigy"), 3);
			default:
				return super.GetWeaponPassiveLevel(category);
		}
	}
	
	override void OnDamageDealtFrom(Actor damageTarget, double damageDealt, Name damageType, Actor inflictor) {
		super.OnDamageDealtFrom(damageTarget, damageDealt, damageType, inflictor);
		
		if (!self.passiveEnabled || !damageTarget.bIsMonster || damageTarget.bFriendly) {
			return;
		}
		
		// Blood Leech:
		int healingAmount = Min(Ceil(damageDealt / 4), 4);
		if (healingAmount <= 0) {
			return;
		}
		A_SpawnItemEx("LeechedBlood", 0, 0, 0, 0, 0, 0, 0, SXF_SETMASTER|SXF_NOPOINTERS|SXF_ISTRACER);
		if (tracer) {
			tracer.target = self;
			tracer.tracer = damageTarget;
			tracer.A_Warp(AAPTR_TRACER, 0, 0, 10);
			LeechedBlood leechedBlood = LeechedBlood(tracer);
			if (leechedBlood) {
				leechedBlood.followTarget = self;
				leechedBlood.healingAmount = healingAmount;
			}
		}
	}
	
	override bool SchismSpecial(int specialId) {
		if (!super.SchismSpecial(specialId)) {
			return false;
		}
		
		if (specialId == 1) {
			WickedRoar();
		}
		else if (specialId == 2) {
			SummonPhylactery();
		}
		
		return true;
	}
	
	virtual void WickedRoar() {
		BlockThingsIterator iter = BlockThingsIterator.Create(self, 300, false);
		while (iter.Next()) {
			Actor target = iter.thing;
			if (SchismInteraction.CanHarmTarget(self, target) && !PlayerPawn(target)) {
				target.target = self;
			}
		}
		A_StartSound("Chozo/WickedRoar", CHAN_5);
		for (int i = 0; i < 100; i++) {
			self.SpawnParticle("Nether", 2 * TICRATE, 2, (FRandom(-60, 60), FRandom(-60, 60), (self.height / 2) + FRandom(-4, 4)), Random(0, 359), Random(-20, -80));
		}
	}
	
	virtual void SummonPhylactery() {
		if (self.phylactery) {
			self.phylactery.Destroy();
		}
		bool spawned;
		Actor phylacteryActor;
		[spawned, phylacteryActor] = A_SpawnItemEx("ChozoPhylactery", 0, 0, 0, 0, 0, 0, 0, SXF_SETMASTER|SXF_NOPOINTERS);
		self.phylactery = ChozoPhylactery(phylacteryActor);
		A_StartSound("Chozo/Phylactery/Summon", CHAN_5);
	}
	
	States {
		Spawn:
			CHOZ A -1;
			Loop;
		See:
			CHOZ ABCD 4;
			Loop;
		Missile:
			CHOZ E 8;
			Goto Spawn;
		Melee:
			CHOZ F 8;
			Goto Spawn;
		Pain:
			CHOZ G 4;
			CHOZ G 4 A_Pain;
			Goto Spawn;
		Death:
			CHOZ G 6;
			CHOZ H 6 A_PlayerScream;
			CHOZ IJ 6;
			CHOZ K 6 A_NoBlocking;
			CHOZ LMNO 6;
			CHOZ P -1;
			Stop;
		XDeath:
		Burn:
		Ice:
		Disintegrate:
			CHOZ Q 5 A_PlayerScream;
			CHOZ R 5 A_NoBlocking;
			CHOZ S 5 A_SkullPop;
			CHOZ TUVWXY 5;
			CHOZ Z -1;
			Stop;
    }
}

#include "zscript/schism/classes/chozo/leechedblood.zs"
#include "zscript/schism/classes/chozo/soulessencefragment.zs"
#include "zscript/schism/classes/chozo/phylactery.zs"