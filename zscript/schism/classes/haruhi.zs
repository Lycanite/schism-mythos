class Haruhi : SchismPlayerPawn {
	Array<Actor> attackHistory;
	int attackHistoryCooldown;
	int attackHistoryCooldownMax;
	int stealthCooldown;
	int pranaModulo;
	int pranaFractions;
	
	Default {
		Speed 1;
		Health 100;
		Radius 16;
		Height 56;
		Mass 100;
		Scale 0.8;
		Gravity 0.75;
		PainChance 255;
		RadiusDamageFactor 0.25;
		BloodColor "ff 00 00";
		
		Player.DisplayName "Haruhi";
		Player.SoundClass "Haruhi";
		//Player.CrouchSprite "RBZC";
		//Player.ScoreIcon "HARUFACE";
		Player.ColorRange 0, 0;
		Player.DamageScreenColor "ff 00 00";
		
		Player.MaxHealth 100;
		Player.ForwardMove 1.1, 1.1;
		Player.SideMove 1.1, 1.1;
		Player.JumpZ 12.0;
		Player.HexenArmor 5, 5, 15, 10, 25;
		Player.InvulnerabilityMode "Ghost";
		Player.HealRadiusType "Health";
		
		Player.FlechetteType "ArtiPoisonBag2";
		Player.StartItem "SwiftDaggers";
		Player.StartItem "WhirlwindCharge", 100;
		
		Player.WeaponSlot 1, "Chainsaw", "Gauntlets"; // Melee
		Player.WeaponSlot 2, "SwiftDaggers"; // Primary
		Player.WeaponSlot 3, "Shotgun", "SuperShotgun", "Crossbow", "StrifeCrossbow", "GlacialNodachi"; // Secondary
		Player.WeaponSlot 4, "Chaingun", "Blaster", "AssaultGun", "CelestialKunai"; // Tertiary
		Player.WeaponSlot 5, "RocketLauncher", "Mace", "MWeapBloodscourge", "MiniMissileLauncher"; // Powerful
		Player.WeaponSlot 6, "PlasmaRifle", "SkullRod", "FlameThrower", "Mauler"; // Special
		Player.WeaponSlot 7, "BFG9000", "PhoenixRod", "StrifeGrenadeLauncher"; // Ultimate

		SchismPlayerPawn.BaseName "Haruhi";
		SchismPlayerPawn.HexenClass "Fighter";
		SchismPlayerPawn.AbilityCooldownMax TICRATE;
		SchismPlayerPawn.AbilityBCooldownMax 10 * TICRATE;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.attackHistoryCooldownMax = 3 * 35;
		self.pranaModulo = 5;
	}
	
	override void Tick() {
		// Voodoo Doll:
		if (!self.player || !self.player.mo || self.player.mo != self) {
			super.Tick();
			return;
		}
		
		super.Tick();
		
		// Attack History Clear (Prana):
		if (self.attackHistoryCooldown > 0) {
			if (--self.attackHistoryCooldown == 0) {
				self.attackHistory.Clear();
			}
		}
		self.comboPoints = self.attackHistory.Size();
		
		// Passive - Stealth:
		if (self.passiveEnabled) {
			if (self.vel.x != 0 && self.vel.y != 0 && self.vel.z != 0) {
				self.stealthCooldown = 5 * 35;
			}
			else if (self.stealthCooldown > 0) {
				self.stealthCooldown--;
			}
			if (self.stealthCooldown <= 0) {
				A_GiveInventory("HaruhiStealth");
			}
			else {
				A_TakeInventory("HaruhiStealth");
			}
		}
	}
	
	override int DamageMobj(Actor inflictor, Actor source, int damage, Name damageType, int flags, double angle) {
		if (damageType == "Falling") {
			return 0;
		}
		return super.DamageMobj(inflictor, source, damage, damageType, flags, angle);
	}
	
	override string GetAmmoClass(int category, int size) {
		string sizeString = "";
		if (size == 1) {
			sizeString = "Large";
		}

		switch(category) {
			case -1:
				return "PranaEnergy" .. sizeString;
			case 0:
				return "WhirlwindCharge" .. sizeString;
			case 1:
				return "GlacialCharge" .. sizeString;
			case 2:
				return "CelestialCharge" .. sizeString;
			default:
				return super.GetAmmoClass(category, size);
		}
	}
	
	override class<Inventory> GetWeaponClass(int weaponCategory, int index) {
		switch(weaponCategory) {
			case 0:
				switch(index) {
					case 0:
						return "SwiftDaggers";
					case 1:
						return "EmpyrealSash";
					case 2:
						return "HarpieSash";
					case 3:
						return "QuetzSash";
					case 4:
						return "TornadoSash";
					default:
						return "";
				}

			case 1:
				switch(index) {
					case 0:
						return "GlacialNodachi";
					case 1:
						return "IcicleFractal";
					case 2:
						return "BlizzardFractal";
					case 3:
						return "ShatterFractal";
					case 4:
						return "EviscerateFractal";
					default:
						return "";
				}

			case 2:
				switch(index) {
					case 0:
						return "CelestialKunai";
					case 1:
						return "MagnetBlade";
					case 2:
						return "AntigravBlade";
					case 3:
						return "SpatialBlade";
					case 4:
						return "BlackHoleBlade";
					default:
						return "";
				}

			default:
				return "";
		}
	}

	override class<Inventory> GetSigilClass() {
		return "TempestRelic";
	}
	
	override bool SchismSpecial(int specialId) {
		if (!super.SchismSpecial(specialId)) {
			return false;
		}
		
		if (specialId == 1) {
			AirStop();
		}
		
		if (specialId == 2) {
			Glide();
		}
		
		return true;
	}
	
	virtual void AirStop() {
		A_StartSound("Haruhi/AirStop", CHAN_5);
		A_TakeInventory("HaruhiAirTime");
		A_GiveInventory("HaruhiAirStop");
		A_ChangeVelocity(0, 0, 0, CVF_RELATIVE|CVF_REPLACE);
		
	}
	
	virtual void Glide() {
		A_StartSound("Haruhi/Glide", CHAN_5);
		SchismStatusEffects.InflictEffect("SchismFloatEffect", self, self, self, 3 * TICRATE, "", 3);
		for (int i = 0; i < 40; i++) {
			self.SpawnParticle("Air", 2 * TICRATE, 1, (FRandom(-40, 40), FRandom(-40, 40), FRandom(-4, 4)), Random(0, 359), Random(-10, 10));
		}
	}
	
	override void OnDamageDealtFrom(Actor damageTarget, double damageDealt, Name damageType, Actor inflictor) {
		super.OnDamageDealtFrom(damageTarget, damageDealt, damageType, inflictor);
		
		if (!damageTarget.bIsMonster || damageTarget.bFriendly) {
			return;
		}
		
		// Prana Consuming Attacks:
		bool attackUsesPrana = false;
		SchismProjectile schismProjectile = SchismProjectile(inflictor);
		if (schismProjectile && !schismProjectile.generateSpecialAmmo) {
			attackUsesPrana = true;
		}
		SchismAttackActor schismAttackActor = SchismAttackActor(inflictor);
		if (schismAttackActor && !schismAttackActor.generateSpecialAmmo) {
			attackUsesPrana = true;
		}
		
		// Update Attack History:
		self.AddAttackHistory(damageTarget);
		
		// Give Prana:
		if (!attackUsesPrana) {
			self.pranaFractions += attackHistory.Size();
			int prana = self.pranaFractions / self.pranaModulo;
			self.pranaFractions = self.pranaFractions % self.pranaModulo;
			A_GiveInventory("PranaEnergy", prana);
		}
	}
	
	virtual void AddAttackHistory(Actor newTarget) {
		if (self.attackHistory.Find(newTarget) >= self.attackHistory.Size()) {
			self.attackHistory.Push(newTarget);
			self.attackHistoryCooldown = self.attackHistoryCooldownMax;
		}
	}
	
	States
    {
		Spawn:
			HARU A -1;
			Loop;
			
		See:
			HARU ABCD 4;
			Loop;
			
		Missile:
			HARU E 8;
			Goto Spawn;
			
		Melee:
			HARU F 8;
			Goto Spawn;
			
		Pain:
			HARU G 4;
			HARU G 4 A_Pain;
			Goto Spawn;
			
		Death:
			HARU H 6;
			HARU I 6 A_PlayerScream;
			HARU JK 6;
			HARU L 6 A_NoBlocking;
			HARU MN 6;
			HARU O -1;
			Stop;
			
		XDeath:
		Burn:
		Ice:
		Disintegrate:
			HARU P 5 A_XScream;
			HARU Q 0 A_NoBlocking;
			HARU R 5 A_SkullPop;
			HARU STUVWX 5;
			HARU Y -1;
			Stop;
    }
}

class HaruhiStealth : PowerInvisibility {
	bool effectApplied;
	
	Default {
		Powerup.Duration 5;
		Powerup.Strength 50;
		Powerup.Mode "Translucent";
		
		+CANTSEEK;
		+SHADOW;
	}
	
	override void InitEffect() {
		super.InitEffect();
		if (!self.owner) {
			return;
		}
		
		self.effectApplied = true;
		self.owner.bNeverTarget = true;
		self.owner.A_SetBlend("255, 255, 255", 0.5, 5);
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (!self.owner || !self.effectApplied) {
			return;
		}
		
		self.owner.bNeverTarget = false;
		self.owner.A_SetBlend("255, 255, 255", 1, 1);
	}
}

class HaruhiAirTime : Powerup {
	bool effectApplied;
	
	Default {
		Powerup.Duration -1;
	}
	
	override void InitEffect() {
		super.InitEffect();
		if (!self.owner || self.owner.bFlyCheat || self.owner.bFloat || self.owner.bNoGravity) {
			return;
		}
		
		self.effectApplied = true;
		self.owner.bFlyCheat = true;
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (!self.owner || !self.effectApplied) {
			return;
		}
		
		self.owner.bFlyCheat = false;
	}
}

class HaruhiAirStop : HaruhiAirTime {
	override void Tick() {
		super.Tick();
		if (!self.owner) {
			return;
		}
		
		if (!self.owner.CheckInventory("HaruhiAirTime", 1)) {
			self.owner.A_ChangeVelocity(0, 0, 0, CVF_RELATIVE|CVF_REPLACE);
		}
	}
}