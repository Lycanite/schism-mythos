class Vess : SchismPlayerPawn {	
	Default {
		Speed 1;
		Health 150;
		Radius 16;
		Height 56;
		Mass 100;
		Scale 0.6;
		PainChance 255;
		RadiusDamageFactor 1.0;

		DamageFactor "Fae", 0;
		DamageFactor "Poison", 0;
		DamageFactor "Slime", 0;
		
		Player.DisplayName "Vess";
		Player.SoundClass "Vess";
		//Player.CrouchSprite "VESC";
		//Player.ScoreIcon "VESFACE";
		Player.ColorRange 0, 0;
		Player.DamageScreenColor "DD 00 55";
		
		Player.MaxHealth 150;
		Player.ForwardMove 1, 1;
		Player.SideMove 1, 1;
		Player.JumpZ 8.0;
		Player.HexenArmor 5, 5, 15, 10, 25;
		Player.InvulnerabilityMode "Reflective";
		Player.HealRadiusType "Health";
		
		Player.FlechetteType "ArtiPoisonBag2";
		Player.StartItem "BloodGlobe";
		Player.StartItem "BloodDrupe", 100;
		
		Player.WeaponSlot 1, "Chainsaw", "Gauntlets"; // Melee
		Player.WeaponSlot 2, "BloodGlobe"; // Primary
		Player.WeaponSlot 3, "Shotgun", "SuperShotgun", "Crossbow", "StrifeCrossbow", "FWeapAxe", "WickedCavort"; // Secondary
		Player.WeaponSlot 4, "Chaingun", "Blaster", "AssaultGun", "FWeapHammer", "MaladyQuiver"; // Tertiary
		Player.WeaponSlot 5, "RocketLauncher", "Mace", "FWeapQuietus", "MiniMissileLauncher"; // Powerful
		Player.WeaponSlot 6, "PlasmaRifle", "SkullRod", "FlameThrower", "Mauler"; // Special
		Player.WeaponSlot 7, "BFG9000", "PhoenixRod", "StrifeGrenadeLauncher"; // Ultimate

		SchismPlayerPawn.BaseName "Vess";
		SchismPlayerPawn.HexenClass "Mage";
		SchismPlayerPawn.AbilityCooldownMax TICRATE;
		SchismPlayerPawn.AbilityBCooldownMax TICRATE;
	}
	
	
	override void BeginPlay() {
		super.BeginPlay();
		self.baseName = "Vess";
	}
	
	override void Tick() {
		// Voodoo Doll:
		if (!self.player || !self.player.mo || self.player.mo != self) {
			super.Tick();
			return;
		}
		
		super.Tick();
	}
	
	override bool CanApplyEffect(class<Inventory> effectClass) {
		if (!self.passiveEnabled) {
			return true;
		}
		return effectClass != "SchismPoisonEffect" && effectClass != "SchismPlagueDebuff" && effectClass != "SchismPandemicEffect";
	}
	
	override string GetAmmoClass(int category, int size) {
		string sizeString = "";
		if (size == 1) {
			sizeString = "Large";
		}

		switch(category) {
			case -1:
				return "Anima" .. sizeString;
			case 0:
				return "BloodDrupe" .. sizeString;
			case 1:
				return "LustDrupe" .. sizeString;
			case 2:
				return "ToxicDrupe" .. sizeString;
			default:
				return super.GetAmmoClass(category, size);
		}
	}
	
	override class<Inventory> GetWeaponClass(int weaponCategory, int index) {
		switch(weaponCategory) {
			case 0:
				switch(index) {
					case 0:
						return "BloodGlobe";
					case 1:
						return "BloodfallsScroll";
					case 2:
						return "HyperbloodScroll";
					case 3:
						return "BlooddrinkerScroll";
					case 4:
						return "FleshbenderScroll";
					case 5:
						return "SanguineScroll";
					default:
						return "";
				}

			case 1:
				switch(index) {
					case 0:
						return "WickedCavort";
					case 1:
						return "GreedyWaltzScroll";
					case 2:
						return "GluttonChasseScroll";
					case 3:
						return "SlothsPirouetteScroll";
					case 4:
						return "EnviousImpetusScroll";
					case 5:
						return "LustfulFouetScroll";
					default:
						return "";
				}

			case 2:
				switch(index) {
					case 0:
						return "MaladyQuiver";
					case 1:
						return "AgonyDartScroll";
					case 2:
						return "StunDartScroll";
					case 3:
						return "ScourgeDartScroll";
					case 4:
						return "PandemicDartScroll";
					case 5:
						return "ContagionScroll";
					default:
						return "";
				}

			default:
				return "";
		}
	}

	override class<Inventory> GetSigilClass() {
		return "GreatDrakeScale";
	}
	
	override int GetWeaponPassiveLevel(int category) {
		switch (category) {
			case 0:
				return min(CountInv("SanguineScroll"), 3);
			case 1:
				return min(CountInv("LustfulFouetScroll"), 3);
			case 2:
				return min(CountInv("LustfulFouetScroll"), 3);
			default:
				return super.GetWeaponPassiveLevel(category);
		}
	}
	
	override bool SchismSpecial(int specialId) {
		if (!super.SchismSpecial(specialId)) {
			return false;
		}
		
		if (specialId == 1) {
			self.FlawlessCharm();
		}
		else if (specialId == 2) {
			self.PerfectHatred();
		}
		
		return true;
	}

	virtual void FlawlessCharm() {
		bool spawned;
		Actor actor;
		[spawned, actor] = self.A_SpawnItemEx(
			"VessFlawlessCharmProjectile",
			0, 0, self.height * 0.8,
			Cos(self.pitch), 0, -Sin(self.pitch),
			0, SXF_MULTIPLYSPEED|SXF_SETTARGET|SXF_NOPOINTERS
		);
		actor.target = self;
	}

	virtual void PerfectHatred() {
		bool spawned;
		Actor actor;
		[spawned, actor] = self.A_SpawnItemEx(
			"VessPerfectHatredProjectile",
			0, 0, self.height * 0.8,
			Cos(self.pitch), 0, -Sin(self.pitch),
			0, SXF_MULTIPLYSPEED|SXF_SETTARGET|SXF_NOPOINTERS
		);
		actor.target = self;
	}
	
	States {
		Spawn:
			VESS A -1;
			Loop;
		See:
			VESS BCDE 4;
			Loop;
		Missile:
			VESS G 8;
			Goto Spawn;
		Melee:
			VESS H 8;
			Goto Spawn;
		Pain:
			VESS I 4;
			VESS I 4 A_Pain();
			Goto Spawn;
		Death:
			VESS J 6;
			VESS K 6 A_PlayerScream();
			VESS L 6;
			VESS M 6 A_NoBlocking();
			VESS N -1;
			Stop;
		XDeath:
		Burn:
		Ice:
		Disintegrate:
			VESS J 6;
			VESS K 6 A_PlayerScream();
			VESS L 6 A_NoBlocking();
			VESS M 6 A_SkullPop();
			VESS N -1;
			Stop;
    }
}

class VessFlawlessCharmProjectile : SchismProjectile {
	Default {
		Radius 20;
		Height 10;
		Scale 1.5;
		Speed 60;
		DamageFunction 0;
		Alpha 0.75;
		RenderStyle "Add";

		SchismProjectile.LifetimeMax 5 * TICRATE;
		
		SeeSound "Vess/FlawlessCharm";
		DeathSound "Vess/FlawlessCharm/Hit";
		
		+NODAMAGETHRUST;
		+PAINLESS;
		+SEEKERMISSILE;
		+SCREENSEEKER;
	}
	
	override int SpecialMissileHit(Actor targetActor) {
		int hit = super.SpecialMissileHit(targetActor);
		
		// Effect:
		if (!targetActor.bCorpse && targetActor.health > 0 && targetActor.bIsMonster) {
			self.AffectTarget(targetActor);
			return hit;
		}
		
		return 1; // Go through non-targets.
	}

	virtual void AffectTarget(Actor targetActor) {
		if (!self.target || targetActor.master == self.target || targetActor.bBoss) {
			return;
		}

		// Charm Target:
		SchismStatusEffects.InflictEffect("SchismCharmEffect", self.target, self, targetActor);
		targetActor.A_StartSound("Vess/FlawlessCharm/Hit", CHAN_5);

		// Ability Cooldown:
		Vess vess = Vess(self.target);
		if (vess) {
			vess.abilityCooldown = 3 * TICRATE;
		}
	}
	
	States {
		Spawn:
			VESA AAAABBBBCCCC 1 A_SeekerMissile(0, 20, SMF_LOOK|SMF_PRECISE);
			Loop;
			
		Death:
			VESA ABCDE 4;
			Stop;
	}
}

class VessPerfectHatredProjectile : VessFlawlessCharmProjectile {
	Default {
		SeeSound "Vess/PerfectHatred";
		DeathSound "Vess/PerfectHatred/Hit";
	}
	
	override void AffectTarget(Actor hateTarget) {
		if (!self.target) {
			return;
		}

		// Target Enemies Onto Hate Target:
		hateTarget.A_StartSound("Vess/PerfectHatred/Hit", CHAN_5);
		BlockThingsIterator iter = BlockThingsIterator.Create(hateTarget, 1000, false);
		while (iter.Next()) {
			Actor target = iter.thing;
			if (!target || target == hateTarget || target.bCorpse || target.bBoss || !target.bIsMonster || !SchismInteraction.CanHarmTarget(self.target, target)) {
				continue;
			}
			target.target = hateTarget;
			target.SetStateLabel("See");
		}

		// Hate Target Particles:
		for (int i = 0; i < 80; i++) {
			self.SpawnParticleFromActor(
				hateTarget, "Nether", 3 * TICRATE, 4,
				(0, 0, hateTarget.height / 2),
				FRandom(0, 359), FRandom(-45, 45),
				1.5
			);
		}

		// Ability Cooldown:
		Vess vess = Vess(self.target);
		if (vess) {
			vess.abilityBCooldown = 10 * TICRATE;
		}
	}
	
	States {
		Spawn:
			VESA FFFFGGGGHHHH 1 A_SeekerMissile(0, 20, SMF_LOOK|SMF_PRECISE);
			Loop;
			
		Death:
			VESA FGHIJ 4;
			Stop;
	}
}