class ChozoPhylactery : Actor {
	Default {
		Radius 16;
		Height 80;
		Scale 1;
		Alpha 0.75;
		RenderStyle "Translucent";
		
		-SOLID
		+NOGRAVITY
	}
	
	action state PhylacteryUpdate() {
		if (!master || master.Health <= 0)
		{
			return ResolveState("Death");
		}
		
		Chozo chozoMaster = Chozo(master);
		if (chozoMaster && chozoMaster.phylactery != self)
		{
			return ResolveState("Death");
		}
		return null;
	}
	
	States {
		Spawn:
			CPHY ABCDEFGHI 2 bright {
				return PhylacteryUpdate();
			}
			Loop;
			
		Death:
			Stop;
	}
}
