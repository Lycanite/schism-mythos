class SchismWeaponSpawnDirectory : Thinker {
	Array<WeaponEntry>[9] weaponSpawns; // 2D Array - Organized by Category (Melee, Primary, Secondary, Tertiary, Powerful, Special, Ultimate) and then Weapons/Upgrades.

	static SchismWeaponSpawnDirectory Get() {
		ThinkerIterator it = ThinkerIterator.Create("SchismWeaponSpawnDirectory", STAT_STATIC);
		SchismWeaponSpawnDirectory thinker = SchismWeaponSpawnDirectory(it.Next());
		if (thinker == null) {
			thinker = New("SchismWeaponSpawnDirectory").Init();
		}
		return thinker;
	}

	SchismWeaponSpawnDirectory Init() {
		ChangeStatNum(STAT_STATIC);
		self.PopulateSpawns();
		return self;
	}

	/** 
	 * Creates an array of weapon entries based on active player classes. Called by on first instance creation.
	 */
	virtual void PopulateSpawns() {
		// Clear:
		for (int weaponCategory = 0; weaponCategory < self.weaponSpawns.Size(); weaponCategory++) {
			self.weaponSpawns[weaponCategory].Clear();
		}
		
		// Add Per Player:
		for (int playerNum = 0; playerNum < players.Size(); playerNum++) {
			if (playeringame[playerNum]) {
				PlayerInfo playerInfoInstance = players[playerNum];
				SchismPlayerPawn schismPlayer = SchismPlayerPawn(playerInfoInstance.mo);
				if (schismPlayer) {
					for (int weaponCategory = 0; weaponCategory < self.weaponSpawns.Size(); weaponCategory++) {
						schismPlayer.PopulateWeaponSpawns(weaponCategory, self.weaponSpawns[weaponCategory]);
						// for (int i = 0; i < WeaponSpawns[weaponCategory].Size(); i++) {
						// 	schismPlayer.A_Log("Added weapon: " .. weaponCategory .. " - " .. WeaponSpawns[weaponCategory][i].weaponClass);
						// }
					}
				}
			}
		}
	}

	/** 
	 * Spawns a weapon.
	 * @param weaponClass The class of the weapon item to spawn, if blank nothing will be spawned.
	 * @param spawnActor The actor to spawn on top of.
	 * @return True if a weapon was successfully spawned.
	 */
	virtual bool SpawnWeapon(class<Inventory> weaponClass, Actor spawnActor) {
		if (!weaponClass) {
			return false;
		}
		return spawnActor.A_SpawnItemEx(weaponClass);
	}

	/** 
	 * Spawns a weapon for each active player class.
	 * @param category The category id of the weapons to spawn. Ex 0 = Foul Menagerie, Firestone Cane, etc 1 = Ebon Command, Druid Bow, Necronomicon, etc.
	 * @param index The index of the weapons in the category to spawn. 0 is usually the base weapon itself and 1-4 are its upgrades then 5 for the passive upgrade.
	 * @param spawnActor The actor to spawn on.
	 * @return Returns how many weapons that were successfully spawned.
	 */
	virtual int SpawnAllWeapons(int category, int index, Actor spawnActor) {
		int spawnedCount = 0;

		for (int playerNum = 0; playerNum < players.Size(); playerNum++) {
			if (!playeringame[playerNum]) {
				continue;
			}
			PlayerInfo playerInfo = players[playerNum];
			SchismPlayerPawn schismPlayer = SchismPlayerPawn(playerInfo.mo);
			if (schismPlayer) {
				class<Inventory> weaponClass = schismPlayer.GetWeaponClass(category, index);
				self.SpawnWeapon(weaponClass, spawnActor);
			}
		}

		return spawnedCount;
	}

	/** 
	 * Spawns a weapon for a random active player class.
	 * @param weaponCategory The category id of the weapon. Ex 0 = Foul Menagerie, Firestone Cane, etc 1 = Ebon Command, Druid Bow, Necronomicon, etc.
	 * @param spawnActor The actor to spawn on.
	 * @return Returns true if a random weapon was successfully spawned.
	 */
	virtual bool SpawnRandomWeapon(int weaponCategory, Actor spawnActor) {
		if (self.weaponSpawns[weaponCategory].Size() == 0 || !spawnActor) {
			return false;
		}
		
		// Get Weapons List To Spawn From:
		Array<WeaponEntry> categoryWeaponSpawns;
		categoryWeaponSpawns.Copy(self.weaponSpawns[weaponCategory]);
		int spawnWeaponNum = 0;
		
		// Needed Weapons:
		if (categoryWeaponSpawns.Size() > 1) {
			bool randomWeapon = true;

			// Get Random Player:
			int playerNum = self.GetPlayerNumToSpawnFor(spawnActor, categoryWeaponSpawns);
			if (playerNum >= 0) {
				if (!playeringame[playerNum]) {
					return false;
				}
				SchismPlayerPawn schismPlayer = SchismPlayerPawn(players[playerNum].mo);
				if (!schismPlayer) {
					return false;
				}

				// Get Needed Weapons:
				Array<int> neededWeapons;
				for (int i = 0; i < categoryWeaponSpawns.Size(); i++) {
					if (categoryWeaponSpawns[i].IsNeeded(schismPlayer)) {
						neededWeapons.push(i);
					}
				}

				// Random Needed Weapon:
				if (neededWeapons.Size()) {
					spawnWeaponNum = neededWeapons[Round(SchismUtil.PositionalVarition(spawnActor.pos, 0, neededWeapons.size() - 1))];
					randomWeapon = false;
				}
			}
				
			// Random If Player Has All (or no Substitute):
			if (randomWeapon) {
				spawnWeaponNum = Random(0, categoryWeaponSpawns.Size() - 1);
			}
		}
		
		// Spawn Chosen Weapon:
		WeaponEntry weaponEntry = categoryWeaponSpawns[spawnWeaponNum];
		if (weaponEntry) {
			self.SpawnWeapon(weaponEntry.weaponClass, spawnActor);
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Get Random Player To Spawn For
	 * @return -1 if the randomly selected player is not a Schism Player Pawn or has no weapons in the current set.
	*/
	virtual int GetPlayerNumToSpawnFor(Actor spawnActor, Array<WeaponEntry> weaponSpawns) {
		Array<int> playerNums;
		for (int i = 0; i < weaponSpawns.Size(); i++) {
			if (!weaponSpawns[i]) {
				continue;
			}
			int weaponPlayerNum = weaponSpawns[i].playerNum;
			if (playerNums.Find(weaponPlayerNum) == playerNums.Size()) {
				playerNums.Push(weaponPlayerNum);
			}
		}
		
		int playerNum = 0;
		
		// Get Random Player:
		if (playerNums.Size() > 1) {
			playerNum = Random(0, playerNums.Size() - 1);
		}
		
		return playerNum;
	}
	
	/** 
	 * Spawns specific weapons on top of Hexen weapons for all active players.
	 * @param spawnActor The actor to spawn on top of.
	 * @return True if a weapon was successfully spawned.
	 */
	virtual bool SpawnHexenWeapons(Actor spawnActor) {
		bool spawned = false;
		for (int i = 0; i < players.Size(); i++) {
			SchismPlayerPawn schismPlayer = SchismPlayerPawn(players[i].mo);
			if (!schismPlayer) {
				continue;
			}

			// Hexen Fighter Spawners:
			if (
				(FWeapFist(spawnActor) && schismPlayer.hexenClass == "Fighter") &&
				(CWeapMace(spawnActor) && schismPlayer.hexenClass == "Cleric") &&
				(MWeapWand(spawnActor) && schismPlayer.hexenClass == "Mage")
			) {
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(0, 0), spawnActor) || spawned;
				continue;
			}
			if (
				(FWeapAxe(spawnActor) && schismPlayer.hexenClass == "Fighter") &&
				(CWeapStaff(spawnActor) && schismPlayer.hexenClass == "Cleric") &&
				(MWeapFrost(spawnActor) && schismPlayer.hexenClass == "Mage")
			) {
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(0, 1), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(0, 5), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(1, 0), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(1, 1), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(1, 5), spawnActor) || spawned;

				if (Reilodos(schismPlayer)) {
					spawned = self.SpawnWeapon("ChaosShards", spawnActor) || spawned;
				}
				continue;
			}
			if (
				(FWeapHammer(spawnActor) && schismPlayer.hexenClass == "Fighter") &&
				(CWeapFlame(spawnActor) && schismPlayer.hexenClass == "Cleric") &&
				(MWeapLightning(spawnActor) && schismPlayer.hexenClass == "Mage")
			) {
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(0, 5), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(1, 5), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(2, 0), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(2, 1), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(2, 5), spawnActor) || spawned;

				if (Reilodos(schismPlayer)) {
					spawned = self.SpawnWeapon("ArcOfChaos", spawnActor) || spawned;
				}
				continue;
			}
			if (
				(FWeaponPiece1(spawnActor) && schismPlayer.hexenClass == "Fighter") &&
				(CWeaponPiece1(spawnActor) && schismPlayer.hexenClass == "Cleric") &&
				(MWeaponPiece1(spawnActor) && schismPlayer.hexenClass == "Mage")
			) {
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(0, 2), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(0, 5), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(1, 2), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(1, 5), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(2, 2), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(2, 5), spawnActor) || spawned;
				continue;
			}
			if (
				(FWeaponPiece2(spawnActor) && schismPlayer.hexenClass == "Fighter") &&
				(CWeaponPiece2(spawnActor) && schismPlayer.hexenClass == "Cleric") &&
				(MWeaponPiece2(spawnActor) && schismPlayer.hexenClass == "Mage")
			) {
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(0, 3), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(1, 3), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(1, 5), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(2, 3), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(2, 5), spawnActor) || spawned;
				continue;
			}
			if (
				(FWeaponPiece3(spawnActor) && schismPlayer.hexenClass == "Fighter") &&
				(CWeaponPiece3(spawnActor) && schismPlayer.hexenClass == "Cleric") &&
				(MWeaponPiece3(spawnActor) && schismPlayer.hexenClass == "Mage")
			) {
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(0, 4), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(1, 4), spawnActor) || spawned;
				spawned = self.SpawnWeapon(schismPlayer.GetWeaponClass(2, 4), spawnActor) || spawned;
				continue;
			}
		}
		
		return spawned;
	}
}

class WeaponEntry : Thinker {
	class<Inventory> weaponClass; // The class of the item (weapon, upgrade, passive).
	class<Inventory> dependantClass; // The optional class of the item that is required to use this item (the weapon and upgrade/passive is for, etc).
	int weaponCategory; // The category of this weapon entry (0 - 6).
	int playerNum; // The number of the player this entry is for.
	int itemCount; // The maximum inventory count for this entry to be considered fully collected. (Usually 1, 3 for passives).

    WeaponEntry Init() {
		ChangeStatNum(STAT_STATIC);
		return self;
	}

	virtual bool IsNeeded(SchismPlayerPawn playerActor) {
		if (playerActor.PlayerNumber() != self.playerNum) {
			return false;
		}

		if (self.dependantClass && !playerActor.CountInv(self.dependantClass)) {
			return false;
		}

		if (playerActor.CountInv(self.weaponClass) >= self.itemCount) {
			return false;
		}

		return true;
	}
}