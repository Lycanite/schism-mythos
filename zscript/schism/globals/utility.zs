class SchismUtil : Thinker {
	/** 
	 * Generates a pseudo random number float min and max based on the provided position.
	 * @param pos The position vector.
	 * @param min The minimum number to return (inclusive).
	 * @param max The maximum number to return (inclusive).
	 * @return A random float based on the provided position from min to max.
	 */
	static float PositionalVarition(vector3 pos, float min = 0, float max = 1) {
		float diff = max - min;
		if (!diff) {
			return min;
		}
		return min + (((pos.x + pos.y + pos.z) / 64) % diff);
	}

	/** 
	 * Determines if the provided actor is anywhere within the provided state label.
	 * @param actor The actor to check.
	 * @param label The state label to check for.
	 * @return True if the actor is anywhere within the state, false if not.
	 */
	static bool ActorInState(Actor actor, StateLabel label) {
		return actor.InStateSequence(actor.curState, actor.ResolveState(label));
	}
}