class SchismAttacks : Thinker {
	/** 
	 * Deals damage in a radius around the inflictor.
	 * @param inflictor The actor directly causing the explosion, can be a projectile, etc.
	 * @param source The overall owner of the explosion, the actor to credit for the damage, can be the shooter of a projectile, etc. Healing is self credited.
	 * @param damage The amount of damage to deal. Negative damage will heal instead and will use help interaction checks instead of harm checks.
	 * @param range The range of the explosion radius.
	 * @param thrust The amount of thrust away from the source actor to apply to affected actors. Defaults to 0.
	 * @param damageRangeScale The percentage of the explosion range where damage or healing is dealt, outside of this range only thrust is applied instead. Defaults to 1 for the entire range. Ex: 0.25 would only damage actors within 25% of the range.
	 * @param ignoreSight If true, line of sight checks are not used when checking for affected actors.
	 * @param hurtSelf It true, the explosion will also affect the inflictor actor themself.
	 * @param damageType The damage or healing type to use. Defaults to "None".
	 * @param actorsHit An optional array to add all actors with damage or healing range to, this will include corpses and non-monsters/players so may be larger that the hit count returned.
	 * @param damageFlags Flags to use when dealing damage, defaults to DMG_THRUSTLESS, these are passed to DamageMobj().
	 * @return Returns the number of monsters and players that were in damage or healing range. For all targets use a count of actorsHit.
	 */
	play static int Explode(Actor inflictor, Actor source, int damage, double range, double thrust = 0, double damageRangeScale = 1, bool ignoreSight = false, bool hurtSelf = false, string damageType = "None", Array<Actor> actorsHit = null, int damageFlags = DMG_THRUSTLESS) {
		int hitCount = 0;
		
		BlockThingsIterator iter = BlockThingsIterator.Create(inflictor, Max(range, range * damageRangeScale), false);
		while (iter.Next()) {
			Actor target = iter.thing;
			if (!target || !source) {
				continue; // Targets and sources can sometimes be removed mid loop.
			}
			
			// Check Target:
			if (target.bDormant) {
				continue; // Ignore dormant actors.
			}
			if (inflictor != source && SchismProjectile(inflictor)) {
				if (target == inflictor) {
					continue; // Projectiles, etc (inflictors that are not the source) should not target themselves.
				}
				if (target.GetClassName() == source.GetClassName()) {
					continue; // Projectiles, etc (inflictors that are not the source) should not target other actors of the same class.
				}
			}
			if (damage >= 0 && !target.bCorpse && !SchismInteraction.CanHarmTarget(source, target, hurtSelf)) {
				continue; // If this is area damage, do not target actors that should not be harmed (allies, etc), but allow corpses.
			}
			if (damage < 0 && !SchismInteraction.CanHelpTarget(source, target)) {
				continue; // If this is area healing, do not target actors that should not be helped (enemies, etc).
			}
			if (!ignoreSight && !inflictor.CheckSight(target)) {
				continue; // If line of sight is enabled and blocked, do not target.
			}
			
			double distance = inflictor.Distance3D(target);
			bool inDamageRange = distance <= range * damageRangeScale;
			bool isPlayer = PlayerPawn(target);

			// Actors Hit:
			if (inDamageRange && actorsHit) {
				actorsHit.Push(target);
			}

			// Damage/Heal Target:
			if (inDamageRange && !target.bCorpse) {
				if (target.bIsMonster || isPlayer) {
					hitCount++;
				}
				if (damage > 0) {
					target.DamageMobj(inflictor, source, damage, damageType, damageFlags);
				} else if (damage < 0) {
					target.A_DamageSelf(damage, damageType, DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
				}
			}
			
			// Thrust Target:
			if (thrust == 0 || distance > range || target.bBoss) {
				continue;
			}
			if (!target.bIsMonster && !target.bCorpse && !isPlayer) {
				continue;
			}
			double threshhold = range * 0.25;
			double relativeThrust = thrust * 0.25;
			relativeThrust += ((distance - threshhold) / (range - threshhold)) * 0.75;
			target.Thrust(relativeThrust, inflictor.AngleTo(target));
			if (target.pos.z != inflictor.pos.z) {
				double zPull = relativeThrust;
				if (target.pos.z < inflictor.pos.z) {
					target.A_ChangeVelocity(target.vel.x, target.vel.y, 0, CVF_REPLACE);
					relativeThrust = -relativeThrust;
				}
				target.AddZ(relativeThrust, false);
			}
		}
		
		return hitCount;
	}
	
	/** 
	 * Thrusts all actors from the inflictor. Essentially an explosion with no damage at all.
	 * @param inflictor The actor that owns the thrust.
	 * @param source The direct source actor of the thrust, could be a projectile, etc.
	 * @param range The range of the thrust.
	 * @param thrust The amount of thrust away from the source actor. Defaults to 100.
	 * @param ignoreSight If true, line of sight checks are not used when checking for affected actors.
	 * @param thrustSelf It true, the thrust will also affect the inflictor actor themself.
	 * @param actorsHit An optional array to add all actors hit by the explosion to, this will include corpses, non-monsters/players so may be larger that the hit count returned.
	 * @return Returns the number of monsters/players/corpses hit.
	 */
	play static int Thrust(Actor inflictor, Actor source, int range, double thrust = 100, bool ignoreSight = false, bool thrustSelf = false, Array<Actor> actorsHit = null) {
		return Explode(inflictor, source, 0, range, thrust, 1, ignoreSight, thrustSelf, "None", actorsHit);
	}
	
	/** 
	 * Creates a following projectile from the provided source actor.
	 * @param projectileClass The class name of the follow projectile to spawn.
	 * @param sourceActor The actor that the follow projectile will spawn on and follow.
	 * @param inflictor The actor that is creating and should be the owner of the follow projectile.
	 */
	play static FollowProjectile EmitFollowProjectile(String projectileClass, Actor sourceActor, Actor inflictor) {
		Actor followProjectileActor;
		if (PlayerPawn(sourceActor)) {
			float zOffset = sourceActor.height / 2;
			sourceActor.A_SpawnItemEx(projectileClass, -3, 0, zOffset, 0, 0, 0, 0, SXF_SETMASTER|SXF_NOCHECKPOSITION|SXF_ISTRACER);
			followProjectileActor = sourceActor.tracer;
		} else {
			followProjectileActor = sourceActor.A_SpawnProjectile(projectileClass, sourceActor.Height / 2, 0, 0, CMF_TRACKOWNER, 0, AAPTR_MASTER);
		}
		FollowProjectile followProjectile = FollowProjectile(followProjectileActor);
		if (followProjectile) {
			followProjectile.target = inflictor;
			followProjectile.setFollowTarget(sourceActor);
		}
		return followProjectile;
	}
}
