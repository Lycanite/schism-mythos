class SchismAmmoSpawnDirectory : Thinker {
	Array<string>[3][2] ammoSpawns; // Organized by Size (0-1 = Small-Large) and then Category (0-2 = A-C).
	Array<string> currentAmmoSpawns; // The current list to spawn from. Used because inline dynamic arrays aren't allowed and multi dimensional arrays cannot be converted properly.
	
	static SchismAmmoSpawnDirectory Get() {
		ThinkerIterator it = ThinkerIterator.Create("SchismAmmoSpawnDirectory", STAT_STATIC);
		SchismAmmoSpawnDirectory thinker = SchismAmmoSpawnDirectory(it.Next());
		if (thinker == null) {
			thinker = new("SchismAmmoSpawnDirectory").Init();
		}
		return thinker;
	}

	SchismAmmoSpawnDirectory Init() {
		ChangeStatNum(STAT_STATIC);
		self.PopulateSpawns();
		return self;
	}

	// Creates an array of ammo info based on active player classes. Called by: SchismAmmoSpawnEventHandler
	virtual void PopulateSpawns() {
		// Clear:
		for (int ammoCategory = 0; ammoCategory < self.ammoSpawns.Size(); ammoCategory++) {
			for (int ammoSize = 0; ammoSize < self.ammoSpawns[ammoCategory].Size(); ammoSize++)
			{
				self.ammoSpawns[ammoCategory][ammoSize].Clear();
			}
		}
		
		// Add Per Player:
		for (int playerNum = 0; playerNum < players.Size(); playerNum++) {
			if (!playeringame[playerNum]) {
				continue;
			}
			PlayerInfo playerInfoInstance = players[playerNum];
			SchismPlayerPawn schismPlayer = SchismPlayerPawn(playerInfoInstance.mo);
			if (schismPlayer) {
				for (int ammoCategory = 0; ammoCategory < self.ammoSpawns.Size(); ammoCategory++) {
					for (int ammoSize = 0; ammoSize < self.ammoSpawns[ammoCategory].Size(); ammoSize++) {
						string ammoSpawn = schismPlayer.GetAmmoClass(ammoCategory, ammoSize);
						if (ammoSpawn) {
							self.ammoSpawns[ammoCategory][ammoSize].Push(ammoSpawn);
						}
					}
				}
			}
		}
	}

	/** 
	 * Spawns ammo for a random active player class.
	 * @param ammoCategory The category id of the ammo, this aligns with weapon categories. Ex 0 = Chaos Energy for Foul Menagerie, etc.
	 * @param ammoSize The size of the ammo to spawn. 0 = Small, 1 = Large.
	 * @param spawnActor The actor to spawn on.
	 * @return Returns true if ammo was successfully spawned.
	 */
	virtual bool SpawnRandomAmmo(int ammoCategory, int ammoSize, Actor spawnActor) {
		if (self.ammoSpawns[ammoCategory][ammoSize].Size() == 0) {
			return false;
		}
		
		if (self.ammoSpawns[ammoCategory][ammoSize].Size() == 1) {
			self.currentAmmoSpawns.Copy(self.ammoSpawns[ammoCategory][ammoSize]);
			spawnActor.A_SpawnItemEx(self.currentAmmoSpawns[0]);
			return true;
		}
		
		self.currentAmmoSpawns.Copy(self.ammoSpawns[ammoCategory][ammoSize]);
		int randomAmmoNum = Random(0, self.ammoSpawns[ammoCategory][ammoSize].Size() - 1);
		spawnActor.A_SpawnItemEx(self.currentAmmoSpawns[randomAmmoNum]);
		
		return true;
	}
}
