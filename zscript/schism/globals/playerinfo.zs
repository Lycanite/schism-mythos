class SchismPlayerInfo : Thinker {
	SchismPlayerStats[MAXPLAYERS] stats;
	
    /** 
     * Finds the global SchismPlayerInfo instance. This is expensive so cache it on the actor or use QueryPlayerInfoMixin.
     */
	static SchismPlayerInfo Get() {
		ThinkerIterator iterator = ThinkerIterator.Create("SchismPlayerInfo", STAT_STATIC);
		SchismPlayerInfo schismPlayerInfo = SchismPlayerInfo(iterator.Next());
		if (schismPlayerInfo == null) {
			schismPlayerInfo = New("SchismPlayerInfo").Init();
		}
		return schismPlayerInfo;
	}
	
	SchismPlayerInfo Init() {
		ChangeStatNum(STAT_STATIC);
		return self;
	}
	
	
	// Add Minion:
	void AddMinion(int playerNum, MinionCreature minion) {
		if (!minion) {
			return;
		}
		stats[playerNum].minions.Push(minion);
	}
	
	// Remove Minion:
	void RemoveMinion(int playerNum, MinionCreature minion) {
		if (!minion) {
			return;
		}
		int minionIndex = stats[playerNum].minions.Find(minion);
		if (minionIndex < stats[playerNum].minions.Size()) {
			stats[playerNum].minions.Delete(minionIndex, 1);
		}
	}
	
	// Get Minion Count All:
	int GetMinionCountAll(int playerNum) {
		stats[playerNum].softMinionCount = stats[playerNum].minions.Size();
		return stats[playerNum].softMinionCount;
	}
	
	// Get Minion Count Soft:
	int GetMinionCountSoft(int playerNum) {
		return stats[playerNum].softMinionCount;
	}
	
	// Get Minion Count:
	clearscope int GetMinionCount(int playerNum, String minionClass = "") {
		// No Class:
		if (!minionClass || minionClass == "") {
			return stats[playerNum].minions.Size();
		}
		
		// Count Matching Class:
		int viableMinionCount = 0;
		for (int i = 0; i < stats[playerNum].minions.Size(); i++) {
			if (stats[playerNum].minions[i] && stats[playerNum].minions[i].GetClassName() == minionClass) {
				viableMinionCount++;
			}
		}
		return viableMinionCount;
	}
	
	/** 
	 * Gets a minion belonging to the specified player number of the specified class and index.
	 * @param playerNum The number of the player to get the minion from.
	 * @param minionClass The class name of the minion to get. If empty any minion is returned, if provided then a minion of that class is provided and index becomes how many to count from instead.
	 * @param minionIndex The index of the minion to get starting from 0. If a negative index is provided, the minions are retreived in reverse instead. Should typically be 1 or -1 when specifying a minion class.
	 * @param shield If true, only minions set as shields (that should take damage in place of their master) will be returned, this will also turn index into a relative count. Defaults to false.
	 * @return The minion actor or null if the index is invalid or there are no minions.
	 */
	MinionCreature GetMinion(int playerNum, string minionClass, int minionIndex, bool shield = false) {
		// Get Any Minion At Index:
		if ((!minionClass || minionClass == "") && !shield) {
			int minionCount = stats[playerNum].minions.Size();
			if (minionIndex < 0) { // From the latest (negative index).
				minionIndex = minionCount + minionIndex;
			}
			if (minionIndex >= 0 && minionIndex < stats[playerNum].minions.Size()) {
				return stats[playerNum].minions[minionIndex];
			}
			return null;
		}
		
		// Get Specific Minion At Relative Count using Index:
		int viableMinionCount = 0;
		if (minionIndex >= 0) {
			for (int i = 0; i < stats[playerNum].minions.Size(); i++) {
				if (stats[playerNum].minions[i] && stats[playerNum].minions[i].GetClassName() == minionClass) {
					if (shield && !stats[playerNum].minions[i].minionShield) {
						continue;
					}
					if (viableMinionCount == minionIndex) {
						return stats[playerNum].minions[i];
					}
					viableMinionCount++;
				}
			}
		} else { // Reverse search (negative index).
			for (int i = stats[playerNum].minions.Size() - 1; i >= 0; i--) {
				if (stats[playerNum].minions[i] && stats[playerNum].minions[i].GetClassName() == minionClass) {
					if (shield && !stats[playerNum].minions[i].minionShield) {
						continue;
					}
					if (viableMinionCount == Abs(minionIndex) - 1) {
						return stats[playerNum].minions[i];
					}
					viableMinionCount++;
				}
			}
		}
		
		return null;
	}
	
	// Is Minion:
	bool IsMinion(int playerNum, MinionCreature minion) {
		return stats[playerNum].minions.Find(minion) < stats[playerNum].minions.Size();
	}
}

struct SchismPlayerStats {
	Array<MinionCreature> minions;
	int softMinionCount;
}

/*
	Notes:
	If you"re getting address zero errors ensure that the SchismPlayerInfo instance is valid, it can fail from action functions for example where SchismPlayerInfo.Get() needs to be reused.
*/