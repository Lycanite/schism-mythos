class GrimRune : Ammo {
	Default {
		//$Category "Ammunition"
		
		Tag "Grim Rune";
		Scale 0.5;
		Inventory.PickupMessage "A Grim Rune seeping with soul tearing power!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "ACHZA0";
		Inventory.PickupSound "Chozo/Ammo";
		Inventory.RestrictedTo "Chozo";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			ACHZ ABC 4;
			Loop;
	}
}


class GrimRuneLarge : GrimRune {
	Default {
		//$Category "Ammunition"
		
		Tag "Large Grim Rune";
		Scale 0.5;
		Inventory.PickupMessage "A large Grim Rune full of power, hungering for souls!";
		Inventory.Amount 100;
	}
	
	States {
		Spawn:
			ACHZ DEF 4;
			Loop;
	}
}


class NecroticRune : Ammo {
	Default {
		//$Category "Ammunition"
		
		Tag "Necrotic Rune";
		Scale 0.5;
		Inventory.PickupMessage "A Necrotic Rune full of dark power!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "ACHZG0";
		Inventory.PickupSound "Chozo/Ammo";
		Inventory.RestrictedTo "Chozo";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			ACHZ GHI 4;
			Loop;
	}
}


class NecroticRuneLarge : NecroticRune {
	Default {
		//$Category "Ammunition"
		
		Tag "Large Necrotic Rune";
		Scale 0.5;
		Inventory.PickupMessage "A large Necrotic Rune trembling with dark energies!";
		Inventory.Amount 100;
	}
	
	States {
		Spawn:
			ACHZ JKL 4;
			Loop;
	}
}


class DeathRune : Ammo {
	Default {
		//$Category "Ammunition"
		
		Tag "Death Rune";
		Scale 0.5;
		Inventory.PickupMessage "A Death Rune marked with the stench of undeath!";
		Inventory.Amount 20;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 300;
		Ammo.BackpackMaxAmount 300;
		
		Inventory.Icon "ACHZM0";
		Inventory.PickupSound "Chozo/Ammo";
		Inventory.RestrictedTo "Chozo";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			ACHZ MNO 4;
			Loop;
	}
}


class DeathRuneLarge : DeathRune {
	Default {
		//$Category "Ammunition"
		
		Tag "Large Death Rune";
		Scale 0.5;
		Inventory.PickupMessage "A large Death Rune flowing with undeath magic!";
		Inventory.Amount 100;
	}
	
	States {
		Spawn:
			ACHZ PQR 4;
			Loop;
	}
}


class SoulEssence : Ammo {
	Default {
		//$Category "Ammunition"
		
		Tag "Soul Essence";
		Radius 64;
		Scale 0.25;
		Gravity 1;
		
		Inventory.PickupMessage "Soul Essence, freshly ripped from the victim's spirit.";
		Inventory.Amount 1;
		Inventory.MaxAmount 200;
		Ammo.BackpackAmount 0;
		Ammo.BackpackMaxAmount 200;
		
		Inventory.Icon "ACHZS0";
		Inventory.PickupSound "Chozo/Soul/Pickup";
		Inventory.RestrictedTo "Chozo";
		
		Renderstyle "Translucent";
		Alpha 0.5;
		
		+FLOATBOB;
		+INVENTORY.IGNORESKILL;
	}

	States {
		Spawn:
			ACHZ STUV 4 A_FadeOut(0.01);
			Loop;
	}
}