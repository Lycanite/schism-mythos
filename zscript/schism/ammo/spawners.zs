class AmmoSpawnerSmallA : Actor {
	States {
		Spawn:
			TNT1 A 1;
			TNT1 A 1 {
				SchismAmmoSpawnDirectory.Get().SpawnRandomAmmo(0, 0, invoker);
			}
			Stop;
	}
}

class AmmoSpawnerSmallB : Actor {
	States {
		Spawn:
			TNT1 A 1;
			TNT1 A 1 {
				SchismAmmoSpawnDirectory.Get().SpawnRandomAmmo(1, 0, invoker);
			}
			Stop;
	}
}

class AmmoSpawnerSmallC : Actor {
	States {
		Spawn:
			TNT1 A 1;
			TNT1 A 1 {
				SchismAmmoSpawnDirectory.Get().SpawnRandomAmmo(2, 0, invoker);
			}
			Stop;
	}
}

class AmmoSpawnerLargeA : Actor {
	States {
		Spawn:
			TNT1 A 1;
			TNT1 A 1 {
				SchismAmmoSpawnDirectory.Get().SpawnRandomAmmo(0, 1, invoker);
			}
			Stop;
	}
}

class AmmoSpawnerLargeB : Actor {
	States {
		Spawn:
			TNT1 A 1;
			TNT1 A 1 {
				SchismAmmoSpawnDirectory.Get().SpawnRandomAmmo(1, 1, invoker);
			}
			Stop;
	}
}

class AmmoSpawnerLargeC : Actor {
	States {
		Spawn:
			TNT1 A 1;
			TNT1 A 1 {
				SchismAmmoSpawnDirectory.Get().SpawnRandomAmmo(2, 1, invoker);
			}
			Stop;
	}
}