class GluttonGlobeProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 8;
		Scale 1;
		Speed 20;
		FastSpeed 25;
		Damage 8;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Nether";
		
		SeeSound "Projectile/GluttonGlobe/Fire";
		DeathSound "Projectile/GluttonGlobe/Explode";
		Decal "CacoScorch";

		SchismProjectile.ParticleName "Nether";
		SchismProjectile.ParticleCount 1;
	}
	
	States {
		Spawn:
			NPGG AB 4 bright;
			Loop;
			
		Death:
			NPGG CDEFGH 3 bright;
			Stop;
	}
}