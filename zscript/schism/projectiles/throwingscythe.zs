class ThrowingScytheProjectile : SchismProjectile {
	Default {
		Radius 8;
		Height 8;
		Scale 1;
		Speed 20;
		FastSpeed 20;
		DamageFunction 4;
		Alpha 1;
		RenderStyle "Normal";
		
		SeeSound "Projectile/ThrowingScythe/Fire";
		DeathSound "Projectile/ThrowingScythe/Explode";
		
		-RIPPER;
		+THRUGHOST;
	}
	
	States {
		Spawn:
			TSCY ABCDEFGH 2;
			Loop;
			
		Death:
			Stop;
	}
}