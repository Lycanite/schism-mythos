class SchismProjectile : Actor {
	mixin SpawnParticleMixin;
	
	string particleName;
	float particleScale;
	float particleSpeed;
	int particleColor;
	int particleCount;
	int particleInterval;
	int particleDuration;
	float particleOffsetZ;
	float particleOffsetRandom;

	float knockbackStrength;
	class<Inventory> statusEffectClass;
	class<SchismStatusEffect> statusEffectSpread;
	int statusEffectDuration;
	int statusEffectStrength;
	
	class<Inventory> giveAmmoClass;
	int giveAmmoMin;
	int giveAmmoMax;
	bool generateSpecialAmmo;

	class<Actor> summonType;
	int summonStance;
	int summonFlags;
	float summonAngleOffset;

	Actor summonMaster;
	bool detonated;
	protected int lifetime;
	protected int lifetimeMax;

	int poolFrom; // The lifetime that this projectile started pooling.
	int poolTics;
	int poolDamage;
	float poolDamageRadius;
	Class<SchismStatusEffect> poolEffectClass;
	int poolEffectTime;
	int poolEffectStrength;
	Class<SchismStatusEffect> poolEffectSpread;
	Class<ProjectilePool> poolClass;
	float poolScale;
	
	property LifetimeMax: lifetimeMax; // If above 0, how long in tics this projectile lasts for before immediately being destroyed, defaults to 0 for infinite.

	property ParticleName: particleName; // The name of the particle to spawn from this projectile, an empty string for no particles.
	property ParticleScale: particleScale; // The scale of the particle to spawn from this projectile.
	property ParticleSpeed: particleSpeed; // The speed of the particle to spawn from this projectile.
	property ParticleColor: particleColor; // The color of the particle to spawn from this projectile.
	property ParticleCount: particleCount; // The number of particles to spawn from this projectile.
	property ParticleInterval: particleInterval; // The frequency in tics to spawn particles from this projectile.
	property ParticleDuration: particleDuration; // The duration that particles spawned from this projectile last in tics.
	property ParticleOffsetZ: particleOffsetZ; // A z offset from the center of this projectile to spawn particles from.
	property ParticleOffsetRandom: particleOffsetRandom; // A random xy offset from the center of this projectile to spawn particles from, 0 for no randomness.

	property StatusEffectClass: statusEffectClass;
	property StatusEffectSpread: statusEffectSpread;
	property StatusEffectDuration: statusEffectDuration;
	property StatusEffectStrength: statusEffectStrength;
	property KnockbackStrength: knockbackStrength;

	property GiveAmmoClass: giveAmmoClass; // The optional class of ammo this projectile gives when dealing damage.
	property GiveAmmoMin: giveAmmoMin; // The minimum amount of ammo this projectile gives when dealing damage.
	property GiveAmmoMax: giveAmmoMax; // The maximum amount of ammo this projectile gives when dealing damage.
	property GenerateSpecialAmmo: generateSpecialAmmo; // If true, ammo is given if set. Also used by some extending projectiles to toggle special ammo generation.

	property SummonType: summonType;
	property SummonStance: summonStance; // The initial stance of the summoned minion. 0 = Wander, 1 = Follow, 2 = Guard
	property SummonFlags: summonFlags; // Additional A_SpawnItemEx flags to add.
	property SummonAngleOffset: summonAngleOffset; // An angle offset to apply to the summoned minion, it matches the projectile's last angle initially.

	property PoolTics: poolTics; // The remaining tics the projectile pools for (requires A_PoolUpdate in a loop).
	property PoolDamage: poolDamage; // The amount damage dealt by this projectile when pooling.
	property PoolDamageRadius: poolDamageRadius; // The radius of the damage dealt by this projectile when pooling.
	property PoolEffectClass: poolEffectClass; // The class of the status effect to inflict on enemies within the pool.
	property PoolEffectTime: poolEffectTime; // The time (in tics) of the status effect to inflict on enemies within the pool.
	property PoolEffectStrength: poolEffectStrength; // The strength of the status effect to inflict on enemies within the pool.
	property PoolEffectSpread: poolEffectSpread; // The status effect to spread from actors caught in the pool (plagues, etc).
	property PoolClass: poolClass; // The class name of the pool sfx actor that this projectile creates on death.
	property PoolScale: poolScale; // The base scale of the pool effect created by this projectile.
	
	Default {
		SchismProjectile.LifetimeMax 0;

		SchismProjectile.GiveAmmoClass "";
		SchismProjectile.GiveAmmoMin 1;
		SchismProjectile.GiveAmmoMax 1;
		SchismProjectile.GenerateSpecialAmmo true;

		SchismProjectile.ParticleName "";
		SchismProjectile.ParticleScale 1.0;
		SchismProjectile.ParticleSpeed 2;
		SchismProjectile.ParticleColor 0xF7000000;
		SchismProjectile.ParticleCount 1;
		SchismProjectile.ParticleInterval 1;
		SchismProjectile.ParticleDuration TICRATE;
		SchismProjectile.ParticleOffsetZ 0;
		SchismProjectile.ParticleOffsetRandom 0;

		SchismProjectile.StatusEffectClass "";
		SchismProjectile.StatusEffectDuration 0;
		SchismProjectile.StatusEffectStrength 1;
		SchismProjectile.KnockbackStrength 0;

		SchismProjectile.SummonType "";
		SchismProjectile.SummonStance 0; // Wander Stance
		SchismProjectile.SummonFlags 0;
		SchismProjectile.SummonAngleOffset 0;

		SchismProjectile.PoolTics 10 * TICRATE;
		SchismProjectile.PoolDamage 1;
		SchismProjectile.PoolDamageRadius 140;
		SchismProjectile.PoolEffectClass "";
		SchismProjectile.PoolEffectTime 10 * TICRATE;
		SchismProjectile.PoolEffectStrength 1;
		SchismProjectile.PoolEffectSpread "";
		SchismProjectile.PoolClass "";
		SchismProjectile.PoolScale 1;
		
		PROJECTILE;
		+FORCEXYBILLBOARD;
		+RANDOMIZE;
	}

	override void PostBeginPlay() {
		super.PostBeginPlay();
		
		// Summoning Master:
		if (!self.summonMaster && self.target) {
			self.summonMaster = self.target; // Start with shooter as master.
			MinionCreature minionShooter = MinionCreature(self.target);
			if (minionShooter) {
				self.summonMaster = minionShooter.GetFinalMaster();
			} else if (self.target.master) {
				self.summonMaster = self.target.master;
			}
		}
	}
	
	override void Tick() {
		super.Tick();
		if (level.IsFrozen() || self.IsFrozen()) {
			return;
		}
		self.SpawnParticles();

		// Lifetime:
		self.lifetime++;
		if (self.lifetimeMax > 0 && self.lifetime >= self.lifetimeMax && !SchismUtil.ActorInState(self, "Death")) {
			self.SetStateLabel("Death");
		}
	}

	override int SpecialMissileHit(Actor target) {
		// Go Through Minions:
		if (self.target) {
			if (target.master == self.target) {
				return 1;
			}
			MinionCreature minionTarget = MinionCreature(target);
			if (minionTarget && minionTarget.GetFinalMaster() == self.target) {
				return 1;
			}
		}
		
		return super.SpecialMissileHit(target);
	}
	
	override int DoSpecialDamage(Actor target, int damage, name damageType) {
		damage = super.DoSpecialDamage(target, damage, damageType);
		Actor source = self;
		if (self.target) {
			source = self.target;
		}
		bool canHarm = SchismInteraction.CanHarmTarget(source, target, false);

		// Prevent Friendly Fire:
		if (damage > 0 && !canHarm) {
			return 0;
		}

		// Status Effects:
		if (canHarm && self.statusEffectDuration > 0 && self.statusEffectClass) {
			Inventory statusEffect = self.InflictEffect(self.statusEffectClass, target, self.statusEffectDuration, self.statusEffectSpread, self.statusEffectStrength);
			if (target && statusEffect) {
				self.OnInflictEffect(target, statusEffect);
			}
		}

		// Knockback:
		if (canHarm && !target.bBoss && self.lifetime > 0) {
			target.Thrust(self.knockbackStrength, self.angle);
		}

		// Give Ammo:
		if (damage > 0 && self.giveAmmoClass && self.giveAmmoMin > 0 && self.generateSpecialAmmo) {
			int amount = self.giveAmmoMin;
			if (self.giveAmmoMax > amount) {
				amount = Random(self.giveAmmoMin, self.giveAmmoMax);
			}
			if (amount) {
				self.GiveAmmo(self.target, self.giveAmmoClass, amount);
			}
		}

		return damage;
	}
	
	/**
	 * Spawns particles from this projectile, called on tick, disabled when pooling as pool actors should spawn particles instead.
	 */
	virtual void SpawnParticles() {
		if (!self.particleName || self.detonated || self.particleCount <= 0 || !self.particleInterval || self.lifetime % self.particleInterval != 0 || self.poolFrom > 0) {
			return;
		}

		for (int i = 0; i < self.particleCount; i++) {
			vector3 offset = (0, 0, 0);
			if (self.particleOffsetRandom > 0) {
				offset = (
					Random(-self.particleOffsetRandom, self.particleOffsetRandom),
					Random(-self.particleOffsetRandom, self.particleOffsetRandom),
					self.particleOffsetZ
				);
			}
			self.SpawnParticle(
				self.particleName, self.particleDuration, self.particleSpeed,
				offset,
				FRandom(0, 359), FRandom(-45, 45),
				self.particleScale, self.particleColor
			);
		}
	}
	
	/**
	 * Inflicts a status effect.
	 * @param effectName The status effect to inflict.
	 * @param targetActor The actor to afflict.
	 * @param effectDuration The duration of the status effect.
	 * @param effectSpread An optional status effect that the inflicted effect should spread to nearby valid targets.
	 * @param effectSrength The strength of the effect, defaults to 1.
	 */
	virtual Inventory InflictEffect(Class<Inventory> effectName, Actor targetActor, int effectDuration = 5 * 35, Class<SchismStatusEffect> effectSpread = "", int effectStrength = 1) {
		return SchismStatusEffects.InflictEffect(effectName, self.target, self, targetActor, effectDuration, effectSpread, effectStrength);
	}

	/**
	 * Called when this projectile inflicts a status effect to its target.
	 * @param target The target actor having an effect inflicted.
	 * @param statusEffect The inflicted status effect instance.
	 */
	virtual void OnInflictEffect(Actor target, Inventory statusEffect) {}

	virtual void GiveAmmo(Actor target, class<Inventory> ammoClass, int amount) {
		if (target) {
			target.A_GiveInventory(ammoClass, amount);
		}
	}

	/**
	 * Sets the velocity and angle of this projectile to a new target position.
	 */
	virtual void Relaunch(vector3 targetPos) {
		float angle = VectorAngle(targetPos.x - self.pos.x, targetPos.y - self.pos.y);
		float pitch = -VectorAngle(targetPos.xy.length(), targetPos.z - self.pos.z);
		self.angle = angle;
		self.pitch = pitch;
		self.Vel3DFromAngle(self.speed, angle, pitch);
	}
	
	/**
	 * Summons a creature and sets up a master minion relation if applicable.
	 */
	virtual Actor SummonMinion(vector3 offset = (0, 0, 0), vector3 vel = (0, 0, 0), class<Actor> overrideType = "") {
		class<Actor> summonType = self.summonType;
		if (overrideType) {
			summonType = overrideType;
		}
		if (!summonType || !self.summonMaster) {
			return null;
		}
		bool spawned = false;
		Actor summonedActor = null;
		[spawned, summonedActor] = A_SpawnItemEx(summonType, offset.x, offset.y, offset.z, vel.x, vel.y, vel.z, self.summonAngleOffset, SXF_ISTRACER|self.summonFlags);
		if (summonedActor) {
			summonedActor.master = self.summonMaster;
			MinionCreature minionCreature = MinionCreature(summonedActor);
			if (minionCreature) {
				minionCreature.stance = self.summonStance;
			}
		}
		return summonedActor;
	}

	/**
	 * Finds a new target within line of site and range.
	 */
	virtual Actor FindNewTarget(float range = 4000) {
		Actor shooter = self.target;
		BlockThingsIterator iter = BlockThingsIterator.Create(self, range, false);
		while (iter.Next()) {
			Actor target = iter.thing;
			if (!target) {
				continue; // Targets can sometimes be lost.
			}

			if (!self.CheckSight(target)) {
				continue;
			}

			if (shooter && !SchismInteraction.CanHarmTarget(shooter, target)) {
				continue;
			}

			return target;
		}
		return null;
	}

	/**
	 * Called by the WorldThingDied event listener when this projectile kills an actor.
	 */
	virtual void OnKill(Actor victim) {}

	/**
	 * Called when this projectile is pooling, passes how many targets were damaged.
	 */
	virtual void OnPoolDamage(int targets) {}

	/**
	 * Called by pooling projectiles, will fade out over the pool duration.
	 * @param fade The amount to fade out by.
	 */
	action void A_PoolUpdate(float fade = 0.1) {
		if (invoker.poolFrom <= 0) {
			invoker.poolFrom = Max(1, invoker.lifetime);
			bool spawned;
			Actor actor;
			[spawned, actor] = A_SpawnItemEx(invoker.poolClass, 0, 0, 0, 0, 0, 0, 0, SXF_CLIENTSIDE);
			ProjectilePool projectilePool = ProjectilePool(actor);
			if (projectilePool) {
				projectilePool.scaleBase = invoker.poolScale;
				projectilePool.remainingTics = invoker.poolTics;
			}
		}

		// Damage:
		Array<Actor> actorsHit;
		int targets = SchismAttacks.Explode(invoker, invoker.target, invoker.poolDamage, invoker.poolDamageRadius, 0, 1, false, false, invoker.damageType, actorsHit);
		invoker.OnPoolDamage(targets);
		invoker.SpawnParticle("Fire", TICRATE, 1, (Random(-60, 60), Random(-60, 60), Random(-4, 4)), Random(0, 359), Random(60, 90));

		// Effect:
		if (invoker.poolEffectClass && invoker.target) {
			for (int i = 0; i < actorsHit.Size(); i++) {
				Actor target = actorsHit[i];
				if (!target || target.bCorpse || target.health <= 0) {
					continue;
				}
				if (!target.bIsMonster && !PlayerPawn(target)) {
					continue;
				}
				if (invoker.poolDamage >= 0 && !SchismInteraction.CanHarmTarget(invoker.target, target)) {
					continue;
				}
				if (invoker.poolDamage < 0 && !SchismInteraction.CanHelpTarget(invoker.target, target)) {
					continue;
				}
				invoker.InflictEffect(invoker.poolEffectClass, target, invoker.poolEffectTime, invoker.poolEffectSpread, invoker.poolEffectStrength);
			}
		}

		// Fade Out:
		A_FadeOut(fade);
	}
}

class SchismAttackActor : Actor {	
	int lifespan;
	bool generateSpecialAmmo;

	property Lifespan: lifespan;
	property GenerateSpecialAmmo: generateSpecialAmmo;
	
	Default {
		SchismAttackActor.Lifespan 5 * TICRATE;
		SchismAttackActor.GenerateSpecialAmmo true;
	}
	
	override void Tick() {
		super.Tick();
		if (level.IsFrozen() || self.IsFrozen()) {
			return;
		}
		self.lifespan--;
	}
}

class FollowProjectile : SchismProjectile {
	Actor followTarget;
	double followHeightOffset;
	double offsetLat;
	double offsetLon;
	double offsetVert;

	double followOffsetLat;
	double followOffsetLon;
	double followOffsetVert;

	bool orbit;
	double orbitRange;
	double orbitAngle;
	double orbitSpeed;

	property FollowOffsetLat: followOffsetLat;
	property FollowOffsetLon: followOffsetLon;
	property FollowOffsetVert: followOffsetVert;
	
	Default {
		Radius 16;
		Height 10;
		Speed 200;
		Gravity 0;
		Damage 0;
		Scale 1;

		FollowProjectile.FollowOffsetLat 0;
		FollowProjectile.FollowOffsetLon 0;
		FollowProjectile.FollowOffsetVert 0;
		
		+THRUGHOST;
		-BLOODSPLATTER;
		+SEEKERMISSILE;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		followHeightOffset = self.Height / 2;
	}
	
	override void Tick() {
		super.Tick();
		if (level.IsFrozen() || self.IsFrozen()) {
			return;
		}
		
		if (self.followTarget) {
			double followX = self.followTarget.pos.x;
			double followY = self.followTarget.pos.y;
			double followZ = self.followTarget.pos.z + self.offsetVert;
			if (self.offsetLat != 0) {
				vector2 followLonXY = self.followTarget.Vec2Angle(self.offsetLat, (self.angle + 90) % 360);
				followX += followLonXY.x - self.followTarget.pos.x;
				followY += followLonXY.y - self.followTarget.pos.y;
			}
			if (self.offsetLon != 0) {
				vector2 followLatXY = self.followTarget.Vec2Angle(self.offsetLon, self.angle);
				followX += followLatXY.x - self.followTarget.pos.x;
				followY += followLatXY.y - self.followTarget.pos.y;
			}

			// Orbit:
			if (self.orbit) { 
				double angle = self.orbitAngle += self.orbitSpeed;
				followX += self.orbitRange * sin(angle);
				followY += self.orbitRange * cos(angle);
				followZ += self.followTarget.height * 0.5;
			}

			self.SetOrigin((followX, followY, followZ), false);
		}
	}
	
	virtual Actor GetFollowTarget() {
		return self.followTarget;
	}
	
	virtual void SetFollowTarget(Actor newTarget, double offsetLat = 0, double offsetLon = 0, double offsetVert = 0) {
		self.followTarget = newTarget;
		self.offsetLat = self.followOffsetLat + offsetLat;
		self.offsetLon = self.followOffsetLon + offsetLon;
		self.offsetVert = self.followOffsetVert + self.followHeightOffset + offsetVert;
		if (self.followTarget) {
			self.offsetVert += self.followTarget.height / 2;
		}
	}
	
	States {
		Spawn:
			TNT1 A 4;
			Loop;
	}
}

class HomingEffectProjectile : SchismProjectile {
	Actor followTarget;
	int followTime;
	int pickupDelayTime;
	bool expired;
	
	Default {
		Scale 0.25;
		Radius 16;
		Height 10;
		Speed 20;
		Gravity 0;
		DamageFunction 0;
		Alpha 1;
		Renderstyle "Add";
		
		SeeSound "SoulEssence/Fire";
		DeathSound "SoulEssence/Explode";
		
		+SEEKERMISSILE;
		+RIPPER;
		+NOCLIP;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		followTime = 10 * 35;
		pickupDelayTime = 5;
		expired = false;
	}
	
	override void Tick() {
		super.Tick();
		if (expired || level.IsFrozen() || self.IsFrozen()) {
			return;
		}

		// Lost Follow Target or Timeout:
		if (!followTarget || followTime-- <= 0) {
			OnTimeout();
			return;
		}
		tracer = followTarget;
		
		// Slowdown on nearing follow target:
		if (Distance3D(followTarget) <= 100) {
			Speed = 10;
		} else {
			Speed = 20;
		}
		
		// Magnetic Pickup Collection:
		if (Distance3D(followTarget) <= 60 && pickupDelayTime-- <= 0) {
			OnImpact();
		}
	}
	
	virtual void OnImpact() {
		expired = true;
		self.A_Die();
	}
	
	virtual void OnTimeout() {
		expired = true;
		self.A_Die();
	}
	
	virtual state A_HomeToTarget(float range = 0) {
		if (!followTarget) {
			return null;
		}
		if (range > 0 && self.Distance3D(followTarget) > range) {
			return null;
		}
		if (expired) {
			return self.ResolveState("Death");
		}
		self.A_SeekerMissile(0, 20, SMF_LOOK|SMF_PRECISE);
		return null;
	}
	
	States {
		Spawn:
			TNT1 A 1 {
				return A_HomeToTarget();
			}
			Loop;
	}
}

class FalseProjectile : FollowProjectile {
	Default {
		Damage 1;
		
		SeeSound "";
		DeathSound "";
		
		+BLOODSPLATTER;
	}
}

class SchismStickyProjectile : SchismProjectile {
	int offsetLat;
	int offsetLon;
	int offsetVert;
	Actor followTarget;
	int expireTime;
	
	Default {
		Damage 1;
		
		SeeSound "";
		DeathSound "";
		
		+EXPLODEONWATER;
		+SKYEXPLODE;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.expireTime = 5 * 35;
	}
	
	override bool CanCollideWith(Actor other, bool passive) {
		if (!passive && !self.followTarget && other != self.target) {
			self.SetFollowTarget(other);
		}
		return super.CanCollideWith(other, passive);
	}
	
	virtual void SetFollowTarget(Actor newTarget) {
		self.followTarget = newTarget;
		self.offsetLat = self.pos.x - newTarget.pos.x;
		self.offsetLon = self.pos.y - newTarget.pos.y;
		self.offsetVert = self.pos.z - newTarget.pos.z;
	}
	
	action state AttachUpdate() { // Call in Death looping state.
		// No Owner
		if (!target) {
			return ResolveState("Detonate");
		}
		master = target;
		
		// Attachment
		invoker.gravity = 0;
		if (invoker.followTarget) {
			invoker.SetOrigin(invoker.followTarget.pos +(invoker.offsetLat, invoker.offsetLon, invoker.offsetVert), false);
		}
		
		// Expiring
		if (invoker.expireTime-- <= 0) {
			return ResolveState("Detonate");
		}
		
		return null;
	}
	
	virtual void Detach() {
		self.expireTime = 0;
	}
	
	action void Detonate() { // Call in Detonate stopping state.
		A_StopSound(CHAN_BODY);
	}
}

class ProjectilePool : Actor {
	mixin SpawnParticleMixin;

	float scaleBase;
	int remainingTics;
	
	string particleName;
	float particleScale;
	float particleSpeed;
	int particleColor;
	int particleCount;
	int particleInterval;
	int particleDuration;
	float particleOffsetZ;
	float particleOffsetRandom;
	float particlePitchMin;
	float particlePitchMax;

	property ScaleBase: scaleBase; // The base scale of this actor, the scale itself is animated.
	property RemainingTics: remainingTics; // How long this effects lasts for in tics.
	
	property ParticleName: particleName; // The name of the particle to spawn from this projectile, an empty string for no particles.
	property ParticleScale: particleScale; // The scale of the particle to spawn from this projectile.
	property ParticleSpeed: particleSpeed; // The speed of the particle to spawn from this projectile.
	property ParticleColor: particleColor; // The color of the particle to spawn from this projectile.
	property ParticleCount: particleCount; // The number of particles to spawn from this projectile.
	property ParticleInterval: particleInterval; // The frequency in tics to spawn particles from this projectile.
	property ParticleDuration: particleDuration; // The duration that particles spawned from this projectile last in tics.
	property ParticleOffsetZ: particleOffsetZ; // A z offset from the center of this projectile to spawn particles from.
	property ParticleOffsetRandom: particleOffsetRandom; // A random xy offset from the center of this projectile to spawn particles from, 0 for no randomness.
	property ParticlePitchMin: particlePitchMin; // The minimum random pitch to set on particles.
	property ParticlePitchMax: particlePitchMax; // The maximum random pitch to set on particles.

	Default {
		Radius 16;
		Height 1;
		Scale 1;
		Gravity 0.25;
		Alpha 1;

		ProjectilePool.ScaleBase 1;
		ProjectilePool.RemainingTics 10 * TICRATE;

		ProjectilePool.ParticleName "";
		ProjectilePool.ParticleScale 1.0;
		ProjectilePool.ParticleSpeed 2;
		ProjectilePool.ParticleColor 0xF7000000;
		ProjectilePool.ParticleCount 1;
		ProjectilePool.ParticleInterval 1;
		ProjectilePool.ParticleDuration TICRATE;
		ProjectilePool.ParticleOffsetZ 0;
		ProjectilePool.ParticleOffsetRandom 0;
		ProjectilePool.ParticlePitchMin 0;
		ProjectilePool.ParticlePitchMax 0;

		+CLIENTSIDEONLY;
		-SOLID;
		+FLATSPRITE;
	}

	override void Tick() {
		super.Tick();
		if (level.IsFrozen() || self.IsFrozen()) {
			return;
		}
		self.SpawnParticles();
	}

	virtual void SpawnParticles() {
		if (!self.particleName || self.particleCount <= 0 || !self.particleInterval) {
			return;
		}

		for (int i = 0; i < self.particleCount; i++) {
			vector3 offset = (0, 0, 0);
			if (self.particleOffsetRandom > 0) {
				offset = (
					Random(-self.particleOffsetRandom, self.particleOffsetRandom),
					Random(-self.particleOffsetRandom, self.particleOffsetRandom),
					self.particleOffsetZ
				);
			}

			float pitch = self.particlePitchMin;
			if (self.particlePitchMax > self.particlePitchMin) {
				pitch = FRandom(self.particlePitchMin, self.particlePitchMax);
			}

			self.SpawnParticle(
				self.particleName, self.particleDuration, self.particleSpeed,
				offset,
				FRandom(0, 359), pitch,
				self.particleScale, self.particleColor
			);
		}
	}
	
	action state A_EffectUpdate(int tics) {
		// Remaining Tics:
		invoker.remainingTics -= tics;
		if (invoker.remainingTics <= 0) {
			return ResolveState("Null");
		}

		// Scale Animation:
		float scale = invoker.scaleBase + ((invoker.scaleBase * 0.5) * Sin(invoker.remainingTics));
		invoker.scale = (scale, scale);

		// Alpha Animation:
		if (invoker.remainingTics < 2 * TICRATE) {
			invoker.alpha = Float(invoker.remainingTics) / (2 * TICRATE);
		}

		return null;
	}
}