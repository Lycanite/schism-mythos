class TendrilboltProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 8;
		Scale 1;
		Speed 10;
		FastSpeed 20;
		Damage 3;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Void";
		
		SeeSound "Projectile/Tendrilbolt/Fire";
		DeathSound "Projectile/Tendrilbolt/Explode";

		SchismProjectile.ParticleName "Void";
		SchismProjectile.ParticleCount 1;
	}
	
	States {
		Spawn:
			NPTB ABCDEFG 4 bright;
			Loop;
			
		Death:
			NPTB HIJKLMNO 4 bright;
			Stop;
	}
}