class HellfireballProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 16;
		Scale 1.25;
		Speed 15;
		FastSpeed 20;
		Damage 8;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Nether";
		
		SeeSound "Projectile/Hellfireball/Fire";
		DeathSound "Projectile/Hellfireball/Explode";
		Decal "BaronScorch";

		SchismProjectile.ParticleName "Hellflame";
		SchismProjectile.ParticleCount 1;
	}
	
	States {
		Spawn:
			NPHB ABCD 4 bright;
			Loop;
			
		Death:
			NPHB EFGHIJ 4 bright;
			Stop;
	}
}