class LifestarProjectile : SchismProjectile {
	Default {
		Radius 13;
		Height 8;
		Scale 1;
		Speed 25;
		FastSpeed 30;
		Damage 2;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Fae";
		
		SeeSound "Projectile/Maelstar/Fire";
		DeathSound "Projectile/Maelstar/Explode";

		SchismProjectile.ParticleName "Anima";
		SchismProjectile.ParticleCount 1;
		SchismProjectile.ParticleScale 0.125;
	}
	
	States {
		Spawn:
			NPLS AB 5 bright;
			Loop;
			
		Death:
			NPLS CDEFG 5 bright;
			Stop;
	}
}