class ChromaboltProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 8;
		Scale 0.5;
		Speed 16;
		FastSpeed 24;
		Damage 2;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Earth";
		
		SeeSound "Projectile/Chromabolt/Fire";
		DeathSound "Projectile/Chromabolt/Explode";

		SchismProjectile.ParticleName "Quake";
		SchismProjectile.ParticleCount 4;
		SchismProjectile.ParticleScale 0.25;
		SchismProjectile.ParticleDuration 10;
		SchismProjectile.StatusEffectClass "SchismBleedDebuff";
		SchismProjectile.StatusEffectDuration 5 * TICRATE;
	}
	
	States {
		Spawn:
			NPCB ABCD 1 bright;
			Loop;
			
		Death:
			NPCB EFGHIJ 4 bright;
			Stop;
	}
}