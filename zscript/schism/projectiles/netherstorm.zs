class NetherstormProjectile : SchismProjectile {
	Default {
		Radius 12;
		Height 6;
		Scale 1;
		Speed 30;
		FastSpeed 30;
		Damage 50;
		Alpha 0.8;
		RenderStyle "Add";
		
		DamageType "Nether";
		
		SeeSound "Projectile/Netherstorm/Fire";
		DeathSound "Projectile/Netherstorm/Explode";
		Decal "CacoScorch";

		SchismProjectile.ParticleName "Nether";
		SchismProjectile.ParticleCount 1;

        +NODAMAGETHRUST;
	}

    States {
        Spawn:
            NPN1 STUVW 1 bright;
            NPN1 W 0 bright A_SpawnProjectile("BaalStormFragment", 0, 0, 90, CMF_AIMDIRECTION | CMF_TRACKOWNER);
            NPN1 W 0 bright A_SpawnProjectile("BaalStormFragment", 0, 0, 270, CMF_AIMDIRECTION | CMF_TRACKOWNER);
            Loop;
        Death:
            NPN1 STUVWSTUVWSTUVWSTUVWSTUVWSTUVWSTUVWSTUVWSTUVWSTUVWSTUVWSTUVW 1 bright A_Explode(16, 32, 0);
            Stop;
    }
}

Class BaalStormFragment : SchismProjectile {
    Default {
        Radius 6;
        Height 12;
		Scale 1;
        Speed 32;
        Damage 5;
        RenderStyle "Add";
        Alpha 0.80;

        Damagetype "Lightning";

        DeathSound "Projectile/Netherstorm/Explode";
		
        +RIPPER;
    }

    States {
        Spawn:
            NPN2 ABC 1 bright;
            Loop;
        Death:
            NPN2 DEFGHIJKLMNO 1 bright;
            Stop;
    }
}