class VoidGrazerProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 8;
		Scale 1;
		Speed 10;
		Damage 10;
		Alpha 0.9;
		RenderStyle "Add";
		
		DamageType "Void";
		
		SeeSound "Projectile/VoidGrazer/Fire";
		DeathSound "Projectile/VoidGrazer/Explode";
		Decal "RevenantScorch";

		SchismProjectile.ParticleName "Void";
		SchismProjectile.ParticleCount 1;

		+SEEKERMISSILE;
		+DONTSEEKINVISIBLE;
	}
	
	States {
		Spawn:
			NPVG ABC 2 bright A_SeekerMissile(45, 45, 2);
			Loop;
			
		Death:
			NPVG D 4 bright A_Explode(5, 90);
			NPVG EFGHI 4 bright;
			Stop;
	}
}