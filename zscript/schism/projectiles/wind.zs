class WindProjectile : SchismProjectile {
	Default {
		Radius 8;
		Height 8;
		Scale 1;
		Speed 14;
		FastSpeed 20;
		DamageFunction 4;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Air";
		
		SeeSound "SwiftDaggers/Projectile";
		DeathSound "SwiftDaggers/Projectile/Explode";
		
		+RIPPER;
	}
	
	States {
		Spawn:
			SWDP ABCDEF 1;
			Loop;
			
		Death:
			SWDP GHIJK 4;
			Stop;
	}
}