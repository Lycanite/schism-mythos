class MaelsphereProjectile : SchismProjectile {
	bool isDead;

	float instability;
	float weaveX;
	float weaveY;

	property Instability: instability;
	property WeaveX: weaveX;
	property WeaveY: weaveY;

	Default {
		Scale 0.75;
		Radius 5;
		Height 7;
		Speed 15;
		Gravity 0.25;
		DamageFunction random(4, 10);
		Alpha 0.75;
		RenderStyle "Add";
		
		DamageType "Chaos";

		SchismProjectile.ParticleName "ChaosSpark";
		SchismProjectile.ParticleDuration 8;

		MaelsphereProjectile.Instability 0;
		MaelsphereProjectile.WeaveX 4;
		MaelsphereProjectile.WeaveY 4;
		
		SeeSound "";
		DeathSound "FirestoneCane/Explode";
		Obituary "%o was ripped asunder by %k's Maelsphere!";
		
		+RANDOMIZE;
		-THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		
		self.weaveX = random(-self.weaveX, self.weaveX);
		self.weaveY = random(-self.weaveY, 0);
		self.isDead = false;
	}
	
	override void Tick() {
		super.Tick();
		if (self.isDead || level.isFrozen() || self.isFrozen()) {
			return;
		}

		self.A_Weave(3, 3, GetWeaveX(), GetWeaveY());
		self.IncreaseInstability(0.025);
	}
	
	action void IncreaseInstability(float amount) {
		invoker.instability += amount;
	}
	
	action float GetWeaveX(void) {
		return invoker.weaveX * invoker.instability;
	}
	
	action float GetWeaveY(void) {
		return invoker.weaveY * invoker.instability;
	}
	
	States {
		Spawn:
			FMEN ABCD 4 bright;
			Loop;
			
		Death:
			FMEN E 4 bright {
				invoker.isDead = true;
			}
			FMEN FGHIJ 4 bright;
			Stop;
	}
}