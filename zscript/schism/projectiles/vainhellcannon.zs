class VainHellcannonProjectile : SchismProjectile {
	Default {
		Radius 8;
		Height 12;
		Scale 1;
		Speed 20;
		FastSpeed 30;
		Damage 20;
		Alpha 1;
		RenderStyle "Normal";
		
		DamageType "Nether";
		
		SeeSound "Projectile/VainHellcannon/Fire";
		DeathSound "Projectile/VainHellcannon/Explode";
		Decal "CacoDecal";

		SchismProjectile.ParticleName "Hellflame";
		SchismProjectile.ParticleCount 1;

		DontHurtShooter;
	}
	
	States {
		Spawn:
			NPVH ABC 2 bright;
			Loop;
			
		Death:
			NPVH D 4 bright A_SetTranslucent(0.67, 1);
			NPVH E 4 bright A_Explode(60, 128, 0);
			NPVH FGH 4 bright;
			Stop;
	}
}