class BloodBelchProjectile : SchismProjectile {
	Default {
		Radius 8;
		Height 8;
		Scale 1;
		Speed 20;
		FastSpeed 20;
		DamageFunction 4;
		Alpha 0.8;
		RenderStyle "Translucent";
		
		DamageType "Fae";
		
		SeeSound "Projectile/BloodBelch/Fire";
		DeathSound "Projectile/BloodBelch/Explode";
		Decal "BloodSplat";

		SchismProjectile.ParticleName "Fae";
	}
	
	States {
		Spawn:
			BLBL ABC 2;
			Loop;
			
		Death:
			BLBL DEF 4;
			Stop;
	}
}