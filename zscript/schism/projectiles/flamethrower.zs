class FlamethrowerProjectile : SchismProjectile {
	Default {
		Radius 8;
		Height 8;
		Scale 1;
		Speed 20;
		FastSpeed 20;
		DamageFunction 1;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Fire";
		
		SeeSound "FirestoneCane/Fire";
		DeathSound "FirestoneCane/Explode";

		SchismProjectile.ParticleName "Fire";
		SchismProjectile.ParticleCount 1;
		SchismProjectile.StatusEffectClass "SchismImmolateDebuff";
		SchismProjectile.StatusEffectDuration 1 * 35;
		
		+RIPPER;
		-THRUGHOST;
	}
	
	States {
		Spawn:
			FSCF ABCDEFGHIHGFEDCB 2 bright;
			Loop;
			
		Death:
			FSCF JKLMNOP 4 bright;
			Stop;
	}
}