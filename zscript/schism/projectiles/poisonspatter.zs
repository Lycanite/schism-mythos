class PoisonSpatterProjectile : SchismProjectile {
	Default {
		Radius 8;
		Height 8;
		Scale 1;
		Speed 10;
		FastSpeed 20;
		DamageFunction 1;
		Alpha 1;
		RenderStyle "Translucent";
		
		DamageType "Poison";
		
		SeeSound "Projectile/PoisonSpatter/Fire";
		DeathSound "Projectile/PoisonSpatter/Explode";

		SchismProjectile.ParticleName "Poison";
		SchismProjectile.ParticleCount 1;
		SchismProjectile.StatusEffectClass "SchismPoisonEffect";
		SchismProjectile.StatusEffectDuration 5 * 35;
	}
	
	States {
		Spawn:
			PSPA AB 4;
			Loop;
			
		Death:
			PSPA ABCDE 4;
			Stop;
	}
}