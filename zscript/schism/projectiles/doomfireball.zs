class DoomfireballProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 8;
		Scale 1;
		Speed 10;
		FastSpeed 20;
		Damage 3;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Nether";
		
		SeeSound "Projectile/Doomfireball/Fire";
		DeathSound "Projectile/Doomfireball/Explode";

		SchismProjectile.ParticleName "Nether";
		SchismProjectile.ParticleCount 1;
	}
	
	States {
		Spawn:
			NPDB AB 4 bright;
			Loop;
			
		Death:
			NPDB CDEFGHIJ 4 bright;
			Stop;
	}
}