class WrathOrbProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 8;
		Scale 1;
		Speed 10;
		FastSpeed 20;
		Damage 5;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Nether";
		
		SeeSound "Projectile/WrathOrb/Fire";
		DeathSound "Projectile/WrathOrb/Explode";

		SchismProjectile.ParticleName "Nether";
		SchismProjectile.ParticleCount 1;
		SchismProjectile.StatusEffectClass "SchismImmolateDebuff";
		SchismProjectile.StatusEffectDuration 1 * 35;

		+SEEKERMISSILE;
	}
	
	States {
		Spawn:
			NPWO ABABABABABABABABABABABABABAB 2 bright;
			NPWO A 0 bright A_SeekerMissile(90, 90);
			Loop;
			
		Death:
			NPWO HIJKLM 4 bright;
			Stop;
	}
}