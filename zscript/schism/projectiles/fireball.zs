class FireballProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 8;
		Scale 1;
		Speed 10;
		FastSpeed 20;
		Damage 3;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Fire";
		
		SeeSound "Projectile/Fireball/Fire";
		DeathSound "Projectile/Fireball/Explode";

		SchismProjectile.ParticleName "Flame";
		SchismProjectile.ParticleCount 1;
		SchismProjectile.StatusEffectClass "SchismImmolateDebuff";
		SchismProjectile.StatusEffectDuration 1 * 35;
	}
	
	States {
		Spawn:
			NPFB AB 4 bright;
			Loop;
			
		Death:
			NPFB CDEFGHIJ 4 bright;
			Stop;
	}
}