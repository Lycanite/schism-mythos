class GreedyBlastProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 8;
		Scale 0.5;
		Speed 10;
		FastSpeed 20;
		Damage 5;
		Alpha 1;
		RenderStyle "Add";
		
		DamageType "Nether";
		
		SeeSound "Projectile/GreedyBlast/Fire";
		DeathSound "Projectile/GreedyBlast/Explode";

		SchismProjectile.ParticleName "Nether";
		SchismProjectile.ParticleCount 2;
	}
	
	States {
		Spawn:
			NPGB ABCDEFGHIJKLMNOPQRSTUVWXY 1 bright;
			Loop;
			
		Death:
			NPGX ABCDEFGHIJK 1 bright A_FadeOut(0.05);
			Loop;
	}
}