class DuskflareProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 8;
		Scale 1;
		Speed 16;
		FastSpeed 24;
		Damage 2;
		Alpha 1;
		RenderStyle "Subtract";
		
		DamageType "Shadow";
		
		SeeSound "Projectile/Duskflare/Fire";
		DeathSound "Projectile/Duskflare/Explode";

		SchismProjectile.ParticleName "Shadow";
		SchismProjectile.ParticleCount 1;
		SchismProjectile.StatusEffectClass "SchismWeaknessDebuff";
		SchismProjectile.StatusEffectDuration 10 * TICRATE;

		+SEEKERMISSILE;
		-THRUGHOST;
	}
	
	States {
		Spawn:
			NPDF ABCD 1 bright A_Weave(3, 3, 1, 1);
			Loop;
			
		Death:
			NPDF EFGHIJ 4 bright;
			Stop;
	}
}