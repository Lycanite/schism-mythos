class HadesSphereProjectile : Actor {
	Mixin SpawnParticleMixin;

    int lifetime;

	Default {
        Tag "Hades Sphere";
        Health 5;
		Radius 24;
		Height 48;
		Scale 1;
		Speed 7;
		FastSpeed 12;
		Damage 8;
		Alpha 1;
		RenderStyle "Translucent";
		
		DamageType "Nether";
		
		SeeSound "Projectile/HadesSphere/Fire";
		DeathSound "Projectile/HadesSphere/Explode";
        Obituary "%o was blown away by a Hades Sphere.";

		Monster;
        +LOOKALLAROUND;
        +DONTGIB;
        +NOTARGET;
        +NOGRAVITY;
        +FLOAT;
        +DONTFALL;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		self.SpawnParticle("Nether", 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
	}

	action state A_HadesChase() {
        if (invoker.lifetime > 10 * TICRATE) {
            return ResolveState("Death");
        }
        if (invoker.bSkullFly) {
            A_Chase(null, null);
            return null;
        }
        A_Chase();
        return null;
    }
	
	States {
		Spawn:
            NPHS EEFFGGHH 2 bright A_Look();
            Loop;

        See:
            NPHS A 0 bright A_StartSound("Projectile/HadesSphere/Idle");
            NPHS AABBCCDD 2 bright A_HadesChase();
            Loop;

        Melee:
            TNT1 A 0 A_SetInvulnerable();
            NPHS HGFE 4 bright;
            TNT1 A 0 {
                A_UnSetInvulnerable();
                A_SetShootable();
                A_Die();
            }
            Stop;

        Death:
            TNT1 A 0 {
                A_NoBlocking();
                A_SetTranslucent(0.9, 1);
                A_Scream();
            }
            NPHS M 4 bright A_Explode(112, 112, XF_NOTMISSILE);
            NPHS NOPQ 5 bright;
            Stop;

        XDeath:
            TNT1 A 0 A_Noblocking();
            NPHS H 5 bright A_StartSound("Projectile/HadesSphere/Explode");
            NPHS GFE 4 bright;
            NPHS RSTUVWX 4 bright;
            Stop;

        Death.Ice:
            "####" "#" 0 { bNoGravity=false; }
            Goto GenericFreezeDeath;
	}
}