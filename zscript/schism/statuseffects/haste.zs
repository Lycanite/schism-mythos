class SchismHasteBuff : PowerDoubleFiringSpeed {
	double ammoCostMultiplier; // Multiplies the amount of ammo used whilst under this effect.

	property AmmoCostMultiplier: ammoCostMultiplier;

	Default {
		Inventory.Amount 1;
		Inventory.MaxAmount 1;
		Powerup.Duration -10;

		SchismHasteBuff.AmmoCostMultiplier 1.0;
		
		+INVENTORY.NOTELEPORTFREEZE;
	}
}