class SchismFearDebuff : SchismStatusEffect {
	Default {
		PowerUp.Duration -10;
		
		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.ParticleName "ShadowEmber";
	}
	
	override void InitEffect() {
		super.InitEffect();
		if (!self.owner || self.owner.bFrightened) {
			self.EffectInvalid = true;
			return;
		}
		
		self.owner.bFrightened = true;
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (self.EffectInvalid || !self.owner) {
			return;
		}
		self.owner.bFrightened = false;
	}
}