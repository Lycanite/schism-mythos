class SchismPoisonEffect : SchismStatusEffect {
	int gasLevel;
	SchismPoisonEffectGas gas;

	Default {
		PowerUp.Duration -5;

		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.EffectInterval 2 * TICRATE;
		SchismStatusEffect.EffectDamage 4;
		SchismStatusEffect.EffectDamageType "Poison";
		SchismStatusEffect.EffectPeriodicDamage true;
		SchismStatusEffect.ParticleName "Poison";
	}

	void Fever(Actor triggerActor, int damage = 1) {
		if (!self.owner) {
			return;
		}

		// Damage:
		Array<Actor> actorsHit;
		SchismAttacks.Explode(self.owner, triggerActor, damage, 100, 0, 1, false, false, "Poison", actorsHit);

		// Particles:
		for (int i = 0; i < actorsHit.Size(); i++) {
			Actor target = actorsHit[i];
			if (!target) continue;
			self.SpawnParticleFromActor(
				target, "PoisonBubble", 1 * TICRATE, Random(1,3),
				(FRandom(-8, 8), FRandom(-8, 8), (target.height / 2) + FRandom(-8, -2)),
				Random(0, 359), Random(260, 280), 1
			);
			self.SpawnParticleFromActor(
				target, "SporeBubble", 1 * TICRATE, Random(1,3),
				(FRandom(-8, 8), FRandom(-8, 8), (target.height / 2) + FRandom(-8, -2)),
				Random(0, 359), Random(260, 280), 1
			);
		}

		// SFX:
		if (self.owner) {
			if (self.effectLifetime % 4) {
				self.SpawnParticleFromActor(
					self.owner, "Spore", 1 * TICRATE, Random(1,3),
					(FRandom(-8, 8), FRandom(-8, 8), (self.owner.height / 2) + FRandom(-8, -2)),
					Random(0, 359), Random(200, 340), 1
				);
			}
			bool spawned;
			Actor actor;
			[spawned, actor] = self.owner.A_SpawnItemEx("SchismPoisonFever", 0, 0, self.owner.height / 2);
			if (actor) {
				actor.target = self.owner;
			}
		}
	}
	
	override void DoEffect() {
		super.DoEffect();

		// Poison Gas:
		if (self.owner && self.gasLevel >= 1 && !self.gas) {
			bool spawned;
			Actor actor;
			[spawned, actor] = self.owner.A_SpawnItemEx("SchismPoisonEffectGas", 0, 0, self.owner.height / 2);
			self.gas = SchismPoisonEffectGas(actor);
			gas.level = self.gasLevel;
			gas.master = self.effectInflictor;
		}
	}
}

class SchismPoisonFever : Actor {
    int lifetime;

	Default {
        Tag "Poison Fever";
		Scale 2;
        Health 5;
		Radius 24;
		Height 48;
		Speed 2;
		Gravity 0;
		Alpha 0.5;
		RenderStyle "Add";
		
		DamageType "Poison";

		SeeSound "Effect/Poison/Fever";

		+NOGRAVITY;
		+NOCLIP;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (self.target) {
			self.SetOrigin((self.target.pos.x, self.target.pos.y, self.target.pos.z + (self.target.height / 2)), false);
		}
	}
	
	States {
		Spawn:
		Death:
			NPTB H 2 NoDelay A_StartSound(self.seeSound);
            NPTB IJKLMNO 2;
            Stop;
	}
}

class SchismPoisonEffectGas : Actor {
	Mixin SpawnParticleMixin;

    int lifetime;
	int level;

	Default {
        Tag "Poison Gas";
		Scale 1.5;
        Health 5;
		Radius 24;
		Height 48;
		Speed 2;
		Gravity 0;
		Alpha 0.75;
		RenderStyle "Translucent";
		
		DamageType "Poison";

		+NOGRAVITY;
	}

	action state A_DriftingGas() {
        if (invoker.lifetime++ >= 5 * TICRATE) {
            return ResolveState("Death");
        }

		// Particles
		invoker.SpawnParticle("PoisonSmoke", 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 1);

		// Move:
		if (invoker.level >= 2) {
			if (invoker.lifetime & (TICRATE * 2) == 0) {
				invoker.angle = Random(0, 359);
			}
			invoker.Vel3DFromAngle(invoker.speed, invoker.angle, 0);
		}

		// Damage:
		if (invoker.lifetime % 8 == 0) {
			Array<Actor> actorsHit;
			Actor owner = self;
			if (self.master) {
				owner = self.master;
			}
			SchismAttacks.Explode(self, owner, 1, 100, 0, 1, false, false, "Poison", actorsHit, DMG_THRUSTLESS|DMG_NO_PAIN);
		}

        return null;
    }
	
	States {
		Spawn:
            NPTB IJKLKJ 2 bright {
				return A_DriftingGas();
			}
            Loop;

        Death:
            NPTB IJKLKJ 2 bright A_FadeOut(0.05);
            Loop;
	}
}