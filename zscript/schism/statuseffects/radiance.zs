class SchismRadianceBuff : SchismStatusEffect {
	Default {
		PowerUp.Duration -10;
		SchismStatusEffect.ParticleName "Light";
		SchismStatusEffect.ParticleInterval 8;
		SchismStatusEffect.ParticleOffsetRandom 50;
	}
	
	override void InitEffect() {
		super.InitEffect();
		if (!self.owner) {
			return;
		}
		self.owner.A_AttachLightDef("RadianceBuff", "RADIANCE_EFFECT");
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (!self.owner) {
			return;
		}
		self.owner.A_RemoveLight("RadianceBuff");
	}
	
	override void DoEffect() {
		super.DoEffect();
		if (!self.owner) {
			return;
		}

		// Melt Projectiles:
		int rate = 1 * TICRATE;
		if (self.strength > 2) {
			rate = 8;
		}
		if (self.effectTics % 8 == 0) {
			ThinkerIterator iterator = ThinkerIterator.Create("Actor", STAT_DEFAULT);
			Actor target;
			while (target = Actor(iterator.next())) {
				if (!target || self.owner.Distance3D(target) > self.owner.radius + target.radius + 100) {
					continue;
				}

				if (target.bMissile && (!target.target || SchismInteraction.CanHarmTarget(target.target, self.owner))) {
					target.Destroy();
					self.owner.A_StartSound("Buff/Radiance/Melt", CHAN_ITEM);
				}
			}
		}
	}
	
	override void ModifyDamage(int damage, Name damageType, out int newDamage, bool passive, Actor inflictor, Actor source, int flags) {
		super.ModifyDamage(damage, damageType, newDamage, passive, inflictor, source, flags);
		if (!passive || damage <= 0 || self.strength < 3 || self.damageType != "Hitscan") { // Requires strength 3+ hitscan only.
			return; // Active mode is for damage being dealt, passive is for damage being received.
		}
		newDamage = Max(1, ApplyDamageFactors(self.GetClass(), damageType, damage, int(double(damage) / Max(1, self.strength - 1))));
	}
}

class SchismRadianceBuffPower : PowerLightAmp {
	Default {
		PowerUp.Duration 3;
		Inventory.MaxAmount 1;
		-Inventory.HUBPOWER;
		+Inventory.ALWAYSPICKUP;
		-Inventory.ADDITIVETIME;
		+INVENTORY.NOSCREENBLINK;
	}
}