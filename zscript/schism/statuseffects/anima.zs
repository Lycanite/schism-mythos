class SchismAnimaBuff : SchismStatusEffect {
	Default {
		PowerUp.Duration -10;
		SchismStatusEffect.ParticleName "Anima";
		SchismStatusEffect.ParticleColor 0xF7000000;
	}
	
	override void DoEffect() {
		super.DoEffect();
		if (self.followEffect) {
			self.particleColor = self.effectTics > TICRATE * 3 ? 0xF7000000 : 0x964B00;
		}
		if (self.owner) {
			self.owner.A_DamageSelf(-1, "None", DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
		}
	}
}
