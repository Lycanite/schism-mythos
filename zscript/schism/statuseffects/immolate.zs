class SchismImmolateDebuff : SchismStatusEffect {
	Default {
		PowerUp.Duration -5;
		
		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.EffectInterval 10;
		SchismStatusEffect.EffectDamage 1;
		SchismStatusEffect.EffectRadius 100;
		SchismStatusEffect.ParticleName "Fire";
		SchismStatusEffect.EffectPeriodicDamage true;
		SchismStatusEffect.ParticleScale 0.5;
	}
	
	override string GetFollowEffectClass() {
		return "SchismImmolateFollowEffect";
	}
	
	override void OnOwnerDamaged(int damage, string damageType, actor inflictor) {
		super.OnOwnerDamaged(damage, damageType, inflictor);
		if (damageType == "Chaos" && self.effectInflictor != self.owner) {
			SchismStatusEffects.InflictEffect("SchismWeaknessDebuff", self.EffectInflictor, self.EffectInflictor, self.owner, 10 * 35);
		}
	}
	
	bool ImmolateFirestorm(Actor triggerActor) {
		SchismImmolateFollowEffect immolateProjectile = SchismImmolateFollowEffect(self.FollowEffect);
		if (immolateProjectile && self.owner && self.owner.Health > 0) {
			return immolateProjectile.A_ImmolateFirestorm(triggerActor);
		}
		return false;
	}
	
	bool ImmolateCombustion(Actor triggerActor) {
		SchismImmolateFollowEffect immolateProjectile = SchismImmolateFollowEffect(self.FollowEffect);
		bool combusted = false;
		if (immolateProjectile && self.owner && self.owner.Health > 0) {
			combusted = immolateProjectile.A_ImmolateCombustion(triggerActor);
			self.RemoveEffect();
		}
		return combusted;
	}
}

class SchismImmolateFollowEffect : SchismFollowEffect {
	int InfernalEnergyInterval;
	
	property InfernalEnergyInterval: InfernalEnergyInterval;
	
	Default {
		Scale 0.75;
		Alpha 0.5;
		RenderStyle "Add";
		
		SchismImmolateFollowEffect.InfernalEnergyInterval 1;
		
		SeeSound "Debuff/Immolate";
		DeathSound "Debuff/Immolate";
		Obituary "%o was burnt to a well seasoned crisp by %k's Immolation!";
	}
	
	override void OnEffectDamageExplode(int targets) {
		if (self.lifetime % self.infernalEnergyInterval == 0 && targets > 0 && self.statusEffect && self.statusEffect.effectInflictor) {
			self.statusEffect.effectInflictor.A_GiveInventory("InfernalCharge", targets);
			// Strong Target Bonus Infernal Charges:
			if (self.statusEffect.owner && self.statusEffect.owner.health > 200) {
				self.statusEffect.effectInflictor.A_GiveInventory("InfernalCharge", Round(float(self.statusEffect.owner.health) / 100));
			}
		}
	}
	
	action bool A_ImmolateFirestorm(Actor triggerActor) {
		if (!invoker.statusEffect || !invoker.statusEffect.effectInflictor || !invoker.GetFollowTarget()) {
			return false;
		}
		Actor effectInflictor = invoker.statusEffect.effectInflictor;
		Actor effectTarget = invoker.GetFollowTarget();
		effectTarget.A_StartSound("FirestoneCane/Fire/Storm");
		int targets = SchismAttacks.Explode(invoker, effectInflictor, 4, 300);
		if (targets > 0) {
			effectInflictor.A_GiveInventory("InfernalCharge", targets);
		}
		for (int i = 0; i <= 60; i++) {
			invoker.SpawnParticleFromActor(
				effectTarget, "Fire", 2 * TICRATE, 4,
				(FRandom(-8, 8), FRandom(-8, 8), (effectTarget.height / 2) + FRandom(-8, -2)),
				Random(0, 359), Random(0, 359), 1
			);
			invoker.SpawnParticleFromActor(
				effectTarget, "Smoke", 2 * TICRATE, 8,
				(FRandom(-8, 8), FRandom(-8, 8), (effectTarget.height / 2) + FRandom(-8, -2)),
				Random(0, 359), Random(0, 359), 1
			);
		}
		return true;
	}
	
	action bool A_ImmolateCombustion(Actor triggerActor) {
		if (!invoker.statusEffect || !invoker.statusEffect.effectInflictor || !invoker.GetFollowTarget()) {
			return false;
		}
		Actor effectInflictor = invoker.statusEffect.effectInflictor;
		Actor effectTarget = invoker.GetFollowTarget();
		effectTarget.A_StartSound("Debuff/Immolate/Combustion");
		effectTarget.A_QuakeEx(2, 2, 2, 10, 0, 500, "world/quake", 0, 1, 1, 0, 0, 1, 1);
		SchismAttacks.Explode(invoker, effectInflictor, 40, 300);
		for (int i = 0; i <= 100; i++) {
			invoker.SpawnParticleFromActor(
				effectTarget, "Fire", 2 * TICRATE, 4,
				(FRandom(-8, 8), FRandom(-8, 8), (effectTarget.height / 2) + FRandom(-8, -2)),
				Random(0, 359), Random(0, 359), 1.5
			);
			invoker.SpawnParticleFromActor(
				effectTarget, "FireBlast", TICRATE, 8,
				(FRandom(-8, 8), FRandom(-8, 8), (effectTarget.height / 2) + FRandom(-8, -2)),
				Random(0, 359), Random(0, 359), 1.5
			);
		}
		return true;
	}
}
