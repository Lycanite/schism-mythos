class SchismPlagueDebuff : SchismStatusEffect {
	Default {
		PowerUp.Duration -5;

		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.EffectInterval 2 * TICRATE;
		SchismStatusEffect.EffectDamage 8;
		SchismStatusEffect.EffectDamageType "Poison";
		SchismStatusEffect.EffectPeriodicDamage true;
		SchismStatusEffect.ParticleName "Poison";
	}
}