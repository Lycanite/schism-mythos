class SchismFloatEffect : SchismStatusEffect {
	float speedOriginal;

	Default {
		PowerUp.Duration -10;
		SchismStatusEffect.EffectSpeedFactor 0.01;
		SchismStatusEffect.ParticleName "Gravity";
	}
	
	override void InitEffect() {
		super.InitEffect();
		self.speedOriginal = self.owner.speed;
		self.owner.speed = self.owner.speed * self.GetSpeedFactor();
	}
	
	override void DoEffect() {
		super.DoEffect();
		if (!self.owner || self.owner.bFlyCheat || self.owner.bFloat || self.owner.bBoss) {
			return;
		}
		
		self.owner.bNoGravity = true;
		float height = self.owner.pos.z - self.owner.floorz;

		// Float or Drop:
		if (height <= 50 + (50 * self.strength)) {
			self.owner.vel.z = 4;
		} else {
			self.owner.vel.z = -0.1;
		}
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (!self.owner) {
			return;
		}
		self.owner.speed = self.speedOriginal;

		if (self.owner.bFlyCheat || self.owner.bFloat) {
			return;
		}
		self.owner.bNoGravity = false;
	}
}