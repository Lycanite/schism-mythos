class SchismStunDebuff : SchismStatusEffect {
	int lastTic;
	
	Default {
		PowerUp.Duration -10;
		
		SchismStatusEffect.Detrimental true;
		SchismStatusEffect.ParticleName "SporeBubble";
	}
	
	override double GetSpeedFactor() {
		return 0;
	}
	
	override void InitEffect() {
		super.InitEffect();
		if (!self.owner) {
			return;
		}
		
		self.lastTic = self.owner.tics;
	}
	
	override void DoEffect() {
		super.DoEffect();
		
		if (!self.owner || self.owner.Health <= 0 || self.owner.bCorpse || self.owner.bBoss) {
			return;
		}
		
		if (self.effectLifetime % TICRATE) {
			self.owner.target = null;
			self.owner.tics = self.lastTic;
		}
	}
}
