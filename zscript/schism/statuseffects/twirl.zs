class SchismTwirlEffect : SchismStatusEffect {
	float speedOriginal;

	Default {
		PowerUp.Duration -10;
		SchismStatusEffect.EffectSpeedFactor 0.01;
		SchismStatusEffect.ParticleName "Air";
	}
	
	override void InitEffect() {
		super.InitEffect();
		self.speedOriginal = self.owner.speed;
		self.owner.speed = self.owner.speed * self.GetSpeedFactor();
	}
	
	override void DoEffect() {
		super.DoEffect();

		// Random Velocity:
		self.owner.vel.x += Random(-self.strength, self.strength);
		self.owner.vel.y += Random(-self.strength, self.strength);

		// Spin with Target Loss:
		if (!self.owner.bBoss) {
			self.owner.angle += 20 * self.strength;
			if (self.effectTics % TICRATE == 0) {
				self.owner.target = null;
			}
		}
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (!self.owner) {
			return;
		}
		self.owner.speed = self.speedOriginal;
	}
}