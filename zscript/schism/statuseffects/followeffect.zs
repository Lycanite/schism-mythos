class SchismFollowEffect : FollowProjectile {
	SchismStatusEffect statusEffect;
	
	Default {
		Radius 16;
		Height 10;
		Scale 0.75;
		Speed 0;
		Gravity 0;
		DamageFunction 0;
		Alpha 0.25;
		RenderStyle "Normal";
		
		+NODAMAGETHRUST;
		+PAINLESS;
		+FORCERADIUSDMG;
		+NOCLIP;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		self.UpdateStatusEffect(self.statusEffect);
	}

	/** 
	 * Updates this follow effect to its status effect, also sets/clears the status effect.
	 * @param statusEffect The status effect that this follow effect is for.
	 */
	virtual void UpdateStatusEffect(SchismStatusEffect statusEffect) {
		self.statusEffect = statusEffect;
		if (!self.statusEffect) {
			return;
		}

		self.target = self.statusEffect.effectInflictor;
		self.tracer = self.statusEffect.owner;

		vector3 offset = self.statusEffect.GetEffectOffset();
		self.SetFollowTarget(self.statusEffect.owner, offset.x, offset.y, offset.z);
		
		self.particleName = self.statusEffect.particleName;
		self.particleScale = self.statusEffect.particleScale;
		self.particleSpeed = self.statusEffect.particleSpeed;
		self.particleColor = self.statusEffect.particleColor;
		self.particleCount = self.statusEffect.particleCount;
		self.particleInterval = self.statusEffect.particleInterval;
		self.particleDuration = self.statusEffect.particleDuration;
		self.particleOffsetZ = self.statusEffect.particleOffsetZ;
		self.particleOffsetRandom = self.statusEffect.particleOffsetRandom;
	}
	
	/** 
	 * Checks if the provided actor was one the one that inflcited this effect.
	 * @param testActor The actor to check.
	 */
	virtual bool InflictedBy(Actor testActor) {
		if (!self.statusEffect) {
			return false;
		}
		return self.statusEffect.InflictedBy(testActor);
	}
	
	/**
	 * Called when this effect deals damage directly to a specific actor (usually the afflicted).
	 * @param target The damage target.
	 */
	virtual void OnEffectDamage(Actor target) {}
	
	/**
	 * Called when this effect deals area damage to one or more targets (usually when the status effect radius is more than 0).
	 * @param targets The number of targets damaged.
	 */
	virtual void OnEffectDamageExplode(int targets) {}
	
	/**
	 * Updates this follow effect projectile.
	 */
	action state A_EffectUpdate() {
		// Expire:
		if (!invoker.statusEffect || invoker.statusEffect.effectTics <= 0 || !invoker.GetFollowTarget() || invoker.GetFollowTarget().health <= 0) {
			return invoker.ResolveState("Death");
		}

		// Match Angle:
		invoker.angle = invoker.GetFollowTarget().angle + invoker.statusEffect.GetEffectAngle();

		return null;
	}
	
	/**
	 * Deals damage based on this follow effect's status effect.
	 */
	action void A_DoEffectDamage() {
		if (!invoker.statusEffect || !invoker.statusEffect.effectPeriodicDamage || invoker.statusEffect.effectDamage == 0 || invoker.statusEffect.effectTics % invoker.statusEffect.effectInterval != 0) {
			return;
		}
		if (invoker.statusEffect.effectRadius != 0) {
			int targets = SchismAttacks.Explode(invoker, invoker.statusEffect.effectInflictor, invoker.statusEffect.effectDamage, invoker.statusEffect.effectRadius);
			invoker.OnEffectDamageExplode(targets);
		}
		else if (invoker.GetFollowTarget()) {
			invoker.GetFollowTarget().DamageMobj(invoker.statusEffect.effectInflictor, invoker, invoker.statusEffect.effectDamage, invoker.statusEffect.effectDamageType, DMG_THRUSTLESS);
			invoker.OnEffectDamage(invoker.GetFollowTarget());
		}
	}
	
	States {
		Spawn:
		Active:
			"####" AAA 1 {
				return A_EffectUpdate();
			}
			"####" A 1 {
				A_DoEffectDamage();
				return A_EffectUpdate();
			}
			Loop;
			
		Death:
			"####" A 1;
			Stop;
	}
}
