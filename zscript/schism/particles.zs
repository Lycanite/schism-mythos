struct SchismParticles {
    /**
     * Spawns a particle.
     * @param name The name of the particle to spawn.
     * @param lifetime The lifetime of the particle in tics.
     * @param pos The absolute positon to spawn at.
     * @param vel The absolute velocity to set.
     * @param scale The relative scale of the particle, defaults to 1.0.
     * @param color The hex color overlay of the particle, defaults to the unset color stencil value for image colors instead.
    */
	static void spawn(string name, int lifetime, vector3 pos, vector3 vel, float scale = 1.0, int color = 0xF7000000) {
        // Particle Params:
		FSpawnParticleParams particleParams;
		particleParams.lifetime = lifetime;
		particleParams.pos = pos;
		particleParams.vel = vel;
        particleParams.color1 = 0xFFFFFF;

        // Fire:
        if (name == "Fire") {
            SchismFireParticle.apply(particleParams);
        } else if (name == "FireEmber") {
            SchismFireEmberParticle.apply(particleParams);
        } else if (name == "FireBlast") {
            SchismFireBlastParticle.apply(particleParams);
        } else if (name == "Flame") {
            SchismFlameParticle.apply(particleParams);
        } else if (name == "Smoke") {
            SchismSmokeParticle.apply(particleParams);
        } else if (name == "Fume") {
            SchismFumeParticle.apply(particleParams);
        }

        // Water:
        else if (name == "Water") {
            SchismWaterParticle.apply(particleParams);
        } else if (name == "Rain") {
            SchismRainParticle.apply(particleParams);
        } else if (name == "RainSplash") {
            SchismRainSplashParticle.apply(particleParams);
        }

        // Earth:
        else if (name == "Earth") {
            SchismEarthParticle.apply(particleParams);
        }

        // Air:
        else if (name == "Air") {
            SchismAirParticle.apply(particleParams);
        }

        // Order:
        else if (name == "Order") {
            SchismOrderParticle.apply(particleParams);
        }
        
        // Chaos:
        else if (name == "Chaos") {
            SchismChaosParticle.apply(particleParams);
        } else if (name == "ChaosEmber") {
            SchismChaosEmberParticle.apply(particleParams);
        } else if (name == "ChaosSpark") {
            SchismChaosSparkParticle.apply(particleParams);
        } else if (name == "ChaosFlare") {
            SchismChaosFlareParticle.apply(particleParams);
        } else if (name == "Maelfire") {
            SchismMaelfireParticle.apply(particleParams);
        } else if (name == "BedlamSmoke") {
            SchismBedlamSmokeParticle.apply(particleParams);
        }

        // Acid:
        else if (name == "Acid") {
            SchismAcidParticle.apply(particleParams);
        }

        // Lava:
        else if (name == "Lava") {
            SchismLavaParticle.apply(particleParams);
        }

        // Lightning:
        else if (name == "Lightning") {
            SchismLightningParticle.apply(particleParams);
        }

        // Light:
        else if (name == "Light") {
            SchismLightParticle.apply(particleParams);
        }

        // Shadow:
        else if (name == "Shadow") {
            SchismShadowParticle.apply(particleParams);
        } else if (name == "ShadowEmber") {
            SchismShadowEmberParticle.apply(particleParams);
        }

        // Arbour:
        else if (name == "Arbour") {
            SchismArbourParticle.apply(particleParams);
        } else if (name == "Leaf") {
            SchismLeafParticle.apply(particleParams);
        }

        // Frost:
        else if (name == "Frost") {
            SchismFrostParticle.apply(particleParams);
        } else if (name == "Snowflake") {
            SchismSnowflakeParticle.apply(particleParams);
        }

        // Fae:
        else if (name == "Fae") {
            SchismFaeParticle.apply(particleParams);
        } else if (name == "Anima") {
            SchismAnimaParticle.apply(particleParams);
        } else if (name == "BloodDrip") {
            SchismBloodDripParticle.apply(particleParams);
        }

        // Poison:
        else if (name == "Poison") {
            SchismPoisonParticle.apply(particleParams);
        } else if (name == "PoisonBubble") {
            SchismPoisonBubbleParticle.apply(particleParams);
        } else if (name == "PoisonSmoke") {
            SchismPoisonSmokeParticle.apply(particleParams);
        } else if (name == "Spore") {
            SchismSporeParticle.apply(particleParams);
        } else if (name == "SporeBubble") {
            SchismSporeBubbleParticle.apply(particleParams);
        }

        // Phase:
        else if (name == "Ghost") {
            SchismGhostParticle.apply(particleParams);
        }

        // Arcane:
        else if (name == "Arcane") {
            SchismArcaneParticle.apply(particleParams);
        }

        // Quake:
        else if (name == "Quake") {
            SchismQuakeParticle.apply(particleParams);
        }

        // Aether:
        else if (name == "Aether") {
            SchismAetherParticle.apply(particleParams);
        }

        // Nether:
        else if (name == "Nether") {
            SchismNetherParticle.apply(particleParams);
        } else if (name == "Hellflame") {
            SchismHellflameParticle.apply(particleParams);
        }

        // Void:
        else if (name == "Void") {
            SchismVoidParticle.apply(particleParams);
        } else if (name == "Nix") {
            SchismNixParticle.apply(particleParams);
        }

        // Nova:
        else if (name == "Nova") {
            SchismNovaParticle.apply(particleParams);
        }

        // Gravity:
        else if (name == "Gravity") {
            SchismGravityParticle.apply(particleParams);
        }

        // Chrono:
        else if (name == "Chrono") {
            SchismChronoParticle.apply(particleParams);
        }

        // Post Particle Params:
		particleParams.size *= scale;
        if (color != 0xF7000000) { // Default no stencil for actors.
            particleParams.style = particleParams.style == STYLE_Add ? STYLE_AddShaded : STYLE_Shaded;
            particleParams.color1 = color;
        }

        // Temporary Roll 180 Fix:
        particleParams.startRoll += 180;

        // Spawn Particle:
		Level.SpawnParticle(particleParams);
	}
}

#include "zscript/schism/particles/fire.zs"
#include "zscript/schism/particles/water.zs"
#include "zscript/schism/particles/earth.zs"
#include "zscript/schism/particles/air.zs"
#include "zscript/schism/particles/order.zs"
#include "zscript/schism/particles/chaos.zs"
#include "zscript/schism/particles/acid.zs"
#include "zscript/schism/particles/lava.zs"
#include "zscript/schism/particles/lightning.zs"
#include "zscript/schism/particles/light.zs"
#include "zscript/schism/particles/shadow.zs"
#include "zscript/schism/particles/arbour.zs"
#include "zscript/schism/particles/frost.zs"
#include "zscript/schism/particles/fae.zs"
#include "zscript/schism/particles/poison.zs"
#include "zscript/schism/particles/phase.zs"
#include "zscript/schism/particles/arcane.zs"
#include "zscript/schism/particles/quake.zs"
#include "zscript/schism/particles/aether.zs"
#include "zscript/schism/particles/nether.zs"
#include "zscript/schism/particles/void.zs"
#include "zscript/schism/particles/nova.zs"
#include "zscript/schism/particles/gravity.zs"
#include "zscript/schism/particles/chrono.zs"