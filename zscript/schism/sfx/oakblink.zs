class OakBlink : Actor {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 12;
		DamageFunction 0;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		
		+RANDOMIZE;
		+CANNOTPUSH;
		+NOTELEPORT;
		+NOBLOCKMAP;
		
		SeeSound "";
		DeathSound "";
	}
	
	States {
		Spawn:
			OBNK A 4 bright nodelay A_StartSound("UndulateOak/Teleport");
		Death:
			OBNK BCDEFGHIJK 4 bright;
			Stop;
	}
}