class Barbtrail : Actor {   
	Default {
		Radius 0;
		Height 1;
		PROJECTILE;
		RENDERSTYLE "Add";
		ALPHA 0.75;
	}
	
	States {
		Spawn:
			NULL A 1 Bright;
			SSFX ABCDEFG 2 Bright;
			Stop;
	}
}