class SchismGoreBase : Actor {
	Default {
		Radius 8;
		Height 5;
		Scale 1;
		Speed 12;
		DamageFunction 0;
		Gravity 1;
		Alpha 1;
		Renderstyle "Normal";
		
		+RANDOMIZE;
		+CANNOTPUSH;
		
		SeeSound "SchismEffect/Gore";
		DeathSound "SchismEffect/Gore";
	}
	
	States {
		Spawn:
			FXGR ABCDABCDABCD 4;
			Goto Death;
			
		Death:
			FXGR EF 4;
			Stop;
	}
}

class SchismGoreHeart : SchismGoreBase {
	States {
		Spawn:
			FXGR GHIJGHIJGHIJ 4;
			Goto Death;
	}
}

class SchismGoreBone : SchismGoreBase {
	Default {
		Scale 1.5;
	}
	
	States {
		Spawn:
			BONE ABCDEFGH 4;
			Goto Death;
	}
}

class SchismGoreBloodSpash : Actor {
	Default {
		Radius 16;
		Height 10;
		Scale 2;
		
		+FLOORHUGGER
	}
	
	States {
		Spawn:
			FXGR K 4;
			Goto Death;
			
		Death:
			FXGR LMNOPQ 4;
			Stop;
	}
}