struct SchismChronoParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/chrono/Chrono01.png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL|SPF_FULLBRIGHT;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-5, 5);
		particleParams.rollAcc = 0;

		particleParams.size = 8.0;
		particleParams.sizeStep = 0.2;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}