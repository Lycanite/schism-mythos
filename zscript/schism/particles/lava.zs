struct SchismLavaParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/lava/Lava01.png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL|SPF_FULLBRIGHT;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-5, 5);
		particleParams.rollAcc = 0;

		particleParams.size = 16.0;
		particleParams.sizeStep = 1;
		particleParams.startAlpha = 0.9;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}