struct SchismAcidParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/acid/Acid01.png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL|SPF_FULLBRIGHT;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-10, 10);
		particleParams.rollAcc = 0;

		particleParams.size = 8.0;
		particleParams.sizeStep = 1;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}