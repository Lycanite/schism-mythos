struct SchismPoisonParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		int frame = Random(1, 3);
		particleParams.texture = TexMan.CheckForTexture("particles/poison/Poison0" .. frame .. ".png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-10, 10);
		particleParams.rollAcc = 0;

		particleParams.size = 32.0;
		particleParams.sizeStep = 1;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismPoisonBubbleParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/poison/PoisonBubble01.png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-5, 5);
		particleParams.rollAcc = 0;

		particleParams.size = 8.0;
		particleParams.sizeStep = 0.25;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismPoisonSmokeParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		int frame = Random(1, 3);
		particleParams.texture = TexMan.CheckForTexture("particles/poison/Poison0" .. frame .. ".png");
		particleParams.style = STYLE_Translucent;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-10, 10);
		particleParams.rollAcc = 0;

		particleParams.size = 32.0;
		particleParams.sizeStep = 1;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismSporeParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		int frame = Random(1, 3);
		particleParams.texture = TexMan.CheckForTexture("particles/poison/Spore0" .. frame .. ".png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-10, 10);
		particleParams.rollAcc = 0;

		particleParams.size = 32.0;
		particleParams.sizeStep = 1;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}

struct SchismSporeBubbleParticle {
	static void apply(out FSpawnParticleParams particleParams) {
		particleParams.texture = TexMan.CheckForTexture("particles/poison/SporeBubble01.png");
		particleParams.style = STYLE_Add;
		particleParams.flags = SPF_ROLL;

		particleParams.accel = (0, 0, 0);
		particleParams.startRoll = Random(0, 359);
		particleParams.rollVel = Random(-5, 5);
		particleParams.rollAcc = 0;

		particleParams.size = 8.0;
		particleParams.sizeStep = 0.25;
		particleParams.startAlpha = 1.0;
		particleParams.fadeStep = particleParams.startAlpha / particleParams.lifetime;
	}
}