class Creature : Actor {
	mixin SpawnParticleMixin;

	int lifetime;
	bool statsScaled;
	string leapSound;

	property LeapSound: leapSound;

	uint creatureFlags;
	flagdef Walker: creatureFlags, 0; // When disabled, the creature will be slowed down to 10% when on land.
	flagdef Swimmer: creatureFlags, 1; // When enabled, the creature will gain float and no gravity when in water and will lose it when out.
	flagdef HoverBob: creatureFlags, 2; // When enabled, the creature will bob vertically via sprite offsets.

	bool walkerSlowdown;
	int originalSpeed;
	bool swimmerFlight;
	int originalGravity;
	
	Default {
		Mass 100;
		Creature.LeapSound "";

		MONSTER;
		+FLOORCLIP;

		+Creature.WALKER;
		-Creature.SWIMMER;
	}

	override void BeginPlay() {
		super.BeginPlay();

		// Boss Scaling:
		if (self.bBoss && !self.statsScaled) {
			int playerCount = players.Size();
			if (playerCount > 1) {
				self.health *= playerCount;
			}
		}
		self.statsScaled = true;

		// Fallback Sounds:
		if (!self.leapSound) {
			self.leapSound = self.meleeSound;
		}
	}
	
	override void Tick() {
		super.Tick();
		self.lifetime++;

		// Frozen:
		if (level.isFrozen() || self.isFrozen() || self.health <= 0) {
			return;
		}

        // Hovering:
        if (self.bHoverBob) {
			float hoverBob = 10 + (Sin(self.lifetime * 4) * 5);
            self.A_SpriteOffset(0, -hoverBob);
        } else {
            self.A_SpriteOffset(0, 0);
        }

		// Dead:
		if (self.health <= 0) {
			return;
		}

		// Walkers:
		if (!self.bWalker && self.waterLevel < 2) {
			if (!self.walkerSlowdown) {
				self.originalSpeed = self.speed;
				self.speed *= 0.1;
				self.walkerSlowdown = true;
			}
		} else if (self.walkerSlowdown) {
			self.speed = self.originalSpeed;
			self.walkerSlowdown = false;
		}

		// Swimmers:
		if (self.bSwimmer) {
			if (self.waterLevel >= 1) {
				if (!self.swimmerFlight) {
					self.originalGravity = self.gravity;
					self.gravity = 0;
					self.bNoGravity = true;
					self.bFloat = true;
					self.swimmerFlight = true;
				}
			} else if (self.swimmerFlight) {
				self.gravity = self.originalGravity;
				self.bNoGravity = false;
				self.bFloat = false;
				self.swimmerFlight = false;
			}
		}
	}
	
	override int DamageMobj(Actor inflictor, Actor source, int damage, Name mod, int flags, double angle) {
		// Better Friendly Fire Checks:
		if (source && !SchismInteraction.CanHarmTarget(source, self)) {
			return 0;
		}
		return super.DamageMobj(inflictor, source, damage, mod, flags, angle);
	}
	
	/** 
	 * Checks if the provided effect can be applied to this creature.
	 * @param effect The effect class to check.
	 * @return True if the effect can be applied.
	 */
	virtual bool CanApplyEffect(class<Inventory> effectClass) {
		return true;
	}

	/**
	 * Called when this creature dies, can be overridden to add death logic.
	 */
	virtual void OnCreatureDie() {}

	// Improved death action that forces gravity, disables float bob, etc.
	action void A_CreatureDie() {
		invoker.A_NoBlocking(true);
		invoker.bFloatBob = false;
		invoker.bHoverBob = false;
		invoker.bNoGravity = false;
		invoker.gravity = 1;
		invoker.OnCreatureDie();
	}

	/** 
	 * Attempts a melee attack if this creature's target is within range and plays the melee sound.
	 * @param damageMin The minimum amount of damage to deal.
	 * @param damageMax The maximum amount of damage to deal, defaults to 0 where only min is used for exact damage.
	 * @param damageType The damage type to deal, defaults to Melee.
	 * @param statusEffect An optional status effect to inflict.
	 * @param effectDuration The duration of the status effect in tics, defaults to 5 seconds.
	 * @param effectSpread An optional status effect to spread from those that are afflicted, for plague like effects.
	 * @param strength The strength of the effect, varies depending on the type of effect, ignored by most effects. Defaults to 1.
	 */
	action void A_CreatureMelee(int damageMin, int damageMax = 0, string particleName = "", string damageType = "Melee", Class<Inventory> statusEffect = "", int effectDuration = 5 * TICRATE, Class<SchismStatusEffect> effectSpread = "", int effectStrength = 1) {
		int damage = damageMax > damageMin ? random(damageMin, damageMax) : damageMin;
		invoker.A_CustomMeleeAttack(damage, invoker.meleeSound, "", damageType);

		// Particles:
		if (particleName) {
			int particleRange = invoker.radius / 4;
			int particleVert = invoker.height / 4;
			for (int i = 0; i < damage * 2; i++) {
				invoker.SpawnParticle(particleName, TICRATE, 3, (
					FRandom(-particleRange, particleRange),
					-10,
					(invoker.height / 2) + FRandom(-particleVert, particleVert)
				), Random(-45, 45), Random(-10, 10), 0.5);
			}
		}

		// Status Effect:
		if (statusEffect) {
			SchismStatusEffects.InflictEffect(statusEffect, invoker, invoker, invoker.target, effectDuration, effectSpread, effectStrength);
		}
	}

	/** 
	 * Attempts a ranged or melee attack based on range.
	 * @param projectileClass The projectile to spawn.
	 * @param damageMin The minimum amount of melee damage to deal.
	 * @param damageMax The maximum amount of melee damage to deal, defaults to 0 where only min is used for exact damage.
	 * @param damageType The melee damage type to deal, defaults to Melee.
	 * @param statusEffect An optional status effect to inflict with the melee attack.
	 * @param effectDuration The duration of the status effect in tics, defaults to 5 seconds.
	 * @param effectSpread An optional status effect to spread from those that are afflicted, for plague like effects.
	 * @param strength The strength of the effect, varies depending on the type of effect, ignored by most effects. Defaults to 1.
	 */
	action void A_CreatureComboAttack(Class<Actor> projectileClass, int damageMin, int damageMax = 0, string particleName = "", string damageType = "Melee", Class<Inventory> statusEffect = "", int effectDuration = 5 * TICRATE, Class<SchismStatusEffect> effectSpread = "", int effectStrength = 1) {
		if (invoker.CheckMeleeRange()) {
			A_CreatureMelee(damageMin, damageMax, particleName, damageType, statusEffect, effectDuration, effectSpread, effectStrength);
			return;
		}
		A_SpawnProjectile("HellfireballProjectile", invoker.height / 2);
	}

	/**
	 * Performs a hitscan attack.
	 * @param damageMin The minimum amount of melee damage to deal.
	 * @param damageMax The maximum amount of melee damage to deal, defaults to 0 where only min is used for exact damage.
	 * @param bulletCount The number of bullets to fire.
	 * @param spreadLat The lateral bullet spread, defaults to 22.5 which mimics the doom shotgun guy spread.
	 * @param spreadVert The vertical bullet spread, defaults to 0.
	 * @param puff The bullet puff actor to spawn at the hit location, can be used to determine the damage type, defaults to BulletPuff.
	 */
    action void A_CreatureHitscan(int damageMin, int damageMax, int bulletCount, float spreadLat = 22.5, float spreadVert = 0, Class<Actor> puff = "BulletPuff") {
        A_StartSound(invoker.attackSound);
		int damage = damageMax > damageMin ? Random(damageMin, damageMax) : damageMin;
        A_CustomBulletAttack(spreadLat, spreadVert, bulletCount, damage, puff, 0, CBAF_NORANDOM);
    }

	/** 
	 * Leaps towards this creature's current target or upwards if no target and plays the creatures leap sound (which defaults ot melee sound).
	 * @param power How powerful the leap is, 20 is a short leap, 40 quite far.
	 * @param height The height of the leap, defaults to half of the power if 0, use a small value like 0.1 for no height.
	 * @param silent If true, no leap sound is played, defaults to false.
	 */
	action void A_Leap(float power, float height = 0, bool silent = false) {
		if (!silent) {
			A_StartSound(invoker.leapSound);
		}
		if (invoker.target) {
			A_FaceTarget();
			invoker.Thrust(power, invoker.AngleTo(invoker.target));
		}
		invoker.AddZ(height ? height : power * 0.5, false);
	}

	/** 
	 * Attempts to melee this creature's target and stops all movement, can be used after A_Leap to deal damage and finish the leap.
	 * @param damageMin The minimum amount of damage to deal.
	 * @param damageMax The maximum amount of damage to deal, defaults to 0 where only min is used for exact damage.
	 * @param damageType The damage type to deal, defaults to Melee.
	 */
	action void A_LeapAttack(int damageMin, int damageMax = 0, string damageType = "Melee") {
		A_CreatureMelee(damageMin, damageMax, damageType);
		A_Stop();
	}

	/**
	 * Spawns a minion creature, sets this creature as its master, copies this creature's target and will also pass on final master if this creature is also a minion.
	 * @param offset The offset relative to this creature's angle (x lateral, y vertical, z longitudal). 
	 * @param velocity The velocity relative to this creature's angle (x lateral, y vertical, z longitudal). 
	 */
	action void A_SpawnMinion(class<Actor> actorClass, vector3 offset, vector3 velocity) {
		bool spawned = false;
		Actor spawnedActor = null;
		[spawned, spawnedActor] = A_SpawnItemEx(actorClass, offset.z, offset.x, offset.y, velocity.z, velocity.x, velocity.y);
		if (!spawnedActor) {
			return;
		}
		spawnedActor.master = invoker;
		if (invoker.master) {
			spawnedActor.master = invoker.master;
		}
		spawnedActor.target = invoker.target;

		MinionCreature spawnedMinion = MinionCreature(spawnedActor);
		if (spawnedMinion) {
			spawnedMinion.GetFinalMaster();
		}
	}
	
	// States:
	States {
		See:
			TNT1 A 1;
			Loop;

		Pain.SelfDamage:
			Goto See;
	}
}
