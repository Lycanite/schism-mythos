class Frostweaver : Creature {
	Default {
		Tag "Frostweaver";
		
		Health 300;
		Radius 20;
		Height 52;
		Speed 14;
		FloatSpeed 14;
		Scale 0.5;
		Gravity 1;
		PainChance 120;
		Damage 1;
		Alpha 1;
		BloodColor "ff 00 00";
		
		SeeSound "Frostweaver/See";
		PainSound "Frostweaver/Pain";
		DeathSound "Frostweaver/Death";
		ActiveSound "Frostweaver/Idle";
		MeleeSound "Frostweaver/Melee";
		Obituary "%o was frozen and then slowly eaten by a Frostweaver!";
		HitObituary "%o was frozen and then slowly eaten by a Frostweaver!";
	}
	
	States {
	Spawn:
		FRWV ABCB 10 A_Look();
		Loop;

	See:
		FRWV ABCDCB 3 A_Chase();
		Loop;

	Melee:
		FRWV E 5 A_FaceTarget();
		FRWV F 4 A_FaceTarget();
		FRWV G 7 A_CreatureMelee(3, 9, "Snowflake", "Ice");
		Goto See;

	Missile:
		FRWV H 5 A_FaceTarget();
		FRWV I 4 A_FaceTarget();
		FRWV J 7 A_SpawnProjectile("FrostshardProjectile", invoker.height / 2);
		Goto See;

	Pain:
		FRWV K 3;
		FRWV K 3 A_Pain();
		Goto See;

	Death:
	XDeath:
		FRWV LM 6;
		FRWV N 5 A_Scream();
		FRWV O 5 A_CreatureDie();
		FRWV PQRSTUVW 5;
		FRWV X -1;
		Stop;
	}
}