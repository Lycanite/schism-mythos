class Caroverm : Creature {
	Default {
		Tag "Caroverm";
		Height 56;
		Radius 30;
		Mass 400;
		Health 50;
		Speed 8;
		PainChance 90;
		
		BloodColor "Yellow";
		Obituary "%o was reduced to bloody mush ready for consumption by a Caroverm.";
		HitObituary "%o was assimilated into a Caroverm.";
		
		SeeSound "Caroverm/See";
		ActiveSound "Caroverm/Idle";
		AttackSound "Caroverm/Melee";
		PainSound "Caroverm/Pain";
		DeathSound "Caroverm/Death";
		
		+MISSILEMORE;
		+SHORTMISSILERANGE;
	}
	
	States {
		Spawn:
			CARO AB 10 A_Look();
			Loop;
		
		See:
			CARO AABBCCDD 3 A_Chase();
			Loop;
		
		Missile:
			CARO E 8 A_FaceTarget;
			CARO F 8 {
				A_StartSound("Caroverm/Projectile/Fire");
				for (int i = 0; i < 5; i++) {
					A_SpawnProjectile("BloodBelchProjectile", 40, 0, FRandom(-10, 10), 0, FRandom(-10, 10));
				}
			}
			CARO G 8;
			Goto See;
		
		Melee:
			CARO EF 8 A_FaceTarget();
			CARO G 8 A_CreatureMelee(3, 6, "Fae", "Fae");
			Goto See;
		
		Pain:
			CARO H 2;
			CARO H 2 A_Pain();
			Goto See;
		
		Death:
			CARO I 8;
			CARO J 8 A_Scream();
			CARO K 4;
			CARO L 4 A_NoBlocking();
			CARO M 4;
			CARO N -1;
			Stop;
		
		Raise:
			CARO NMLKJI 5;
			Goto See;
	}
}

class CarovermGroup : Actor {
	States {
		spawn:
			TNT1 A 0 A_SpawnItemEx("Caroverm");
			TNT1 A 0 A_SpawnItemEx("Caroverm", 60);
			TNT1 A 0 A_SpawnItemEx("Caroverm", -60);
			Stop;
	}
}