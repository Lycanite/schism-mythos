class Serpix : Creature {
	Default {
		Tag "Serpix";
		
		Health 1000;
		Radius 40;
		Height 64;
		Speed 14;
		Mass 1000;
		FloatSpeed 14;
		Scale 1;
		Gravity 1;
		PainChance 120;
		Damage 1;
		Alpha 1;
		BloodColor "ff 66 00";
		
		SeeSound "Serpix/See";
		PainSound "Serpix/Pain";
		DeathSound "Serpix/Death";
		ActiveSound "Serpix/Idle";
		MeleeSound "Serpix/Melee";
		Obituary "%o was devoured by a Serpix!";
		HitObituary "%o was devoured by a Serpix!";

		+DONTTHRUST;
	}
	
	States {
		Spawn:
			SRPX ABCB 6 A_Look();
			Loop;

		See:
			SRPX DEFGFE 6 A_Chase();
			Loop;

		Melee:
			SRPX HI 4 A_FaceTarget();
			SRPX J 6 A_CreatureMelee(20, 40);
			Goto See;

		Missile:
			SRPX HIJ 6 A_FaceTarget();
			SRPX JKLK 4 A_SpawnProjectile("SerpixProjectile", invoker.height / 2, 0, Random(-5, 5));
			SRPX I 0 A_MonsterRefire(10, "See");
			Goto Missile + 3;

		Pain:
			SRPX M 3;
			SRPX M 3 A_Pain();
			Goto See;

		Death:
		XDeath:
			SRPX MN 6;
			SRPX O 5 A_Scream();
			SRPX P 5 A_CreatureDie();
			SRPX QRS 5;
			SRPX T -1;
			Stop;
		}
}

class SerpixProjectile : FrostshardProjectile {
	Default {
		Radius 6;
		Height 6;
		Speed 6;
		DamageFunction 1;
		Scale 0.125;

		SchismProjectile.ParticleCount 1;
		SchismProjectile.ParticleInterval 4;
		SchismProjectile.KnockbackStrength 0;
	}
}