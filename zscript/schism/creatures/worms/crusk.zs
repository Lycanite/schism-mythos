class Crusk : Creature {
	Default {
		Tag "Crusk";
		
		Health 1200;
		Radius 40;
		Height 64;
		Speed 14;
		Mass 1000;
		FloatSpeed 14;
		Scale 1;
		Gravity 1;
		PainChance 120;
		Damage 1;
		Alpha 1;
		BloodColor "ff 66 00";
		
		SeeSound "Crusk/See";
		PainSound "Crusk/Pain";
		DeathSound "Crusk/Death";
		ActiveSound "Crusk/Idle";
		MeleeSound "Crusk/Melee";
		Obituary "%o was devoured by a Crusk!";
		HitObituary "%o was devoured by a Crusk!";

		+DONTTHRUST;
	}
	
	States {
		Spawn:
			CRSK ABCB 6 A_Look();
			Loop;

		See:
			CRSK DEFGFE 6 A_Chase();
			Loop;

		Melee:
			CRSK H 4 A_FaceTarget();
			CRSK I 4 A_FaceTarget();
			CRSK J 6 A_CreatureMelee(20, 40);
			Goto See;

		Pain:
			CRSK K 3;
			CRSK K 3 A_Pain();
			Goto See;

		Death:
		XDeath:
			CRSK LM 6;
			CRSK N 5 A_Scream();
			CRSK O 5 A_CreatureDie();
			CRSK PQRS 5;
			CRSK T -1;
			Stop;
	}
}