class ReilodosDoppelganger : MinionCreature {
	int wanderTicks;
	bool wandering;
	
	Default {
		Health 200;
		Radius 16;
		Height 56;
		Speed 16;
		Scale 0.7;
		PainChance 100;
		Damage 2;
		BloodColor "00 ff 66";
		
		RenderStyle "Shadow";
		Alpha 1.0;

		MinionCreature.SummonTime 100 * 24 * 60 * 60 * TICRATE;
		
		SeeSound "Reilodos/Taunt";
		PainSound "Reilodos/Pain";
		DeathSound "Reilodos/Death";
		ActiveSound "Reilodos/Taunt";
		MeleeSound "Reilodos/Melee";
		Obituary "%o was killed by Reilodos' Doppelganger!";
		HitObituary "%o was killed by Reilodos' Doppelganger!";
		
		+GHOST;
		-SOLID;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		wanderTicks = 0;
		wandering = true;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (Health > 0) {
			self.SpawnParticle("Shadow", 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
		}
	}
	
	action void RandomWander() {
		A_Look();
		if (invoker.wanderTicks++ >= 35) {
			invoker.wanderTicks = 0;
			invoker.wandering = !invoker.wandering;
		}
		if (invoker.wandering) {
			A_Wander();
			A_SpawnItemEx("ReilodosDoppelgangerBlur");
		}
	}
	
	action void ShadowShock() {
		A_StartSound("EbonCommand/Fire", CHAN_WEAPON);
		for (int i = 0; i <= 10; i++) {
			A_SpawnProjectile("EbonCommandProjectile", random(-20, 20), random(-5, 5), random(-30, 30));
		}
	}
	
	action void DoppelgangerBlur() {
		A_SpawnItemEx("ReilodosDoppelgangerBlur", 0, 0, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION|SXF_SETMASTER|SXF_NOPOINTERS|SXF_ISTRACER);
		if (invoker.tracer) {
			invoker.tracer.scale = invoker.scale;
		}
	}
	
	States {
		Spawn:
			REIL ABCD 4 RandomWander();
			Loop;
			
		See:
			REIL ABCD 4 {
				A_Chase();
				DoppelgangerBlur();
			}
			Loop;
			
		Missile:
		Melee:
			REIL E 4 A_FaceTarget();
			REIL F 4 ShadowShock();
			REIL CD 4;
			Goto See;
			
		Pain:
			REIL G 4;
			REIL G 4 A_Pain();
			Goto See;
			
		Death:
			REIL H 6;
			REIL I 6 A_Scream();
			REIL JK 6;
			REIL L 6 A_NoBlocking();
			REIL MNOP 6;
			Stop;
			
		XDeath:
		Burn:
		Ice:
		Disintegrate:
			REIL Q 5 A_Scream();
			REIL R 5 A_NoBlocking();
			REIL STUVWXY 5;
			Stop;
	}
}

class ReilodosDoppelgangerBlur : Actor {
	Default {
		Scale 0.7;
		RenderStyle "Shadow";
		Alpha 0.6;
		
		+NOBLOCKMAP;
		+NOGRAVITY;
	}
	
	States {
		Spawn:
			REIL ABCD 4 A_FadeOut(0.4);
		
		Death:
			REIL A 1 A_FadeOut(0.4);
			Loop;
	}
}