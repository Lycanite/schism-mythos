class DrujBelph : Belph {
	Default {
		Tag "Druj Belph";
		
		SeeSound "DrujBelph/See";
		ActiveSound "DrujBelph/Idle";
		PainSound "DrujBelph/Pain";
		DeathSound "DrujBelph/Death";
		MeleeSound "DrujBelph/Melee";
		Obituary "%o was poisoned by a Druj Belph!";
		HitObituary "%o was plagued by a Druj Belph!";
	}
	
	States {
		Spawn:
			DRBL A 10 A_Look();
			Loop;
			
		See:
			DRBL BBCCDDEE 3 A_Chase();
			Loop;
			
		Melee:
			DRBL EF 8 A_FaceTarget();
			DRBL G 6 A_CreatureMelee(3, 24, "Poison", "Poison");
			Goto See;

		Missile:
			DRBL EF 8 A_FaceTarget();
			DRBL G 6 A_SpawnProjectile("PoisonSpatterProjectile", invoker.height / 2);
			Goto See;
				
		Pain:
			DRBL H 2;
			DRBL H 2 A_Pain();
			Goto See;
			
		Death:
			DRBL I 4 ;
			DRBL J 4 A_Scream();
			DRBL K 4;
			DRBL L 4 A_CreatureDie();
			DRBL M -1;
			Stop;
		
		XDeath:
			DRBL N 4 ;
			DRBL O 4 A_XScream();
			DRBL P 4;
			DRBL Q 4 A_CreatureDie();
			DRBL RST 4;
			DRBL U -1;
			Stop;
			
		Raise:
			DRBL MLKJI 8;
			Goto See;
	}
}