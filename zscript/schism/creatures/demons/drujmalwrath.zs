class DrujMalwrath : Malwrath {
	Default {
		Tag "Druj Malwrath";
		Health 350;
		Speed 10;
		
		SeeSound "DrujMalwrath/See";
		ActiveSound "DrujMalwrath/Idle";
		PainSound "DrujMalwrath/Pain";
		DeathSound "DrujMalwrath/Death";
		MeleeSound "DrujMalwrath/Melee";
		Obituary "%o was poisoned by a Druj Malwrath.";
		HitObituary "%o was gobbled by a Druj Malwrath.";
	}

	action void A_DrujMalwrathProjectile() {
		for (int i = 0; i < 10; i++) {
			A_SpawnProjectile("PoisonSpatterProjectile", (invoker.height / 2) + Random(-10, 10), Random(-10, 10), Random(-10, 10));
		}
	}
		
	States {
		Spawn:
			DMAL A 10 A_Look();
			Loop;
			
		See:
			DMAL A 3 A_Chase();
			Loop;
			
		Melee:
			DMAL AB 5 A_FaceTarget();
			DMAL C 5 bright A_CreatureMelee(20, 40, "PoisonBubble", "Poison", "SchismPoisonEffect", 5 * TICRATE);
			Goto See;
			
		Missile:
			DMAL A 5 {
				A_FaceTarget();
				A_StartSound(invoker.meleeSound);
			}
			DMAL D 5 A_FaceTarget();
			DMAL E 5 bright A_DrujMalwrathProjectile();
			Goto See;
		
		Pain:
			DMAL F 6 A_Pain();
			Goto See;
		
		Death:
			DMAL F 4;
			DMAL G 4 A_Scream();
			DMAL HI 4;
			DMAL J 4 A_CreatureDie();
			DMAL K -1 A_SetFloorClip();
			Stop;
			
		Raise:
			DMAL K 4 A_UnSetFloorClip();
			DMAL JIHGF 4;
			Goto See;
	}
}