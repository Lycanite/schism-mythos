class DrujBehemophet : Behemophet {
	Default {
		Tag "DrujBehemophet";
		Radius 40;
		Height 110;
		Health 4000;
		Speed 16;
		Mass 1000;
		PainChance 20;
		MinMissileChance 160;
		
		SeeSound "DrujBehemophet/See";
		ActiveSound "DrujBehemophet/Idle";
		PainSound "DrujBehemophet/Pain";
		DeathSound "DrujBehemophet/Death";
		Obituary "%o was destroyed by a Druj Behemophet.";

		+BOSS;
		+BOSSDEATH;
		+MISSILEMORE;
		+NORADIUSDMG;
		+DONTMORPH;
	}
		
	States {
		Spawn:
			DRBH AB 10 A_Look();
			Loop;
			
		See:
			DRBH A 3 A_StartSound("DrujBehemophet/Step");
			DRBH ABBCC 3 A_Chase();
			DRBH D 3 A_StartSound("DrujBehemophet/Metal");
			DRBH D 3 A_Chase();
			Loop;
		
		Missile:
			DRBH E 6 A_FaceTarget();
			DRBH F 12 bright A_SpawnProjectile("ToxicRocketProjectile", 32);
			DRBH E 12 A_FaceTarget();
			DRBH F 12 bright A_SpawnProjectile("ToxicRocketProjectile", 32);
			DRBH E 12 A_FaceTarget();
			DRBH F 12 bright A_SpawnProjectile("ToxicRocketProjectile", 32);
			Goto See;
		
		Pain:
			DRBH G 10 A_Pain();
			Goto See;
		
		Death:
			DRBH H 10;
			DRBH I 10 A_Scream();
			DRBH JKL 10;
			DRBH M 10 A_CreatureDie();
			DRBH NO 10;
			DRBH P -1 A_BossDeath();
			Stop;
			
		Raise:
			DRBH PONMLKJIH 5;
			Goto See;
	}
}