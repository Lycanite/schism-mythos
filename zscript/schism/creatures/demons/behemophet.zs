class Behemophet : Creature {
	Default {
		Tag "Behemophet";
		Health 1000;
		Radius 24;
		Height 64;
		Mass 1000;
		Speed 8;
		PainChance 50;
		
		SeeSound "Behemophet/See";
		ActiveSound "Behemophet/Idle";
		PainSound "Behemophet/Pain";
		DeathSound "Behemophet/Death";
		MeleeSound "Behemophet/Melee";
		Obituary "%o was destroyed by a Behemophet.";

		+BOSSDEATH;
	}
		
	States {
		Spawn:
			BHMP AB 10 A_Look();
			Loop;
			
		See:
			BHMP AABBCCDD 3 A_Chase();
			Loop;
		
		Melee:
		Missile:
			BHMP EF 8 A_FaceTarget();
			BHMP G 8 bright A_CreatureComboAttack("HellfireballProjectile", 10, 80, "Hellflame", "Nether");
			Goto See;
		
		Pain:
			BHMP H 4;
			BHMP H 4 A_Pain();
			Goto See;
		
		Death:
			BHMP I 8;
			BHMP J 8 A_Scream();
			BHMP K 8;
			BHMP L 8 A_CreatureDie();
			BHMP MN 8;
			BHMP O -1 A_BossDeath();
			Stop;
			
		Raise:
			BHMP N 8;
			BHMP MLKJI 8;
			Goto See;
	}
}