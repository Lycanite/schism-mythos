class Malifex : Creature { 
	Default {
		Tag "Malifex";
		Health 500;
		Radius 25;
		Height 56;
		Mass 400;
		Speed 8;
		PainChance 112;
		Gravity 0;
		
		SeeSound "Malifex/See";
		ActiveSound "Malifex/Idle";
		PainSound "Malifex/Pain";
		DeathSound "Malifex/Death";
		Obituary "%o got blasted by a Malifex.";
		HitObituary "%o got blasted by a Malifex.";
		
		+NOGRAVITY;
		+FLOAT;
	}
	
	States { 
		Spawn: 
			MLFX ABCD 8 A_Look();
			Loop;
		
		See:
			MLFX AABBCCDD 4 A_Chase();
			MLFX A 0 A_Jump(64,1);
			Goto See;
			MLFX A 0;
			Goto See2;
		
		See2:
			MLFX EEFFGGHH 2 A_Chase();
			MLFX E 0 A_Jump(32,1);
			Goto See2;
			MLFX E 0;
			Goto See;
		
		Missile:
			MLFX I 1 A_StartSound("Malifex/Attack");
			MLFX I 4 A_FaceTarget();
			MLFX J 5 BRIGHT A_FaceTarget();
			MLFX KKKK 2 BRIGHT A_SpawnProjectile("WrathOrbProjectile", 60, 0, 0);
			MLFX L 5;
			Goto See;
		
		Pain: 
			MLFX M 3;
			MLFX M 3 A_Pain();
			MLFX M 3;
			Goto See;
		
		Death: 
			MLFX N 5 A_Scream();
			MLFX OP 5;
			MLFX P 0 A_SpawnItemEX("MalifexTorso", 0, 0, 48, 0, 0, 0, 0, 16);
			WICT A 5 A_CreatureDie();
			WICT BCDEF 5;
			WICT G -1 A_SetFloorClip();
			Stop;
		
		Raise:
			WICT G 0 A_RemoveChildren(TRUE);
			MLFX G 0 A_SpawnItemEx("MalifexTorsoRes", 0, 0, 0, 0, 0, 1, 0, 0);
			WICT G 5 A_UnSetFloorClip();
			WICT FEDBC 5;
			MLFX PON 5;
			Goto See;
	} 
}

class MalifexTorso : Actor {
	Default {
		Mass 1000000;
		Radius 1;
		Height 1;
		
		+ISMONSTER;
		+CORPSE;
	}
	
	States {
		Spawn:
			TNT1 A 0;
			MLFX Q 5 A_StartSound("Malifex/Torso/See");
			MLFX R 5;
			Wait;
		Crash:
			MLFX S 1 A_SetFloorClip;
			MLFX S 4 A_StartSound("Malifex/Torso/Crash");
			MLFX TUV 5;
			MLFX W -1;
			Stop;
	}
}

class MalifexTorsoRes : Actor {   
	Default {
		Radius 24;
		Height 24;
		
		+NOGRAVITY;
		+NOBLOCKMAP;
		+NOCLIP;
	}
	
	States {
		Spawn:
			MLFX WVUTSRQ 5;
			Stop;
	}
}
