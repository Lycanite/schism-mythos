class Belial : Creature {
	Default {
		Tag "Belial";
		Health 600;
		Radius 48;
		Height 64;
		Mass 1000;
		Speed 8;
		PainChance 80;
		
		SeeSound "Belial/See";
		ActiveSound "Belial/Idle";
		PainSound "Belial/Pain";
		DeathSound "Belial/Death";
		Obituary "%o was melted by a Belial!";
		
		+BOSSDEATH;
	}

	States {
		Spawn:
			DRUJ AB 15 A_Look();
			Loop;
		See:
			DRUJ AABBCCDDEEFF 4 A_Chase();
			Loop;
		Missile:
			DRUJ G 20 A_FatRaise();
			DRUJ H 10 bright A_FatAttack1("GluttonGlobeProjectile");
			DRUJ IG 5 A_FaceTarget();
			DRUJ H 10 bright A_FatAttack2("GluttonGlobeProjectile");
			DRUJ IG 5 A_FaceTarget();
			DRUJ H 10 bright A_FatAttack3("GluttonGlobeProjectile");
			DRUJ IG 5 A_FaceTarget();
			Goto See;
		Pain:
			DRUJ J 3;
			DRUJ J 3 A_Pain();
			Goto See;
		Death:
			DRUJ K 6;
			DRUJ L 6 A_Scream();
			DRUJ M 6 A_CreatureDie();
			DRUJ NOPQRS 6;
			DRUJ T -1 A_BossDeath();
			Stop;
		Raise:
			DRUJ R 5;
			DRUJ QPONMLK 5;
			Goto See;
	}
}