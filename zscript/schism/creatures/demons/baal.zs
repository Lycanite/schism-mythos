Class Baal : Creature {
    Default {
        Tag "Baal";
        Health 3000;
        Radius 43;
        Height 90;
        Mass 1000;
        Speed 10;
        PainChance 40;
        MeleeRange 68;
        Gravity 0;

        DamageFactor "Lightning", 0;
        
        SeeSound "Baal/See";
        ActiveSound "Baal/Idle";
        PainSound "Baal/Pain";
        DeathSound "Baal/Death";
        Obituary "%o was smote by the Baal.";
        HitObituary "The Baal ate %o for lunch.";
	
        +NORADIUSDMG;
        +BOSS;
        +BOSSDEATH;
        +NOTARGET;
        +MISSILEEVENMORE;
        +FLOAT;
        +NOGRAVITY;
        +DONTFALL;
    }

    States {
        Spawn:
            BAAL A 10 A_Look();
            Loop;

        See:
            BAAL A 0 A_UnSetInvulnerable();
            BAAL AABBAACC 3 A_Chase();
            BAAL A 0 A_Jump(32,"Teleport");
            goto See + 1;

        Missile:
            BAAL D 0 A_Jump(256, "MissileHades", "MissileStorm", "MissileGlutton");
        MissileHades:
            BAAL DE 8 A_FaceTarget();
            BAAL FFFFF 4 bright A_PainAttack("HadesSphereProjectile");
            goto See;
        MissileStorm:
            BAAL GH 8 Bright A_FaceTarget();
            BAAL H 1 Bright A_StartSound("Projectile/Netherstorm/Explode");
            BAAL H 7 Bright A_SpawnProjectile("NetherstormProjectile", 43, 0, 0, 0, 0);
            goto See;
        MissileGlutton:
            BAAL JJJJJJJJ 1 A_FaceTarget();
        Missile3Loop:
            BAAL K 4 Bright {
                A_SpawnProjectile("GluttonGlobeProjectile", 92, -40, Random(-3, 3), 0, Random(-3, 3));
                A_SpawnProjectile("GluttonGlobeProjectile",  8, -40, Random(-3, 3), 0, Random(-3, 3));
                A_SpawnProjectile("GluttonGlobeProjectile", 92,  40, Random(-3, 3), 0, Random(-3, 3));
                A_SpawnProjectile("GluttonGlobeProjectile",  8,  40, Random(-3, 3), 0, Random(-3, 3));
                A_SpawnProjectile("GluttonGlobeProjectile",  54, -50, Random(-3, 3), 0, Random(-3, 3));
                A_SpawnProjectile("GluttonGlobeProjectile",  54,  50, Random(-3, 3), 0, Random(-3, 3));
            }
            BAAL J 4 A_SpidRefire();
            BAAL J 0 A_Jump(32, "See");
            Loop;

        Melee:
            BAAL ADF 5 A_FaceTarget();
            BAAL E 5 A_CreatureMelee(20, 160, "Nether", "Nether");
            goto See;

        Pain:
            BAAL L 6;
            BAAL L 6 A_Pain();
            BAAL L 0 A_Jump(40,"Teleport");
            goto See;

        Teleport:
            BAAL F 0 A_SetInvulnerable();
            BAAL F 1 A_StartSound("Teleport");
            BAAL F 1 A_SetTranslucent(0.90);
            BAAL F 1 A_SetTranslucent(0.80);
            BAAL F 1 A_SetTranslucent(0.70);
            BAAL F 1 A_SetTranslucent(0.60);
            BAAL F 1 A_SetTranslucent(0.50);
            BAAL F 1 A_SetTranslucent(0.40);
            BAAL F 1 A_SetTranslucent(0.30);
            BAAL F 1 A_SetTranslucent(0.20);
            BAAL F 1 A_SetTranslucent(0.10);
            BAAL F 0 A_Jump(128,25);
            TNT1 AAAAAAAAAAAAAAAAAAAAAAAA 0 A_Chase(null, null, CHF_NIGHTMAREFAST);
            TNT1 A 0 A_Jump(128,25);
            TNT1 AAAAAAAAAAAAAAAAAAAAAAAA 0 A_Chase(null, null, CHF_NIGHTMAREFAST);
            BAAL F 1 A_StartSound("Teleport");
            BAAL F 1 A_SetTranslucent(0.10);
            BAAL F 1 A_SetTranslucent(0.20);
            BAAL F 1 A_SetTranslucent(0.30);
            BAAL F 1 A_SetTranslucent(0.40);
            BAAL F 1 A_SetTranslucent(0.50);
            BAAL F 1 A_SetTranslucent(0.60);
            BAAL F 1 A_SetTranslucent(0.70);
            BAAL F 1 A_SetTranslucent(0.80);
            BAAL F 1 A_SetTranslucent(0.90);
            BAAL F 1 A_SetTranslucent(1.0);
            BAAL F 0 A_UnSetInvulnerable();
            goto See;

        Death:
            BAAL M 0 bright A_FaceTarget();
            BAAL M 8 bright A_Scream();
            BAAL NO 8 bright;
            BAAL P 8 bright {
                for (int i = 0; i < 5; i++) {
                    A_SpawnProjectile("BaalCorpseFlesh1", Random(0, 90), Random(0, 40), Random(-180, 180), CMF_AIMDIRECTION, Random(-15, 15));
                    A_SpawnProjectile("BaalCorpseFlesh2", Random(0, 90), Random(0, 40), Random(-180, 180), CMF_AIMDIRECTION, Random(-15, 15));
                }
                for (int i = 0; i < 10; i++) {
                    A_SpawnProjectile("BaalCorpseFlesh3", Random(0, 90), Random(0, 40), Random(-180, 180), CMF_AIMDIRECTION, Random(-15, 15));
                    A_SpawnProjectile("BaalCorpseFlesh4", Random(0, 90), Random(0, 40), Random(-180, 180), CMF_AIMDIRECTION, Random(-15, 15));
                    A_SpawnProjectile("BaalCorpseFlesh5", Random(0, 90), Random(0, 40), Random(-180, 180), CMF_AIMDIRECTION, Random(-15, 15));
                    A_SpawnProjectile("BaalCorpseFlesh6", Random(0, 90), Random(0, 40), Random(-180, 180), CMF_AIMDIRECTION, Random(-15, 15));
                }
                A_SpawnProjectile("BaalCorpseBigArm1", 40, -40, -90, CMF_AIMDIRECTION, Random( -1, 1));
                A_SpawnProjectile("BaalCorpseBigArm2", 40,  40,  90,  CMF_AIMDIRECTION, Random( -1, 1));
                A_SpawnProjectile("BaalCorpseSmallArm1", 100, -30, -90, CMF_AIMDIRECTION, Random(-15, 15));
                A_SpawnProjectile("BaalCorpseSmallArm1", 100,  30, 90, CMF_AIMDIRECTION, Random(-15, 15));
                A_SpawnProjectile("BaalCorpseSmallArm2", 100, -30, -90, CMF_AIMDIRECTION, Random(-15, 15));
                A_SpawnProjectile("BaalCorpseSmallArm2", 100,  30, 90, CMF_AIMDIRECTION, Random(-15, 15));
                A_SpawnProjectile("BaalCorpseHorn1", 110, -16, -90, CMF_AIMDIRECTION, Random(-15, 15));
                A_SpawnProjectile("BaalCorpseHorn2", 110, 16,  90, CMF_AIMDIRECTION, Random(-15, 15));
            }
            BAAL QRSTUV 8 bright;
            Stop;
    }
}

Class BaalCorpseFlesh1 : Actor {
    Default {
        Speed 8;
        Mass 100;
        Radius 1;
        Height 1;
        PROJECTILE;
        +THRUGHOST;
        +LOWGRAVITY;
        -NOGRAVITY;
        +CLIENTSIDEONLY;
    }

    States {
        Spawn:
            BAF1 ACEGIKM 5;
            Loop;
        Death:
            BAF1 O 3;
            BAF1 Q -1;
            Stop;
        }
}

Class BaalCorpseFlesh2 : BaalCorpseFlesh1 {
    States {
        Spawn:
            BAF1 BDFHJLN 5;
            Loop;
        Death:
            BAF1 P 3;
            BAF1 R -1;
            Stop;
        }
}

Class BaalCorpseFlesh3 : BaalCorpseFlesh1 {
    States {
        Spawn:
            BAF2 ACEG 5;
            Loop;
        Death:
            BAF2 I -1;
            Loop;
        }
}

Class BaalCorpseFlesh4 : BaalCorpseFlesh1 {
    States {
        Spawn:
            BAF2 BDFH 5;
            Loop;
        Death:
            BAF2 J -1;
            Loop;
        }
}

Class BaalCorpseFlesh5 : BaalCorpseFlesh1 {
    States {
        Spawn:
            BAF3 ACEGI 5;
            Loop;
        Death:
            BAF3 K -1;
            Loop;
        }
}

Class BaalCorpseFlesh6 : BaalCorpseFlesh1 {
    States {
        Spawn:
            BAF3 BDFHJ 5;
            Loop;
        Death:
            BAF3 L -1;
            Loop;
        }
}

Class BaalCorpseBigArm1 : BaalCorpseFlesh1 {
    States {
        Spawn:
            BAF4 ACEGI 5;
            Loop;
        Death:
            BAF4 K 3;
            BAF4 M -1;
            Stop;
        }
}

Class BaalCorpseBigArm2 : BaalCorpseFlesh1 {
    States {
        Spawn:
            BAF4 BDFHJ 5;
            Loop;
        Death:
            BAF4 L 3;
            BAF4 N -1;
            Stop;
        }
}

Class BaalCorpseSmallArm1 : BaalCorpseFlesh1 {
    States {
        Spawn:
            BAF5 ACEG 5;
            Loop;
        Death:
            BAF5 I -1;
            Stop;
        }
}

Class BaalCorpseSmallArm2 : BaalCorpseFlesh1 {
    States {
        Spawn:
            BAF5 BDFH 5;
            Loop;
        Death:
            BAF5 J -1;
            Stop;
        }
}

Class BaalCorpseHorn1 : BaalCorpseFlesh1 {
    States {
        Spawn:
            BAF6 ACEGI 5;
            Loop;
        Death:
            BAF6 K -1;
            Stop;
        }
}

Class BaalCorpseHorn2 : BaalCorpseFlesh1 {
    States {
        Spawn:
            BAF6 BDFHJ 5;
            Loop;
        Death:
            BAF6 L -1;
            Stop;
        }
}