class VulgarBelph : Belph {
	Default {
		Tag "Vulgar Belph";

		Health 70;
		Mass 120;
		Speed 7;
		PainChance 210;

		SeeSound "VulgarBelph/See";
		ActiveSound "VulgarBelph/Idle";
		PainSound "VulgarBelph/Pain";
		DeathSound "VulgarBelph/Death";
		MeleeSound "VulgarBelph/Melee";
		Obituary "%o was poisoned by a Vulgar Belph.";
		HitObituary "A Vulgar Belph had %o for lunch.";
	}
	
	states {
		Spawn:
			VULG AB 10 A_Look();
			Loop;
		
		See:
			VULG AABBCCDD 3 A_Chase();
			Loop;
		
		Melee:
			VULG E 8 A_FaceTarget();
			VULG F 1 A_StartSound("Vulgar/Melee");
			VULG F 7 A_FaceTarget();
			VULG G 6 A_CreatureMelee(3, 18, "Acid", "Acid", "SchismPoisonEffect", 5 * TICRATE);
			goto See;
		
		Missile:
			VULG EF 8 A_FaceTarget();
			VULG G 6 A_SpawnProjectile("AcidBarbProjectile", invoker.height / 2);
			goto See;
		
		Pain:
			VULG H 2;
			VULG H 2 A_Pain();
			goto See;
		
		Death:
			VULG I 8;
			VULG J 8 A_Scream();
			VULG KL 6;
			VULG M 6 A_CreatureDie();
			VULG N -1;
			Stop;
		
		XDeath:
			VULG O 5;
			VULG P 5 A_XScream();
			VULG Q 5;
			VULG R 5 A_CreatureDie();
			VULG STU 5;
			VULG V -1;
			Stop;
		
		Raise:
			VULG ML 8;
			VULG KJI 6;
			goto See;
	}
}