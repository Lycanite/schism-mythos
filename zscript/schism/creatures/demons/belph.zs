class Belph : Creature {
	Default {
		Tag "Belph";

		Health 60;
		Radius 20;
		Height 56;
		Mass 100;
		Speed 8;
		PainChance 200;
		
		SeeSound "Belph/See";
		ActiveSound "Belph/Idle";
		PainSound "Belph/Pain";
		DeathSound "Belph/Death";
		MeleeSound "Belph/Melee";
		Obituary "%o was burnt by a Belph!";
		HitObituary "%o was ripped apart by a Belph!";
	}
	
	States {
		Spawn:
			BELP A 10 A_Look();
			Loop;
			
		See:
			BELP BBCCDDEE 2 A_Chase();
			Loop;
			
		Melee:
			BELP EF 8 A_FaceTarget();
			BELP G 6 A_CreatureMelee(3, 24, "Flame", "Fire");
			Goto See;

		Missile:
			BELP EF 8 A_FaceTarget();
			BELP G 6 A_SpawnProjectile("DoomfireballProjectile", invoker.height / 2);
			Goto See;
				
		Pain:
			BELP H 2;
			BELP H 2 A_Pain();
			Goto See;
			
		Death:
			BELP I 4 ;
			BELP J 4 A_Scream();
			BELP K 4;
			BELP L 4 A_CreatureDie();
			BELP M -1;
			Stop;
		
		XDeath:
			BELP N 4 ;
			BELP O 4 A_XScream();
			BELP P 4;
			BELP Q 4 A_CreatureDie();
			BELP RS 4;
			BELP T -1;
			Stop;
			
		Raise:
			BELP MLKJI 8;
			Goto See;
	}
}