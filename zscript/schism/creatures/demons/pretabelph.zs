class PretaBelph : Belph {
	bool teleporting;

	Default {
		Tag "Preta Belph";

		Health 60;
		Radius 22;
		Height 56;
		Mass 200;
		Speed 18;
		PainChance 64;
		
		SeeSound "PretaBelph/See";
		ActiveSound "PretaBelph/Idle";
		PainSound "PretaBelph/Pain";
		DeathSound "PretaBelph/Death";
		MeleeSound "PretaBelph/Melee";
		Obituary "%o was slashed by a Preta Belph.";
		HitObituary "%o was slashed by a Preta Belph.";
	}
	
	action void A_SetTeleporting(bool teleporting) {
		invoker.teleporting = teleporting;
		invoker.bINVULNERABLE = teleporting;
		invoker.bNONSHOOTABLE = teleporting;
		invoker.bSHOOTABLE = !teleporting;
		invoker.bNOTELEPORT = teleporting;
		invoker.bNORADIUSDMG = teleporting;
		invoker.bDROPOFF = teleporting;
		invoker.bNORADIUSDMG = teleporting;
	}
	
	States {
		Spawn:
			3WRT A 1;
			3WRT A 0 A_JumpIf(Args[0] > 0, "Spawn2");
			3WRT A 0 A_JumpIf(invoker.teleporting, "StopTeleport");
			3WRT A 1 A_Look();
			Goto Spawn + 1;
			
		Spawn2:
			3WRT A 0 A_JumpIf(invoker.teleporting, "StopTeleport");
			3WRT A 1 A_LookEx(8, 0, 0, 0, 0, "Teleport");
			Loop;
			
		StopTeleport:
			3WRT A 0 A_SpawnItem("TeleportFog");
			3WRT A 0 A_SetTeleporting(false);
			Goto Spawn;
			
		See:
			3WRT BBBCCCDDDEEE 2 A_Chase;
			3WRT B 0 A_JumpIf(Args[1] > 0, 1);
			Goto See+1;
			3WRT B 0 A_Jump(16, "Missile");
			Goto See+1;
			
		Melee:
			3WRT A 0 A_JumpIf(invoker.teleporting, 5);
			3WRT A 6 A_FaceTarget();
			3WRT F 5 A_FaceTarget();
			3WRT G 0 A_StartSound("Wraith/Melee", CHAN_WEAPON);
			3WRT G 8 A_CreatureMelee(5, 10, "Shadow", "Shadow");
			Goto See;
			3WRT A 0 A_SpawnItem("TeleportFog");
			3WRT A 0 A_SetTeleporting(false);
			3WRT F 5 A_FaceTarget();
			3WRT G 0 A_StartSound("Wraith/Melee", CHAN_WEAPON);
			3WRT G 8 A_CreatureMelee(5, 10, "Shadow", "Shadow");
			Goto See;
			
		Missile:
			3WRT A 0 A_JumpIfCloser(128, 2);
			3WRT A 0 A_JumpIfCloser(1024, 2);
			3WRT A 0;
			Goto See;
			3WRT A 0 A_Jump(128, "Teleport");
			Goto See;
			
		Teleport:
			3WRT AAA 6 A_FaceTarget();
			TNT1 A 0 A_SpawnItem("TeleportFog");
			TNT1 A 0 A_SetTeleporting(true);
			TNT1 AAAAAAAAAAAAAAAAAAAAAAAAA 0 A_ExtChase(1, 0, 0, 0);
			TNT1 AAAAAAAAAAAAAAAAAAAAAAAAA 0 A_ExtChase(1, 0, 0, 0);
			TNT1 A 1 A_ExtChase(1, 0, 0, 0);
			3WRT A 0 A_SpawnItem("TeleportFog");
			3WRT A 0 A_SetTeleporting(false);
			3WRT AA 6 A_FaceTarget();
			Goto See;
			
		Pain:
			3WRT A 4;
			3WRT A 4 A_Pain();
			Goto See;
			
		Death:
			3WRT H 8;
			3WRT I 8 A_Scream();
			3WRT J 6;
			3WRT K 6 A_CreatureDie();
			3WRT L -1;
			Stop;
			
		Raise:
			TROO LKJIH 8;
			Goto See;
	}
}