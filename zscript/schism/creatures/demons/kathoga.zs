class Kathoga : Creature {
	Default {
		Tag "Kathoga";
		Health 150;
		Radius 30;
		Height 56;
		Mass 400;
		Speed 12;
		PainChance 180;
		
		Obituary "%o was devoured by a Kathoga.";
		HitObituary "%o was devoured by a Kathoga.";
		SeeSound "Kathoga/See";
		ActiveSound "Kathoga/Idle";
		PainSound "Kathoga/Pain";
		DeathSound "Kathoga/Death";
		MeleeSound "Kathoga/Melee";
	}
		
	States {
		Spawn:
			KATH AB 10 A_Look();
			Loop;
			
		See:
			KATH AABBCCDD 2 fast A_Chase();
			Loop;
		
		Melee:
			KATH EF 8 fast A_FaceTarget();
			KATH G 8 fast A_CreatureMelee(4, 40, "Quake", "Quake");
			Goto See;
		
		Pain:
			KATH H 2 fast;
			KATH H 2 fast A_Pain();
			Goto See;
		
		Death:
			KATH I 8;
			KATH J 8 A_Scream();
			KATH K 8;
			KATH L 8 A_CreatureDie();
			KATH M 8;
			KATH N -1;
			Stop;
			
		Raise:
			KATH N 5;
			KATH MLKJI 5;
			Goto See;
	}
}