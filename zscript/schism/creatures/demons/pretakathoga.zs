class PretaKathoga : Kathoga {
	Default {
		Tag "Preta Kathoga";
		
		Bloodcolor "Grey";

		Obituary "%o never saw it coming.";
		HitObituary "%o never saw it coming.";
		SeeSound "PretaKathoga/See";
		ActiveSound "PretaKathoga/Idle";
		AttackSound "PretaKathoga/Melee";
		PainSound "PretaKathoga/Pain";
		DeathSound "PretaKathoga/Death";

		+SHADOW;
	}
		
	States {
		Spawn:
			PKTH AB 10 A_Look();
			Loop;
			
		See:
			PKTH AABBCCDD 2 fast A_Chase();
			Loop;
		
		Melee:
			PKTH EF 8 fast A_FaceTarget();
			PKTH G 8 fast A_CreatureMelee(4, 40, "Shadow", "Shadow", "SchismWeaknessDebuff", 5 * TICRATE);
			Goto See;
		
		Pain:
			PKTH H 2 fast;
			PKTH H 2 fast A_Pain();
			Goto See;
		
		Death:
			PKTH I 8;
			PKTH J 8 A_Scream();
			PKTH K 8;
			PKTH L 8 A_CreatureDie();
			PKTH M 8;
			PKTH N -1;
			Stop;
			
		Raise:
			PKTH N 5;
			PKTH MLKJI 5;
			Goto See;
	} 
}