class Corvazar : Creature {
	Default {
		Tag "Corvazar";
		Health 500;
		Radius 24;
		Height 64;
		Mass 1000;
		Speed 8;
		PainChance 50;
		
		SeeSound "Corvazar/See";
		ActiveSound "Corvazar/Idle";
		PainSound "Corvazar/Pain";
		DeathSound "Corvazar/Death";
		MeleeSound "Corvazar/Melee";
		Obituary "%o was corrupted by a Corvazar.";
	}
		
	States {
		Spawn:
			CRVZ AB 10 A_Look();
			Loop;
			
		See:
			CRVZ AABBCCDD 3 A_Chase();
			Loop;
		
		Melee:
		Missile:
			CRVZ EF 8 A_FaceTarget();
			CRVZ G 8 bright A_CreatureComboAttack("HellfireballProjectile", 10, 80, "Hellflame", "Nether");
			Goto See;
		
		Pain:
			CRVZ H 4;
			CRVZ H 4 A_Pain();
			Goto See;
		
		Death:
			CRVZ I 8 A_Scream();
			CRVZ J 8 A_CreatureDie();
			CRVZ K 8;
			CRVZ L -1;
			Stop;
		
		XDeath:
			CRVZ M 8;
			CRVZ N 8 A_XScream();
			CRVZ O 8 A_CreatureDie();
			CRVZ PQRS 8;
			CRVZ T -1;
			Stop;
			
		Raise:
			CRVZ KJI 8;
			Goto See;
	}
}