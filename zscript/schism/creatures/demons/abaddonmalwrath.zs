class AbaddonMalwrath : Malwrath {	
	Default {
		Tag "Abaddon Malwrath";
		
		SeeSound "AbaddonMalwrath/See";
		ActiveSound "AbaddonMalwrath/Idle";
		PainSound "AbaddonMalwrath/Pain";
		DeathSound "AbaddonMalwrath/Death";
		MeleeSound "AbaddonMalwrath/Melee";
		Obituary "%o was melted by an Abaddon Malwrath.";
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		self.SpawnParticle("Fume", TICRATE, 3, (FRandom(-8, 8), FRandom(-8, 8), FRandom(20, 30)), Random(80, 100), Random(-10, 10), 0.5);
		self.SpawnParticle("Fume", TICRATE, 3, (FRandom(-8, 8), FRandom(-8, 8), FRandom(20, 30)), Random(-100, -80), Random(-10, 10), 0.5);
	}

	action void A_AbaddonBurst() {
		A_PainDie("Volcan");
	}
	
	States {
		Spawn:
		Idle:
			ABDN ABCD 4 A_Look();
			Loop;
			
		See:
			ABDN  AABBCCDD 2 A_Chase();
			Loop;
			
		Melee:
			ABDN EF 5 A_FaceTarget();
			ABDN G 5 bright A_CreatureMelee(20, 40, "Lava", "Lava", "SchismImmolateDebuff", 5 * TICRATE);
			Goto See;
			
		Missile:
			ABDN H 5 {
				A_FaceTarget();
				A_StartSound(invoker.meleeSound);
			}
			ABDN I 5 A_FaceTarget();
			ABDN J 5 bright A_SpawnProjectile("MagmaProjectile", invoker.height / 2);
			Goto See;
			
		Pain:
			ABDN K 6;
			ABDN K 6 A_Pain();
			Goto See;
			
		Death:
			ABDN L 8 bright;
			ABDN M 8 bright A_Scream();
			ABDN NO 8 bright;
			ABDN P 8 bright {
				A_CreatureDie();
				A_AbaddonBurst();
			}
			ABDN Q 8;
			Stop;
	}
}