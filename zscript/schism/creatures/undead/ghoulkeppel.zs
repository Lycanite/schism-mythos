class GhoulKeppel : Ghoul {
	Default {
		Tag "Keppel Ghoul";
	}
	
	States {
		Spawn:
			GHUK AB 10 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			GHUK AB 10 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			GHUK ABCD 4 {
				return A_FollowMaster();
			}
			Loop;
			
		See:
			GHUK ABCD 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			GHUK EF 4 A_FaceTarget();
			GHUK G 4 A_GhoulAttack();
			Goto See;
			
		Pain:
			GHUK H 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			GHUK I 5 A_PoisonBurst();
			GHUK J 5 A_Scream();
			GHUK K 5;
			GHUK L 5 A_CreatureDie();
			GHUK MNOPQR 5;
			GHUK S -1;
			Stop;
	}
}