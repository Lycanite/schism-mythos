class Ghoul : MinionCreature {
	Default {
		Tag "Ghoul";
		
		Health 100;
		Radius 10;
		Height 50;
		Speed 15;
		Scale 1;
		Gravity 1;
		PainChance 120;
		Damage 1;
		BloodColor "00 44 00";

		MinionCreature.SummonTime 60 * TICRATE;
		
		SeeSound "Ghoul/See";
		PainSound "Ghoul/Pain";
		DeathSound "Ghoul/Death";
		ActiveSound "Ghoul/See";
		MeleeSound "Ghoul/Melee";
		Obituary "%o was rotted away by a Ghoul!";
		HitObituary "%o was rotted away by a Ghoul!";
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (Health <= 0) {
			return;
		}
	}
	
	action void A_GhoulAttack() {
		A_CreatureMelee(10, 30, "Poison", "Poison");
	}
	
	action void A_PoisonBurst() {
		for (int i = 0; i < 20; i += 20) {
			invoker.SpawnParticle("Poison", 2 * TICRATE, 1, (Random(-4, 4), 0, Random(-4, 4)), Random(0, 359), Random(-45, 45));
			invoker.SpawnParticle("PoisonBubble", 2 * TICRATE, 1, (Random(-4, 4), 0, Random(-4, 4)), Random(0, 359), Random(-45, 45));
		}
		int poisonDurarionTics = 5 * TICRATE;
		int poisonDamageAmount = 2;
		BlockThingsIterator iter = BlockThingsIterator.Create(invoker, 300, false);
		while (iter.Next()) {
			Actor poisonTarget = iter.thing;
			if (self.CheckSight(poisonTarget) && poisonTarget.bIsMonster && !poisonTarget.bCorpse && SchismInteraction.CanHarmTarget(invoker, poisonTarget, false)) {
				if (poisonTarget.PoisonDurationReceived < poisonDurarionTics && poisonTarget.PoisonDamageReceived <= poisonDamageAmount) {
					poisonTarget.PoisonMobj(target, invoker, poisonDamageAmount, poisonDurarionTics, 10, "poison");
				}
			}
		}
	}
	
	States {
		Spawn:
			GHOU AB 10 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			GHOU AB 10 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			GHOU ABCD 4 {
				return A_FollowMaster();
			}
			Loop;
			
		See:
			GHOU ABCD 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			GHOU EF 4 A_FaceTarget();
			GHOU G 4 A_GhoulAttack();
			Goto See;
			
		Pain:
			GHOU H 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			GHOU I 5 A_PoisonBurst();
			GHOU J 5 A_Scream();
			GHOU K 5;
			GHOU L 5 A_CreatureDie();
			GHOU MNOPQR 5;
			GHOU S -1;
			Stop;
	}
}