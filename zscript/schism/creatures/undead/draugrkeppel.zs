class DraugrKeppel : Draugr {
	Default {
		Tag "Keppel Draugr";
	}
	
	States {
		Spawn:
			DRGK B 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			DRGK B 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			DRGK ABCDEF 4 {
				return A_FollowMaster();
			}
			Loop;
			
		See:
			DRGK ABCDEF 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			DRGK GH 4 A_FaceTarget();
			DRGK I 4 A_DraugrAttack();
			Goto See;
			
		Pain:
			DRGK J 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			DRGK J 5;
			DRGK K 5 A_Scream();
			DRGK L 5;
			DRGK M 5 A_CreatureDie();
			DRGK N 5;
			DRGK O -1 A_BoneBurst();
			Stop;
	}
}