#include "zscript/schism/creatures/demons/belph.zs"
#include "zscript/schism/creatures/demons/drujbelph.zs"
#include "zscript/schism/creatures/demons/vulgarbelph.zs"
#include "zscript/schism/creatures/demons/pretabelph.zs"
#include "zscript/schism/creatures/demons/anzubelph.zs"
#include "zscript/schism/creatures/demons/molochbelph.zs"

#include "zscript/schism/creatures/demons/corvazar.zs"

#include "zscript/schism/creatures/demons/behemophet.zs"
#include "zscript/schism/creatures/demons/anzubehemophet.zs"
#include "zscript/schism/creatures/demons/paimon.zs"

#include "zscript/schism/creatures/demons/kathoga.zs"
#include "zscript/schism/creatures/demons/molochkathoga.zs"
#include "zscript/schism/creatures/demons/pretakathoga.zs"

#include "zscript/schism/creatures/demons/buer.zs"
#include "zscript/schism/creatures/demons/malphas.zs"

#include "zscript/schism/creatures/demons/malwrath.zs"
#include "zscript/schism/creatures/demons/drujmalwrath.zs"
#include "zscript/schism/creatures/demons/abaddonmalwrath.zs"
#include "zscript/schism/creatures/demons/maramalwrath.zs"
#include "zscript/schism/creatures/demons/pyravon.zs"
#include "zscript/schism/creatures/demons/malifex.zs"

#include "zscript/schism/creatures/demons/vorach.zs"

#include "zscript/schism/creatures/demons/apollyon.zs"
#include "zscript/schism/creatures/demons/maraapollyon.zs"

#include "zscript/schism/creatures/demons/belial.zs"

#include "zscript/schism/creatures/demons/drujbehemophet.zs"
#include "zscript/schism/creatures/demons/abraxas.zs"
#include "zscript/schism/creatures/demons/baal.zs"
