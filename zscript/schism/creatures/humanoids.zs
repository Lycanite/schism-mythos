// ========== Creatures ==========
// Peasants:
#include "zscript/schism/creatures/humanoids/peasants.zs"

// Mages:
#include "zscript/schism/creatures/humanoids/hieromancer.zs"
#include "zscript/schism/creatures/humanoids/shadowmancer.zs"

// Warriors:
#include "zscript/schism/creatures/humanoids/hieroknight.zs"

// Characters:
#include "zscript/schism/creatures/humanoids/machiavel.zs"
#include "zscript/schism/creatures/humanoids/mirdun.zs"
#include "zscript/schism/creatures/humanoids/sidhual.zs"

// Former:
#include "zscript/schism/creatures/humanoids/formerprivate.zs"
#include "zscript/schism/creatures/humanoids/formercorporal.zs"

#include "zscript/schism/creatures/humanoids/formersergent.zs"
#include "zscript/schism/creatures/humanoids/formerlieutenant.zs"

#include "zscript/schism/creatures/humanoids/formermajor.zs"
#include "zscript/schism/creatures/humanoids/formercaptain.zs"
#include "zscript/schism/creatures/humanoids/formerbrigadier.zs"