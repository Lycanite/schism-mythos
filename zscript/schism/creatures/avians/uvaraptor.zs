class Uvaraptor : MinionCreature {
	Default {
		Tag "Uvaraptor";

		Health 200;
		Radius 26;
		Height 72;
		Speed 30;
		Scale 0.75;
		PainChance 250;
		Damage 2;
		Gravity 0.5;
		BloodColor "ff 00 00";

		MinionCreature.SummonTime 5 * 35;

		+JUMPDOWN;
		
		SeeSound "Uvaraptor/See";
		PainSound "Uvaraptor/Pain";
		DeathSound "Uvaraptor/Death";
		ActiveSound "Uvaraptor/See";
		MeleeSound "Uvaraptor/Melee";
		Obituary "%o was ripped apart by a Uvaraptor!";
		HitObituary "%o was ripped apart by a Uvaraptor!";
	}
	
	States {
		Spawn:
			URAP AABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			URAP DEFE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			URAP DEF 4 {
				return A_MinionChase();
			}
			URAP E 4 {
				if (FRandom(0, 1) >= 0.75) {
					A_Leap(7, Random(50, 100));
				}
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			URAP G 4 A_FaceTarget();
			URAP H 4 A_CreatureMelee(4);
			URAP I 4;
			Goto See;
			
		Pain:
			URAP J 6;
			URAP J 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			URAP KL 5;
			URAP M 5 A_Scream();
			URAP N 5;
			URAP P 5 A_CreatureDie();
			URAP P -1;
			Stop;
	}
}
