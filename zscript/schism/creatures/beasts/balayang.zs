class Balayang : MinionCreature {
	Default {
		Tag "Balayang";

		Health 120;
		Radius 20;
		Height 40;
		Speed 26;
		Scale 0.5;
		Gravity 0;
		PainChance 180;
		Damage 2;
		BloodColor "ff 00 00";

		MinionCreature.SummonTime 5 * 35;

		+FLOAT;
		
		SeeSound "Balayang/See";
		PainSound "Balayang/Pain";
		DeathSound "Balayang/Death";
		ActiveSound "Balayang/See";
		MeleeSound "Balayang/Melee";
		Obituary "%o was chewed up by a Balayang!";
		HitObituary "%o was chewed up by a Balayang!";
	}
	
	States {
		Spawn:
			BYAN AABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			BYAN DE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			BYAN F 4 {
				A_StartSound("Balayang/Fly");
				A_Look();
				return A_FollowMaster(1000);
			}
			BYAN E 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			BYAN DEFE 4 {
				return A_MinionChase();
			}
			BYAN F 4 {
				A_StartSound("Balayang/Fly");
				return A_MinionChase();
			}
			BYAN E 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			BYAN G 8 A_FaceTarget();
			BYAN H 8 A_CreatureMelee(4);
			BYAN I 8;
			Goto See;
			
		Pain:
			BYAN J 6;
			BYAN J 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			BYAN KL 5;
			BYAN M 5 A_Scream();
			BYAN NO 5;
			BYAN P 5 A_CreatureDie();
			BYAN QRSTUVW 5;
			BYAN X -1;
			Stop;
	}
}
