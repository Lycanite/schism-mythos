class Wraamon : ChaosMinion {
	Default {
		Tag "Wraamon";

		Health 60;
		Radius 16;
		Height 16;
		Speed 16;
		Scale 0.75;
		PainChance 250;
		Damage 2;
		BloodColor "00 ff 66";
		MinionCreature.SummonTime 10 * 35;

		+JUMPDOWN
		
		SeeSound "Wraamon/See";
		PainSound "Wraamon/Pain";
		DeathSound "Wraamon/Death";
		ActiveSound "Wraamon/See";
		MeleeSound "Wraamon/Melee";
		Obituary "%o was devoured by the Wraamon!";
		HitObituary "%o was devoured by the Wraamon!";
	}
	
	States {
		Spawn:
			WRAA CDEF 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			WRAA CDEF 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			WRAA CDEF 4 {
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			WRAA CDEF 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			WRAA A 4 A_FaceTarget();
			WRAA B 4 A_MinionHavocMeleeAttack(2);
			WRAA AB 4;
			Goto See;
			
		Missile:
			WRAA A 0 A_Jump(128, "See");
			WRAA GG 3 A_FaceTarget();
			WRAA H 10 A_Leap(60);
			WRAA I 8 A_LeapAttack(2);
			WRAA G 8;
			Goto See;
			
		Pain:
			WRAA G 6;
			WRAA D 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			WRAA G 5;
			WRAA J 5 A_Scream();
			WRAA K 5;
			WRAA L 5 A_NoBlocking();
			WRAA M 5;
			WRAA NO 5;
			Stop;
	}
}
