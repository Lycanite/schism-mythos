class ChupacabraPhosphor : Chupacabra {
	int menagerieTick;
	
	Default {
		Tag "Phosphorescent Chupacabra";

		Obituary "%o was devoured by a Chaotic Chupacabra!";
		HitObituary "%o was devoured by a Chaotic Chupacabra!";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.burstHeal = true;
		self.menagerieTick = 0;
	}
	
	override FollowProjectile EmitFollowProjectile(String projectileClass, int minionCount) {
		FollowProjectile followProjectile = super.EmitFollowProjectile(projectileClass, minionCount);
		if (self.menagerieTick++ >= 4) {
			self.menagerieTick = 0;
			if (minionCount < 50) {
				A_SpawnProjectile("FoulMenageriePortalBoltUnstable", 4, Random(-6, 6), Random(-180, 180), CMF_AIMDIRECTION, Random(-45, 10));
			}
		}
		return followProjectile;
	}
	
	States {
		Spawn:
			CHPP AABBCC 4 {
				A_Look();
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			CHPP CDEF 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			CHPP DEFGHI 4 {
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			CHPP DEFGHI 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			CHPP J 4 A_FaceTarget();
			CHPP K 4 A_MinionHavocMeleeAttack(3, 6);
			CHPP BC 4;
			Goto See;
			
		Pain:
			CHPP L 6;
			CHPP C 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			CHPP L 5;
			CHPP M 5 A_Scream();
			CHPP NO 5;
			CHPP P 5 A_NoBlocking();
			CHPP QRS 5;
		FadeOut:
			CHPP S 1 A_FadeOut(2);
			Loop;
	}
}
