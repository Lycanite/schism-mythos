class Chupacabra : ChaosMinion {
	Default {
		Tag "Chupacabra";

		Health 200;
		Radius 14;
		Height 40;
		Speed 30;
		Scale 0.5;
		PainChance 250;
		Damage 2;
		BloodColor "ff 00 00";

		MinionCreature.SummonTime 5 * 35;
		MinionCreature.Stance 1; // Follow

		+JUMPDOWN;
		
		SeeSound "Chupacabra/See";
		PainSound "Chupacabra/Pain";
		DeathSound "Chupacabra/Death";
		ActiveSound "Chupacabra/See";
		MeleeSound "Chupacabra/Melee";
		Obituary "%o was devoured by a Chupacabra!";
		HitObituary "%o was devoured by a Chupacabra!";
	}
	
	States {
		Spawn:
			CHUP AABBCC 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			CHUP CDEF 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			CHUP DEFGHI 4 {
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			CHUP DEFGHI 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			CHUP J 4 A_FaceTarget();
			CHUP K 4 A_MinionHavocMeleeAttack(3, 6);
			CHUP BC 4;
			Goto See;
			
		Pain:
			CHUP L 6;
			CHUP C 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			CHUP L 5;
			CHUP M 5 A_Scream();
			CHUP NO 5;
			CHUP P 5 A_CreatureDie();
			CHUP QR 5;
			CHUP S -1;
			Stop;
	}
}
