class Salamander : MinionCreature {
	Default {
		Health 300;
		Radius 28;
		Height 56;
		Speed 24;
		Mass 500;
		FloatSpeed 10;
		Scale 0.75;
		PainChance 120;
		Damage 2;
		BloodColor "ff 66 00";

		DamageFactor "Fire", 0.1;
		DamageFactor "Lava", 0.1;
		
		SeeSound "Salamander/See";
		PainSound "Salamander/Pain";
		DeathSound "Salamander/Death";
		ActiveSound "Salamander/Idle";
		MeleeSound "Salamander/Melee";
		Obituary "%o was roasted by a Salamander!";
		HitObituary "%o was roasted by a Salamander!";
	}

	override bool CanApplyEffect(class<Inventory> effectClass) {
		return effectClass != "SchismImmolateDebuff";
	}

	action void A_SalamanderAttack(int damageMin, int damageMax = 0) {
		invoker.A_CreatureMelee(damageMin, damageMax, "FireEmber");
		SchismStatusEffects.InflictEffect("SchismImmolateDebuff", invoker, invoker, invoker.target, 5 * 35, "SchismImmolateDebuff");
	}
	
	States {
		Spawn:
			SALA ABCB 4 {
				A_Look();
			}
			Loop;
			
		See:
			SALA DEF 4 {
				A_Chase("Melee");
			}
			SALA E 4 {
				if (FRandom(0, 1) >= 0.75) {
					A_Leap(Random(10, 20), Random(10, 30));
				}
				A_Chase("Melee");
			}
			Loop;
			
		Melee:
			SALA GG 4 A_FaceTarget();
			SALA H 8 A_SalamanderAttack(5, 10);
			SALA II 4 A_FaceTarget();
			Goto See;
			
		Pain:
			SALA J 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			SALA K 5;
			SALA L 5 A_Scream();
			SALA M 5;
			SALA N 5 A_CreatureDie();
			SALA O 5;
			SALA P -1;
			Stop;
	}
}