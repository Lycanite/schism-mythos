class Miredon : Creature {
	Default {
		Tag "Miredon";
		
		Health 200;
		Radius 20;
		Height 56;
		Speed 24;
		FloatSpeed 24;
		Scale 1;
		PainChance 250;
		Damage 2;
		BloodColor "ff 00 00";

		+JUMPDOWN;

		-Creature.WALKER;
		+Creature.SWIMMER;
		
		SeeSound "Miredon/See";
		ActiveSound "Miredon/Idle";
		PainSound "Miredon/Pain";
		MeleeSound "Miredon/Melee";
		DeathSound "Miredon/Death";
		Obituary "%o was ripped apart by a Miredon!";
		HitObituary "%o was ripped apart by a Miredon!";
	}
	
	States {
		Spawn:
			MDON AABBAACC 4 A_Look();
			Loop;
			
		See:
			MDON ABAC 4 A_Chase();
			Loop;
			
		Melee:
			MDON D 4 A_FaceTarget();
			MDON E 4 A_CreatureMelee(4);
			MDON ED 4;
			Goto See;
			
		Pain:
			MDON F 6;
			MDON F 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			MDON F 5;
			MDON G 5 A_Scream();
			MDON HI 5;
			MDON K 5 A_NoBlocking();
			MDON K 5;
			MDON L -1;
			Stop;
	}
}
