class MogDalek : MinionCreature {
	Default {
		Health 5000;
		Radius 10;
		Height 20;
		Speed 20;
		Scale 1.5;
		Gravity 0;
		PainChance 50;
		Damage 2;
		BloodColor "ff 99 00";

		MinionCreature.SummonTime 30 * 35;

		+FLOAT;
		+MinionCreature.TAUNT;
		
		SeeSound "MogDalek/See";
		PainSound "MogDalek/Pain";
		DeathSound "MogDalek/Death";
		ActiveSound "MogDalek/See";
		MeleeSound "";
		Obituary "%o was incinerated by Fluffy!";
		HitObituary "%o was incinerated by Fluffy!";
		
		DamageFactor "Fire", 0;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (Health > 0) {
			self.SpawnParticle("Fire", TICRATE, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10));
			self.SpawnParticle("FireEmber", 2 * TICRATE, 3, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10));
		}
	}
	
	action void A_FireSpray() {
		if (Random(0.0, 1.0) >= 0.95) {
			A_StartSound("FirestoneCane/Fire");
		}
		A_SpawnProjectile("FirestoneCaneGlasmaFragmentProjectile", 8, Random(-6, 6), Random(-20, 20), CMF_OFFSETPITCH|CMF_TRACKOWNER, Random(-2, 2));
		for (int i = 0; i <= 2; i++) {
			A_SpawnProjectile("FirestoneCaneProjectile", 8, Random(-6, 6), Random(-20, 20), CMF_OFFSETPITCH|CMF_TRACKOWNER, Random(-2, 2));
		}
	}
	
	action void A_FireStorm() {
		if (Random(0.0, 1.0) >= 0.95) {
			A_StartSound("FirestoneCane/Explode");
		}
		for (int i = 0; i <= 6; i++) {
			A_SpawnProjectile("FirestoneCaneProjectile", 8, Random(-6, 6), Random(-180, 180), CMF_OFFSETPITCH|CMF_TRACKOWNER, Random(-4, 4));
		}
	}
	
	States {
		Spawn:
			MDLK ABCD 4 {
				A_Look();
				A_Wander();
			}
			Loop;
			
		See:
			MDLK ABCD 4 {
				A_Chase("Melee", "Missile", CHF_FASTCHASE);
				A_FireStorm();
			}
			Loop;
			
		Melee:
		Missile:
			MDLK E 4 A_FaceTarget();
			MDLK F 4 bright A_FireSpray();
			MDLK G 1 A_MonsterRefire(130, "See");
			Goto Missile + 1;
			
		Pain:
			MDLK H 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			MDLK H 5;
			MDLK I 5 A_Scream();
			MDLK J 5;
			MDLK K 5 A_NoBlocking();
			MDLK LMNOPQRST 5;
			MDLK U 5;
			Stop;
	}
}