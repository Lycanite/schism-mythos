class Volcan : MinionCreature {
	Default {
		Tag "Volcan";
		
		Health 100;
		Radius 10;
		Height 50;
		Speed 12;
		FloatSpeed 15;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		Alpha 1;
		BloodColor "ff 66 00";

		DamageFactor "Fire", 0.1;
		DamageFactor "Lava", 0.1;
		
		SeeSound "Volcan/See";
		PainSound "Volcan/Pain";
		DeathSound "Volcan/Death";
		ActiveSound "Volcan/Idle";
		MeleeSound "Volcan/Melee";
		Obituary "%o was melted by a Volcan!";
		HitObituary "%o was melted by a Volcan!";
		
		+FLOAT;
		+DONTHARMCLASS;
	}

	override bool CanApplyEffect(class<Inventory> effectClass) {
		return effectClass != "SchismImmolateDebuff";
	}
	
	States {
		Spawn:
			VLCN AABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			VLCN DEFE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			VLCN DEFE 4 {
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			VLCN GH 4 A_FaceTarget();
			VLCN I 8 A_CreatureMelee(10, 30);
			Goto See;
		
		Missile:
			VLCN GH 10 A_FaceTarget();
			VLCN I 10 A_SpawnProjectile("MagmaProjectile", 64);
			Goto See;
			
		Pain:
			VLCN J 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			VLCN J 2;
			VLCN K 2 A_Scream();
			VLCN L 2;
			VLCN M 2 A_CreatureDie();
			VLCN NOPQRST 2;
			VLCN UVWX 2 A_FadeOut(0.1);
		Fade:
			VLCN P 1 A_FadeOut(0.1);
			Loop;
	}
}