class Cinder : MinionCreature {
	Default {
		Tag "Cinder";
		
		Health 100;
		Radius 10;
		Height 50;
		Speed 15;
		FloatSpeed 15;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		Alpha 0.8;
		BloodColor "ff 66 00";
		
		SeeSound "Cinder/See";
		PainSound "Cinder/Pain";
		DeathSound "Cinder/Death";
		ActiveSound "Cinder/Idle";
		MeleeSound "Cinder/Melee";
		Obituary "%o was immolated by a Cinder!";
		HitObituary "%o was immolated by a Cinder!";

		DamageFactor "Fire", 0.1;
		
		+FLOAT;
		+SHORTMISSILERANGE;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		self.SpawnParticle("Fire", 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
		self.SpawnParticle("FireEmber", 2 * 8, 3, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
	}
	
	action void A_Burst() {
		for (int i = 0; i < 20; i += 20) {
			invoker.SpawnParticle("Fire", 8, 1, (FRandom(-6, 6), FRandom(-6, 6), FRandom(10, 40)), i, Random(-10, 10));
			invoker.SpawnParticle("FireEmber", 2 * 8, 3, (FRandom(-6, 6), FRandom(-6, 6), FRandom(10, 40)), i, Random(-10, 10));
		}
	}

	override bool CanApplyEffect(class<Inventory> effectClass) {
		return effectClass != "SchismImmolateDebuff";
	}
	
	States {
		Spawn:
			CIND BBCCDDCC 4 bright {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			CIND EFGF 4 bright {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			CIND EFGF 4 bright {
				return A_MinionChase();
			}
			Loop;
		
		Missile:
			CIND H 20 bright A_FaceTarget();
			CIND IJKJ 4 bright A_SpawnProjectile("CinderProjectile", invoker.height / 2);
			CIND G 0 bright A_MonsterRefire(10, "See");
			Goto Missile + 1;
			
		Pain:
			CIND L 6 bright A_Pain();
			Goto See;
			
		Death:
		XDeath:
			CIND L 5 bright;
			CIND M 5 bright A_Scream();
			CIND N 5 bright;
			CIND O 5 bright A_NoBlocking();
			CIND PQRST 5 bright;
			CIND U 5 bright A_Burst();
		Fade:
			CIND U 1 bright A_FadeOut(0.1);
			Loop;
	}
}

class CinderProjectile : FlamethrowerProjectile {
	Default {
		Radius 6;
		Height 6;
		Speed 6;
		DamageFunction 1;
		Scale 1;

		SchismProjectile.ParticleCount 1;
		SchismProjectile.ParticleInterval 4;
		SchismProjectile.KnockbackStrength 0;
	}
}