class Jengu : MinionCreature {
	Default {
		Tag "Jengu";
		
		Health 150;
		Radius 10;
		Height 70;
		Speed 15;
		FloatSpeed 15;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		Alpha 0.8;
		BloodColor "00 66 ff";

		DamageFactor "Water", 0.1;
		
		SeeSound "Jengu/See";
		PainSound "Jengu/Pain";
		DeathSound "Jengu/Death";
		ActiveSound "Jengu/Idle";
		MeleeSound "Jengu/Melee";
		Obituary "%o was washed away by a Jengu!";
		HitObituary "%o was washed away by a Jengu!";
		
		+FLOAT;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		A_SpawnItemEx("JenguTrail", random(-2, -8), random(8, -8), random(8, -8), 0, 0, frandom(0, 0.6), 0, SXF_NOCHECKPOSITION|SXF_CLIENTSIDE);
	}
	
	action void A_Burst() {
		for (int i = 0; i < 20; i += 20) {
			invoker.A_SpawnItemEx("JenguTrail", 0, 0, 10, random(-6, 6), random(-6, 6), random(10, 40), i, SXF_CLIENTSIDE);
		}
	}
	
	States {
		Spawn:
			JENG AAAABBBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			JENG CDED 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			JENG CDED 4 {
				return A_MinionChase();
			}
			Loop;
		
		Missile:
			JENG F 20 A_FaceTarget();
			JENG G 4 bright A_SpawnProjectile("JenguProjectile", 64);
			JENG G 4 bright;
			Goto See;
			
		Pain:
			JENG H 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			JENG H 5;
			JENG I 5 A_Scream();
			JENG J 5;
			JENG K 5 A_NoBlocking();
			JENG LM 5;
			JENG N 5 A_Burst();
		Fade:
			JENG N 1 bright A_FadeOut(0.1);
			Loop;
	}
}

class JenguProjectile : SchismProjectile {
	Default {
		Radius 6;
		Height 6;
		Speed 6;
		Damage 5;
		Scale 2;
		SeeSound "Jengu/Projectile/Fire";
		DeathSound "Jengu/Projectile/Explode";

		SchismProjectile.ParticleName "Water";
		SchismProjectile.ParticleCount 3;
		SchismProjectile.StatusEffectClass "SchismSoakDebuff";
		SchismProjectile.StatusEffectDuration 3 * 35;
		SchismProjectile.KnockbackStrength 20;
		
		PROJECTILE;
	}
	
	States {
		Spawn:
			JENG OP 2;
			Loop;
		Death:
			JENG QRSTU 6;
			Stop;
	}
}

class JenguTrail : Actor {
	Default {
		Scale 0.3;
		Radius 1;
		Height 1;
		Scale 1;
		Speed 0;
		Renderstyle "Translucent";
		Alpha 0.25;
		
		PROJECTILE;
		+NOCLIP;
	}
	
	states {
	Spawn:
		JENG QRSTU 4;
		Stop;
	}
}