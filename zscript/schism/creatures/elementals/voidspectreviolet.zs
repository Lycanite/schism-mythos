class VoidSpectreViolet : VoidSpectre {
	Default {
		Tag "Violet Spectre";
	}
	
	States {
		Spawn:
			SPTV ABCB 4 A_MinionIdle();
			Loop;
			
		See:
			SPTV ABCB 4 {
				A_Chase("Melee");
			}
			Loop;
			
		Melee:
			SPTV DE 4 A_FaceTarget();
			SPTV F 4 A_CreatureMelee(10, 30);
			SPTV E 4 A_FaceTarget();
			Goto See;
			
		Pain:
			SPTV G 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			SPTV G 5;
			SPTV H 5 A_Scream();
			SPTV I 5;
			SPTV J 5 A_NoBlocking();
			SPTV KLMN 5;
			Stop;
	}
}