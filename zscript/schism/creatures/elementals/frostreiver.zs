class FrostReiver : MinionCreature {
	Default {
		Tag "Reiver";
		
		Health 100;
		Radius 10;
		Height 50;
		Speed 15;
		FloatSpeed 15;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		Alpha 1;
		BloodColor "66 bb ff";
		
		SeeSound "FrostReiver/See";
		PainSound "FrostReiver/Pain";
		DeathSound "FrostReiver/Death";
		ActiveSound "FrostReiver/Idle";
		MeleeSound "FrostReiver/Melee";
		Obituary "%o was put into deep freeze by a Reiver!";
		HitObituary "%o was put into deep freeze by a Reiver!";

		DamageFactor "Ice", 0.1;
		
		+FLOAT;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		self.SpawnParticle("Frost", 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
		self.SpawnParticle("Snowflake", 2 * 8, 3, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
	}
	
	action void A_ParticleBurst() {
		for (int i = 0; i < 20; i += 20) {
			invoker.SpawnParticle("Frost", 8, 1, (FRandom(-6, 6), FRandom(-6, 6), FRandom(10, 40)), i, Random(-10, 10));
			invoker.SpawnParticle("Snowflake", 2 * 8, 3, (FRandom(-6, 6), FRandom(-6, 6), FRandom(10, 40)), i, Random(-10, 10));
		}
	}
	
	States {
		Spawn:
			FREV BBCCDDCC 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			FREV EFGF 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			FREV EFGF 4 {
				return A_MinionChase();
			}
			Loop;
		
		Missile:
			FREV H 20 A_FaceTarget();
			FREV I 8 A_FaceTarget();
			FREV J 8 A_SpawnProjectile("FrostshardProjectile", invoker.height / 2);
			FREV KJ 8 A_FaceTarget();
			FREV G 8 A_MonsterRefire(10, "See");
			Goto Missile;
			
		Pain:
			FREV L 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			FREV L 5;
			FREV M 5 A_Scream();
			FREV N 5;
			FREV O 5 A_NoBlocking();
			FREV PQRST 5;
			FREV U 5 A_ParticleBurst();
		Fade:
			FREV U 1 A_FadeOut(0.1);
			Loop;
	}
}