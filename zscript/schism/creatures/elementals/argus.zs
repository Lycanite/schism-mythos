class Argus : ChaosMinion {
	Default {
		Tag "Argus";

		Health 200;
		Radius 14;
		Height 64;
		Speed 20;
		FloatSpeed 20;
		Scale 0.5;
		Gravity 0;
		PainChance 250;
		Damage 2;
		BloodColor "ff 00 ff";

		DamageFactor "Chaos", 0.1;

		MinionCreature.SummonTime 30 * 35;

		+FLOAT;
		
		SeeSound "Argus/See";
		PainSound "Argus/Pain";
		DeathSound "Argus/Death";
		ActiveSound "Argus/See";
		MeleeSound "Argus/Melee";
		Obituary "%o was destroyed by an Argus!";
		HitObituary "%o was destroyed by an Argus!";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		burstEnergy = 0;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (self.health > 0) {
			self.SpawnParticle("ChaosSpark", 8, 2, (FRandom(-8, 8), FRandom(-8, 8), FRandom(32, 40)), Random(0, 200));
		}
	}
	
	States {
		Spawn:
			ARGU ABCDE 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			ARGU ABCDE 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			ARGU ABCDE 4 {
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			ARGU ABCDE 4 {
				A_MinionChase();
			}
			ARGU GGH 4 {
				A_MinionChase();
				A_FaceTarget();
			}
			ARGU H 4 bright {
				A_MinionChase();
				A_SpawnProjectile("MaelsphereProjectile", 32);
			}
			Loop;
			
		Melee:
		Missile:
			ARGU G 8 A_FaceTarget();
			ARGU H 8 bright A_SpawnProjectile("MaelsphereProjectile", 32);
			Goto See;
			
		Pain:
			ARGU F 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			ARGU F 5;
			ARGU I 5 A_Scream();
			ARGU J 5;
			ARGU K 5 A_NoBlocking();
			ARGU LMNOPQRST 5;
			ARGU U 5;
			Stop;
	}
}