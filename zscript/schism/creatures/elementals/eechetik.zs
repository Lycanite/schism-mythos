class Eechetik : MinionCreature {
	Default {
		Health 200;
		Radius 10;
		Height 70;
		Speed 26;
		Mass 500;
		FloatSpeed 10;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		BloodColor "22 FF 55";

		DamageFactor "Poison", 0.1;

		MinionCreature.SummonTime 30 * 35;

		+FLOAT;
		+MinionCreature.TAUNT;
		
		SeeSound "Eechetik/See";
		PainSound "Eechetik/Pain";
		DeathSound "Eechetik/Death";
		ActiveSound "Eechetik/Idle";
		MeleeSound "Eechetik/Melee";
		Obituary "%o was plagued by an Eechetik!";
		HitObituary "%o was plagued by an Eechetik!";
	}

	override bool CanApplyEffect(class<Inventory> effectClass) {
		return effectClass != "SchismPlagueDebuff";
	}
	
	States {
		Spawn:
			EECH ABCB 4 {
				A_Look();
			}
			Loop;
			
		See:
			EECH DEFE 4 {
				A_Chase("Melee");
			}
			Loop;
			
		Melee:
			EECH GG 4 A_FaceTarget();
			EECH H 8 A_CreatureMelee(5, 10, "PoisonBubble", "Poison", "SchismPlagueDebuff", 5 * TICRATE, "SchismPlagueDebuff", 3);
			EECH II 4 A_FaceTarget();
			Goto See;
			
		Pain:
			EECH K 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			EECH K 5;
			EECH L 5 A_Scream();
			EECH M 5;
			EECH N 5 A_CreatureDie();
			EECH OPQ 5;
			EECH R -1;
			Stop;
	}
}