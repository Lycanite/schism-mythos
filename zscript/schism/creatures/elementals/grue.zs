class Grue : MinionCreature {
	Default {
		Health 200;
		Radius 10;
		Height 70;
		Speed 30;
		FloatSpeed 15;
		Scale 0.5;
		Gravity 0;
		PainChance 120;
		Damage 1;
		BloodColor "44 00 44";

		DamageFactor "Shadow", 0.1;

		MinionCreature.SummonTime 30 * 35;

		+FLOAT;
		+MinionCreature.TAUNT;
		
		SeeSound "Grue/See";
		PainSound "Grue/Pain";
		DeathSound "Grue/Death";
		ActiveSound "Grue/See";
		MeleeSound "Grue/Melee";
		Obituary "%o was eaten by a Grue!";
		HitObituary "%o was eaten by a Grue!";
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (self.health <= 0) {
			return;
		}
		self.SpawnParticle("Shadow", 8, 1, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10), 0.25);
	}
	
	States {
		Spawn:
			GRUE ABCB 4 {
				A_Look();
			}
			Loop;
			
		See:
			GRUE ABCB 4 {
				A_Chase("Melee");
			}
			Loop;
			
		Melee:
			GRUE DD 4 A_FaceTarget();
			GRUE B 8 A_CreatureMelee(10, 30);
			Goto See;
			
		Pain:
			GRUE E 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			GRUE E 5;
			GRUE F 5 A_Scream();
			GRUE G 5;
			GRUE H 5 A_NoBlocking();
			GRUE IJKLMN 5;
			Stop;
	}
}