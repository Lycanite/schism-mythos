class GrueVerdant : Grue {
	States {
		Spawn:
			GRUV ABCB 4 {
				A_Look();
				A_Wander();
			}
			Loop;
			
		See:
			GRUV ABCB 4 {
				A_Chase("Melee");
			}
			Loop;
			
		Melee:
			GRUV DD 4 A_FaceTarget();
			GRUV B 8 A_CreatureMelee(10, 30);
			Goto See;
			
		Pain:
			GRUV E 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Death:
		XDeath:
			GRUV E 5;
			GRUV F 5 A_Scream();
			GRUV G 5;
			GRUV H 5 A_NoBlocking();
			GRUV IJKLMN 5;
			Stop;
	}
}