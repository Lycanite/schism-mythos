class ClinkN : Creature {
	Default {
		Tag "Clink";
		
		Health 150;
		Radius 20;
		Height 64;
		Speed 14;
		FloatSpeed 14;
		Scale 1;
		Gravity 1;
		PainChance 120;
		Damage 1;
		Alpha 1;
		BloodColor "ff 66 00";
		
		SeeSound "Clink/See";
		PainSound "Clink/Pain";
		DeathSound "Clink/Death";
		ActiveSound "Clink/Idle";
		MeleeSound "Clink/Melee";
		Obituary "%o was sliced by a Clink!";
		HitObituary "%o was sliced by a Clink!";
	}
	
	States {
		Spawn:
			CLNK AB 10 A_Look();
			Loop;

		See:
			CLNK ABCD 3 A_Chase();
			Loop;

		Melee:
			CLNK E 5 A_FaceTarget();
			CLNK F 4 A_FaceTarget();
			CLNK G 7 A_CustomMeleeAttack(Random(3, 9), "Clink/Melee", "Clink/Melee");
			Goto See;

		Missile:
			CLNK E 5 A_FaceTarget();
			CLNK F 4 A_FaceTarget();
			CLNK G 7 A_SpawnProjectile("ThrowingScytheProjectile", invoker.height / 2);
			Goto See;

		Pain:
			CLNK H 3;
			CLNK H 3 A_Pain();
			Goto See;

		Death:
		XDeath:
			CLNK IJ 6;
			CLNK K 5 A_Scream();
			CLNK L 5 A_CreatureDie();
			CLNK MN 5;
			CLNK O -1;
			Stop;
	}
}