class Abtu : Creature {
	Default {
		Health 40;
		Radius 10;
		Height 24;
		Speed 24;
		FloatSpeed 24;
		Scale 0.5;
		PainChance 250;
		Damage 2;
		BloodColor "ff 00 00";

		+FLOORCLIP;
		+JUMPDOWN;

		-Creature.WALKER;
		+Creature.SWIMMER;
		
		SeeSound "Abtu/See";
		ActiveSound "Abtu/Idle";
		PainSound "Abtu/Pain";
		MeleeSound "Abtu/Melee";
		DeathSound "Abtu/Death";
		Obituary "%o was consumed by an Abtu!";
		HitObituary "%o was consumed by an Abtu!";
	}
	
	States {
		Spawn:
			ABTU ABCBADED 4 {
				A_Look();
			}
			Loop;
			
		See:
			ABTU A 4 {
				A_Chase();
				return A_Jump(127, "Jump");
			}
			ABTU BCBADED 4 {
				return A_Chase();
			}
			Loop;
			
		Melee:
			ABTU B 4 A_FaceTarget();
			ABTU C 4 A_CreatureMelee(4);
			ABTU ED 4;
			Goto See;
			
		Pain:
			ABTU I 6;
			ABTU I 6 A_Pain();
			Goto See;
			
		Pain.SelfDamage:
			Goto See;
			
		Jump:
			ABTU F 6 {
                ThrustThingZ(0, 12, 0, 1);
            }
			ABTU GH 6;
			Goto See;
			
		Death:
		XDeath:
			ABTU I 5;
			ABTU J 5 A_Scream();
			ABTU K 5;
			ABTU L 5 A_CreatureDie();
			ABTU M -1;
			Stop;
	}
}
