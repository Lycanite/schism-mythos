class Wadjet : ChaosMinion {
	int portalOpened; // Tracks portal lifetime, refreshed on foul burst. At 0 the portal is considered closed.
	WadjetPortal currentWadjetPortal;
	Wadjet entangledToWadjet; // The wadject to warp actors to.
	Wadjet entangledFromWadjet; // The wadjet that actors will warp to this wadjet from.
	vector3 summonPos; // The position that this wadject will warp actors to.

	float teleportRange;

	Property TeleportRange: teleportRange;

	Default {
		Tag "Wadjet";

		Health 600;
		Radius 14;
		Height 64;
		Mass 1000;
		Speed 14;
		Scale 0.5;
		PainChance 50;
		Damage 2;
		BloodColor "99 99 99";

		DamageFactor "Chaos", 0.5;

		Wadjet.TeleportRange 80;

		MinionCreature.SummonTime 25 * 35;
		MinionCreature.Stance 2;
		
		SeeSound "Wadjet/See";
		PainSound "Wadjet/Pain";
		DeathSound "Wadjet/Death";
		ActiveSound "Wadjet/See";
		MeleeSound "Wadjet/Melee";
		Obituary "%o was evicerated by a Wadjet!";
		HitObituary "%o was evicerated by a Wadjet!";
	}
	
	override void Die(Actor source, Actor inflictor, int dmgflags) {
		super.Die(source, inflictor, dmgflags);
		self.UntangleWadjet();
	}
	
	override void OnDestroy() {
		super.OnDestroy();
		self.UntangleWadjet();
	}

	override void PostBeginPlay() {
		self.EntangleWadjet();
		super.PostBeginPlay();
	}

	virtual void UntangleWadjet() {
		if (self.entangledToWadjet && self.entangledToWadjet.entangledFromWadjet == self) {
			self.entangledToWadjet.entangledFromWadjet = self.entangledFromWadjet;
		}
		if (self.entangledFromWadjet && self.entangledFromWadjet.entangledToWadjet == self) {
			self.entangledFromWadjet.entangledToWadjet = self.entangledToWadjet;
		}
		self.entangledToWadjet = null;
		self.entangledFromWadjet = null;
	}

	virtual void EntangleWadjet() {
		if (!self.HasPlayerMaster()) {
			return;
		}

		Wadjet fromWadjet = Wadjet(self.GetSchismPlayerInfo().GetMinion(PlayerNumber(), self.GetClassName(), -1));
		if (fromWadjet) {
			self.entangledToWadjet = fromWadjet.entangledToWadjet;
			if (!self.entangledToWadjet) {
				self.entangledToWadjet = fromWadjet;
			}
			self.entangledToWadjet.entangledFromWadjet = self;
			fromWadjet.entangledToWadjet = self;
			self.entangledFromWadjet = fromWadjet;
		}
	}

	override state CheckMinionState() {
		return self.portalOpened > 0 ? ResolveState("Portal") : super.CheckMinionState();
	}
	
	override FollowProjectile EmitFollowProjectile(String projectileClass, int minionCount) {
		FollowProjectile followProjectile = super.EmitFollowProjectile(projectileClass, minionCount);
		if (self.entangledToWadjet) {
			self.portalOpened = 4;
		}
		return followProjectile;
	}

	action state A_WadjetPortal() {
		if (invoker.portalOpened-- <= 0) {
			invoker.currentWadjetPortal = null;
			return ResolveState("Spawn");
		}

		// Create Portal:
		if (!invoker.currentWadjetPortal) {
			bool portalSpawned;
			Actor portalActor;
			[portalSpawned, portalActor] = invoker.A_SpawnItemEx("WadjetPortal", 0, 0, invoker.height + 60, 0, 0, 0, 0, SXF_SETMASTER|SXF_NOPOINTERS);
			invoker.currentWadjetPortal = WadjetPortal(portalActor);
			invoker.A_StartSound("EntropicState/Summon");
		}

		// Teleport Allies:
		if (invoker.entangledToWadjet && invoker.entangledToWadjet.health > 0) {
			BlockThingsIterator iter = BlockThingsIterator.Create(invoker, invoker.teleportRange * 2, false);
			while (iter.Next()) {
				Actor target = iter.thing;
				if (!target || target.bCorpse || Wadjet(target) || !SchismInteraction.CanHelpTarget(invoker, target)) {
					continue; // Ignore removed, corpse, wadjet or unhelpable targets.
				}
				if (target.Distance3D(invoker) > invoker.teleportRange) { // More accurate distance check.
					continue;
				}
				if (target.CountInv("SchismEntropicEffect") > 0) { // Don't warp entropic targets.
					continue;
				}
				invoker.tracer = target; // Tracer is used as the A_Warp target.
				state successState;
				bool warpSuccess;
				[successState, warpSuccess] = invoker.A_Warp(
					AAPTR_TRACER,
					invoker.entangledToWadjet.summonPos.x,
					invoker.entangledToWadjet.summonPos.y,
					invoker.entangledToWadjet.summonPos.z,
					0,
					WARPF_ABSOLUTEPOSITION|WARPF_NOCHECKPOSITION|WARPF_MOVEPTR
				);
				if (warpSuccess) {
					SchismStatusEffects.InflictEffect("SchismEntropicEffect", invoker, invoker, target, 3 * TICRATE);
					target.A_StartSound("EntropicState/Teleport");
				}
			}
		}

		return null;
	}
	
	States {
		Spawn:
			WADJ AABBCCBB 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			WADJ AABBCCBB 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			WADJ DEFE 4 {
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			WADJ DEFE 4 {
				return A_MinionChase();
			}
			Loop;
			
		Portal:
			WADJ GGHHII 4 {
				return A_WadjetPortal();
			}
			Loop;
			
		Melee:
			WADJ J 8 A_FaceTarget();
			WADJ K 4 A_CreatureMelee(25, 45);
			WADJ LK 4;
			Goto See;
			
		Pain:
			WADJ M 6;
			WADJ M 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			WADJ N 8;
			WADJ O 8 A_Scream();
			WADJ PQ 8;
			WADJ R 8 A_CreatureDie();
			WADJ STUVW 8;
			WADJ X -1;
			Stop;
	}
}

class WadjetPortal : Actor {
	mixin SpawnParticleMixin;
	
	Default {
		Radius 16;
		Height 80;
		Scale 1;
		RenderStyle "add";
		
		-SOLID
		+NOGRAVITY
	}
	
	action state A_WadjetPortal() {
		invoker.A_StartSound("EntropicState/Active", CHAN_BODY, CHANF_LOOPING);
		invoker.SpawnParticle("ChaosSpark", 8, 2, (FRandom(-64, 64), FRandom(-64, 64), FRandom(-8, 88)), Random(0, 200));

		if (!invoker.master || invoker.master.health <= 0) {
			return ResolveState("Death");
		}
		
		Wadjet wadjetMaster = Wadjet(invoker.master);
		if (wadjetMaster && wadjetMaster.currentWadjetPortal != self) {
			return ResolveState("Death");
		}
		return null;
	}
	
	States {
		Spawn:
			REIP EFGH 2 bright {
				return A_WadjetPortal();
			}
			Loop;
			
		Death:
			REIP EEFFGGHH 1 A_FadeOut(0.025);
			Stop;
	}
}