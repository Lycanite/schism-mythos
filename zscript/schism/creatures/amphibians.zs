// ========== Creatures ==========
// Melee Light:
#include "zscript/schism/creatures/amphibians/miredon.zs"
#include "zscript/schism/creatures/amphibians/aglebemu.zs"
#include "zscript/schism/creatures/amphibians/aglebemutadpole.zs"
#include "zscript/schism/creatures/amphibians/salamander.zs"