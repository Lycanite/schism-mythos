class Jouste : MinionCreature {
	Default {
		Health 150;
		Radius 18;
		Height 52;
		Speed 24;
		Scale 0.75;
		PainChance 120;
		Damage 2;
		BloodColor "ff 88 22";
		
		SeeSound "Jouste/See";
		PainSound "Jouste/Pain";
		DeathSound "Jouste/Death";
		ActiveSound "Jouste/Idle";
		MeleeSound "Jouste/Melee";
		Obituary "%o was pierced by a Jouste!";
		HitObituary "%o was pierced by a Jouste!";
	}
	
	States {
		Spawn:
			JOUS AABBCCBB 4 {
				A_Look();
				return A_StayWithMaster();
			}
			Loop;
			
		Follow:
			JOUS DEFE 4 {
				A_Look();
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			JOUS DEF 4 {
				return A_MinionChase();
			}
			JOUS E 4 {
				A_StartSound("Jouste/Step");
				return A_MinionChase();
			}
			Loop;
			
		Melee:
			JOUS G 4 A_FaceTarget();
			JOUS H 4 A_CreatureMelee(4);
			JOUS I 4;
			Goto See;
			
		Pain:
			JOUS J 6;
			JOUS J 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			JOUS KL 5;
			JOUS M 5 A_Scream();
			JOUS N 5;
			JOUS P 5 A_CreatureDie();
			JOUS PQRSTUVW 5;
			JOUS X -1;
			Stop;
	}
}
