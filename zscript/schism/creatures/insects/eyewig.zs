class Eyewig : Creature {
	Default {
		Tag "Eyewig";
		
		Health 400;
		Radius 20;
		Height 64;
		Speed 14;
		FloatSpeed 14;
		Scale 0.6;
		Gravity 1;
		PainChance 120;
		Damage 1;
		Alpha 1;
		BloodColor "22 FF 55";

		DamageFactor "Poison", 0.1;
		
		SeeSound "Eyewig/See";
		PainSound "Eyewig/Pain";
		DeathSound "Eyewig/Death";
		ActiveSound "Eyewig/Idle";
		MeleeSound "Eyewig/Melee";
		Obituary "%o was pinced by an Eyewig!";
		HitObituary "%o was pinced by an Eyewig!";
	}
	
	States {
		Spawn:
			EYEW ABCB 10 A_Look();
			Loop;

		See:
			EYEW DEFE 3 A_Chase();
			Loop;

		Melee:
			EYEW GH 5 A_FaceTarget();
			EYEW I 7 A_CreatureMelee(3, 9, "PoisonBubble", "Poison");
			Goto See;

		Missile:
			EYEW JK 5 A_FaceTarget();
			EYEW LM 8 {
				A_FaceTarget();
				A_SpawnProjectile("EyewigProjectile", invoker.height / 2);
			}
			EYEW M 0 A_MonsterRefire(10, "See");
			Goto Missile + 2;

		Pain:
			EYEW N 3;
			EYEW N 3 A_Pain();
			Goto See;

		Death:
		XDeath:
			EYEW NO 6;
			EYEW P 5 A_Scream();
			EYEW Q 5 A_CreatureDie();
			EYEW RS 5;
			EYEW T -1;
			Stop;
	}
}

class EyewigProjectile : PoisonSpatterProjectile {
	Default {
		Radius 6;
		Height 6;
		Speed 20;
		DamageFunction 1;
		Scale 1;

		RenderStyle "Add";

		SchismProjectile.ParticleCount 1;
		SchismProjectile.ParticleInterval 4;
		SchismProjectile.KnockbackStrength 0;
	}
}