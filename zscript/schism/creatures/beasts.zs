// ========== Creatures ==========
// Melee Light:
#include "zscript/schism/creatures/beasts/wraamon.zs"
#include "zscript/schism/creatures/beasts/wraamonverdant.zs"
#include "zscript/schism/creatures/beasts/balayang.zs"

// Melee Medium:
#include "zscript/schism/creatures/beasts/chupacabra.zs"
#include "zscript/schism/creatures/beasts/chupacabraphosphor.zs"