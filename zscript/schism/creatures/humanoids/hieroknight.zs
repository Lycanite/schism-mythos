class Hieroknight : Creature {
	Default {
		Tag "Hieroknight";
		Health 80;
		Radius 20;
		Height 56;
		Mass 100;
		Speed 20;
		FloatSpeed 4;
		Scale 1;
		Gravity 1;
		PainChance 170;
		DamageFunction 2;
		BloodColor "ff 00 00";

		MONSTER;
		+FLOORCLIP;
		
		SeeSound "Hieroknight/See";
		PainSound "Hieroknight/Pain";
		DeathSound "Hieroknight/Death";
		ActiveSound "Hieroknight/See";
		MeleeSound "Hieroknight/Melee";
		Obituary "%o was slain by a Hieroknight!";
		HitObituary "%o was slain by a Hieroknight!";
	}
	
	action void A_MeleeAttack() {
		invoker.A_CustomMeleeAttack(random(10, 30), "Hieroknight/Melee");
		for (int i = 0; i < 5; i++) {
			invoker.SpawnParticle("Order", 2 * TICRATE, 3, (FRandom(-8, 8), FRandom(-8, 8), FRandom(10, 40)), Random(45, 315), Random(-10, 10));
		}
	}
	
	States {
		Spawn:
			HIEK A 4 A_Look();
			Loop;
			
		See:
			HIEK A 4 {
				A_AlertMonsters(0, AMF_TARGETNONPLAYER);
				A_Chase("Melee", "Missile");
			}
			HIEK BC 4 A_Chase("Melee", "Missile");
			HIEK D 4 {
				if (FRandom(0, 1) >= 0.75) {
					A_Leap(Random(10, 20), Random(10, 30));
				}
				A_Chase("Melee", "Missile");
			}
			Loop;
			
		Melee:
			HIEK EF 4 A_FaceTarget();
			HIEK G 4 A_MeleeAttack();
			Goto See;
			
		Pain:
			HIEK H 3;
			HIEK H 3 A_Pain();
			Goto See;
			
		Death:
			HIEK H 5;
			HIEK I 5 A_Scream();
			HIEK J 5;
			HIEK K 5 A_NoBlocking();
			HIEK LMNOP 5;
			HIEK Q -1;
			Stop;
			
		XDeath:
			HIEK R 5;
			HIEK S 5 A_XScream();
			HIEK T 5;
			HIEK U 5 A_NoBlocking();
			HIEK VWXY 5;
			HIEK Z -1;
			Stop;
			
		Raise:
			HIEK MLKJIHG 5;
			Goto See;
	}
}