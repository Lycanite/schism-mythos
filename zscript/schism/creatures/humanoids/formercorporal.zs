class FormerCorporal : FormerPrivate {
	Default {
		Tag "Former Corporal";
		Health 30;
		PainChance 170;

        DropItem "Clip";

		AttackSound "FormerCorporal/Fire";
		Obituary "%o was put out of commision by a Former Corporal!";
    }

    States {
        Spawn:
			FCPL AB 10 A_Look();
			Loop;
			
		See:
			FCPL AABBCCDD 4 A_Chase();
			Loop;
			
		Missile:
			FCPL E 10 A_FaceTarget();
			FCPL FEF 2 A_CreatureHitscan(1, 5, 3);
            FCPL E 8;
			Goto See;
			
		Pain:
			FCPL G 3;
			FCPL G 3 A_Pain();
			Goto See;
			
		Death:
			FCPL H 5;
			FCPL I 5 A_Scream();
			FCPL J 5 A_CreatureDie();
			FCPL K 5;
			FCPL L -1;
			Stop;
			
		XDeath:
			FCPL M 5;
			FCPL N 5 A_Scream();
			FCPL O 5 A_CreatureDie();
			FCPL PQRST 5;
			FCPL U -1;
			Stop;
			
		Raise:
			FCPL KJIH 5;
			Goto See;
    }
}