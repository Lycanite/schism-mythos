class FormerBrigadier : FormerMajor {
	Default {
		Tag "Former Brigadier";

        DropItem "Plasmarifle";

		AttackSound "FormerBrigadier/Fire";
		Obituary "%o was put out of commision by a Former Brigadier!";
    }

    States {
        Spawn:
			FBRG AB 10 A_Look();
			Loop;
			
		See:
			FBRG AABBCCDD 4 A_Chase();
			Loop;
			
		Missile:
			FBRG E 10 A_FaceTarget();
			FBRG FE 4 A_CreatureHitscan(1, 2, 15, 43, 0, "PlasmaPuff");
            FBRG F 1 A_MonsterRefire(40, "See");
			Goto Missile + 1;
			
		Pain:
			FBRG G 3;
			FBRG G 3 A_Pain();
			Goto See;
			
		Death:
			FBRG H 5;
			FBRG I 5 A_Scream();
			FBRG J 5 A_CreatureDie();
			FBRG K 5;
			FBRG L -1;
			Stop;
			
		XDeath:
			FBRG O 5;
			FBRG P 5 A_Scream();
			FBRG Q 5 A_CreatureDie();
			FBRG RSTU 5;
			FBRG V -1;
			Stop;
			
		Raise:
			FBRG KJIH 5;
			Goto See;
    }
}