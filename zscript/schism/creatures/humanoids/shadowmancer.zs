class Shadowmancer : Actor {
	Default {
		Tag "Shadowmancer";
		Health 50;
		Radius 20;
		Height 56;
		Mass 100;
		Speed 8;
		FloatSpeed 4;
		Scale 0.8;
		Gravity 1;
		PainChance 170;
		DamageFunction 2;
		BloodColor "ff 00 00";

		MONSTER;
		+FLOORCLIP;
		
		SeeSound "Shadowmancer/See";
		PainSound "Shadowmancer/Pain";
		DeathSound "Shadowmancer/Death";
		ActiveSound "Shadowmancer/See";
		MeleeSound "Shadowmancer/Melee";
		Obituary "%o was killed by a Shadowmancer!";
		HitObituary "%o was killed by a Shadowmancer!";
	}
	
	action void ShadowShock() {
		A_StartSound("EbonCommand/Fire", CHAN_WEAPON);
		for (int i = 0; i <= 3; i++) {
			A_SpawnProjectile("EbonCommandProjectile", random(-20, 20), random(-5, 5), random(-30, 30));
		}
	}
	
	States {
		Spawn:
			SHDM A 4 A_Look();
			Loop;
			
		See:
			SHDM ABCD 4 A_Chase("Melee", "Missile");
			Loop;
			
		Melee:
		Missile:
			SHDM E 8 A_FaceTarget();
			SHDM F 8 bright ShadowShock();
			Goto See;
			
		Pain:
			SHDM G 3;
			SHDM G 3 A_Pain();
			Goto See;
			
		Death:
			SHDM G 5;
			SHDM H 5 A_Scream();
			SHDM I 5;
			SHDM J 5 A_NoBlocking();
			SHDM KLM 5;
			SHDM N -1;
			Stop;
			
		XDeath:
			SHDM O 5;
			SHDM P 5 A_XScream();
			SHDM Q 5;
			SHDM R 5 A_NoBlocking();
			SHDM STUVW 5;
			SHDM X -1;
			Stop;
			
		Raise:
			SHDM NMLKJIHG 5;
			Goto See;
	}
}