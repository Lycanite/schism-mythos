class FormerMajor : FormerPrivate {
	Default {
		Tag "Former Major";
		Health 70;
		PainChance 170;

        DropItem "Chaingun";

		AttackSound "FormerMajor/Fire";
		Obituary "%o was mowed down by a Former Major!";
    }

    States {
        Spawn:
			FMAJ AB 10 A_Look();
			Loop;
			
		See:
			FMAJ AABBCCDD 4 A_Chase();
			Loop;
			
		Missile:
			FMAJ E 10 A_FaceTarget();
			FMAJ FE 4 A_CreatureHitscan(5, 15, 1);
            FMAJ F 1 A_MonsterRefire(40, "See");
			Goto Missile + 1;
			
		Pain:
			FMAJ G 3;
			FMAJ G 3 A_Pain();
			Goto See;
			
		Death:
			FMAJ H 5;
			FMAJ I 5 A_Scream();
			FMAJ J 5 A_CreatureDie();
			FMAJ KLMNO 5;
			FMAJ P -1;
			Stop;
			
		XDeath:
			FMAJ Q 5;
			FMAJ R 5 A_Scream();
			FMAJ S 5 A_CreatureDie();
			FMAJ STUVWX 5;
			FMAJ Y -1;
			Stop;
			
		Raise:
			FMAJ ONMLKJIH 5;
			Goto See;
    }
}