class SchismPeasant : Actor {
	bool user_canWander;
	bool user_canFight;
	
	Default {
		Tag "Peasant";
		Health 25;
		Radius 20;
		Height 56;
		Mass 100;
		Speed 8;
		FloatSpeed 4;
		Scale 1;
		Gravity 1;
		PainChance 170;
		DamageFunction 2;
		BloodColor "ff 00 00";

		MONSTER;
		+FLOORCLIP;
		
		SeeSound "Peasant/See";
		PainSound "Peasant/Pain";
		DeathSound "Peasant/Death";
		ActiveSound "Peasant/See";
		MeleeSound "Peasant/Melee";
		Obituary "%o was killed by a lowly Peasant!";
		HitObituary "%o was killed by a lowly Peasant!";
	}
	
	action state A_PeasantIdle() {
		if (invoker.user_canWander) {
			return ResolveState("See");
		}
		if (invoker.user_canFight) {
			A_Look();
		}
		return null;
	}
	
	action void A_PeasantUpdate() {
		if (invoker.user_canFight) {
			A_Chase("Melee");
		}
		else if (invoker.user_canWander) {
			A_Wander();
		}
	}
	
	action void A_MeleeAttack() {
		A_CustomMeleeAttack(random(5, 10), "Peasant/Melee");
	}
	
	States {
		Spawn:
			PEAS A 4 { return A_PeasantIdle(); }
			Loop;
			
		See:
			PEAS ABCD 4 A_PeasantUpdate();
			Loop;
			
		Melee:
			DRGR E 6 A_FaceTarget();
			DRGR F 4 A_MeleeAttack();
			Goto See;
			
		Pain:
			PEAS O 3 A_AlertMonsters(0, AMF_TARGETNONPLAYER);
			PEAS O 3 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			PEAS O 5;
			PEAS G 5 A_Scream();
			PEAS H 5;
			PEAS I 5 A_NoBlocking();
			PEAS JKLM 5;
			PEAS N -1;
			Stop;
	}
}

class SchismPeasant2 : SchismPeasant {
	States {
		Spawn:
			THUG A 4 { return A_PeasantIdle(); }
			Loop;
			
		See:
			THUG ABCD 4 A_PeasantUpdate();
			Loop;
			
		Melee:
			THUG E 6 A_FaceTarget();
			THUG F 4 A_MeleeAttack();
			Goto See;
			
		Pain:
			THUG O 3;
			THUG O 3 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			THUG O 5;
			THUG G 5 A_Scream();
			THUG H 5;
			THUG I 5 A_NoBlocking();
			THUG JKLM 5;
			THUG N -1;
			Stop;
	}
}