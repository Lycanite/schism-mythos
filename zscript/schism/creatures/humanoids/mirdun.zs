class Mirdun : Hieromancer {
	Default {
		Tag "Mirdun";
		Health 5000;
		Radius 20;
		Height 120;
		Mass 100;
		Speed 8;
		FloatSpeed 4;
		Scale 1.5;
		Gravity 1;
		PainChance 5;
		DamageFunction 2;
		BloodColor "ff 00 00";

		+BOSS;
		+MISSILEMORE;
		
		SeeSound "Mirdun/See";
		PainSound "Mirdun/Pain";
		DeathSound "Mirdun/Death";
		ActiveSound "Mirdun/See";
		MeleeSound "Mirdun/Melee";
		Obituary "%o was put to justice by Mirdun!";
		HitObituary "%o was put to justice by Mirdun!";
	}
	
	States {
		Spawn:
			MIRD MN 8 A_Look();
			Loop;
			
		See:
			MIRD MNOP 4 A_Chase("Melee", "Missile");
			Loop;
			
		Melee:
		Missile:
			MIRD RS 4 A_FaceTarget();
			MIRD T 2 bright A_SpawnProjectile("HieroboltProjectile", 16, invoker.height * 0.75);
			MIRD T 2 bright A_SpawnProjectile("HieroboltProjectile", 16, invoker.height * 0.75);
			MIRD T 2 bright A_SpawnProjectile("HieroboltProjectile", 16, invoker.height * 0.75);
			MIRD T 2 bright A_SpawnProjectile("HieroboltProjectile", 16, invoker.height * 0.75);
			MIRD R 4;
			Goto See;
			
		Pain:
			MIRD Q 3;
			MIRD Q 3 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			MIRD G 5;
			MIRD H 5 A_Scream();
			MIRD I 5;
			MIRD J 5 A_NoBlocking();
			MIRD KL 5;
			Stop;
			
		Raise:
			MIRD ABCDEFG 5;
			Goto See;
	}
}