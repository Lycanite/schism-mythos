class Hieromancer : Actor {
	Default {
		Tag "Hieromancer";
		Health 50;
		Radius 20;
		Height 56;
		Mass 100;
		Speed 8;
		FloatSpeed 4;
		Scale 1;
		Gravity 1;
		PainChance 170;
		DamageFunction 2;
		BloodColor "ff 00 00";

		MONSTER;
		+FLOORCLIP;
		
		SeeSound "Hieromancer/See";
		PainSound "Hieromancer/Pain";
		DeathSound "Hieromancer/Death";
		ActiveSound "Hieromancer/See";
		MeleeSound "Hieromancer/Melee";
		Obituary "%o was put to justice by a Hieromancer!";
		HitObituary "%o was put to justice by a Hieromancer!";
	}
	
	States {
		Spawn:
			HIER A 4 A_Look();
			Loop;
			
		See:
			HIER A 4 {
				A_AlertMonsters(0, AMF_TARGETNONPLAYER);
				A_Chase("Melee", "Missile");
			}
			HIER BCD 4 A_Chase("Melee", "Missile");
			Loop;
			
		Melee:
		Missile:
			HIER E 8 A_FaceTarget();
			HIER F 8 bright A_SpawnProjectile("HieroboltProjectile", 16);
			Goto See;
			
		Pain:
			HIER G 3;
			HIER G 3 A_Pain();
			Goto See;
			
		Death:
			HIER G 5;
			HIER H 5 A_Scream();
			HIER I 5;
			HIER J 5 A_NoBlocking();
			HIER KL 5;
			HIER M -1;
			Stop;
			
		XDeath:
			HIER N 5;
			HIER O 5 A_XScream();
			HIER P 5;
			HIER Q 5 A_NoBlocking();
			HIER RSTUV 5;
			HIER W -1;
			Stop;
			
		Raise:
			HIER MLKJIHG 5;
			Goto See;
	}
}