class AnzuAsnarax : Creature {
	Default {
		Tag "Anzu Asnarax";
		Scale 1;
		Health 4000;
		Radius 128;
		Height 100;
		Speed 12;
		Gravity 1;
		Mass 1000;
		PainChance 40;
		
		SeeSound "AnzuAsnarax/See";
		ActiveSound "AnzuAsnarax/Active";
		PainSound "AnzuAsnarax/Pain";
		DeathSound "AnzuAsnarax/Death";
		AttackSound "AnzuAsnarax/Fire";
		Obituary "%o was exterminated by an Anzu Asnarax!";
		HitObituary "%o was exterminated by an Anzu Asnarax!";
		
		+BOSS;
		+BOSSDEATH;
		+MISSILEMORE;
		+NORADIUSDMG;
		+DONTMORPH;
	}

	action void A_AsnaraxFire() {
		A_CreatureHitscan(5, 15, 3);
	}

	action void A_AsnaraxChromabolt() {
		for (int i = 0; i < 5; i++) {
			A_SpawnProjectile("ChromaboltProjectile", Random(20, 24), 0, Random(-2, 2));
		}
	}

	action void A_AsnaraxSmite() {
		A_SpawnProjectile("SmiteProjectile", 20, 0, 0);
		for (int i = 0; i < 20; i++) {
			A_SpawnProjectile("SmiteProjectile", 20, 0, Random(-180, 180));
		}
	}
	
	States {
		Spawn:
			AZRX A 10 A_Look();
			Loop;
			
		See:
			AZRX A 3 {
				A_Chase();
				A_StartSound("AnzuAsnarax/Step");
			}
			AZRX ABB 3 A_Chase();
			AZRX C 3 {
				A_Chase();
				A_StartSound("AnzuAsnarax/Step");
			}
			AZRX CDD 3 A_Chase();
			AZRX E 3 {
				A_Chase();
				A_StartSound("AnzuAsnarax/Step");
			}
			AZRX EFF 3 A_Chase();
			Loop;
			
		Missile:
			AZRX A 20 A_FaceTarget();
            AZRX A 0 A_Jump(256, "MissileStar", "MissileRuiner");
		MissileStar:
			AZRX H 4 bright {
				A_AsnaraxChromabolt();
				A_AsnaraxFire();
			}
			AZRX G 4 bright A_AsnaraxFire();
            AZRX G 0 A_Jump(24, "MissileRuiner");
			AZRX G 1 A_MonsterRefire(10, "See");
			Goto MissileStar;
		MissileRuiner:
			AZRX J 4;
			AZRX K 4 bright;
			AZRX LM 8 bright {
				A_AsnaraxSmite();
				A_AsnaraxFire();
			}
			AZRX G 1 A_MonsterRefire(10, "See");
			Goto MissileStar;
			
		Pain:
			AZRX I 3;
			AZRX I 3 A_Pain();
			Goto See;
			
		Death:
			AZRX N 6;
			AZRX O 6 A_Scream();
			AZRX PQ 6;
			AZRX R 6 A_CreatureDie();
			AZRX S 6;
			AZRX T -1 A_BossDeath();
			Stop;
			
		Crush:
			AZRX U -1;
			Stop;
	}
}