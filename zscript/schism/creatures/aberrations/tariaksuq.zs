class Tariaksuq : Creature {
	Default {
		Tag "Tariaksuq";
		Health 80;
		Radius 20;
		Height 56;
		Mass 100;
		Speed 12;
		PainChance 160;

		SeeSound "Tariaksuq/See";
		ActiveSound "Tariaksuq/Active";
		PainSound "Tariaksuq/Pain";
		DeathSound "Tariaksuq/Death";
		Obituary "%o didn't the Tariaksuq coming.";
	}
	
	states {
		Spawn:
			TRKQ AB 10 A_Look();
			Loop;
		
		See:
			TRKQ A 0 A_SetTranslucent(0.75,0);
			TRKQ AABBCCDD 3 A_Chase();
			Goto See+1;
		
		Missile:
			TRKQ E 0 A_SetTranslucent(1.0,0);
			TRKQ EE 4 bright A_FaceTarget;
			TRKQ F 8 bright A_SpawnProjectile("WrathOrbProjectile",32,0,0,0,0);
			TRKQ B 4 A_SetTranslucent(0.87,0);
			Goto See;
		
		Pain:
			TRKQ G 0 A_SetTranslucent(1.0,0);
			TRKQ G 2;
			TRKQ G 2 A_Pain();
			Goto See;
		
		Death:
			TRKQ H 4 A_SetTranslucent(1.0,0);
			TRKQ I 4 A_SetTranslucent(0.9,0);
			TRKQ I 0 A_Scream();
			TRKQ J 4 A_SetTranslucent(0.8,0);
			TRKQ K 0 A_CreatureDie();
			TRKQ K 4 A_SetTranslucent(0.7,0);
			TRKQ L 4 A_SetTranslucent(0.6,0);
			TRKQ M 4 A_SetTranslucent(0.5,0);
			TRKQ N 4 A_SetTranslucent(0.4,0);
			TRKQ O 4 A_SetTranslucent(0.3,0);
			TRKQ P 4 A_SetTranslucent(0.2,0);
			Stop;
	}
}