class Astaroth : MinionCreature {
	Default {
		Tag "Astaroth";
		Scale 0.5;
		Health 500;
		Radius 64;
		Height 64;
		Speed 14;
		Gravity 1;
		PainChance 128;

		MinionCreature.SummonTime 30 * 35;
		
		SeeSound "Astaroth/See";
		ActiveSound "Astaroth/Active";
		PainSound "Astaroth/Pain";
		DeathSound "Astaroth/Death";
		Obituary "%o was annihilated by an Astaroth!";
		HitObituary "%o was annihilated by an Astaroth!";
		
		+BOSSDEATH;
	}

	action void A_AstarothBurst() {
		invoker.A_SpawnMinion("Trite", (0, 50, 100), (0, 5, 10));
		invoker.A_SpawnMinion("Trite", (50, 50, 50), (5, 5, 5));
		invoker.A_SpawnMinion("Trite", (-50, 50, 50), (-5, 5, 5));
	}
	
	States {
		Spawn:
			ASTA AB 10 A_Look();
			Loop;
			
		See:
			ASTA B 20;
			ASTA C 3 A_Chase();
			ASTA D 3 {
				A_Chase();
				A_StartSound("Astaroth/Step");
			}
			ASTA DE 3 A_Chase();
			ASTA F 3 {
				A_Chase();
				A_StartSound("Astaroth/Step");
			}
			ASTA F 3 A_Chase();
			Goto See + 1;
			
		Missile:
			ASTA H 20 A_FaceTarget();
            ASTA D 0 A_Jump(256, "MissileStar", "MissileRuiner");
		MissileStar:
			ASTA G 4 bright A_SpawnProjectile("EntrostarProjectile", 64);
			ASTA H 4 bright;
			ASTA H 1 A_MonsterRefire(10, "See");
			Goto MissileStar;
		MissileRuiner:
			ASTA G 8 bright A_SpawnProjectile("RuinerBlastProjectile", 64);
			ASTA H 8 bright;
			Goto See;
			
		Pain:
			ASTA I 3;
			ASTA I 3 A_Pain();
			Goto See;
			
		Despawn:
			ASTA J 0 A_AstarothBurst();
		Death:
			ASTA J 6;
			ASTA K 6 A_Scream();
			ASTA L 6;
			ASTA M 6 {
				A_CreatureDie();
				A_AstarothBurst();
			}
			ASTA NOP 6;
			ASTA Q -1 A_BossDeath();
			Stop;
	}
}