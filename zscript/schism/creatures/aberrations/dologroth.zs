class Dologroth : Creature {	
	Default {
		Tag "Dologroth";
		Health 400;
		Radius 31;
		Height 56;
		Mass 400;
		Speed 8;
		PainChance 128;
		BloodColor "Red";
		
		SeeSound "Dologroth/See";
		ActiveSound "Dologroth/Idle";
		PainSound "Dologroth/Pain";
		DeathSound "Dologroth/Death";
		MeleeSound "Dologroth/Melee";
		Obituary "%o was consumed, both flesh and soul, by a Dologroth.";
		
		+FLOAT;
		+NOGRAVITY;
		+CREATURE.HOVERBOB;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		self.SpawnParticle("Nether", TICRATE, 1, (FRandom(-48, -32), FRandom(-8, 8), FRandom(30, 60)), Random(-10, 10), Random(-10, 10), 1);
		self.SpawnParticle("Nether", TICRATE, 1, (FRandom(32, 48), FRandom(-8, 8), FRandom(30, 60)), Random(-10, 10), Random(-10, 10), 1);
	}

	action void A_DologrothBurst() {
		A_PainDie("NetherWraith");
	}
	
	States {
		Spawn:
			// DOLG A 4 A_SpriteOffset(0, -10);
			DOLG A 4 A_Look();
			Loop;
			
		See:
			DOLG  A 2 A_Chase();
			Loop;
			
		Missile:
			DOLG B 5 A_FaceTarget();
			DOLG C 5 A_FaceTarget();
			DOLG B 5 bright {
				A_FaceTarget();
				A_DualPainAttack("NetherWraith");
			}
			Goto See;
			
		Pain:
			DOLG D 6;
			DOLG D 6 A_Pain();
			Goto See;
			
		Death:
			DOLG E 8 bright;
			DOLG F 8 bright A_Scream();
			DOLG GH 8 bright;
			DOLG I 8 bright {
				A_CreatureDie();
				A_DologrothBurst();
			}
			DOLG JKL 8;
			Stop;
	}
}