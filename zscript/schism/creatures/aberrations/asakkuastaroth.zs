class AsakkuAstaroth : Creature {
	Default {
		Tag "Asakku Astaroth";
		Health 320;
		Radius 64;
		Height 64;
		Mass 600;
		Speed 18;
		PainChance 128;
		
		SeeSound "AsakkuAstaroth/See";
		ActiveSound "AsakkuAstaroth/Active";
		PainSound "AsakkuAstaroth/Pain";
		DeathSound "AsakkuAstaroth/Death";
		Obituary "%o was destroyed by an Asakku Astaroth!";
		
		+BOSSDEATH;
	}
	States {
		Spawn:
			ASAK A 10 A_Look();
			Loop;

		See:
			ASAK A 20;
			ASAK A 3 {
				A_Chase();
				A_StartSound("AsakkuAstaroth/Step");
			}
			ASAK ABBCC 3 A_Chase();
			ASAK D 3 {
				A_Chase();
				A_StartSound("AsakkuAstaroth/Step");
			}
			ASAK DEEFF 3 A_Chase();
			Goto See + 1;

		Missile:
			ASAK A 20 bright A_FaceTarget();
			ASAK G 4 bright A_SpawnProjectile("LifestarProjectile", 20);
			ASAK H 4 bright;
			ASAK H 1 bright A_MonsterRefire(10, "See");
			Goto Missile + 1;

		Pain:
			ASAK I 3;
			ASAK I 3 A_Pain;
			Goto See+1;

		Death:
			ASAK J 20 A_Scream();
			ASAK K 7 A_CreatureDie();
			ASAK LMNO 7;
			ASAK P -1 A_BossDeath();
			Stop;

		Raise:
			ASAK P 5;
			ASAK ONMLKJ 5;
			Goto See + 1;
	}
}