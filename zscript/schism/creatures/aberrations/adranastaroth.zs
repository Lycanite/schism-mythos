class AdranAstaroth : Astaroth {
	Default {
		Tag "Adran Astaroth";
		Scale 0.5;
		Health 500;
		Radius 32;
		Height 64;
		Speed 14;
		Gravity 1;
		PainChance 128;
		BloodColor "99 00 dd";
		
		SeeSound "AdranAstaroth/See";
		ActiveSound "AdranAstaroth/Active";
		PainSound "AdranAstaroth/Pain";
		DeathSound "AdranAstaroth/Death";
		Obituary "%o was mutated by a Adran Astaroth!";
		HitObituary "%o was mutated by a Adran Astaroth!";
	}

	action void A_AdranAstarothBurst() {
		invoker.A_SpawnMinion("AdranTrite", (0, 50, 100), (0, 5, 10));
		invoker.A_SpawnMinion("AdranTrite", (50, 50, 50), (5, 5, 5));
		invoker.A_SpawnMinion("AdranTrite", (-50, 50, 50), (-5, 5, 5));
	}
	
	States {
		Spawn:
			VASR AB 10 A_Look();
			Loop;
			
		See:
			VASR B 20;
			VASR C 3 A_Chase();
			VASR D 3 {
				A_Chase();
				A_StartSound("AdranAstaroth/Step");
			}
			VASR DE 3 A_Chase();
			VASR F 3 {
				A_Chase();
				A_StartSound("AdranAstaroth/Step");
			}
			VASR F 3 A_Chase();
			Goto See + 1;
			
		Missile:
			VASR H 20 A_FaceTarget();
            VASR D 0 A_Jump(256, "MissileSplint", "MissileGrazer");
		MissileSplint:
			VASR G 4 bright A_SpawnProjectile("VoidSplintProjectile", 64);
			VASR H 4 bright;
			VASR H 1 A_MonsterRefire(10, "See");
			Goto MissileSplint;
		MissileGrazer:
			VASR G 8 bright A_SpawnProjectile("VoidGrazerProjectile", 64);
			VASR H 8 bright;
			Goto See;
			
		Pain:
			VASR I 3;
			VASR I 3 A_Pain();
			Goto See;
			
		Despawn:
			VASR J 0 A_AdranAstarothBurst();
		Death:
			VASR J 6;
			VASR K 6 A_Scream();
			VASR L 6;
			VASR M 6 {
				A_CreatureDie();
				A_AdranAstarothBurst();
			}
			VASR NOPQ 6;
			Stop;
	}
}