class Grell : Creature {
	Default {
		Tag "Grell";
		Health 500;
		Radius 31;
		Height 56;
		Mass 400;
		Speed 8;
		PainChance 128;
		BloodColor "99FF22";
		
		SeeSound "Grell/See";
		ActiveSound "Grell/Idle";
		PainSound "Grell/Pain";
		DeathSound "Grell/Death";
		MeleeSound "Grell/Melee";
		Obituary "%o was plagued by a Grell.";
		
		+DROPOFF;
		+NOGRAVITY;
		+FLOAT;
	}
		
	States {
		Spawn:
			GREL A 10 A_Look();
			Loop;
			
		See:
			GREL A 0 A_SentinelBob();
			GREL AAB 3 A_Chase();
			GREL B 0 A_SentinelBob();
			GREL BCC 3 A_Chase();
			Loop;
		
		Missile:
			GREL D 0 A_StartSound("Grell/Melee");
			GREL D 4 A_FaceTarget();
			GREL E 4 bright A_FaceTarget();
			GREL F 4 bright {
				A_SpawnProjectile("AcidBarbProjectile", invoker.height / 2, 0, 0);
				A_SpawnProjectile("AcidBarbProjectile", invoker.height / 2, -4, 10);
				A_SpawnProjectile("AcidBarbProjectile", invoker.height / 2, -4, -10);
			}
			Goto See;
		
		Pain:
			GREL G 3;
			GREL G 3 A_Pain;
			Goto See;
		
		Death:
			GREL H 5;
			GREL I 0 A_CreatureDie();
			GREL I -1 A_Scream();
		Crash:
			GREL J 4 A_StartSound("Grell/Crash", CHAN_AUTO);
			GREL KLM 4;
			GREL N -1;
			Stop;
			
		Raise:
			GREL MLKJIH 5;
			Goto See;
	}
}