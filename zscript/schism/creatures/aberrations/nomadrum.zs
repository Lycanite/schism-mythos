class Nomadrum : ChaosMinion {
	Default {
		Tag "Nomadrum";

		Health 500;
		Radius 32;
		Height 100;
		Speed 20;
		FloatSpeed 10;
		Scale 0.5;
		Gravity 0;
		PainChance 50;
		Damage 15;
		BloodColor "00 ff 00";

		DamageFactor "Chaos", 0.1;

		MinionCreature.SummonTime 120 * 35;
		ChaosMinion.BurstEnergy 0;

		+FLOAT;
		
		SeeSound "Nomadrum/See";
		PainSound "Nomadrum/Pain";
		DeathSound "Nomadrum/Death";
		ActiveSound "Nomadrum/Idle";
		MeleeSound "Nomadrum/Melee";
		Obituary "%o was destroyed by a Nomadrum!";
		HitObituary "%o was destroyed by a Nomadrum!";
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (self.health > 0) {
			self.SpawnParticle("ChaosSpark", 32, 2, (FRandom(-32, 32), FRandom(-32, 32), FRandom(32, 64)), Random(0, 259));
			self.SpawnParticle("BedlamSmoke", 8, 2, (FRandom(-32, 32), FRandom(-32, 32), FRandom(32, 64)), Random(0, 259), 0, FRandom(1, 2));
		}
	}
	
	States {
		Spawn:
		Idle:
			NMAD AABBCCBB 4 {
				return A_MinionIdle();
			}
			Loop;
			
		Guard:
			NMAD AABBCCBB 4 {
				return A_MinionGuard();
			}
			Loop;
			
		Follow:
			NMAD DDEEFFEE 4 {
				return A_FollowMaster(1000);
			}
			Loop;
			
		See:
			NMAD DDEEFFEE 4 {
				A_MinionChase();
			}
			Loop;
			
		Melee:
			NMAD GH 5 A_FaceTarget();
			NMAD I 7 A_CreatureMelee(15, 25, "BedlamSmoke", "Chaos");
			Goto See;

		Missile:
			NMAD J 8 A_FaceTarget();
			NMAD KL 8 {
				A_FaceTarget();
				for (int i = 0; i < 3; i++) {
					A_SpawnProjectile("MaelsphereProjectile", invoker.height / 2);
				}
			}
			NMAD J 0 A_MonsterRefire(10, "See");
			Goto Missile + 2;

		Turret:
			NMAD JKLJ 8 {
				A_FaceTarget();
				for (int i = 0; i < 3; i++) {
					A_SpawnProjectile("MaelsphereProjectile", invoker.height / 2);
				}
			}
			Goto See;
			
		Pain:
			NMAD M 6 A_Pain();
			Goto See;
			
		Death:
		XDeath:
			NMAD MN 8;
			NMAD O 8 A_Scream();
			NMAD P 8;
			NMAD Q 8 A_NoBlocking();
			NMAD RSTU 8;
			NMAD V -1;
			Stop;
	}
}