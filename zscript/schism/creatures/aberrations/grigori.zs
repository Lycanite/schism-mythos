class Grigori : Creature {
	Default {
		Tag "Grigori";
		Radius 16;
		Height 56;
		Health 100;
		Mass 50;
		Speed 8;
		Damage 3;
		PainChance 256;
		
		BloodColor "red";
		
		SeeSound "Grigori/See";
		ActiveSound "Grigori/Idle";
		PainSound "Grigori/Pain";
		DeathSound "Grigori/Death";
		MeleeSound "Grigori/Melee";
		Obituary "%o was dissolved by a Grigori." ;
		HitObituary "%o was devoured by a Grigori.";
		
		+FLOAT;
		+NOGRAVITY;
		+RETARGETAFTERSLAM;
		+CREATURE.HOVERBOB;
	}
	
	States {
		Spawn:
			GRIG AB 8 A_Look;
			Loop;
			
		See:
			GRIG AABB 2 A_Chase;
			Loop;
			
		Missile:
			GRIG C 8 A_FaceTarget();
			GRIG D 6 {
				A_FaceTarget();
				A_StartSound(invoker.meleeSound);
			}
			GRIG E 4 A_SkullAttack();
			GRIG FE 4;
			Goto Missile + 2;
			
		Pain:
			GRIG I 3;
			GRIG I 3 A_Pain();
			Goto See;
			
		Death:
			GRIG I 5;
			GRIG J 5 A_Scream();
			GRIG K 5;
			GRIG L 5 A_CreatureDie();
			GRIG MN 5;
			GRIG O -1;
			Stop;
			
		XDeath:
			GRIG P 3;
			GRIG Q 0 A_FaceTarget();
			GRIG Q 0 A_SpawnItemEx("GrigoriGibs", 0, 0, 0, 0, 0, 0, 0, 128);
			GRIG Q 0 A_SpawnProjectile("GrigoriGib1", 14, 0, random(-180,180), 2, random(10,40));
			GRIG Q 0 A_SpawnProjectile("GrigoriGib2", 6, 2, random(-180,180), 2, random(0,25));
			GRIG Q 0 A_SpawnProjectile("GrigoriGib2B", 10, -2, random(-180,180), 2, random(0,25));
			GRIG Q 0 A_SpawnProjectile("GrigoriGib3", 8, 0, random(-180,180), 2, random(0,35));
			GRIG Q 0 A_SpawnProjectile("GrigoriGib4", 12, 5, random(-180,180), 2, random(-5,40));
			GRIG Q 0 A_SpawnProjectile("GrigoriGib4B", 5, -5, random(-180,180), 2, random(0,30));
			GRIG Q 0 A_SpawnProjectile("GrigoriGib5", 6, 3, random(-180,180), 2, random(10,60));
			GRIG Q 0 A_SpawnProjectile("GrigoriGib5", 8, 0, random(-180,180), 2, random(-10,55));
			GRIG Q 0 A_SpawnProjectile("GrigoriGib6", 12, 0, 0, 2, 0);
			GRIG Q 3 A_XScream();
			GRIG R 3 A_CreatureDie();
			GRIG STU 3;
			Stop;
			
		Raise:
			GRIG NMLKJI 5;
			Goto See;
	}
}

class GrigoriGibs : Actor {
	Default {
		+NOCLIP;
	}
	
	States {
		Spawn:
			TNT1 A 0 A_SpawnProjectile("GrigoriGib1", 14, 0, random(-180,180), 2, random(10,40));
			TNT1 A 0 A_SpawnProjectile("GrigoriGib2", 6, 2, random(-180,180), 2, random(0,25));
			TNT1 A 0 A_SpawnProjectile("GrigoriGib2B", 10, -2, random(-180,180), 2, random(0,25));
			TNT1 A 0 A_SpawnProjectile("GrigoriGib3", 8, 0, random(-180,180), 2, random(0,35));
			TNT1 A 0 A_SpawnProjectile("GrigoriGib4", 12, 5, random(-180,180), 2, random(-5,40));
			TNT1 A 0 A_SpawnProjectile("GrigoriGib4B", 5, -5, random(-180,180), 2, random(0,30));
			TNT1 A 0 A_SpawnProjectile("GrigoriGib5", 6, 3, random(-180,180), 2, random(10,60));
			TNT1 A 0 A_SpawnProjectile("GrigoriGib5", 8, 0, random(-180,180), 2, random(-10,55));
			TNT1 A 0 A_SpawnProjectile("GrigoriGib6", 12, 0, 0, 2, 0);
			Stop;
	}
}

class GrigoriGib1 : Actor {
	Default {
		Speed 8;
		Mass 100;
		Radius 1;
		Height 1;
		Projectile;
		-NOGRAVITY;
		+DROPOFF;
	}
	
	States {
		Spawn:
			GRG2 ABCD 4 ;
			Loop;
			
			Death:
			GRG2 E -1;
			Stop;
	}
}

class GrigoriGib2 : GrigoriGib1 {
	Default {
		Speed 8;
		Mass 100;
		Radius 1;
		Height 1;
		Projectile;
		-NOGRAVITY;
		+DROPOFF;
		+CLIENTSIDEONLY;
	}
	
	States {
		Spawn:
			GRG3 ABCD 4;
			Loop;
			
			Death:
			GRG3 I -1;
			Stop;
	}
}

class GrigoriGib2B : GrigoriGib1 {
	Default {
		+CLIENTSIDEONLY;
	}
	
	States {
		Spawn:
			GRG3 EFGH 4;
			Loop;
			
			Death:
			GRG3 J -1;
			Stop;
	}
}

class GrigoriGib3 : GrigoriGib1 {
	Default {
		+CLIENTSIDEONLY;
	}
	
	States {
		Spawn:
			GRG4 ABCD 4;
			Loop;
			
			Death:
			GRG4 E -1;
			Stop;
	}
}

class GrigoriGib4 : GrigoriGib1 {
	Default {
		+CLIENTSIDEONLY;
	}
	
	States {
		Spawn:
			GRG5 ABCD 4 ;
			Loop;
			
			Death:
			GRG5 I -1;
			Stop;
	}
}

class GrigoriGib4B : GrigoriGib1 {
	Default {
		+CLIENTSIDEONLY;
	}
	
	States {
		Spawn:
			GRG5 EFGH 4;
			Loop;
			
			Death:
			GRG5 J -1;
			Stop;
	}
}

class GrigoriGib5 : GrigoriGib1 {
	Default {
		+CLIENTSIDEONLY;
	}
	
	States {
		Spawn:
			GRG6 ABCD 4 ;
			Loop;
			
			Death:
			GRG6 E -1;
			Stop;
	}
}

class GrigoriGib6 : GrigoriGib1 {
	Default {
		+CLIENTSIDEONLY;
		Speed 0;
	}
	
	States {
		Spawn:
			GRG7 A 4;
			Loop;
			
			Death:
			GRG7 BC 4;
			GRG7 D -1;
			Stop;
	}
}
