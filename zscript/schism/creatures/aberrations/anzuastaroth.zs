class AnzuAstaroth : Creature {
	Default {
		Tag "Anzu Astaroth";
		Health 500;
		Radius 64;
		Height 64;
		Mass 600;
		Speed 12;
		PainChance 128;
		
		SeeSound "AnzuAstaroth/See";
		ActiveSound "AnzuAstaroth/Active";
		PainSound "AnzuAstaroth/Pain";
		DeathSound "AnzuAstaroth/Death";
		Obituary "%o was destroyed by an Anzu Astaroth!";
		
		+BOSSDEATH;
	}
	States {
		Spawn:
			AZAS B 10 A_Look();
			Loop;

		See:
			AZAS A 20;
			AZAS A 3 {
				A_Chase();
				A_StartSound("AnzuAstaroth/Step");
			}
			AZAS ABBCC 3 A_Chase();
			Goto See + 1;

		Missile:
			AZAS A 20 bright A_FaceTarget();
			AZAS D 8 bright {
				A_SpawnProjectile("ChromaboltProjectile", 20, -12);
				A_SpawnProjectile("ChromaboltProjectile", 20, 12);
			}
			AZAS E 8 bright;
			AZAS E 1 bright A_MonsterRefire(10, "See");
			Goto Missile + 1;

		Pain:
			AZAS F 3;
			AZAS F 3 A_Pain;
			Goto See+1;

		Death:
			AZAS G 20 A_Scream();
			AZAS H 7 A_CreatureDie();
			AZAS IJK 7;
			AZAS L -1 A_BossDeath();
			Stop;

		Raise:
			AZAS LKJIHG 5;
			Goto See + 1;
	}
}