mixin class SpawnParticleMixin {
    /** 
     * Spawns a particle relative to the actor.
     * @param name The name of the particle to spawn.
     * @param lifetime The lifetime of the particle in tics.
     * @param speed The speed of the particle (ex: 0.8 for a slow fountain, 3 for flames). Defaults to 0 for no velocity.
     * @param offset The latitudal, longitudal and vertical offset relative to the actor, defaults to (0, 0, 0).
     * @param angle The angle relative to the actor, defaults to 0.
     * @param pitch The pitch relative to the actor, where 0 is ahead, 90 is downwards, 270 is upwards, defaults to 0.
     * @param scale The relative scale of the particle, defaults to 1.0.
     * @param color The hex color overlay of the particle, defaults to the unset color stencil value for image colors instead.
     */
	virtual void SpawnParticle(string name, int lifetime, float speed = 0, vector3 offset = (0, 0, 0), float angle = 0, float pitch = 0, float scale = 1.0, int color = 0xF7000000) {
        if (Actor(self)) {
            self.SpawnParticleFromActor(Actor(self), name, lifetime, speed, offset, angle, pitch, scale, color);
        }
	}

    /** 
     * Spawns a particle relative to the actor.
     * @param actor The actor to spawn partcles relative to.
     * @param name The name of the particle to spawn.
     * @param lifetime The lifetime of the particle in tics.
     * @param speed The speed of the particle (ex: 0.8 for a slow fountain, 3 for flames). Defaults to 0 for no velocity.
     * @param offset The latitudal, longitudal and vertical offset relative to the actor, defaults to (0, 0, 0).
     * @param angle The angle relative to the actor, defaults to 0.
     * @param pitch The pitch relative to the actor, where 0 is ahead, 90 is downwards, 270 is upwards, defaults to 0.
     * @param scale The relative scale of the particle, defaults to 1.0.
     * @param color The hex color overlay of the particle, defaults to the unset color stencil value for image colors instead.
     */
	virtual void SpawnParticleFromActor(actor actor, string name, int lifetime, float speed = 0, vector3 offset = (0, 0, 0), float angle = 0, float pitch = 0, float scale = 1.0, int color = 0xF7000000) {
        // Offset:
        vector3 pos = actor.pos;
        if (offset.x != 0 || offset.y != 0) {
            float offsetDistance = Sqrt(offset.x * offset.x + offset.y * offset.y);
            float offsetAngle = (actor.angle + Atan2(offset.x, offset.y)) % 360;
            vector2 latLon = actor.Vec2Angle(offsetDistance, offsetAngle);
            pos.x = latLon.x;
            pos.y = latLon.y;
        }
        if (offset.z != 0) {
            vector3 vert = actor.Vec3Offset(0, 0, offset.z);
            pos.z = vert.z;
        }

        // Velocity:
        vector3 vel = (0, 0, 0);
        if (speed != 0) {
            vector2 lonVert = (Cos(actor.pitch + pitch), -(Sin(actor.pitch + pitch)));
            vector2 latLon = (Cos(actor.angle + angle), Sin(actor.angle + angle));
            vel = ((latLon.x * lonVert.x) * speed, (latLon.y * lonVert.x) * speed, lonVert.y * speed);
        }

        // Spawn:
        SchismParticles.spawn(name, lifetime, pos, vel, scale, color);
	}

    /** 
     * Spawns a particle from a source actor aimed towards a target actor.
     * @param source The source actor to spawn partcles from.
     * @param target The target actor to spawn partcles towards.
     * @param name The name of the particle to spawn.
     * @param lifetime The lifetime of the particle in tics.
     * @param speed The speed of the particle (ex: 0.8 for a slow fountain, 3 for flames). Defaults to 0 for no velocity.
     * @param offset The latitudal, longitudal and vertical offset relative to the source actor, defaults to (0, 0, 0).
     * @param scale The relative scale of the particle, defaults to 1.0.
     * @param color The hex color overlay of the particle, defaults to the unset color stencil value for image colors instead.
     */
	virtual void SpawnParticleActorToActor(actor source, actor target, string name, int lifetime, float speed = 0, vector3 offset = (0, 0, 0), float scale = 1.0, int color = 0xF7000000) {
        double angle = source.AngleTo(target) - target.angle - source.angle;
        self.SpawnParticleFromActor(source, name, lifetime, speed, offset, angle, -source.pitch, scale, color);
	}
}