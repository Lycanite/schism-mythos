class SchismFlower : Actor {
	Default {
		Tag "Flower";
		Scale 0.5;
		Radius 16;
		Height 16;
	}
}

class SchismFlowerRose : SchismFlower {
	Default {
		Tag "Rose Flower";
	}
	
	States {
		Spawn:
			NFLW A -1;
			Stop;
	}
}

class SchismFlowerBegonia : SchismFlower {
	Default {
		Tag "Begonia Flower";
	}
	
	States {
		Spawn:
			NFLW B -1;
			Stop;
	}
}

class SchismFlowerViola : SchismFlower {
	Default {
		Tag "Viola Flower";
	}
	
	States {
		Spawn:
			NFLW C -1;
			Stop;
	}
}

class SchismFlowerPetunia : SchismFlower {
	Default {
		Tag "Petunia Flower";
	}
	
	States {
		Spawn:
			NFLW D -1;
			Stop;
	}
}

class SchismFlowerLotus : SchismFlower {
	Default {
		Tag "Lotus Flower";
		Gravity 0;
	}
	
	States {
		Spawn:
			NFLW E -1;
			Stop;
	}
}

class SchismFlowerBat : SchismFlower {
	Default {
		Tag "Bat Flower";
	}
	
	States {
		Spawn:
			NFLW F -1;
			Stop;
	}
}

class SchismFlowerOsteospermum : SchismFlower {
	Default {
		Tag "Osteospermum Flower";
	}
	
	States {
		Spawn:
			NFLW G -1;
			Stop;
	}
}

class SchismFlowerCalliandra : SchismFlower {
	Default {
		Tag "Calliandra Flower";
	}
	
	States {
		Spawn:
			NFLW H -1;
			Stop;
	}
}

class SchismFlowerAzalea : SchismFlower {
	Default {
		Tag "Azalea Flower";
	}
	
	States {
		Spawn:
			NFLW I -1;
			Stop;
	}
}

class SchismFlowerHydrangea : SchismFlower {
	Default {
		Tag "Hydrangea Flower";
	}
	
	States {
		Spawn:
			NFLW J -1;
			Stop;
	}
}

class SchismFlowerChicory : SchismFlower {
	Default {
		Tag "Chicory Flower";
	}
	
	States {
		Spawn:
			NFLW K -1;
			Stop;
	}
}

class SchismFlowerLarkspur : SchismFlower {
	Default {
		Tag "Larkspur Flower";
	}
	
	States {
		Spawn:
			NFLW L -1;
			Stop;
	}
}

class SchismFlowerAlchemilla : SchismFlower {
	Default {
		Tag "Alchemilla Flower";
	}
	
	States {
		Spawn:
			NFLW M -1;
			Stop;
	}
}

class SchismFlowerTulip : SchismFlower {
	Default {
		Tag "Tulip Flower";
	}
	
	States {
		Spawn:
			NFLW N -1;
			Stop;
	}
}

class SchismFlowerYarrow : SchismFlower {
	Default {
		Tag "Yarrow Flower";
	}
	
	States {
		Spawn:
			NFLW O -1;
			Stop;
	}
}

class SchismFlowerArum : SchismFlower {
	Default {
		Tag "Arum Flower";
	}
	
	States {
		Spawn:
			NFLW P -1;
			Stop;
	}
}

class SchismFlowerCastilleja : SchismFlower {
	Default {
		Tag "Castilleja Flower";
	}
	
	States {
		Spawn:
			NFLW Q -1;
			Stop;
	}
}

class SchismFlowerBalsam : SchismFlower {
	Default {
		Tag "Balsam Flower";
	}
	
	States {
		Spawn:
			NFLW R -1;
			Stop;
	}
}