class SchismCandleA : Actor {
	Default {
		Tag "Candle A";
		
		Radius 8;
		Height 16;
		ProjectilePassHeight -16;
		
		+SOLID;
	}
	
	States {
		Spawn:
			NCND A -1 bright;
			Stop;
	}
}

class SchismCandleB : SchismCandleA {
	Default {
		Tag "Candle B";
	}
	
	States {
		Spawn:
			NCND B -1 bright;
			Stop;
	}
}

class SchismCandleC : SchismCandleA {
	Default {
		Tag "Candle C";
	}
	
	States {
		Spawn:
			NCND C -1 bright;
			Stop;
	}
}

class SchismCandleD : SchismCandleA {
	Default {
		Tag "Candle D";
	}
	
	States {
		Spawn:
			NCND D -1 bright;
			Stop;
	}
}

class SchismCandlesHanging : SchismCandleA {
	Default {
		Tag "Hanging Candles";
		
		Height 24;
		Gravity 0;
		ProjectilePassHeight 0;
		
		+SPAWNCEILING;
	}
	
	States {
		Spawn:
			NCND E -1 bright;
			Stop;
	}
}

class SchismTorchA : Actor {
	Default {
		Tag "Torch A";
		
		Radius 16;
		Height 32;
		ProjectilePassHeight -16;
		
		+SOLID;
	}
	
	States {
		Spawn:
			NTRC ABC 4 bright;
			Loop;
	}
}

class SchismTorchB : SchismTorchA {
	Default {
		Tag "Torch B";
	}
	
	States {
		Spawn:
			NTRC DEFGH 2 bright;
			Loop;
	}
}

class SchismLantern : SchismTorchA {
	Default {
		Tag "Lantern A";
	}
	
	States {
		Spawn:
			NTRC IJK 4 bright;
			Loop;
	}
}

class SchismTorchWall : SchismTorchA {
	Default {
		Tag "Wall Torch";
		
		Gravity 0;
	}
	
	States {
		Spawn:
			NTRC LMNO 2 bright;
			Loop;
	}
}

class SchismTorchWallGreen : SchismTorchWall {
	Default {
		Tag "Wall Torch Green";
		
		Gravity 0;
	}
	
	States {
		Spawn:
			NTRC QRST 2 bright;
			Loop;
	}
}

class SchismTorchGreen : SchismTorchA {
	Default {
		Tag "Torch Green";
	}
	
	States {
		Spawn:
			NTR1 BCDE 2 bright;
			Loop;
	}
}

class SchismTorchRed : SchismTorchGreen {
	Default {
		Tag "Torch Red";
	}
	
	States {
		Spawn:
			NTR1 FGHI 2 bright;
			Loop;
	}
}

class SchismTorchBlue : SchismTorchGreen {
	Default {
		Tag "Torch Blue";
	}
	
	States {
		Spawn:
			NTR1 JKLM 2 bright;
			Loop;
	}
}

class SchismTorchTallGreen : SchismTorchGreen {
	Default {
		Tag "Tall Torch Green";
	}
	
	States {
		Spawn:
			NTR2 BCDE 2 bright;
			Loop;
	}
}

class SchismTorchTallRed : SchismTorchTallGreen {
	Default {
		Tag "Tall Torch Red";
	}
	
	States {
		Spawn:
			NTR2 FGHI 2 bright;
			Loop;
	}
}

class SchismTorchTallBlue : SchismTorchTallGreen {
	Default {
		Tag "Tall Torch Blue";
	}
	
	States {
		Spawn:
			NTR2 JKLM 2 bright;
			Loop;
	}
}

class SchismTorchBowl : SchismTorchA {
	Default {
		Tag "Bowl Torch";
	}
	
	States {
		Spawn:
			NTR3 ABCDEFGH 2 bright;
			Loop;
	}
}

class SchismLampHangingA : Actor {
	Default {
		Tag "Lamp Hanging A";
		
		Radius 8;
		Height 16;
		ProjectilePassHeight -16;
		Gravity 0;
		
		+SOLID;
		+SPAWNCEILING;
	}
	
	States {
		Spawn:
			NLMP ABCD 4 bright;
			Loop;
	}
}

class SchismLampHangingB : Actor {
	Default {
		Tag "Lamp Hanging B";
	}
	
	States {
		Spawn:
			NLMP EFGH 4 bright;
			Loop;
	}
}