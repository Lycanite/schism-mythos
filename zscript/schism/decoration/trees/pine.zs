class SchismTreePine : SchismTree {
	Default {
		Tag "Pine Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR03 A -1;
			Stop;

		Spring.2:
			TR03 B -1;
			Stop;

		Summer.1:
			TR03 E -1;
			Stop;

		Summer.2:
			TR03 F -1;
			Stop;

		Autumn.1:
			TR03 I -1;
			Stop;

		Autumn.2:
			TR03 J -1;
			Stop;

		Winter.1:
			TR03 M -1;
			Stop;

		Winter.2:
			TR03 N -1;
			Stop;

		Death.Spring.1:
			TD03 A -1;
			Stop;

		Death.Spring.2:
			TD03 B -1;
			Stop;

		Death.Summer.1:
			TD03 E -1;
			Stop;

		Death.Summer.2:
			TD03 F -1;
			Stop;

		Death.Autumn.1:
			TD03 I -1;
			Stop;

		Death.Autumn.2:
			TD03 J -1;
			Stop;

		Death.Winter.1:
			TD03 M -1;
			Stop;

		Death.Winter.2:
			TD03 N -1;
			Stop;
	}
}