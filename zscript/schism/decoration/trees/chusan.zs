class SchismTreeChusan : SchismTree {
	Default {
		Tag "Chusan Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR16 A -1;
			Stop;

		Spring.2:
			TR16 B -1;
			Stop;

		Summer.1:
			TR16 E -1;
			Stop;

		Summer.2:
			TR16 F -1;
			Stop;

		Autumn.1:
			TR16 I -1;
			Stop;

		Autumn.2:
			TR16 J -1;
			Stop;

		Winter.1:
			TD16 M -1;
			Stop;

		Winter.2:
			TD16 N -1;
			Stop;

		Death.Spring.1:
			TD16 A -1;
			Stop;

		Death.Spring.2:
			TD16 B -1;
			Stop;

		Death.Summer.1:
			TD16 E -1;
			Stop;

		Death.Summer.2:
			TD16 F -1;
			Stop;

		Death.Autumn.1:
			TD16 I -1;
			Stop;

		Death.Autumn.2:
			TD16 J -1;
			Stop;

		Death.Winter.1:
			TD16 M -1;
			Stop;

		Death.Winter.2:
			TD16 N -1;
			Stop;
	}
}