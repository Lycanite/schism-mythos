class SchismTreeDate : SchismTree {
	Default {
		Tag "Date Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR14 A -1;
			Stop;

		Spring.2:
			TR14 B -1;
			Stop;

		Summer.1:
			TR14 E -1;
			Stop;

		Summer.2:
			TR14 F -1;
			Stop;

		Autumn.1:
			TR14 I -1;
			Stop;

		Autumn.2:
			TR14 J -1;
			Stop;

		Winter.1:
			TD14 M -1;
			Stop;

		Winter.2:
			TD14 N -1;
			Stop;

		Death.Spring.1:
			TD14 A -1;
			Stop;

		Death.Spring.2:
			TD14 B -1;
			Stop;

		Death.Summer.1:
			TD14 E -1;
			Stop;

		Death.Summer.2:
			TD14 F -1;
			Stop;

		Death.Autumn.1:
			TD14 I -1;
			Stop;

		Death.Autumn.2:
			TD14 J -1;
			Stop;

		Death.Winter.1:
			TD14 M -1;
			Stop;

		Death.Winter.2:
			TD14 N -1;
			Stop;
	}
}