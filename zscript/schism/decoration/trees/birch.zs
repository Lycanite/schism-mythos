class SchismTreeBirch : SchismTree {
	Default {
		Tag "Birch Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR10 A -1;
			Stop;

		Spring.2:
			TR10 B -1;
			Stop;

		Summer.1:
			TR10 E -1;
			Stop;

		Summer.2:
			TR10 F -1;
			Stop;

		Autumn.1:
			TR10 I -1;
			Stop;

		Autumn.2:
			TR10 J -1;
			Stop;

		Winter.1:
			TD10 M -1;
			Stop;

		Winter.2:
			TD10 N -1;
			Stop;

		Death.Spring.1:
			TD10 A -1;
			Stop;

		Death.Spring.2:
			TD10 B -1;
			Stop;

		Death.Summer.1:
			TD10 E -1;
			Stop;

		Death.Summer.2:
			TD10 F -1;
			Stop;

		Death.Autumn.1:
			TD10 I -1;
			Stop;

		Death.Autumn.2:
			TD10 J -1;
			Stop;

		Death.Winter.1:
			TD10 M -1;
			Stop;

		Death.Winter.2:
			TD10 N -1;
			Stop;
	}
}