class SchismTreeAsh : SchismTree {
	Default {
		Tag "Ash Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR09 A -1;
			Stop;

		Spring.2:
			TR09 B -1;
			Stop;

		Summer.1:
			TR09 E -1;
			Stop;

		Summer.2:
			TR09 F -1;
			Stop;

		Autumn.1:
			TR09 I -1;
			Stop;

		Autumn.2:
			TR09 J -1;
			Stop;

		Winter.1:
			TD09 M -1;
			Stop;

		Winter.2:
			TD09 N -1;
			Stop;

		Death.Spring.1:
			TD09 A -1;
			Stop;

		Death.Spring.2:
			TD09 B -1;
			Stop;

		Death.Summer.1:
			TD09 E -1;
			Stop;

		Death.Summer.2:
			TD09 F -1;
			Stop;

		Death.Autumn.1:
			TD09 I -1;
			Stop;

		Death.Autumn.2:
			TD09 J -1;
			Stop;

		Death.Winter.1:
			TD09 M -1;
			Stop;

		Death.Winter.2:
			TD09 N -1;
			Stop;
	}
}