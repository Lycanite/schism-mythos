class SchismTreeWalnut : SchismTree {
	Default {
		Tag "Walnut Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR07 A -1;
			Stop;

		Spring.2:
			TR07 B -1;
			Stop;

		Summer.1:
			TR07 E -1;
			Stop;

		Summer.2:
			TR07 F -1;
			Stop;

		Autumn.1:
			TR07 I -1;
			Stop;

		Autumn.2:
			TR07 J -1;
			Stop;

		Winter.1:
			TD07 M -1;
			Stop;

		Winter.2:
			TD07 N -1;
			Stop;

		Death.Spring.1:
			TD07 A -1;
			Stop;

		Death.Spring.2:
			TD07 B -1;
			Stop;

		Death.Summer.1:
			TD07 E -1;
			Stop;

		Death.Summer.2:
			TD07 F -1;
			Stop;

		Death.Autumn.1:
			TD07 I -1;
			Stop;

		Death.Autumn.2:
			TD07 J -1;
			Stop;

		Death.Winter.1:
			TD07 M -1;
			Stop;

		Death.Winter.2:
			TD07 N -1;
			Stop;
	}
}