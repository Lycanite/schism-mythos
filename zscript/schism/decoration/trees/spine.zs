class SchismTreeSpine : SchismTree {
	Default {
		Tag "Spine Tree";

		Scale 2;

		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR26 A -1;
			Stop;

		Spring.2:
			TR26 B -1;
			Stop;

		Summer.1:
			TR26 E -1;
			Stop;

		Summer.2:
			TR26 F -1;
			Stop;

		Autumn.1:
			TR26 I -1;
			Stop;

		Autumn.2:
			TR26 J -1;
			Stop;

		Winter.1:
			TR26 M -1;
			Stop;

		Winter.2:
			TR26 N -1;
			Stop;

		Death.Spring.1:
			TD26 A -1;
			Stop;

		Death.Spring.2:
			TD26 B -1;
			Stop;

		Death.Summer.1:
			TD26 E -1;
			Stop;

		Death.Summer.2:
			TD26 F -1;
			Stop;

		Death.Autumn.1:
			TD26 I -1;
			Stop;

		Death.Autumn.2:
			TD26 J -1;
			Stop;

		Death.Winter.1:
			TD26 M -1;
			Stop;

		Death.Winter.2:
			TD26 N -1;
			Stop;
	}
}