class SchismTreeOak : SchismTree {
	Default {
		Tag "Oak Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR11 A -1;
			Stop;

		Spring.2:
			TR11 B -1;
			Stop;

		Summer.1:
			TR11 E -1;
			Stop;

		Summer.2:
			TR11 F -1;
			Stop;

		Autumn.1:
			TR11 I -1;
			Stop;

		Autumn.2:
			TR11 J -1;
			Stop;

		Winter.1:
			TD11 M -1;
			Stop;

		Winter.2:
			TD11 N -1;
			Stop;

		Death.Spring.1:
			TD11 A -1;
			Stop;

		Death.Spring.2:
			TD11 B -1;
			Stop;

		Death.Summer.1:
			TD11 E -1;
			Stop;

		Death.Summer.2:
			TD11 F -1;
			Stop;

		Death.Autumn.1:
			TD11 I -1;
			Stop;

		Death.Autumn.2:
			TD11 J -1;
			Stop;

		Death.Winter.1:
			TD11 M -1;
			Stop;

		Death.Winter.2:
			TD11 N -1;
			Stop;
	}
}