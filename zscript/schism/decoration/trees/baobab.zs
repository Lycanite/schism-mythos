class SchismTreeBaobab : SchismTree {
	Default {
		Tag "Baobab Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR25 A -1;
			Stop;

		Spring.2:
			TR25 B -1;
			Stop;

		Summer.1:
			TR25 E -1;
			Stop;

		Summer.2:
			TR25 F -1;
			Stop;

		Autumn.1:
			TR25 I -1;
			Stop;

		Autumn.2:
			TR25 J -1;
			Stop;

		Winter.1:
			TR25 M -1;
			Stop;

		Winter.2:
			TR25 N -1;
			Stop;

		Death.Spring.1:
			TD25 A -1;
			Stop;

		Death.Spring.2:
			TD25 B -1;
			Stop;

		Death.Summer.1:
			TD25 E -1;
			Stop;

		Death.Summer.2:
			TD25 F -1;
			Stop;

		Death.Autumn.1:
			TD25 I -1;
			Stop;

		Death.Autumn.2:
			TD25 J -1;
			Stop;

		Death.Winter.1:
			TD25 M -1;
			Stop;

		Death.Winter.2:
			TD25 N -1;
			Stop;
	}
}