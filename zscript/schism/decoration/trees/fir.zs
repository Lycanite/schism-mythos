class SchismTreeFir : SchismTree {
	Default {
		Tag "Fir Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR02 A -1;
			Stop;

		Spring.2:
			TR02 B -1;
			Stop;

		Summer.1:
			TR02 E -1;
			Stop;

		Summer.2:
			TR02 F -1;
			Stop;

		Autumn.1:
			TR02 I -1;
			Stop;

		Autumn.2:
			TR02 J -1;
			Stop;

		Winter.1:
			TR02 M -1;
			Stop;

		Winter.2:
			TR02 N -1;
			Stop;

		Death.Spring.1:
			TD02 A -1;
			Stop;

		Death.Spring.2:
			TD02 B -1;
			Stop;

		Death.Summer.1:
			TD02 E -1;
			Stop;

		Death.Summer.2:
			TD02 F -1;
			Stop;

		Death.Autumn.1:
			TD02 I -1;
			Stop;

		Death.Autumn.2:
			TD02 J -1;
			Stop;

		Death.Winter.1:
			TD02 M -1;
			Stop;

		Death.Winter.2:
			TD02 N -1;
			Stop;
	}
}