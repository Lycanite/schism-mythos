class SchismTreeAlder : SchismTree {
	Default {
		Tag "Alder Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR06 A -1;
			Stop;

		Spring.2:
			TR06 B -1;
			Stop;

		Summer.1:
			TR06 E -1;
			Stop;

		Summer.2:
			TR06 F -1;
			Stop;

		Autumn.1:
			TR06 I -1;
			Stop;

		Autumn.2:
			TR06 J -1;
			Stop;

		Winter.1:
			TD06 M -1;
			Stop;

		Winter.2:
			TD06 N -1;
			Stop;

		Death.Spring.1:
			TD06 A -1;
			Stop;

		Death.Spring.2:
			TD06 B -1;
			Stop;

		Death.Summer.1:
			TD06 E -1;
			Stop;

		Death.Summer.2:
			TD06 F -1;
			Stop;

		Death.Autumn.1:
			TD06 I -1;
			Stop;

		Death.Autumn.2:
			TD06 J -1;
			Stop;

		Death.Winter.1:
			TD06 M -1;
			Stop;

		Death.Winter.2:
			TD06 N -1;
			Stop;
	}
}