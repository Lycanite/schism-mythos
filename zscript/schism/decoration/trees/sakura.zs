class SchismTreeSakura : SchismTree {
	Default {
		Tag "Sakura Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR23 A 4 A_DropLeaves("SchismLeafSakura");
			Loop;

		Spring.2:
			TR23 B 4 A_DropLeaves("SchismLeafSakura");
			Loop;

		Summer.1:
			TR23 E -1;
			Stop;

		Summer.2:
			TR23 F -1;
			Stop;

		Autumn.1:
			TR23 I -1;
			Stop;

		Autumn.2:
			TR23 J -1;
			Stop;

		Winter.1:
			TD23 M -1;
			Stop;

		Winter.2:
			TD23 N -1;
			Stop;

		Death.Spring.1:
			TD23 A -1;
			Stop;

		Death.Spring.2:
			TD23 B -1;
			Stop;

		Death.Summer.1:
			TD23 E -1;
			Stop;

		Death.Summer.2:
			TD23 F -1;
			Stop;

		Death.Autumn.1:
			TD23 I -1;
			Stop;

		Death.Autumn.2:
			TD23 J -1;
			Stop;

		Death.Winter.1:
			TD23 M -1;
			Stop;

		Death.Winter.2:
			TD23 N -1;
			Stop;
	}
}