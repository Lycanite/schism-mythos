class SchismTreeSpruce : SchismTree {
	Default {
		Tag "Spruce Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR04 A -1;
			Stop;

		Spring.2:
			TR04 B -1;
			Stop;

		Summer.1:
			TR04 E -1;
			Stop;

		Summer.2:
			TR04 F -1;
			Stop;

		Autumn.1:
			TR04 I -1;
			Stop;

		Autumn.2:
			TR04 J -1;
			Stop;

		Winter.1:
			TR04 M -1;
			Stop;

		Winter.2:
			TR04 N -1;
			Stop;

		Death.Spring.1:
			TD04 A -1;
			Stop;

		Death.Spring.2:
			TD04 B -1;
			Stop;

		Death.Summer.1:
			TD04 E -1;
			Stop;

		Death.Summer.2:
			TD04 F -1;
			Stop;

		Death.Autumn.1:
			TD04 I -1;
			Stop;

		Death.Autumn.2:
			TD04 J -1;
			Stop;

		Death.Winter.1:
			TD04 M -1;
			Stop;

		Death.Winter.2:
			TD04 N -1;
			Stop;
	}
}