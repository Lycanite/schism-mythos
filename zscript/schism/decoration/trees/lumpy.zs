class SchismTreeLumpy : SchismTree {
	Default {
		Tag "Lumpy Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR27 A -1;
			Stop;

		Spring.2:
			TR27 B -1;
			Stop;

		Summer.1:
			TR27 E -1;
			Stop;

		Summer.2:
			TR27 F -1;
			Stop;

		Autumn.1:
			TR27 I -1;
			Stop;

		Autumn.2:
			TR27 J -1;
			Stop;

		Winter.1:
			TR27 M -1;
			Stop;

		Winter.2:
			TR27 N -1;
			Stop;

		Death.Spring.1:
			TD27 A -1;
			Stop;

		Death.Spring.2:
			TD27 B -1;
			Stop;

		Death.Summer.1:
			TD27 E -1;
			Stop;

		Death.Summer.2:
			TD27 F -1;
			Stop;

		Death.Autumn.1:
			TD27 I -1;
			Stop;

		Death.Autumn.2:
			TD27 J -1;
			Stop;

		Death.Winter.1:
			TD27 M -1;
			Stop;

		Death.Winter.2:
			TD27 N -1;
			Stop;
	}
}