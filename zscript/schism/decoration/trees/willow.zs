class SchismTreeWillow : SchismTree {
	Default {
		Tag "Willow Tree";
		SchismTree.SpringVariants 2;
		SchismTree.SummerVariants 2;
		SchismTree.AutumnVariants 2;
		SchismTree.WinterVariants 2;
	}
	
	States {
		Spring.1:
			TR17 A -1;
			Stop;

		Spring.2:
			TR17 B -1;
			Stop;

		Summer.1:
			TR17 E -1;
			Stop;

		Summer.2:
			TR17 F -1;
			Stop;

		Autumn.1:
			TR17 I -1;
			Stop;

		Autumn.2:
			TR17 J -1;
			Stop;

		Winter.1:
			TD17 M -1;
			Stop;

		Winter.2:
			TD17 N -1;
			Stop;

		Death.Spring.1:
			TD17 A -1;
			Stop;

		Death.Spring.2:
			TD17 B -1;
			Stop;

		Death.Summer.1:
			TD17 E -1;
			Stop;

		Death.Summer.2:
			TD17 F -1;
			Stop;

		Death.Autumn.1:
			TD17 I -1;
			Stop;

		Death.Autumn.2:
			TD17 J -1;
			Stop;

		Death.Winter.1:
			TD17 M -1;
			Stop;

		Death.Winter.2:
			TD17 N -1;
			Stop;
	}
}