class SchismHerb : Actor {
	Default {
		Tag "Herb Herb";
		Scale 0.75;
		
		Radius 16;
		Height 16;
	}
}

class SchismHerbNettle : SchismHerb {
	Default {
		Tag "Nettle Herb";
	}
	
	States {
		Spawn:
			NHRB A -1;
			Stop;
	}
}

class SchismHerbReed : SchismHerb {
	Default {
		Tag "Reed Herb";
	}
	
	States {
		Spawn:
			NHRB B -1;
			Stop;
	}
}

class SchismHerbPigweed : SchismHerb {
	Default {
		Tag "Pigweed Herb";
	}
	
	States {
		Spawn:
			NHRB C -1;
			Stop;
	}
}

class SchismHerbSeaweed : SchismHerb {
	Default {
		Tag "Seaweed Herb";
	}
	
	States {
		Spawn:
			NHRB DE 4;
			Loop;
	}
}

class SchismHerbAloevera : SchismHerb {
	Default {
		Gravity 0;
		Tag "Aloevera Herb";
	}
	
	States {
		Spawn:
			NHRB F -1;
			Stop;
	}
}

class SchismHerbSpearwort : SchismHerb {
	Default {
		Tag "Spearwort Herb";
	}
	
	States {
		Spawn:
			NHRB G -1;
			Stop;
	}
}

class SchismHerbLilypad : SchismHerb {
	Default {
		Gravity 0;
		Tag "Lilypad Herb";
	}
	
	States {
		Spawn:
			NHRB H -1;
			Stop;
	}
}

class SchismHerbNightshade : SchismHerb {
	Default {
		Gravity 0;
		Tag "Nightshade Herb";
	}
	
	States {
		Spawn:
			NHRB I -1;
			Stop;
	}
}

class SchismHerbWheat : SchismHerb {
	Default {
		Gravity 0;
		Tag "Wheat Herb";
	}
	
	States {
		Spawn:
			NHRB J -1;
			Stop;
	}
}

class SchismHerbAmaranthus : SchismHerb {
	Default {
		Gravity 0;
		Tag "Amaranthus Herb";
	}
	
	States {
		Spawn:
			NHRB K -1;
			Stop;
	}
}

class SchismHerbDock : SchismHerb {
	Default {
		Tag "Dock Herb";
	}
	
	States {
		Spawn:
			NHRB L -1;
			Stop;
	}
}