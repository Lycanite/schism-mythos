class SchismCharacterPose : Actor {
	Default {
		Scale 1;
		Radius 24;
		Height 40;
		Alpha 1;
		RenderStyle "Translucent";
	}
}

class ReilodosVoidFallen : SchismCharacterPose {
	Default {
		Scale 0.7;
		Tag "Reilodos";
	}
	
	States {
		Spawn:
			NPS1 A -1;
			Stop;
	}
}