class ChaosRiftHazard : Actor {
	mixin SpawnParticleMixin;

	int damageTics;

	Default {
		Radius 32;
		Height 80;
		Scale 1;
		RenderStyle "add";

		Obituary "%o was ripped asunder a Chaotic Rift!";
		
		-SOLID
		+NOGRAVITY
	}
	
	action void A_ChaosRift() {
		invoker.A_StartSound("EntropicState/Active", CHAN_BODY, CHANF_LOOPING);
		invoker.SpawnParticle("ChaosSpark", 8, 3, (FRandom(-4, 4), FRandom(-4, 4), 0), Random(0, 359), Random(-45, 45));
		if (invoker.damageTics-- <= 0) {
			invoker.damageTics = 35; // Every 1 sec per action tick.
			SchismAttacks.Explode(invoker, invoker, 5, 50 * invoker.scale.x, 0, 1, true, false, "Chaos");
		}
	}
	
	States {
		Spawn:
			REIP MNOP 2 bright A_ChaosRift();
			Loop;
			
		Death:
			REIP MMNNOOPP 1 A_FadeOut(0.025);
			Stop;
	}
}