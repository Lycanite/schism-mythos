class SchismAmbientSound : Actor {
	string user_soundName;

	bool warningSent;
	
	Default {
		Tag "Ambient Sound";
		Gravity 0;
		
		+NOBLOCKMAP;
		+NOCLIP;
		+NOSECTOR;
		+DONTSPLASH;
		+DONTBLAST;
		+NOTELEPORT;
		+NOGRAVITY;
	}

	override void Activate(Actor activator) {
		super.Activate(activator);
		self.bDormant = false;
	}

	override void Deactivate(Actor deactivator) {
		super.Deactivate(deactivator);
		self.bDormant = true;
	}
	
	/** 
	 * Plays an ambient sound.
	 * @arg 0 volume The volume % of the sound, at 0 defaults to 100 (100%).
	 * @arg 1 fadingDistance The distance from which the sound begins to fade, not implemented yet.
	 * @arg 2 maxDistance The cutoff distance of the sound, not implemented yet.
	 * @arg 3 range The range of the sound.
	 */
	action void A_PlayAmbientSound() {
		// Missing Sound:
		if (!invoker.user_soundName) {
			if (!invoker.warningSent) {
				A_Log("No Ambient Sound set!");
				invoker.warningSent = true;
			}
			return;
		}

		double range = invoker.args[3] > 0 ? double(invoker.args[3]) : 64;

		// Dormant and Player Range Check:
		if (invoker.bDormant || CheckRange(range, true)) {
			A_StopSound(CHAN_5);
			return;
		}

		// Sound Loop:
		double volume = invoker.args[0] > 0 ? double(invoker.args[0]) / 100 : 1;
		double attenuation = 1200 / range;
		A_StartSound("Ambient/"..invoker.user_soundName, CHAN_5, CHANF_LOOPING, volume, attenuation);
	}
	
	States {
		Spawn:
			TNT1 A 8 NoDelay A_PlayAmbientSound();
			Loop;
	}
}