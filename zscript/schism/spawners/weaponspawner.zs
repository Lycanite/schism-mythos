class SchismWeaponsSpawner : Actor {
	/** 
	 * Spawns weapons for all active player classes.
	 * @arg 0 The category of the weapon.
	 * @arg 1 The index of the weapon within the category. (0 = Base Weapon, 1-4 = Upgrades, 5 = Passive Upgrade)
	 */
	action void A_SpawnWeapons() {
		SchismWeaponSpawnDirectory.Get().SpawnAllWeapons(invoker.args[0], invoker.args[1], invoker);
	}

	States {
		Spawn:
			TNT1 A 1 NoDelay A_SpawnWeapons();
			Stop;
	}
}