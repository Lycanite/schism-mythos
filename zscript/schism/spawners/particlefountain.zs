class SchismParticleFountain : Actor {
	mixin SpawnParticleMixin;

	string user_particleName; // The name of the particle to spawn. See SchismParticles.
	float user_spreadLat; // A random lateral spread, no spreading at 0.
	float user_spreadLon; // A random longitudal spread, no spreading at 0.
	float user_spreadVert; // A random vertical spread, no spreading at 0.
	bool user_skybox; // If true, forces this fountain to always produce particles reglardless of player distance, etc.

	int particleTics;

	Default {
		Tag "Particle Fountain";
		Gravity 0;
		
		+NOBLOCKMAP;
		+NOCLIP;
		+DONTSPLASH;
		+DONTBLAST;
		+NOTELEPORT;
		+NOGRAVITY;
	}

	override void Activate(Actor activator) {
		super.Activate(activator);
		self.bDormant = false;
	}

	override void Deactivate(Actor deactivator) {
		super.Deactivate(deactivator);
		self.bDormant = true;
	}
	
	/** 
	 * Emits this fountain's particles.
	 * @arg 0 speed The speed of the particles, sets velocity to 10% of this value based on angle and pitch, defaults to 0 for no movement.
	 * @arg 1 interval The interval of the particle emission (tics between spawning), defaults to 60.
	 * @arg 2 count The number of the particles per emission, defaults to 1.
	 * @arg 3 duration The duration of the particles in seconds, defaults to 5.
	 * @arg 4 renderRangeFixed An override for the render range cutoff, defaults to 0 for automatic range based on spread values.
	 */
	action void A_ParticleFountain() {
		// Dormant Check:
		if (invoker.bDormant) {
			return;
		}

		// Parse Args:
		float particleSpeed = invoker.args[0] * 0.1;
		int interval = invoker.args[1] > 0 ? invoker.args[1] : 60;
		int count = invoker.args[2] > 0 ? invoker.args[2] : 1;
		int duration = invoker.args[3] > 0 ? invoker.args[3] : 5;
		int renderRangeFixed = invoker.args[4] > 0 ? invoker.args[4] : 0;
		int color = invoker.fillColor;

		// Range/Skybox Check:
		if (!invoker.user_skybox) {
			if (renderRangeFixed) {
				if (invoker.CheckRange(renderRangeFixed, true)) {
					return;
				}
			} else {
				float renderRange = Max(512, Abs(invoker.user_spreadLat) * 10);
				renderRange = Max(renderRange, Abs(invoker.user_spreadLon) * 10);
				renderRange = Max(renderRange, Abs(invoker.user_spreadVert) * 10);
				if (invoker.CheckRange(renderRange, true)) {
					return;
				}
				if (invoker.CheckRange(renderRange / 2, true)) {
					interval *= 2;
					count = Max(Round(Float(count) / 2), 1);
				}
			}
		}

		// Name Check:
		if (!invoker.user_particleName) {
			A_Log("No Particle Name set for Particle Fountain! Location: x" .. invoker.pos.x .. " y" .. invoker.pos.y .. " z" .. invoker.pos.z);
			return;
		}

		if (invoker.particleTics++ % interval == 0) {
			for (int i = 0; i < count; i++) {
				float offsetLat = invoker.user_spreadLat != 0 ? Random(-invoker.user_spreadLat, invoker.user_spreadLat) : 0;
				float offsetLon = invoker.user_spreadLon != 0 ? Random(-invoker.user_spreadLon, invoker.user_spreadLon) : 0;
				float offsetVert = invoker.user_spreadVert != 0 ? Random(-invoker.user_spreadVert, invoker.user_spreadVert) : 0;
				invoker.SpawnParticle(
					invoker.user_particleName,
					duration * TICRATE,
					particleSpeed,
					(offsetLat, offsetLon, offsetVert),
					0, 0,
					1.0,
					color
				);
			}
		}
	}
	
	States {
		Spawn:
			TNT1 A 1 NoDelay A_ParticleFountain();
			Loop;
	}
}