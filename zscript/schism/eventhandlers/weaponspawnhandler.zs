class SchismWeaponsSpawnEventHandler : StaticEventHandler {
	SchismWeaponSpawnDirectory spawner;

	virtual SchismWeaponSpawnDirectory GetSpawnDirectory() {
		if (!self.spawner) {
			self.spawner = SchismWeaponSpawnDirectory.Get();
		}
		return self.spawner;
	}
	
	override void WorldThingSpawned(WorldEvent e) {
		super.WorldThingSpawned(e);
		
		/*int dropChance = GetDropChance(e.thing);
		if (dropChance < 1.0 && random(0.0, 1.0) > dropChance) {
			return;
		}*/

		Actor actor = e.thing;

		// Dehacked Pickups:
		DehackedPickup dehackedPickup = DehackedPickup(actor);
		if (dehackedPickup) {
			actor = dehackedPickup.realPickup;
			if (!actor) { // Hack: Have a clone of dehacked item pick itself up to resolve it's real pickup, this will trigger a second spawn event on the clone.
				bool cloneSpawned;
				Actor cloneActor;
				[cloneSpawned, cloneActor] = dehackedPickup.A_SpawnItemEx(dehackedPickup.GetClassName());
				DehackedPickup cloneDehackedPickup = DehackedPickup(cloneActor);
				cloneDehackedPickup.CallTryPickup(cloneDehackedPickup);
				return;
			}
			if (!actor) {
				return;
			}
		}
		
		// Check Class if no immediate Name:
		if (!actor.GetClassName() || SchismWeapon(actor) || WeaponUpgrade(actor) || (!Weapon(actor) && !WeaponPiece(actor))) {
			return;
		}
		
		// Specific Spawners:
		if (self.GetSpawnDirectory().SpawnHexenWeapons(actor)) {
			return;
		}
		
		// Generic Spawners:
		string className = actor.GetClassName();
		string itemName = className.MakeLower();
		if (IsUltimateWeaponSpawner(actor, itemName)) { // BFG 9000 Tier
			self.GetSpawnDirectory().SpawnRandomWeapon(6, actor);
			return;
		}
		if (IsSpecialWeaponSpawner(actor, itemName)) { // Plasma Rifle Tier
			self.GetSpawnDirectory().SpawnRandomWeapon(5, actor);
			return;
		}
		if (IsPowerfulWeaponSpawner(actor, itemName)) { // Rocket Launcher Tier
			self.GetSpawnDirectory().SpawnRandomWeapon(4, actor);
			return;
		}
		if (IsMeleeWeaponSpawner(actor, itemName)) { // Chainsaw Tier
			self.GetSpawnDirectory().SpawnRandomWeapon(0, actor);
			return;
		}
		if (IsTertiaryWeaponSpawner(actor, itemName)) { // Chaingun Tier
			self.GetSpawnDirectory().SpawnRandomWeapon(3, actor);
			return;
		}
		if (IsSecondaryWeaponSpawner(actor, itemName)) { // Shotgun Tier
			self.GetSpawnDirectory().SpawnRandomWeapon(2, actor);
			return;
		}
		if (IsPrimaryWeaponSpawner(actor, itemName)) { // Pistol Tier
			self.GetSpawnDirectory().SpawnRandomWeapon(1, actor);
			return;
		}
		
		// Random Spawner:
		self.GetSpawnDirectory().SpawnRandomWeapon(Random(0, 6), actor);
	}
	
	override void WorldThingDied(WorldEvent e) {
		super.WorldThingDied(e);
		if (!e.thing) {
			return;
		}
		Actor killer = e.thing.target;
		if (!killer || !e.thing) {
			return;
		}
		
		// Check CVar:
		CVar weaponDropCvar = CVar.FindCVar("schism_weapon_drops");
		if (weaponDropCvar && !weaponDropCvar.GetBool()) {
			return;
		}
		
		// Minion Killer:
		MinionCreature killerMinion = MinionCreature(killer);
		if (killerMinion && killerMinion.GetFinalMaster()) {
			killer = killerMinion.GetFinalMaster();
		}
		
		SchismPlayerPawn schismPlayer = SchismPlayerPawn(killer);
		if (!schismPlayer) {
			return;
		}
		
		if (Random(0, 25) == 0) {
			self.GetSpawnDirectory().SpawnRandomWeapon(random(0, 6), e.thing);
		}
	}
	
	/** 
	 * Gets the drop chance for spawning a weapon on top of the provided actor.
	 * (Shotguns and chainguns being dropped by monsters means that we don't want class weapons spawning on them every time.)
	 */
	virtual double GetDropChance(Actor checkActor) {
		if (Shotgun(checkActor)) {
			return 0.5;
		}
		if (Chaingun(checkActor)) {
			return 0.75;
		}
		return 1;
	}
	
	/** 
	 * Checks if a melee category weapon should be spawned on the provided actor.
	 */
	virtual bool IsMeleeWeaponSpawner(Actor checkActor, string itemName) {
		if (Chainsaw(checkActor) || Gauntlets(checkActor)) {
			return true;
		}
		return itemName.IndexOf("pitchfork") >= 0 || itemName.IndexOf("voodoo") >= 0;
	}
	
	/** 
	 * Checks if a primary category weapon should be spawned on the provided actor.
	 */
	virtual bool IsPrimaryWeaponSpawner(Actor checkActor, string itemName) {
		if (Pistol(checkActor) || Staff(checkActor)) {
			return true;
		}
		return itemName.IndexOf("pistol") >= 0 || itemName.IndexOf("flaregun") >= 0;
	}
	
	/** 
	 * Checks if a secondary category weapon should be spawned on the provided actor.
	 */
	virtual bool IsSecondaryWeaponSpawner(Actor checkActor, string itemName) {
		if (Shotgun(checkActor) || Crossbow(checkActor) || StrifeCrossbow(checkActor)) {
			return true;
		}
		return itemName.IndexOf("shotgun") >= 0 || itemName.IndexOf("crossbow") >= 0 || itemName.IndexOf("sawed") >= 0 || itemName.IndexOf("daggerproj") >= 0;
	}
	
	/** 
	 * Checks if a tertiary category weapon should be spawned on the provided actor.
	 */
	virtual bool IsTertiaryWeaponSpawner(Actor checkActor, string itemName) {
		if (Chaingun(checkActor) || Blaster(checkActor) || AssaultGun(checkActor)) {
			return true;
		}
		return itemName.IndexOf("chaingun") >= 0 || itemName.IndexOf("minigun") >= 0 || itemName.IndexOf("tommy") >= 0 || itemName.IndexOf("vampirekiller") >= 0;
	}
	
	/** 
	 * Checks if a powerful category weapon should be spawned on the provided actor.
	 */
	virtual bool IsPowerfulWeaponSpawner(Actor checkActor, string itemName) {
		if (RocketLauncher(checkActor) || Mace(checkActor) || MiniMissileLauncher(checkActor)) {
			return true;
		}
		return itemName.IndexOf("rocket") >= 0 || itemName.IndexOf("missile") >= 0 || itemName.IndexOf("napalm") >= 0;
	}
	
	/** 
	 * Checks if a special category weapon should be spawned on the provided actor.
	 */
	virtual bool IsSpecialWeaponSpawner(Actor checkActor, string itemName) {
		if (PlasmaRifle(checkActor) || SuperShotgun(checkActor) || SkullRod(checkActor) || FlameThrower(checkActor) || Mauler(checkActor)) {
			return true;
		}
		return itemName.IndexOf("plasma") >= 0 || itemName.IndexOf("tesla") >= 0 || itemName.IndexOf("super") >= 0 || itemName.IndexOf("incinerator") >= 0;
	}
	
	/** 
	 * Checks if an ultimate category weapon should be spawned on the provided actor.
	 */
	virtual bool IsUltimateWeaponSpawner(Actor checkActor, string itemName) {
		if (BFG9000(checkActor) || PhoenixRod(checkActor) || StrifeGrenadeLauncher(checkActor)) {
			checkActor.A_Log("Ultimate Weapon Spawning from Cast: " .. itemName);
			return true;
		}
		return itemName.IndexOf("bfg") >= 0 || itemName.IndexOf("phoenix") >= 0 || itemName.IndexOf("lifeleech") >= 0 || itemName.IndexOf("perforator") >= 0 || itemName.IndexOf("heatwave") >= 0;
	}
}