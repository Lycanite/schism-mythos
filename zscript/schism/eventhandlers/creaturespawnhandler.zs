class SchismCreatureSpawnEventHandler : StaticEventHandler {
    SchismCreatureSpawnDirectory spawns;

	virtual SchismCreatureSpawnDirectory GetSpawns() {
		if (!self.spawns) {
			self.spawns = SchismCreatureSpawnDirectory.Get();
            self.PopulateSchismDoomSpawns(self.spawns);

            Cvar fullReplacementCvar = Cvar.FindCvar("schism_spawners_full_replace");
            bool fullReplacement = fullReplacementCvar.GetBool();
            if (fullReplacement) {
                self.PopulateClassicMonsterRoles(self.spawns);
            }
		}
		return self.spawns;
	}

    /**
     * Called on event handler registration.
     */
    override void OnRegister() {
		self.SetOrder(0);
    }

    /**
     * Called during actor class replacement.
     * @param event.replacee The actor class being replaced.
     * @param event.replacement The replacement actor class, can be edited.
     * @param event.isFinal If true, another event handler has set a replacement already, should probably not be changed further.
     */
    override void CheckReplacement(ReplaceEvent event) {
        if (event.isFinal) {
            return;
        }
        Class<Actor> replacement = self.GetSpawns().GetReplacement(event.replacee);
        if (replacement) {
            event.replacement = replacement;
        }
    }

    /**
     * Called when checking replacement actors via other functions (A_BossDeath), etc. Should match CheckReplacement.
     * @param event The actor replacement event. Replacement and replacee are reversed compared to CheckReplacement.
     */
    override void CheckReplacee(ReplacedEvent event) {
        if (event.isFinal) {
            return;
        }
        Class<Actor> replacement = self.GetSpawns().GetReplacement(event.replacement);
        if (replacement) {
            event.replacee = replacement;
        }
    }

    /**
     * Called when an actor is spawned into the world.
     * @param event The world event. Use event.thing to get the spawned actor.
     */
    override void WorldThingSpawned(WorldEvent event) {
        self.GetSpawns().OnActorSpawn(event.thing);
    }

	/**
	 * Populates doom replacement entries on the creature spawner, called each time a new creature spawner thinker is created.
	 */
	virtual void PopulateSchismDoomSpawns(SchismCreatureSpawnDirectory creatureSpawns) {
        creatureSpawns.AddSpawn("FormerPrivate", SPAWN_ROLE_GRUNT, "schism");
        creatureSpawns.AddSpawn("FormerCorporal", SPAWN_ROLE_GRUNT, "schism");

        creatureSpawns.AddSpawn("FormerSergent", SPAWN_ROLE_BOUNCER, "schism");
        creatureSpawns.AddSpawn("FormerLieutenant", SPAWN_ROLE_BOUNCER, "schism");

        creatureSpawns.AddSpawn("FormerMajor", SPAWN_ROLE_SNIPER, "schism");
        creatureSpawns.AddSpawn("FormerCaptain", SPAWN_ROLE_SNIPER, "schism");
        creatureSpawns.AddSpawn("FormerBrigadier", SPAWN_ROLE_SNIPER, "schism");

        creatureSpawns.AddSpawn("Belph", SPAWN_ROLE_SLINGER, "schism");
        creatureSpawns.AddSpawn("DrujBelph", SPAWN_ROLE_SLINGER, "schism");
        creatureSpawns.AddSpawn("VulgarBelph", SPAWN_ROLE_SLINGER, "schism");
        creatureSpawns.AddSpawn("AnzuBelph", SPAWN_ROLE_SLINGER, "schism");
        creatureSpawns.AddSpawn("MolochBelph", SPAWN_ROLE_SLINGER, "schism");
        creatureSpawns.AddSpawn("Tariaksuq", SPAWN_ROLE_SLINGER, "schism");

        creatureSpawns.AddSpawn("Corvazar", SPAWN_ROLE_LOBBER, "schism");

        creatureSpawns.AddSpawn("Behemophet", SPAWN_ROLE_BARON, "schism");
        creatureSpawns.AddSpawn("AnzuBehemophet", SPAWN_ROLE_BARON, "schism");
        creatureSpawns.AddSpawn("Paimon", SPAWN_ROLE_BARON, "schism");

        creatureSpawns.AddSpawn("Trite", SPAWN_ROLE_FODDER, "schism");
        creatureSpawns.AddSpawn("AdranTrite", SPAWN_ROLE_FODDER, "schism");
        creatureSpawns.AddSpawn("PretaBelph", SPAWN_ROLE_FODDER, "schism");
        creatureSpawns.AddSpawn("Caroverm", SPAWN_ROLE_FODDER, "schism");

        creatureSpawns.AddSpawn("Kathoga", SPAWN_ROLE_BRUTE, "schism");
        creatureSpawns.AddSpawn("MolochKathoga", SPAWN_ROLE_BRUTE, "schism");
        creatureSpawns.AddSpawn("Trite", SPAWN_ROLE_BRUTE, "schism", false, 5);
        creatureSpawns.AddSpawn("AdranTrite", SPAWN_ROLE_BRUTE, "schism", false, 5);
        creatureSpawns.AddSpawn("Caroverm", SPAWN_ROLE_BRUTE, "schism", false, 3);

        creatureSpawns.AddSpawn("PretaKathoga", SPAWN_ROLE_ASSASSIN, "schism");
        creatureSpawns.AddSpawn("PretaBelph", SPAWN_ROLE_ASSASSIN, "schism", false, 3);

        creatureSpawns.AddSpawn("NetherWraith", SPAWN_ROLE_SOUL, "schism");
        creatureSpawns.AddSpawn("Grigori", SPAWN_ROLE_SOUL, "schism");
        
        creatureSpawns.AddSpawn("Buer", SPAWN_ROLE_DART, "schism");
        creatureSpawns.AddSpawn("Malphas", SPAWN_ROLE_DART, "schism");

        creatureSpawns.AddSpawn("Malwrath", SPAWN_ROLE_GHAST, "schism");
        creatureSpawns.AddSpawn("DrujMalwrath", SPAWN_ROLE_GHAST, "schism");
        creatureSpawns.AddSpawn("AbaddonMalwrath", SPAWN_ROLE_GHAST, "schism");
        creatureSpawns.AddSpawn("MaraMalwrath", SPAWN_ROLE_GHAST, "schism");
        creatureSpawns.AddSpawn("Pyravon", SPAWN_ROLE_GHAST, "schism");
        creatureSpawns.AddSpawn("Malifex", SPAWN_ROLE_GHAST, "schism");
        creatureSpawns.AddSpawn("Grell", SPAWN_ROLE_GHAST, "schism");

        creatureSpawns.AddSpawn("Dologroth", SPAWN_ROLE_BROOD, "schism");

        creatureSpawns.AddSpawn("Astaroth", SPAWN_ROLE_TANK, "schism");
        creatureSpawns.AddSpawn("AsakkuAstaroth", SPAWN_ROLE_TANK, "schism");
        creatureSpawns.AddSpawn("AnzuAstaroth", SPAWN_ROLE_TANK, "schism");

        creatureSpawns.AddSpawn("AnzuOssifex", SPAWN_ROLE_SEEKER, "schism");
        creatureSpawns.AddSpawn("Vorach", SPAWN_ROLE_SEEKER, "schism");

        creatureSpawns.AddSpawn("Apollyon", SPAWN_ROLE_CASTER, "schism");
        creatureSpawns.AddSpawn("MaraApollyon", SPAWN_ROLE_CASTER, "schism");

        creatureSpawns.AddSpawn("Belial", SPAWN_ROLE_CANNON, "schism");

        creatureSpawns.AddSpawn("DrujBehemophet", SPAWN_ROLE_DESTROYER, "schism");
        creatureSpawns.AddSpawn("Abraxas", SPAWN_ROLE_DESTROYER, "schism");
        creatureSpawns.AddSpawn("Baal", SPAWN_ROLE_DESTROYER, "schism");

        creatureSpawns.AddSpawn("AsakkuAsnarax", SPAWN_ROLE_MASTERMIND, "schism");
        creatureSpawns.AddSpawn("AnzuAsnarax", SPAWN_ROLE_MASTERMIND, "schism");

        creatureSpawns.AddSpawn("Baal", SPAWN_ROLE_URGHAST, "schism");
    }

	/**
	 * Populates the roles of classic monsters, this does not add them so will fully replace them.
     * The schism-mythos-class-monsters mod has a cvar option to add them back in.
	 */
	virtual void PopulateClassicMonsterRoles(SchismCreatureSpawnDirectory creatureSpawns) {
        // Doom:
		CVar doomReplaceCvar = CVar.FindCVar("schism_replace_doom");
		if (!doomReplaceCvar || doomReplaceCvar.GetBool()) {
			creatureSpawns.AddRoleMapping("ZombieMan", SPAWN_ROLE_GRUNT);
            creatureSpawns.AddRoleMapping("ShotgunGuy", SPAWN_ROLE_BOUNCER);
            creatureSpawns.AddRoleMapping("ChaingunGuy", SPAWN_ROLE_SNIPER);
            creatureSpawns.AddRoleMapping("DoomImp", SPAWN_ROLE_SLINGER);
            creatureSpawns.AddRoleMapping("HellKnight", SPAWN_ROLE_LOBBER);
            creatureSpawns.AddRoleMapping("BaronOfHell", SPAWN_ROLE_BARON);
            creatureSpawns.AddRoleMapping("Demon", SPAWN_ROLE_BRUTE);
            creatureSpawns.AddRoleMapping("Spectre", SPAWN_ROLE_ASSASSIN);
            creatureSpawns.AddRoleMapping("LostSoul", SPAWN_ROLE_SOUL);
            creatureSpawns.AddRoleMapping("Cacodemon", SPAWN_ROLE_GHAST);
            creatureSpawns.AddRoleMapping("PainElemental", SPAWN_ROLE_BROOD);
            creatureSpawns.AddRoleMapping("Arachnotron", SPAWN_ROLE_TANK);
            creatureSpawns.AddRoleMapping("Revenant", SPAWN_ROLE_SEEKER);
            creatureSpawns.AddRoleMapping("Archvile", SPAWN_ROLE_CASTER);
            creatureSpawns.AddRoleMapping("Fatso", SPAWN_ROLE_CANNON);
            creatureSpawns.AddRoleMapping("Cyberdemon", SPAWN_ROLE_DESTROYER);
            creatureSpawns.AddRoleMapping("SpiderMastermind", SPAWN_ROLE_MASTERMIND);
		}

        // Doom Legacy of Rust:
        CVar doomRustReplaceCvar = CVar.FindCVar("schism_replace_doom_rust");
		if (!doomRustReplaceCvar || doomRustReplaceCvar.GetBool()) {
			creatureSpawns.AddRoleMappingName("ForgottenSoul", SPAWN_ROLE_DART);
            creatureSpawns.AddRoleMappingName("BurningSoul", SPAWN_ROLE_SOUL);
            creatureSpawns.AddRoleMappingName("Mindweaver", SPAWN_ROLE_TANK);
            creatureSpawns.AddRoleMappingName("Shocktrooper", SPAWN_ROLE_SNIPER);
            creatureSpawns.AddRoleMappingName("Vassago", SPAWN_ROLE_BARON);
            creatureSpawns.AddRoleMappingName("Tyrant", SPAWN_ROLE_CANNON);
		}

		// Heretic:
        CVar hereticReplaceCvar = CVar.FindCVar("schism_replace_heretic");
		if (!hereticReplaceCvar || hereticReplaceCvar.GetBool()) {
			creatureSpawns.AddRoleMapping("Mummy", SPAWN_ROLE_GRUNT);
            creatureSpawns.AddRoleMapping("MummyLeader", SPAWN_ROLE_BOUNCER);
            creatureSpawns.AddRoleMapping("Clink", SPAWN_ROLE_SNIPER);
            creatureSpawns.AddRoleMapping("Knight", SPAWN_ROLE_SLINGER);
            creatureSpawns.AddRoleMapping("Ironlich", SPAWN_ROLE_BARON);
            creatureSpawns.AddRoleMapping("Beast", SPAWN_ROLE_BRUTE);
            creatureSpawns.AddRoleMapping("MummyGhost", SPAWN_ROLE_ASSASSIN);
            creatureSpawns.AddRoleMapping("HereticImp", SPAWN_ROLE_DART);
            creatureSpawns.AddRoleMapping("Wizard", SPAWN_ROLE_GHAST);
            creatureSpawns.AddRoleMapping("Snake", SPAWN_ROLE_SEEKER);
		}

		// Hexen:
        CVar hexenReplaceCvar = CVar.FindCVar("schism_replace_hexen");
		if (!hexenReplaceCvar || hexenReplaceCvar.GetBool()) {
			creatureSpawns.AddRoleMapping("Ettin", SPAWN_ROLE_BRUTE);
            creatureSpawns.AddRoleMapping("Demon1", SPAWN_ROLE_SLINGER);
            creatureSpawns.AddRoleMapping("Demon2", SPAWN_ROLE_LOBBER);
            creatureSpawns.AddRoleMapping("Centaur", SPAWN_ROLE_SNIPER);
            creatureSpawns.AddRoleMapping("Stalker", SPAWN_ROLE_ASSASSIN);
            creatureSpawns.AddRoleMapping("FireDemon", SPAWN_ROLE_DART);
            creatureSpawns.AddRoleMapping("Wraith", SPAWN_ROLE_GHAST);
            creatureSpawns.AddRoleMapping("Bishop", SPAWN_ROLE_BROOD);
            creatureSpawns.AddRoleMapping("CentaurLeader", SPAWN_ROLE_SEEKER);
            creatureSpawns.AddRoleMapping("Iceguy", SPAWN_ROLE_CANNON);
		}

        // TODO Strife:
        CVar strifeReplaceCvar = CVar.FindCVar("schism_replace_strife");
		if (!strifeReplaceCvar || strifeReplaceCvar.GetBool()) {

        }
	}
}