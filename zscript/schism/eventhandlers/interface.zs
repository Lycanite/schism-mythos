class SchismInterfaceEventHandler : StaticEventHandler {
	// UI Properties:
	ui Vector2 hudScale;
	ui double textureScale;
	ui int vw;
	ui int vh;
	ui int cx;
	ui int cy;
	ui int right;
	ui int bottom;
	ui int clipX;
	ui int clipY;
	ui int clipW;
	ui int clipH;
	
	ui int barFrameWidth;
	ui int barFrameHeight;
	ui int barFrameX;
	ui int barFrameY;

	ui int screenblocks;
	
	// Player Properties:
	ui SchismPlayerPawn schismPlayer;
	
	// Animation:
	ui int animationLoop;
	
	
	override void OnRegister() {
		super.OnRegister();
	}
	
	override void RenderOverlay(RenderEvent e) {
		self.InitUI();

		// Hidden HUD:
		if (screenblocks == 12) {
			return;
		}
		
		// Invalid Playter Class:
		self.schismPlayer = SchismPlayerPawn(e.Camera);
		if (!self.schismPlayer) {
			return;
		}
		
		// Draw Schism HUD:
		self.DrawPowerBar(e);
		self.DrawPlayers(e);
		self.DrawCooldowns(e);
	}
	
	ui virtual void InitUI() {
		self.screenblocks = Cvar.FindCVar("screenblocks").GetInt();

		self.hudScale = StatusBar.GetHUDScale();
		self.textureScale = 0.5; // Changing this currently doesn't work.
		self.vw = Screen.GetWidth() / self.hudScale.x;
		self.vh = Screen.GetHeight() / self.hudScale.y;
		
		self.cx = Screen.GetWidth() / 2;
		self.cy = Screen.GetHeight() / 2;
		self.right = Screen.GetHeight();
		self.bottom = Screen.GetHeight();
		
		[self.clipX, self.clipY, self.clipW, self.clipH] = Screen.GetClipRect();
		
		self.animationLoop++;
	}
	
	
	// Scaling:
	ui virtual int ScaleX(int pos) {
		return pos * self.hudScale.x * self.textureScale;
	}
	
	ui virtual int ScaleY(int pos) {
		return pos * self.hudScale.y * self.textureScale;
	}
	
	
	// Clipping:
	ui virtual void SetClip(int x, int y, int width, int height) {
		int scaledX = x * self.textureScale;
		int scaledY = y * self.textureScale;
		int scaledW = width;
		int scaledH = height;
		Screen.SetClipRect(scaledX, scaledY, scaledX + scaledW, scaledY + scaledH);
	}
	
	ui virtual void ResetClip() {
		Screen.SetClipRect(self.clipX, self.clipY, self.clipW, self.clipH);
	}
	
	
	// Drawing:
	ui virtual void DrawTexture(TextureID texture, int x, int y, int width, int height) {
		int scaledX = x / self.hudScale.x / self.textureScale;
		int scaledY = y / self.hudScale.y / self.textureScale;
		int scaledVW = self.vw / self.textureScale;
		int scaledVH = self.vh / self.textureScale;
		self.SetClip(x, y, width, height);
		Screen.DrawTexture(texture, true, scaledX, scaledY, DTA_KeepRatio, true, DTA_VIRTUALWIDTH, scaledVW, DTA_VIRTUALHEIGHT, scaledVH);
		self.ResetClip();
	}
	
	ui virtual void DrawBox(Color color, int x, int y, int width, int height) {
		Screen.Clear(x, y, x + width, y + height, color);
	}
	
	ui virtual void DrawText(String text, Font font, Color color, int x, int y) {
		Screen.DrawText(font, color, x, y, text);
	}
	
	
	// Power Bar and Weapon Upgrades Widget:
	ui virtual void DrawPowerBar(RenderEvent e) {		
		string playerClass = self.schismPlayer.baseName;
		int power = self.schismPlayer.CountInv(self.schismPlayer.GetAmmoClass(-1, 0));
		double powerNormal = double(power) / 200;
		
		
		// Power Bar:
		TextureID barFrame = TexMan.CheckForTexture("graphics/hud/"..playerClass.."/powerbar_back.png", TexMan.Type_Any);
		self.barFrameWidth = self.ScaleX(512);
		self.barFrameHeight = self.ScaleY(51);
		self.barFrameX = self.screenblocks == 11 ? self.cx - (self.barFrameWidth / 2) : self.ScaleX(2);

		self.barFrameY = self.bottom - self.barFrameHeight - self.ScaleY(2);
		
		String fillTextureSuffix = "";
		if (powerNormal >= 1) {
			fillTextureSuffix = "_100";
		} else if (powerNormal >= 0.75) {
			fillTextureSuffix = "_75";
		} else if (powerNormal >= 0.5) {
			fillTextureSuffix = "_50";
		} else if (powerNormal >= 0.25) {
			fillTextureSuffix = "_25";
		}
		TextureID barFill = TexMan.CheckForTexture("graphics/hud/" .. playerClass .. "/powerbar_fill" .. fillTextureSuffix .. ".png", TexMan.Type_Any);
		int barFillWidth = double(self.ScaleX(497)) * powerNormal;
		int barFillHeight = self.ScaleY(42);
		int fillOffsetX = self.ScaleX(8);
		int fillOffsetY = self.ScaleY(4);
		int barFillX = self.barFrameX + fillOffsetX;
		int barFillY = self.barFrameY + fillOffsetY;
		double barFillScale = powerNormal;
		
		
		// Draw Background:
		self.DrawTexture(barFill, barFillX, barFillY, barFillWidth, barFillHeight);
		self.DrawTexture(barFrame, self.barFrameX, self.barFrameY, self.barFrameWidth, self.barFrameHeight);
		
		
		// Draw Foreground:
		self.DrawText(""..power, BigFont, "#BB2233", self.barFrameX + self.ScaleX(32), self.barFrameY + (double(barFrameHeight) / 2) - self.ScaleY(8));
		
		
		// Upgrade Items:
		int activeWeaponCategory = self.schismPlayer.GetActiveWeaponCategory();
		if (activeWeaponCategory >= 0) {
			String weaponLetter = "a";
			if (activeWeaponCategory == 1) {
				weaponLetter = "b";
			} else if (activeWeaponCategory == 2) {
				weaponLetter = "c";
			}
			
			int upgradeItemWidth = self.ScaleX(52);
			int upgradeItemHeight = self.ScaleY(52);
			int upgradeItemSpacing = self.ScaleX(4);
			for (int upgradeIndex = 1; upgradeIndex <= 4; upgradeIndex++) {
				if (!self.schismPlayer.HasWeaponUpgrade(activeWeaponCategory, upgradeIndex)) {
					continue;
				}
				TextureID upgradeItem = TexMan.CheckForTexture("graphics/hud/" .. playerClass .. "/upgrade_" .. weaponLetter .. "0" .. upgradeIndex .. ".png", TexMan.Type_Any);
				int upgradeItemX = self.barFrameX + self.barFrameWidth - ((upgradeItemWidth + upgradeItemSpacing) * (5 - upgradeIndex)) - (upgradeItemSpacing * 2);
				int upgradeItemY = self.barFrameY - (double(upgradeItemHeight) * 0.6);
				self.DrawTexture(upgradeItem, upgradeItemX, upgradeItemY, upgradeItemWidth, upgradeItemHeight);
			}
			
			int weaponPassiveLevel = self.schismPlayer.GetWeaponPassiveLevel(activeWeaponCategory);
			if (weaponPassiveLevel > 0) {
				TextureID upgradeItem = TexMan.CheckForTexture("graphics/hud/" .. playerClass .. "/passive_" .. weaponLetter .. ".png", TexMan.Type_Any);
				int upgradeItemX = self.barFrameX + self.barFrameWidth - ((upgradeItemWidth + upgradeItemSpacing) * 5) - (upgradeItemSpacing * 2);
				int upgradeItemY = self.barFrameY - (double(upgradeItemHeight) * 0.6);
				self.DrawTexture(upgradeItem, upgradeItemX, upgradeItemY, upgradeItemWidth, upgradeItemHeight);
				self.DrawText("" .. weaponPassiveLevel, BigFont, "#FFFFFF", upgradeItemX + self.ScaleX(30), upgradeItemY + self.ScaleX(30));
			}
		}
		
		
		// Combo Points:
		TextureID comboPoint = TexMan.CheckForTexture("graphics/hud/"..playerClass.."/combopoint.png", TexMan.Type_Any);
		int comboWidth = self.ScaleX(24);
		int comboHeight = self.ScaleY(24);
		int comboStartX = self.barFrameX + self.ScaleX(64);
		int comboY = self.barFrameY + (double(barFrameHeight) / 2) - (double(comboHeight) / 2);
		for (int i = 0; i < schismPlayer.comboPoints; i++) {
			self.DrawTexture(comboPoint, comboStartX + ((comboWidth + 2) *(i % 20)), comboY, comboWidth, comboHeight);
		}
		
		
		// Minion Count:
		if (schismPlayer.schismPlayerInfo) {
			int minionCount = 0;
			minionCount = schismPlayer.schismPlayerInfo.GetMinionCount(self.schismPlayer.PlayerNumber());
			if (minionCount > 0) {
				self.DrawText("Minions: "..minionCount, BigFont, "#225533", self.barFrameX + 2, barFillY - 14);
			}
		}
	}
	
	
	// Player Notifications Widget:
	ui virtual void DrawPlayers(RenderEvent e) {
		// Setup Portrait:
		int portraitW = self.ScaleX(64);
		int portraitH = self.ScaleY(64);
		int portraitStartX = self.screenblocks == 11 ? self.barFrameX - self.ScaleX(10) : self.right - self.ScaleX(2);
		int portraitStartY = self.bottom - portraitH - self.ScaleY(10);
		int portraitSpacing = self.ScaleX(4);
		int portraitBounce = self.ScaleY(sin(self.animationLoop * 8 % 359) * 5);
		
		// Setup Icons:
		TextureID beaconIcon = TexMan.CheckForTexture("graphics/hud/beacon.png", TexMan.Type_Any);
		TextureID deadIcon = TexMan.CheckForTexture("graphics/hud/dead.png", TexMan.Type_Any);
		int iconsW = self.ScaleX(32);
		int iconsH = self.ScaleY(32);
		int iconsY = portraitStartY - (iconsH / 2);
		
		// Setup Healthbar:
		TextureID healthbarFill = TexMan.CheckForTexture("graphics/hud/healthbar_fill.png", TexMan.Type_Any);
		int healthbarW = self.ScaleX(64);
		int healthbarH = self.ScaleY(16);
		int healthbarY = portraitStartY + portraitH - (healthbarH / 2);
		
		int i = 0;
		for (int playerNum = 0; playerNum < players.Size(); playerNum++) {
			if (!playeringame[playerNum]) {
				continue;
			}
			PlayerInfo playerInfoInstance = players[playerNum];
			SchismPlayerPawn schismPlayer = SchismPlayerPawn(playerInfoInstance.mo);
			if (schismPlayer && schismPlayer != self.schismPlayer) {
				string playerClass = schismPlayer.baseName;
				bool playerAlert = schismPlayer.health <= 0 || schismPlayer.beaconing;
				
				// Player Portrait:
				TextureID portrait = TexMan.CheckForTexture("graphics/hud/" .. playerClass .. "/portrait.png", TexMan.Type_Any);
				int portraitX = portraitStartX - (portraitW *(i + 1)) - (portraitSpacing *(i + 1));
				int portraitY = portraitStartY;
				if (playerAlert)
				{
					portraitY += portraitBounce;
				}
				
				// Icons:
				int iconsX = portraitX + portraitW - (iconsW / 2);
				
				// Player Healthbar:
				double healthNormal = min(1, double(schismPlayer.health) / max(schismPlayer.maxHealth, 1));
				TextureID healthbar = TexMan.CheckForTexture("graphics/hud/" .. playerClass .. "/healthbar.png", TexMan.Type_Any);
				int healthbarX = portraitX;
				int healthbarFillW = double(healthbarW - self.ScaleX(4)) * healthNormal;
				int healthbarFillH = healthbarH - self.ScaleY(4);
				
				// Draw:
				self.DrawTexture(healthbar, healthbarX, healthbarY, healthbarW, healthbarH);
				self.DrawTexture(healthbarFill, healthbarX + self.ScaleX(2), healthbarY + self.ScaleY(2), healthbarFillW, healthbarFillH);
				self.DrawTexture(portrait, portraitX, portraitY, portraitW, portraitH);
				if (schismPlayer.beaconing) {
					self.DrawTexture(beaconIcon, iconsX, iconsY, iconsW, iconsH);
				}
				if (schismPlayer.health <= 0) {
					self.DrawTexture(deadIcon, iconsX, iconsY + (iconsH / 2), iconsW, iconsH);
				}
				
				i++;
			}
		}
	}
	
	
	// Cooldowns Widget:
	ui virtual void DrawCooldowns(RenderEvent e) {
		// Setup:
		string playerClass = self.schismPlayer.baseName;
		TextureID abilityAIcon = TexMan.CheckForTexture("graphics/hud/" .. playerClass .. "/ability_a.png", TexMan.Type_Any);
		TextureID abilityBIcon = TexMan.CheckForTexture("graphics/hud/" .. playerClass .. "/ability_b.png", TexMan.Type_Any);
		TextureID beaconTeleportIcon = TexMan.CheckForTexture("graphics/hud/beacon_teleport.png", TexMan.Type_Any);
		int cooldownW = self.ScaleX(64);
		int cooldownH = self.ScaleY(64);
		int cooldownStartX = self.barFrameX + self.barFrameWidth + self.ScaleX(10);
		int cooldownY = self.bottom - cooldownH - self.ScaleY(10);
		int cooldownSpacing = self.ScaleX(4);
		int cooldownTextOffsetX = cooldownW / 2;
		int cooldownTextY = cooldownY + cooldownH - self.ScaleY(16);
		
		// Draw:
		int cooldownX = cooldownStartX;
		if (self.schismPlayer.abilityCooldown > 0) {
			self.DrawTexture(abilityAIcon, cooldownX, cooldownY, cooldownW, cooldownH);
			self.DrawText(formatCooldown(self.schismPlayer.abilityCooldown), BigFont, "#225533", cooldownX + cooldownTextOffsetX, cooldownTextY);
			cooldownX += cooldownW + cooldownSpacing;
		}
		if (self.schismPlayer.abilityBCooldown > 0) {
			self.DrawTexture(abilityBIcon, cooldownX, cooldownY, cooldownW, cooldownH);
			self.DrawText(formatCooldown(self.schismPlayer.abilityBCooldown), BigFont, "#225533", cooldownX + cooldownTextOffsetX, cooldownTextY);
			cooldownX += cooldownW + cooldownSpacing;
		}
		if (self.schismPlayer.beaconTeleportCooldown > 0) {
			self.DrawTexture(beaconTeleportIcon, cooldownX, cooldownY, cooldownW, cooldownH);
			self.DrawText(formatCooldown(self.schismPlayer.beaconTeleportCooldown), BigFont, "#225533", cooldownX + cooldownTextOffsetX, cooldownTextY);
			cooldownX += cooldownW + cooldownSpacing;
		}
	}
	
	ui string formatCooldown(int cooldown) {
		double scaledCooldown = double(cooldown) / 35;
		return String.Format("%.1f%s", scaledCooldown, "s");
	}
}

// https://github.com/coelckers/gzdoom/blob/master/wadsrc/static/zscript/ui/statusbar/statusbar.zs
// https://github.com/coelckers/gzdoom/blob/master/wadsrc/static/zscript/base.zs#L203
