/**
 * More customisable teleport destination with physics allowing scrolling and 3d floors to move it, etc.
 */
class SchismTeleportDest : TeleportDest3 {
    default {
        -NOBLOCKMAP;
        -NOSECTOR;
        -PUSHABLE;
        -SHOOTABLE;
        -SOLID;
    }
}

/**
 * Advanced map spot that can hold information about its sector tag, line tag and a generic status id.
 * If tagged, it will call ACS scripts providing additional sector information such as floor texture.
 */
class SchismNodeSpot : MapSpot {
    int user_sector_tag;
    int user_line_tag;
    int user_status_id;
    int user_north_node_tag;
    int user_east_node_tag;
    int user_south_node_tag;
    int user_west_node_tag;
}

/**
 * Easy to animate skybox viewpoint without hacky acs, etc.
 */
class SchismSkyViewpoint : SkyViewpoint {
    double user_spin_angle;
    double user_spin_pitch;
    double user_spin_roll;
    
    override void Tick() {
        super.Tick();
        if (level.isFrozen() || self.isFrozen()) {
            return;
        }

        self.angle += self.user_spin_angle;
        self.pitch += self.user_spin_pitch;
        self.roll += self.user_spin_roll;
    }
}

/**
* Counts the number of actors (defaults to players and monsters) in the same sector each tick then calls this counter's special on change.
* Args 0, 1, 3 and 4 are passed as the special.
* Arg 2 of the special called will be the number of detected actors (script param 2).
* @arg 2 A tag to restrict detected actors to.
*/
class SchismSectorActorCounter : Actor {
    double user_searchRange; // The range to search for same sector actors, defaults to 1024.
	bool user_noPlayers; // If true, players will not be counted, defaults to false.
	bool user_noMonsters; // If true, monsters will not be counted, defaults to false.
	bool user_projectiles; // If true, projectiles will be counted, defaults to false.
	bool user_misc; // If true, any other actor will be counted, defaults to false.
    int lastCount; // Count from last tick, used to detect a change in count.

	override void Activate(Actor activator) {
		super.Activate(activator);
		self.bDormant = false;
	}

	override void Deactivate(Actor deactivator) {
		super.Deactivate(deactivator);
		self.bDormant = true;
	}
    
    override void BeginPlay() {
        super.BeginPlay();
        if (self.user_searchRange == 0) {
            self.user_searchRange = 1024;
        }
    }
    
    override void Tick() {
        super.Tick();
		if (self.bDormant) {
			return;
		}
        
        Sector sector = self.CurSector;
        if (sector) {
			int tag = self.args[2];
            int currentCount = 0;
            BlockThingsIterator iter = BlockThingsIterator.Create(self, self.user_searchRange, false);
            while (iter.Next()) {
                Actor target = iter.thing;
                if (!target) {
                    continue;
                }

				// Check Actor Tag and Sector:
                if (tag > 0 && target.tid != tag) {
                    continue;
                }
                if (!target.CurSector || target.CurSector.Index() != sector.Index()) {
                    continue;
                }

				// Check Actor Type:
				if (!self.user_noMonsters && target.bIsMonster) {
					currentCount++;
					continue;
				}
				if (!self.user_noPlayers && PlayerPawn(target)) {
					currentCount++;
					continue;
				}
				if (self.user_projectiles && target.bMissile) {
					currentCount++;
					continue;
				}
				if (self.user_misc) {
					currentCount++;
				}
            }
            
            if (self.lastCount != currentCount) {
                //ACS_ExecuteAlways(self.args[0], self.args[1], currentCount, self.args[3], self.args[4]);
                A_CallSpecial(self.special, self.args[0], self.args[1], currentCount, self.args[3], self.args[4]);
            }
            self.lastCount = currentCount;
        }
    }
}

/**
* Damages actors in this thing's sector based the sector's udmf damage type with actor range.
* @arg 0 The amount of damage to deal, negative for healing.
* @arg 1 The interval to deal damage on.
*/
class SchismSectorDamage : Actor {
    double user_searchRange;
    double user_actorTag;
    int lifetime;

	override void Activate(Actor activator) {
		super.Activate(activator);
		self.bDormant = false;
	}

	override void Deactivate(Actor deactivator) {
		super.Deactivate(deactivator);
		self.bDormant = true;
	}
    
    override void BeginPlay() {
        super.BeginPlay();
        if (self.user_searchRange == 0) {
            self.user_searchRange = 1024;
        }
    }
    
    override void Tick() {
        super.Tick();
		if (self.bDormant) {
			return;
		}
		int damage = self.args[0];
		int interval = self.args[1];

		// Interval:
		if (++self.lifetime % interval != 0) {
			return;
		}

        Sector sector = self.CurSector;
        if (sector) {
            BlockThingsIterator iter = BlockThingsIterator.Create(self, self.user_searchRange, false);
            while (iter.Next()) {
                Actor target = iter.thing;
                if (!target) {
                    continue;
                }
                if (self.user_actorTag > 0 && target.tid != self.user_actorTag) {
                    continue;
                }
                if (!target.CurSector || target.CurSector.Index() != sector.Index()) {
                    continue;
                }
                
				// Damage/Heal:
				if (damage > 0) {
					target.DamageMobj(null, null, damage, damageType, DMG_THRUSTLESS);
				} else if (damage < 0) {
					target.A_DamageSelf(damage, damageType, DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
				}
            }
        }
    }
}