class WeaponPassive : WeaponUpgrade {
	Default {
		Inventory.PickupMessage "You have found a weapon passive upgrade!";
		Inventory.Amount 1;
		Inventory.MaxAmount 3;
		WeaponUpgrade.ExtraAmmoAmount 0;
	}
	
	States {
		Spawn:
			REIS A 1 bright;
			Loop;
	}
}
