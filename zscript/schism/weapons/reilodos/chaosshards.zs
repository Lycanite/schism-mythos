class ChaosShards : MWeapFrost {
	Default {
		Weapon.SlotNumber 3;
		Inventory.PickupMessage "The Frost Shards spell imbued with Chaos!";
		Inventory.PickupSound "ReilodosTaunt";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		
		+INVENTORY.RESTRICTABSOLUTELY;
	}
	
	action void A_FireChaosCone() {
		bool conedone = false;
		FTranslatedLineTarget t;

		if (player == null)
		{
			return;
		}

		Weapon weapon = player.ReadyWeapon;
		if (weapon != null)
		{
			if (!weapon.DepleteAmmo(weapon.bAltFire))
				return;
		}
		A_StartSound("MageShardsFire", CHAN_WEAPON);

		int damage = 90+(random[MageCone]() & 15);
		for (int i = 0; i < 16; i++)
		{
			double ang = angle + i*(45./16);
			double slope = AimLineAttack(ang, DEFMELEERANGE, t, 0., ALF_CHECK3D);
			if (t.linetarget)
			{
				t.linetarget.DamageMobj(self, self, damage, "Ice", DMG_USEANGLE, t.angleFromSource);
				conedone = true;
				break;
			}
		}

		// didn't find any creatures, so fire projectiles
		if (!conedone)
		{
			Actor mo = SpawnPlayerMissile("ChaosShardProjectile");
			if (mo)
			{
				mo.special1 = FrostMissile.SHARDSPAWN_LEFT|FrostMissile.SHARDSPAWN_DOWN|FrostMissile.SHARDSPAWN_UP|FrostMissile.SHARDSPAWN_RIGHT;
				mo.special2 = 3; // Set sperm count(levels of reproductivity)
				mo.target = self;
				mo.args[0] = 3;		// Mark Initial shard as super damage
			}
		}
	}
	
	States {
		Ready:
			REH1 B 0 A_GunFlash("Overlay", GFF_NOEXTCHANGE);
			REH1 BCDBCDBC 6 A_WeaponReady;
			Loop;
			
		Overlay:
			REFS ABCABCABCABC 4;
			Stop;
			
		Deselect:
			REH1 A 1 bright A_Lower(12);
			Loop;
			
		Select:
			REH1 A 1 bright A_Raise(12);
			Loop;
			
		Fire:
			REH1 E 0 A_GunFlash("Flash");
			REH1 E 3;
			REH1 F 4;
		
		Hold:
			REH1 G 0 A_GunFlash("FlashHold");
			REH1 G 3;
			REH1 H 5;
			REH1 I 3 A_FireChaosCone();
			REH1 G 3;
			REH1 B 9;
			REH1 B 10 A_ReFire();
			Goto Ready;
			
		Flash:
			REFS DEDE 2;
			Stop;
			
		FlashHold:
			REFS DEDEDEDEDEDEDEDE 2;
			REFS D 1;
			Stop;
			
		Spawn:
			WMCS ABC 8 bright;
			Loop;
	}
}


class ChaosShardProjectile : FrostMissile {
	Default {
		Obituary "%o was frozen by %k's Chaotic Frost Shards!";
	}
	
	void A_ShedChaosShard() {
		int spawndir = special1;
		int spermcount = special2;
		Actor mo;

		if (spermcount <= 0)
		{
			return;				// No sperm left, lol
		}
		special2 = 0;
		spermcount--;

		// every so many calls, spawn a new missile in its set directions
		if (spawndir & SHARDSPAWN_LEFT)
		{
			mo = SpawnMissileAngleZSpeed(pos.z, "ChaosShardProjectile", angle + 5, 0,(20. + 2 * spermcount), target);
			if (mo)
			{
				mo.special1 = SHARDSPAWN_LEFT;
				mo.special2 = spermcount;
				mo.Vel.Z = Vel.Z;
				mo.args[0] =(spermcount==3)?2:0;
			}
		}
		if (spawndir & SHARDSPAWN_RIGHT)
		{
			mo = SpawnMissileAngleZSpeed(pos.z, "ChaosShardProjectile",	angle - 5, 0,(20. + 2 * spermcount), target);
			if (mo)
			{
				mo.special1 = SHARDSPAWN_RIGHT;
				mo.special2 = spermcount;
				mo.Vel.Z = Vel.Z;
				mo.args[0] =(spermcount==3)?2:0;
			}
		}
		if (spawndir & SHARDSPAWN_UP)
		{
			mo = SpawnMissileAngleZSpeed(pos.z + 8., "ChaosShardProjectile", angle, 0,(15. + 2 * spermcount), target);
			if (mo)
			{
				mo.Vel.Z = Vel.Z;
				if (spermcount & 1)			// Every other reproduction
					mo.special1 = SHARDSPAWN_UP | SHARDSPAWN_LEFT | SHARDSPAWN_RIGHT;
				else
					mo.special1 = SHARDSPAWN_UP;
				mo.special2 = spermcount;
				mo.args[0] =(spermcount==3)?2:0;
			}
		}
		if (spawndir & SHARDSPAWN_DOWN)
		{
			mo = SpawnMissileAngleZSpeed(pos.z - 4., "ChaosShardProjectile", angle, 0,(15. + 2 * spermcount), target);
			if (mo)
			{
				mo.Vel.Z = Vel.Z;
				if (spermcount & 1)			// Every other reproduction
					mo.special1 = SHARDSPAWN_DOWN | SHARDSPAWN_LEFT | SHARDSPAWN_RIGHT;
				else
					mo.special1 = SHARDSPAWN_DOWN;
				mo.special2 = spermcount;
				mo.target = target;
				mo.args[0] =(spermcount==3)?2:0;
			}
		}
	}
	
	States {
		Spawn:
			CSHD A 2 bright;
			CSHD A 3 bright A_ShedChaosShard();
			CSHD B 3 bright;
			CSHD C 3 bright;
			Loop;
			
		Death:
			CSHD DEFGH 5 bright;
			Stop;
	}
}