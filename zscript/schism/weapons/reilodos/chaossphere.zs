class ChaosSphere : WeaponUltimate {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You have obtained a Chaos Sphere! With this you may activate a weapon's ultimate ability!";
		
		Inventory.Icon "AREIV0";
		Inventory.PickupSound "Reilodos/Taunt";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	
	States {
		Spawn:
			AREI V 1 bright;
			Loop;
	}
}
