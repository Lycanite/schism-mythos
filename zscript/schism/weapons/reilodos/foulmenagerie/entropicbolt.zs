class EntropicBoltScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Foul Menagerie can now cast Entropic Bolt to charge targets with chaos and pull explosive Wraamon towards them!";
		
		Inventory.Icon "REISB0";
		Inventory.PickupSound "FoulMenagerie/Pickup";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "ChaosEnergy";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			REIS B 1 bright;
			Loop;
	}
}


class FoulMenagerieEntropicbolt : SchismProjectile {
	int primaryCost;
	int secondaryCost;
	
	FoulMenagerie foulMenagerie;
	
	Default {
		Radius 2;
		Height 2;
		Scale 0.5;
		Speed 100;
		DamageFunction 1;
		Alpha 0.75;
		RenderStyle "Translucent";
		
		SeeSound "FoulMenagerie/Fire/Entropic";
		DeathSound "FoulMenagerie/Projectile/Explode/Entropic";
		
		+NODAMAGETHRUST;
		+PAINLESS;
		+SEEKERMISSILE;
		+SCREENSEEKER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		primaryCost = 1;
		secondaryCost = 10;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}

		FireEffects();
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		damage = super.DoSpecialDamage(targetActor, damage, damageType);
		Array<MinionCreature> minions;
		
		// Pull Wraamon:
		if (damage > 0) {
			bool minionsPulled = false;
			SchismPlayerInfo schismPlayerInfo = SchismPlayerInfo.Get();
			if (schismPlayerInfo) {
				minions.Clear();
				minions.Copy(schismPlayerInfo.stats[target.PlayerNumber()].minions);
				for (int i = 0; i < minions.Size(); i++) {
					WraamonVerdant minion = WraamonVerdant(minions[i]);
					if (minion) {
						minion.Thrust(minion.Distance2D(targetActor) / 2, minion.AngleTo(targetActor));
						minion.AddZ(targetActor.pos.z - minion.pos.z);
						minion.SetDetonationTarget(targetActor, "EntropicBoltBurstProjectile", 100);
						minionsPulled = true;
					}
				}
			}
			
			if (minionsPulled) {
				target.A_TakeInventory("ChaosEnergy", primaryCost);
				target.A_TakeInventory("HavocEnergy", secondaryCost);
			}
		}
		
		// Entropic Charge (Enemies and Allies):
		SchismStatusEffects.InflictEffect("SchismEntropicEffect", self.target, self.foulMenagerie, targetActor);
		
		return damage;
	}
	
	virtual void FireEffects() {
		self.SpawnParticle("ChaosSpark", 8, 3, (FRandom(-4, 4), FRandom(-4, 4), 0), Random(0, 359), Random(-45, 45));
	}
	
	States {
		Spawn:
			FMEN OOOOPPPPQQQQRRRR 1 A_SeekerMissile(0, 90, SMF_LOOK|SMF_PRECISE);
			Loop;
			
		Death:
			FMEN STU 4;
			Stop;
	}
}


class EntropicBoltBurstProjectile : FoulBurstProjectile {
	Default {
		Scale 2;
		SeeSound "FoulMenagerie/Ascension";
		DeathSound "FoulMenagerie/Ascension/Finish";
		Obituary "%o was evicerated by %k's Entropic Bolt!";
	}
	
	override void FoulBurst(bool giveHavoc) {
		A_QuakeEx(2, 2, 2, 10, 0, 500, "world/quake", 0, 1, 1, 0, 0, 1, 1);
		int targets = SchismAttacks.Explode(self, self.target, 10, 300, 0, 1, false, false, "Chaos");
		if (giveHavoc && targets > 0 && target) {
			target.A_GiveInventory("HavocEnergy", targets * 2, AAPTR_MASTER);
		}
		FireEffects();
	}
}
