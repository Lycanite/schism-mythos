class ChaoticChupacabraScroll : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Foul Menagerie will now call upon Chaotic Phosphorescent Chupacabra!";
		
		Inventory.Icon "REISM0";
		Inventory.PickupSound "FoulMenagerie/Pickup";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "ChaosEnergy";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			REIS M 1 bright;
			Loop;
	}
}

class FoulMenagerieChaoticChupacabraBolt : FoulMenageriePortalBolt {
	Default {
		Radius 10;
		Height 15;
		Scale 2;
		Speed 20;
		Gravity 1;
		DamageFunction 0;
		
		SeeSound "FoulMenagerie/Hounds";
		DeathSound "FoulMenagerie/Hounds/Finish";
		Obituary "%o was mauled by %k's Chupacabra!";

		SchismProjectile.SummonType "ChupacabraPhosphor";
	}
	
	override Actor SummonMinion() {
		Actor minion = super.SummonMinion();
		MinionCreature minionCreature = MinionCreature(minion);
		if (minionCreature) {
			minionCreature.permanentSummon = true;
			minionCreature.requireMasterWeapon = "FoulMenagerie";
		}
		return minion;
	}
}
