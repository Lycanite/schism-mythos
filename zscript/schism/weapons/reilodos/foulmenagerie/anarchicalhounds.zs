class AnarchicalHoundsScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Foul Menagerie can now cast Anarchical Hounds to summon a pair of Chupacabra!";
		
		Inventory.Icon "REISC0";
		Inventory.PickupSound "FoulMenagerie/Pickup";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "ChaosEnergy";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			REIS C 1 bright;
			Loop;
	}
}

class FoulMenagerieAnarchicalHounds : FoulMenageriePortalBolt {
	int summonCost;
	int summonCostSecondary;
	
	Default {
		Radius 10;
		Height 15;
		Scale 2;
		Speed 20;
		Gravity 1;
		DamageFunction 0;
		
		SeeSound "FoulMenagerie/Hounds";
		DeathSound "FoulMenagerie/Hounds/Finish";
		Obituary "%o was mauled by %k's Anarchical Hounds!";
	}
	
	override void BeginPlay() {	
		super.BeginPlay();
		summonType = "ChupacabraPhosphor";
		summonCost = 1;
		summonCostSecondary = 50;
	}
	
	override Actor SummonMinion() {
		Actor minion = super.SummonMinion();
		MinionCreature minionCreature = MinionCreature(minion);
		if (minionCreature)
		{
			minionCreature.permanentSummon = true;
			minionCreature.master.A_TakeInventory("ChaosEnergy", summonCost);
			minionCreature.master.A_TakeInventory("HavocEnergy", summonCostSecondary);
		}
		return minion;
	}
}
