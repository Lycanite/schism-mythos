class NixianRiftScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Void Manipulation can now rip open a Nixian Rift which can be teleported to unleashing void spints on repeat use!";
		
		Inventory.Icon "REISL0";
		Inventory.PickupSound "VoidManipulation/Pickup";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "VoidEnergy";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			REIS L 1 bright;
			Loop;
	}
}


class VoidManipulationRiftNixian : VoidManipulationRift {
	Default {
		Scale 1.0;
		Height 120;
		Alpha 1.0;
		RenderStyle "Add";

		VoidManipulationRift.LoopSound "VoidManipulation/VoidRift/Nixian";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		energy = 4000;
		energyMax = 0; // Pernament
		energySustain = 4000;
		sustained = true;
		pullForce = 0;
		pullRange = 50;
		minionClass = "";
	}
	
	States {
		Spawn:
			NIXR ABCD 2 bright {
				A_StartSound(invoker.loopSound, CHAN_BODY, CHANF_LOOPING);
				invoker.SpawnParticle("Void", 8, 3, (FRandom(-64, 64), FRandom(-64, 64), FRandom(-32, 32)), Random(0, 359), Random(-60, -10));
				return A_VoidRiftUpdate();
			}
			Loop;
			
		Death:
			NIXR AABBCCDD 1 A_FadeOut(0.025);
			Stop;
	}
}
