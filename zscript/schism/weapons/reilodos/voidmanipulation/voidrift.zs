class VoidManipulationRift : Actor {
	mixin SpawnParticleMixin;

	int lifetime;
	string loopSound;
	property LoopSound: loopSound;

	VoidManipulation voidManipulation;
	int havocEnergy;
	
	int energy; // The current amount of energy the rift has.
	int energyMax; // The maximum energy of the rift, this is depleted over time closing the rift. If 0 the rift is pernament.
	int energySustain; // The energy needed to sustain the rift.
	bool sustained; // If true, the rift has become sustained and is now depleting energy over time (if max is above 0).
	float baseScale;
	float sizeScale;
	float pullForce;
	float pullRange;
	int summonCountdown;
	int summonCountdownMax;
	bool ignoreSight;
	string minionClass;
	
	Default {
		Radius 32;
		Height 40;
		Scale 0.5;
		Alpha 0.5;
		RenderStyle "Translucent";

		VoidManipulationRift.LoopSound "VoidManipulation/VoidRift";
		
		-SOLID;
		+NOGRAVITY;
		+SHOOTABLE; // Required for A_SeekerMissile
		+NOTARGET;
		+INVULNERABLE;
		+DONTRIP;
		+SPECTRAL;
		+PAINLESS;
		+DONTTHRUST;
		+FORCEXYBILLBOARD;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		havocEnergy = 1;
		
		energy = 10;
		energyMax = 320;
		energySustain = 300;
		sustained = false;
		baseScale = 0.5;
		sizeScale = 1;
		pullForce = 4;
		pullRange = 250;
		summonCountdownMax = 5 * 35;
		summonCountdown = summonCountdownMax;
		ignoreSight = true;
		minionClass = "VoidSpectreViolet";
	}
	
	override void Tick() {
		super.Tick();
		self.lifetime++;
	}
	
	override bool Used(Actor user) {
		bool isUsed = super.Used(user);
		if (self.master && user == self.master) {
			self.SetStateLabel("Death");
		}
		return isUsed;
	}
	
	override void Die(Actor source, Actor inflictor, int dmgflags) {
		if (self.voidManipulation) {
			self.voidManipulation.RemoveRift(self);
		}
		super.Die(source, inflictor, dmgflags);
	}
	
	override void OnDestroy() {
		if (self.voidManipulation) {
			self.voidManipulation.RemoveRift(self);
		}
		super.OnDestroy();
	}
	
	// Empower:
	virtual void Empower(int energy) {
		if (self.energy < self.energyMax) {
			self.energy += energy;
			if (self.energy > self.energyMax) {
				self.energy = self.energyMax;
			}
		}
		if (self.sustained && self.pullForce > 0) {
			return;
		}
		
		self.UpdateSize();
	}
	
	// Update Size:
	virtual void UpdateSize() {
		if (self.energy >= self.energySustain) {
			self.sustained = true;
		}
		
		float xScaleOffset = 0.2;
		if (self.sustained) {
			self.baseScale = 1.5;
			xScaleOffset = 1.0;
		}
		else {
			self.baseScale = 0.2 + ((Float(self.energy) / Float(self.energySustain)) * 0.7);
		}
		self.scale.x = ((self.baseScale * xScaleOffset) + frandom(0, 0.2)) * self.sizeScale;
		self.scale.y = (self.baseScale + frandom(0, 0.2)) * self.sizeScale;
		
		self.height = 40 * self.baseScale;
	}
	
	
	// Rift Update:
	action state A_VoidRiftUpdate() {
		if (!invoker.voidManipulation) {
			return ResolveState("Death");
		}
		if (invoker.energyMax > 0) {
			if (--invoker.energy <= 0) {
				return ResolveState("Death");
			}
			invoker.UpdateSize();
		}
		A_VoidPull();
		
		// Summoning:
		if (invoker.sustained && invoker.minionClass) {
			if (--invoker.summonCountdown <= 0) {
				invoker.summonCountdown = invoker.summonCountdownMax;
				A_SummonMinion();
			}
		}
		
		return null;
	}
	
	// Pull:
	action void A_VoidPull() {
		if (!invoker.pullForce) {
			return;
		}
		float pullPower = 1;
		if (!invoker.sustained) {
			pullPower = (Float(invoker.energy) / Float(invoker.energySustain)) * 0.25;
		}
		int riftDamage = pullPower >= 0.5 ? 1 + (pullPower / 2) : 0;
		Array<Actor> actorsHit;
		double range = invoker.pullRange * pullPower;
		SchismAttacks.Explode(invoker, invoker.target, riftDamage, range, -invoker.pullForce * pullPower, 0.75, false, false, "Void", actorsHit);
		if (invoker.lifetime % 8) {
			for (int i = 0; i < actorsHit.Size(); i++) {
				Actor target = actorsHit[i];
				if (!target) continue;
				invoker.voidManipulation.OnRiftDamage(invoker, target);
				double distance = invoker.Distance3D(target);
				invoker.SpawnParticleActorToActor(target, invoker, "Nix", TICRATE, 0.5 + ((distance / range) * 5), (0, 0, target.height * 0.5), 0.5, 0x95AA11FF);
			}
		}
	}
	
	// Summon Minion:
	action void A_SummonMinion(float xOffset = 0, float yOffset = 0, float zOffset = 0, float xVel = 0, float yVel = 0, float zVel = 0) {
		invoker.A_SpawnItemEx(invoker.minionClass, xOffset, yOffset, zOffset, xVel, yVel, zVel, 0, SXF_ISTRACER);
		if (invoker.tracer && invoker.voidManipulation) {
			invoker.tracer.master = invoker.voidManipulation.owner;
			if (invoker.voidManipulation.owner && invoker.voidManipulation.owner.master) {
				invoker.tracer.master = invoker.voidManipulation.owner.master;
			}
			VoidSpectre voidSpectre = VoidSpectre(tracer);
			if (voidSpectre) {
				voidSpectre.homeRift = invoker;
			}
		}
	}
	
	
	States {
		Spawn:
			VDRF ABCDEFGHIJKLMNOPQRSTUVWXY 1 bright {
				A_StartSound(invoker.loopSound, CHAN_BODY, CHANF_LOOPING);
				invoker.SpawnParticle("Void", 8, 3, (FRandom(-64, 64), FRandom(-64, 64), FRandom(-32, 32)), Random(0, 359), Random(-60, -10));
				return A_VoidRiftUpdate();
			}
			Loop;
			
		Death:
			VDRF ABCDEFGHIJKLMNOP 1 A_FadeOut(0.1);
			Stop;
	}
}
