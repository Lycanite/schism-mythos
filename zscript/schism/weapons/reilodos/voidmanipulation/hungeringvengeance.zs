class HungeringVengeanceScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Void Manipulation can unleash a hungering bolt of void energy that will drain life!";
		
		Inventory.Icon "REISJ0";
		Inventory.PickupSound "VoidManipulation/Pickup";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "VoidEnergy";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			REIS J 1 bright;
			Loop;
	}
}

class HungeringVengeanceProjectile : SchismProjectile {
	Default {
		Radius 20;
		Height 10;
		Scale 1.5;
		Speed 20;
		FastSpeed 20;
		DamageFunction 50;
		Alpha 0.75;
		RenderStyle "Translucent";
		
		SeeSound "VoidManipulation/Fire/Hunger";
		DeathSound "VoidManipulation/Projectile/Explode/Hunger";
		
		+NODAMAGETHRUST;
		+SEEKERMISSILE;
		+SCREENSEEKER;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		
		// Drain Health:
		if (specialDamage > 0 && !targetActor.bFriendly && target) {
			target.A_DamageSelf(-float(specialDamage) / 2, "None", DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
		}
		
		return specialDamage;
	}

	action state A_HungeringUpdate() {
		// Seek new Tracer:
		if (!invoker.tracer && invoker.lifetime % TICRATE == 0) {
			invoker.tracer = invoker.FindNewTarget();
		}

		// Home on Tracer:
		A_SeekerMissile(0, 20, SMF_LOOK|SMF_PRECISE);
		if (invoker.lifetime > 60 * TICRATE) {
			return ResolveState("Death");
		}
		
		return null;
	}
	
	States {
		Spawn:
			VDSP EFGH 4 {
				return A_HungeringUpdate();
			}
			Loop;
			
		Death:
			VDPT ABCDEFGHI 1 A_FadeOut(0.05);
			Loop;
	}
}
