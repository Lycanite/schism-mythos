class LobDarklingsProjectile : EbonCommandProjectile {
	Default {
		Radius 5;
		Height 7;
		Scale 1;
		Speed 10;
		Gravity 0.25;
		DamageFunction random(10, 20);
		Alpha 1.0;
		RenderStyle "Normal";

		SchismProjectile.SummonType "DarklingKeppel";
		SchismProjectile.SummonStance 0; // Wander
		
		SeeSound "EbonCommand/LobDarkling";
		DeathSound "EbonCommand/LobDarkling/Explode";
		Obituary "%o was consumed by the shadows of %k's Ebon Command!";
		
		PROJECTILE;
		+RANDOMIZE;
		-NOGRAVITY;
	}
	
	States {
		Spawn:
			EBCM IJKLMNOP 4;
			Loop;
			
		Death:
			EBCM E 4 {
				invoker.SummonMinion((6, 0, 0), (10, 0, 10));
				invoker.SummonMinion((-4, 4, 0), (-3, 7, 10));
				invoker.SummonMinion((-4, -4, 0), (3, -7, 10));
			}
			EBCM FGH 4;
			Stop;
	}
}