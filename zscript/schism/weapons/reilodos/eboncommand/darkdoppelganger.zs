class DarkDoppelgangerScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Ebon Command can now summon a Doppelganger at the cost of Havoc!";
		
		Inventory.Icon "REISG0";
		Inventory.PickupSound "EbonCommand/Pickup";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "ShadowEnergy";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}	

	States {
		Spawn:
			REIS G 1 bright;
			Loop;
	}
}
