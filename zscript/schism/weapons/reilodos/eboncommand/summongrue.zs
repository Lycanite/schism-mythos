class GrueScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Ebon Command can now summon Grues at the cost of Havoc!";
		
		Inventory.Icon "REISE0";
		Inventory.PickupSound "Grue/See";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "ShadowEnergy";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			REIS E 1 bright;
			Loop;
	}
}

class EbonCommandProjectileGrue : EbonCommandProjectile {
	Default {
		Radius 15;
		Height 25;
		Scale 1.5;

		SchismProjectile.SummonType "GrueVerdant";
		SchismProjectile.SummonStance 0; // Wander
		
		SeeSound "EbonCommand/Fire/Burst";
		DeathSound "EbonCommand/Explode";
		
		PROJECTILE;
		+RANDOMIZE;
	}
	
	States {
		Death:
			EBCM E 4 SummonMinion();
			EBCM FGH 4;
			Stop;
	}
}
