class NoxPromenadeProjectile : SchismProjectile {
	EbonCommand ebonCommand;
	
	Default {
		Radius 10;
		Height 10;
		Speed 30;
		DamageFunction 20;
		Gravity 0;
		Alpha 1;
		Renderstyle "Translucent";
		Projectile;
		
		DamageType "Shadow";
		
		SeeSound "EbonCommand/Fire/Nox";
		DeathSound "";
		
		+SKYEXPLODE;
	}
	
	action void A_CallStorm() {
		int stormSize = 3;
		int cloudSize = 200;
		for (int x = -(stormSize * cloudSize); x <= stormSize * cloudSize; x += cloudSize) {
			for (int y = -(stormSize * cloudSize); y <= stormSize * cloudSize; y += cloudSize) {
				A_SpawnItemEx("NoxPromenadeCloud", x, y, 0, 0, 0, 0, 0, SXF_ISTRACER);
				NoxPromenadeCloud cloud = NoxPromenadeCloud(invoker.tracer);
				if (cloud) {
					cloud.ebonCommand = invoker.ebonCommand;
				}
				
				A_SpawnItemEx("NoxPromenadeCloudFloor", x, y, 0, 0, 0, 0, 0, SXF_ISTRACER);
				cloud = NoxPromenadeCloud(invoker.tracer);
				if (cloud) {
					cloud.ebonCommand = invoker.ebonCommand;
				}
			}
		}
	}
	
	States {
		Spawn:
		Death:
			EBCM A 1;
			EBCM A 1 A_CallStorm();
			Stop;
	}
}

class NoxPromenadeCloud : SchismProjectile {
	int randomStart;
	EbonCommand ebonCommand;
	
	Default {
		Radius 1;
		Height 1;
		Scale 2;
		Speed 15;
		DamageFunction 2;
		Alpha 0.25;
		Gravity 0;
		Renderstyle "Fuzzy";
		
		SeeSound "";
		DeathSound "";
		
		+DONTSPLASH;
		+SPAWNCEILING;
		+CEILINGHUGGER;
		+NODAMAGETHRUST;
		+PAINLESS;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		if (random(0, 3) == 0) {
			randomStart = random(0, 90);
		}
		else {
			randomStart = 0;
		}
	}
	
	action void A_NoxDoppelganger() {
		A_StartSound("EbonCommand/Fire/Nox", CHAN_BODY, CHANF_LOOPING);
		if (!invoker.ebonCommand || !invoker.ebonCommand.owner || invoker.randomStart <= 0) {
			return;
		}
		if (--invoker.randomStart == 0) {
			float floorZ = GetZAt(0, 0, 0, GZF_3DRESTRICT|GZF_NO3DFLOOR);

			bool spawned = false;
			Actor minionActor = null;
			[spawned, minionActor] = invoker.A_SpawnItemEx("ReilodosDoppelganger", 0, 0, floorZ - invoker.pos.z, 0, 0, 0, 0, SXF_NOCHECKPOSITION|SXF_NOPOINTERS);
			ReilodosDoppelganger ganger = ReilodosDoppelganger(minionActor);
			if (ganger) {
				ganger.master = invoker.ebonCommand.owner;
				ganger.scale = invoker.ebonCommand.owner.scale;
			}
		}
	}
	
	States {
		Spawn:
			NCLV FGHIJABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDE 4 A_NoxDoppelganger();
		Death:
			NCLV F 4 A_StopSound(CHAN_BODY);
		Death.Fade:
			NCLV GGGGHHHHIIIIJJJJ 1 A_FadeOut(0.1);
			Loop;
	}
}

class NoxPromenadeCloudFloor : NoxPromenadeCloud {
	Default {
		-CEILINGHUGGER;
		+FLOORHUGGER;
	}
}

class ReilodosDoppelgangerNox : ReilodosDoppelganger {
	Default {
		Health 50;
		Alpha 0.5;

		MinionCreature.SummonTime 30 * TICRATE;
		MinionCreature.MinionShield true;
	}
}