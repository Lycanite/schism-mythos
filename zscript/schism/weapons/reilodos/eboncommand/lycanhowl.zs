class HowlScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Ebon Command can now unleash the terrifying Howl of a Lycan!";
		
		Inventory.Icon "REISF0";
		Inventory.PickupSound "EbonCommand/Up";
		Inventory.RestrictedTo "Reilodos", "ReilodosHavocSerpent";
		WeaponUpgrade.ExtraAmmoClass "ShadowEnergy";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			REIS F 1 bright;
			Loop;
	}
}

class EbonCommandProjectileHowl : EbonCommandProjectile {
	Default {
		Radius 15;
		Height 25;
		Scale 1.5;
		DamageFunction random(2, 4);
		
		SeeSound "EbonCommand/Projectile/Howl";
		DeathSound "EbonCommand/Explode";

		SchismProjectile.KnockbackStrength 20;
		
		PROJECTILE;
		+RANDOMIZE;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0 && targetActor.bIsMonster && !targetActor.bBoss) {
			if (!targetActor.bFrightened && targetActor.CountInv("SchismFearDebuff") == 0) {
				SchismFearDebuff debuff = SchismFearDebuff(targetActor.GiveInventoryType("SchismFearDebuff"));
				if (debuff) {
					debuff.ApplyEffect(self.target, self);
				}
			}
		}
		return specialDamage;
	}
	
	States {
		Spawn:
			EBCM QRS 4;
			Loop;
			
		Death:
			EBCM TUVW 4;
			Stop;
	}
}
