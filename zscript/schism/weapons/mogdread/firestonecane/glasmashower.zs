class GlasmaShowerGem : WeaponUpgrade {
	Default {
		Scale 1;
		Inventory.PickupMessage "Your Firestone Cane can release a shower of deadly glasma, beyond plasma!";
		
		Inventory.Icon "FSCNT0";
		Inventory.PickupSound "FirestoneCane/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "FirestoneCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			FSCN T 1 bright;
			Loop;
	}
}

class FirestoneCaneGlasmaProjectile : FirestoneCaneProjectile {
	Default {
		Scale 1;
		Speed 8;
		Alpha 1;
		
		DamageType "Nova";

		SchismProjectile.ParticleName "Nova";
		SchismProjectile.ParticleInterval 2;

		+RIPPER;
	}

	action void A_Disperse() {
		// Seek new Tracer:
		if (!invoker.tracer && invoker.lifetime % TICRATE == 0) {
			invoker.tracer = invoker.FindNewTarget();
		}

		// Break Off Fragments:
		invoker.A_ScaleVelocity(0.5);
		A_FadeOut(0.04);
		A_SpawnItemEx("FirestoneCaneGlasmaFragmentProjectile", 0, 0, 0, Random(-10, 10), Random(-10, 10), -10, 0, SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
	}

	States {
		Spawn:
			FSCD ABCDCBABCDCBA 4 bright A_ScaleVelocity(0.75);
		Death:
			FSCD ABCDEFGHIJKLMNOP 4 bright A_Disperse();
			Loop;
	}
}

class FirestoneCaneGlasmaFragmentProjectile : FirestoneCaneProjectile {
	Default {
		Radius 5;
		Height 5;
		Scale 0.5;
		Speed 20;
		DamageFunction 2;
		Alpha 1;
		
		DamageType "Nova";

		SchismProjectile.ParticleName "Nova";
		SchismProjectile.ParticleScale 0.25;
		
		DeathSound "FirestoneCane/Explode/Glasma";

		+RIPPER;
		+SEEKERMISSILE;
		+SCREENSEEKER;
	}

	action state A_GlasmaFragmentUpdate() {
		// Seek new Tracer:
		if (!invoker.tracer && invoker.lifetime % TICRATE == 0) {
			invoker.tracer = invoker.FindNewTarget();
		}

		// Home on Tracer:
		A_SeekerMissile(0, 20, SMF_LOOK|SMF_PRECISE);
		if (invoker.lifetime > 5 * TICRATE) {
			return ResolveState("Death");
		}
		
		return null;
	}

	States {
		Spawn:
			FSCD ABCDEFGHIJKLMNOP 4 bright {
				return A_GlasmaFragmentUpdate();
			}
			Loop;
		Death:
			FSCD ABCDEFGHIJKLMNOPPPPP 4 bright A_FadeOut(0.05);
			Stop;
	}
}