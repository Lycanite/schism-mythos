class DelphicNovaeGem : WeaponPassive {
	Default {
		Scale 1;
		Inventory.PickupMessage "Your Firestone Cane can now produce delphic novae, mystical flames that drain the victim's energy.";
		
		Inventory.Icon "FSCNS0";
		Inventory.PickupSound "FirestoneCane/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "FirestoneCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			FSCN S 1 bright;
			Loop;
	}
}

class FirestoneCaneDelphicProjectile : FirestoneCaneProjectile {
	Actor followTarget;
	float orbitRange;
	float orbitAngle;
	float orbitOffsetZ;
	int orbitSpeed;

	property OrbitSpeed: orbitSpeed;

	Default {
		Scale 0.5;
		Speed 16;
		Alpha 1;
		
		DamageType "Nova";

		SchismProjectile.ParticleName "Nova";
		FirestoneCaneDelphicProjectile.OrbitSpeed 8;

		+RIPPER;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		
		// Orbit Follow Target:
		if (self.followTarget) {
			float angle = self.orbitAngle += self.orbitSpeed;
			int offsetX = self.orbitRange * Sin(angle);
			int offsetY = self.orbitRange * Cos(angle);
			self.SetOrigin(self.followTarget.pos + (offsetX, offsetY, self.followTarget.height * 0.65 + self.orbitOffsetZ), false);
			self.orbitRange += self.orbitSpeed;
		}
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0) {
			InflictEffect("SchismAstonishDebuff", targetActor, 20 * TICRATE);
		}
		return specialDamage;
	}
	
	virtual void SetFollowTarget(Actor newTarget, float orbitAngle, float orbitOffsetZ) {
		self.followTarget = newTarget;
		self.orbitRange = 0;
		self.orbitAngle = orbitAngle;
		self.orbitOffsetZ = orbitOffsetZ;
		if (newTarget) {
			self.orbitAngle += newTarget.angle;
		}
	}

	States {
		Spawn:
			FSCD ABCDEFGHIJKLMNOP 4 bright;
		Death:
			Stop;
	}
}