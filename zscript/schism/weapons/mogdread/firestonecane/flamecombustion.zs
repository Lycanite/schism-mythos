class FlameCombustionGem : WeaponUpgrade {
	Default {
		Scale 1;
		Inventory.PickupMessage "Your Firestone Cane can now combust every enemy that you have immolated at the cost of Firestone Charges!";
		
		Inventory.Icon "FSCNR0";
		Inventory.PickupSound "FirestoneCane/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "FirestoneCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			FSCN R 1 bright;
			Loop;
	}
}
