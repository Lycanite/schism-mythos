class InfernoSpiralGem : WeaponUpgrade {
	Default {
		Scale 1;
		Inventory.PickupMessage "Your Firestone Cane can unleash a spiral blast of piercing fire at the cost of Firestone Charges!";
		
		Inventory.Icon "FSCNQ0";
		Inventory.PickupSound "FirestoneCane/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "FirestoneCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			FSCN Q 1 bright;
			Loop;
	}
}

class FirestoneCaneSpiralProjectile : FirestoneCaneProjectile {
	Default {
		Scale 1;
		Speed 30;
		DamageFunction 5;
		
		SeeSound "FirestoneCane/Fire";
		
		+RIPPER;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		A_Weave(10, 10, 4.0, -4.0);
	}
	
	override void SpawnParticles() {
		self.SpawnParticle("Fire", TICRATE, 1, (Random(-40, 40), 0, Random(-4, 4)), Random(0, 359), Random(-45, 45));
	}
	
	States {
		Spawn:
			FSCF ABCDEFGHIJKLMNOP 4 bright;
			Stop;
			
		Death:
			Stop;
	}
}
