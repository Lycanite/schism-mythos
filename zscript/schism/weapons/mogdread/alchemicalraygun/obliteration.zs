class AlchemicalRaygunObliterationProjectile : AlchemicalRaygunProjectile {
	Default {
		Scale 2.0;
		Radius 80;
		Height 64;
		Speed 10;
		
		AlchemicalRaygunProjectile.ImmolateSeconds 0;

		SeeSound "AlchemicalRaygun/Fire/Obliteration";
	}
}

class AlchemicalRaygunObliterationStormProjectile : AlchemicalRaygunObliterationProjectile {
	Default {
		Damage 10;
	}
}



class AlchemicalRaygunObliterationSeedProjectile : SchismProjectile {
	Default {
		Radius 10;
		Height 10;
		Speed 30;
		DamageFunction 20;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		Projectile;
		
		DamageType "Fire";
		
		SeeSound "AlchemicalRaygun/Fire/Obliteration";
		DeathSound "";
		
		+SKYEXPLODE;
	}
	
	action void callStorm() {
		int stormSize = 3;
		int cloudSize = 200;
		for (int x = -(stormSize * cloudSize); x <= stormSize * cloudSize; x += cloudSize)
		{
			for (int y = -(stormSize * cloudSize); y <= stormSize * cloudSize; y += cloudSize)
			{
				A_SpawnItemEx("AlchemicalRaygunObliterationCloud", x, y, 0, 0, 0, 0, 0);
			}
		}
	}
	
	States {
		Spawn:
			ALCR Y 4 bright;
			Loop;
			
		Death:
			ALCR Y 4 bright callStorm();
			Stop;
	}
}

class AlchemicalRaygunObliterationCloud : SchismProjectile {
	int randomStart;
	
	Default {
		Radius 1;
		Height 1;
		Scale 2;
		Speed 15;
		DamageFunction 2;
		Alpha 1;
		Gravity 0;
		Renderstyle "Add";
		
		DamageType "Fire";
		
		SeeSound "";
		DeathSound "";
		
		+DONTSPLASH;
		+SPAWNCEILING;
		+CEILINGHUGGER;
		+NODAMAGETHRUST;
		+PAINLESS;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		randomStart = random(0, 90);
	}
	
	action void stormCloud() {
		if (invoker.randomStart <= 0)
		{
			return;
		}
		if (--invoker.randomStart == 0)
		{
			A_StartSound("AlchemicalRaygun/Fire", CHAN_BODY);
			invoker.pitch = 90;
			invoker.master = invoker.target;
			A_CustomRailgun(0, 0, "ff ff 99", "ff ff dd", RGF_SILENT|RGF_FULLBRIGHT, 0, 0, "BulletPuff", 0, 0, 0, 0, 20.0, 10.0, "AlchemicalRaygunObliterationStormProjectile");
		}
	}
	
	States {
		Spawn:
			NCLL FGHIJABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDE 4 bright stormCloud();
		Death:
			NCLL F 4 bright A_StopSound(CHAN_BODY);
			NCLL GHIJ 4 bright;
			Stop;
	}
}
