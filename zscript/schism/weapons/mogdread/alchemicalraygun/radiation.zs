class RadiationModule : WeaponPassive {
	Default {
		Scale 0.75;
		Inventory.PickupMessage "Your Alchemical Raygun now emit finely controlled radiation to light both the way and melt incoming projectiles!";
		
		Inventory.Icon "ALCRU0";
		Inventory.PickupSound "AlchemicalRaygun/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "LightCharge";
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			ALCR U 1 bright;
			Loop;
	}
}