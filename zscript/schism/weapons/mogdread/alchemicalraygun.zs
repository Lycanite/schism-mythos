class AlchemicalRaygun : SchismWeapon {
	Default {
		Radius 20;
		Height 16;
		Scale 0.75;
		
		Inventory.RestrictedTo "Mogdread";
		
		Inventory.PickupSound "AlchemicalRaygun/Pickup";
		Weapon.UpSound "AlchemicalRaygun/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "AlchemicalRaygun/Fire/Fail";
		
		Weapon.AmmoType1 "LightCharge";
		Weapon.AmmoType2 "InfernalCharge";
		
		SchismWeapon.PrimaryCost 2;
		SchismWeapon.SecondaryCost 4;
		SchismWeapon.SpecialACost 50;
		SchismWeapon.SpecialBCost 25;
		SchismWeapon.SpecialCCost 25;
		SchismWeapon.SpecialDCost 50;
		
		SchismWeapon.SpecialItemA "ReactionModule";
		SchismWeapon.SpecialItemB "CombustionModule";
		SchismWeapon.SpecialItemC "LaserModule";
		SchismWeapon.SpecialItemD "ExperimentalGlasmaModule";
		SchismWeapon.PassiveItem "RadiationModule";
		
		Inventory.PickupMessage "Mogdread's custom built Alchemical Raygun!";
		Obituary "%o was annihilated by %k's Alchemical Raygun!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Light Charges to use this ability.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Infernal Charges to use this ability.";
		SchismWeapon.NoSpecialAMessage "\caYou need a Reaction Module to use this ability.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Combustion Module to use this ability.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Laser Module to use this ability.";
		SchismWeapon.NoSpecialDMessage "\caYou need an Experimental Glasma Module to use this ability.";

		SchismWeapon.IdleBob 10;
		
		+NOGRAVITY;
		+INVENTORY.RESTRICTABSOLUTELY;
		+WEAPON.ALT_AMMO_OPTIONAL;
	}
	
	action void A_OnAttack() {
		A_ClearBobbing();
		A_Light2();
	}
	
	// Rail Attack
	action void A_AlchemicalRailAttack(int damage, string railActor) {
		A_Light2();
		A_RailAttack(damage, 4, false, "ff ff 99", "ff ff dd", RGF_FULLBRIGHT|RGF_SILENT, 0, "BulletPuff", 0, 0, 0, 35, 10.0, 10.0, railActor, -1, 180, 0);
	}
	
	// Primary Light Ray:
	action void A_FireLightRay(bool useAmmo) {
		if (useAmmo && !A_SpendAmmo(1)) {
			return;
		}
		A_AlchemicalRailAttack(2, "AlchemicalRaygunProjectile");
	}
	
	// Secondary Cluster Flares:
	action void A_ClusterFlares() {
		if (!A_SpendAmmo(2)) {
			return;
		}
		A_StartSound("AlchemicalRaygun/Fire/Cluster");
		A_FireProjectile("AlchemicalRaygunClusterProjectile", 0, false);
	}
	
	// Special 1 Reaction:	
	action void A_FireReaction() {
		if (!A_SpendAmmo(3)) {
			return;
		}
		A_StartSound("AlchemicalRaygun/Fire/Reaction");
		A_FireProjectile("AlchemicalRaygunReactionProjectile", 0, false, 0, 0, FPF_NOAUTOAIM, 0);
	}
	
	// Special 2 Combustion:
	action void A_FireCombustion() {
		if (!A_SpendAmmo(4)) {
			return;
		}
		A_StartSound("AlchemicalRaygun/Fire/Combustion");
		A_AlchemicalRailAttack(4, "AlchemicalRaygunCombustionProjectile");
	}
	
	// Special 3 Laser:
	action void A_FireLaser() {
		if (!A_SpendAmmo(5)) {
			return;
		}
		A_StartSound("AlchemicalRaygun/Fire/Laser");
		A_FireProjectile("AlchemicalRaygunLaserProjectile", 0, false, 0, -20, FPF_NOAUTOAIM, 0);
	}
	
	// Special 4 Experimental Glasma:
	action void A_ExperimentalGlasma() {
		if (!A_SpendAmmo(6)) {
			return;
		}
		A_AlchemicalRailAttack(50, "AlchemicalRaygunGlasmaProjectile");
	}
	
	// Ultimate Obliteration:
	action void A_Obliteration(bool primary) {
		if (primary) {
			A_SpawnItemEx("AlchemicalRaygunObliterationSeedProjectile", 0, 0, invoker.height / 2, 0, 0, 10);
		}
		A_AlchemicalRailAttack(50, "AlchemicalRaygunObliterationProjectile");
	}
	
	// Passive Radiation:
	action void A_Radiation() {
		int passiveLevel = invoker.GetPassiveLevel();
		if (passiveLevel <= 0 || invoker.passiveCooldown > 0) {
			return;
		}
		invoker.passiveCooldown = 1 * TICRATE;
		
		Actor targetActor = invoker.owner;
		SchismStatusEffects.InflictEffect("SchismRadianceBuff", invoker.owner, invoker.owner, invoker.owner, 2 * TICRATE, "", passiveLevel);
	}
	
	States {
		Ready:
			ALCR ABCB 6 {
				A_Radiation();
				A_Light1();
				return A_SchismWeaponReady();
			}
			Loop;
			
		Deselect:
			ALCR A 1 A_Lower(12);
			Loop;
			
		Select:
			ALCR A 1 A_Raise(12);
			Loop;
			
		Fire:
			ALCR D 4 {
				A_OnAttack();
				return A_CheckAmmoJump(1, "Ready");
			}
			ALCR E 4;
			ALCR F 4 {
				A_StartSound("AlchemicalRaygun/Fire");
				A_FireLightRay(true);
			}
			ALCR G 4 A_Light1();
			ALCR KLM 6;
			goto Ready;
			
		AltFire:
			ALCR D 4 {
				A_OnAttack();
				return A_CheckAmmoJump(2, "Ready");
			}
			ALCR E 4;
			ALCR F 4 A_ClusterFlares();
			ALCR J 4 A_Light1();
			ALCR KLM 6;
			Goto Ready;
		
		User1:
			ALCR D 4 A_OnAttack();
			ALCR E 4 {
				return A_CheckSpecialJump(1, "Ready");
			}
			ALCR F 4 A_FireReaction();
			ALCR J 4 A_Light1();
			ALCR KLM 6;
			goto Ready;
		
		User2:
			ALCR D 4 A_OnAttack();
			ALCR E 4 {
				return A_CheckSpecialJump(2, "Ready");
			}
			ALCR F 4 A_FireCombustion();
			ALCR G 4 A_Light1();
			ALCR KLM 6;
			goto Ready;
		
		User3:
			ALCR D 4 A_OnAttack();
			ALCR E 4 {
				return A_CheckSpecialJump(3, "Ready");
			}
			ALCR F 4 A_FireLaser();
			ALCR J 4 A_Light1();
			ALCR KLM 6;
			goto Ready;
		
		User4:
			ALCR D 4 A_OnAttack();
			ALCR E 4 {
				return A_CheckSpecialJump(4, "Ready");
			}
			ALCR F 4 A_ExperimentalGlasma();
			ALCR G 4 A_Light1();
			ALCR KLM 6;
			goto Ready;
		
		Ultimate:
			ALCR D 4 {
				A_OnAttack();
				A_StartSound("AlchemicalRaygun/Fire/Obliteration");
			}
			ALCR EFGFG 4 A_FireLightRay(false);
			ALCR H 4 A_Obliteration(true);
			ALCR IHI 4 A_Obliteration(false);
			ALCR F 4 A_FireLightRay(false);
			ALCR JKLM 6 A_Light1();
			goto Ready;
			
		Spawn:
			ALCR Z -1;
			Stop;
	}
}

class AlchemicalRaygunProjectile : SchismProjectile {
	int immolationSeconds;

	property ImmolateSeconds: immolationSeconds;
	
	Default {
		Radius 4;
		Height 8;
		Scale 0.25;
		Speed 25;
		DamageFunction 2;
		Gravity 0;
		Alpha 0.5;
		Renderstyle "Add";
		
		DamageType "Light";

		SchismProjectile.ParticleScale 0.25;
		SchismProjectile.ParticleInterval 4;
		AlchemicalRaygunProjectile.ImmolateSeconds 10;
		
		SeeSound "AlchemicalRaygun/Flare";
		DeathSound "AlchemicalRaygun/Explode";
		
		+FORCERADIUSDMG;
		+PAINLESS;
		+FORCEXYBILLBOARD;
		-THRUGHOST;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0) {
			self.InflictEffect("SchismImmolateDebuff", targetActor, self.immolationSeconds * TICRATE);
		}
		return specialDamage;
	}
	
	States {
		Spawn:
			ALCP ABABCBC 1 bright;
			
		Death:
			ALCP DCDEDEFEFGFGHGHIHIJIJKJKLKLMLMNMNONO 1 bright;
			Stop;
	}
}

class AlchemicalRaygunFlareProjectile : AlchemicalRaygunProjectile {
	int effectTick;
	
	Default {
		Radius 4;
		Height 8;
		Scale 0.25;
		Speed 80;
		DamageFunction 2;
		Renderstyle "Add";

		SchismProjectile.ParticleName "FireEmber";
		SchismProjectile.ParticleInterval 3;
		
		+RIPPER;
		+BLOODLESSIMPACT;
		-BLOODSPLATTER;
	}
	
	States {
		Spawn:
			ALCP ABCB 4 bright;
			Loop;
		
		Death:
			ALCP DEFGHIJKLMNO 2 bright;
			Stop;
	}
}

class AlchemicalRaygunClusterProjectile : AlchemicalRaygunFlareProjectile {
	string clusterProjectile;
	Actor sourceTarget;
	
	Default {
		Scale 0.5;
		Speed 140;
		DamageFunction 25;
		
		-RIPPER;
		-BLOODLESSIMPACT;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		clusterProjectile = "AlchemicalRaygunFlareProjectile";
	}
	
	virtual void SetSourceTarget(Actor newSourcetarget) {
		sourceTarget = newSourcetarget;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		if (sourceTarget && sourceTarget == targetActor) {
			return 0;
		}
		
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0 && targetActor && targetActor.bIsMonster && clusterProjectile != "")
		{
			for (int i = 0; i < 360; i += 10) {
				Actor projectileActor = A_SpawnProjectile(clusterProjectile, 0, targetActor.Radius + 4, i, CMF_AIMDIRECTION, 0);
				AlchemicalRaygunClusterProjectile clusterActor = AlchemicalRaygunClusterProjectile(projectileActor);
				if (clusterActor) {
					clusterActor.SetSourceTarget(targetActor);
				}
			}
			clusterProjectile = "";
		}
		
		return specialDamage;
	}
}

#include "zscript/schism/weapons/mogdread/alchemicalraygun/reaction.zs"
#include "zscript/schism/weapons/mogdread/alchemicalraygun/combustion.zs"
#include "zscript/schism/weapons/mogdread/alchemicalraygun/laser.zs"
#include "zscript/schism/weapons/mogdread/alchemicalraygun/experimentalglasma.zs"
#include "zscript/schism/weapons/mogdread/alchemicalraygun/radiation.zs"
#include "zscript/schism/weapons/mogdread/alchemicalraygun/obliteration.zs"
