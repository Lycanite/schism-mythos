class LavaCannon : SchismWeapon {
	Default {
		Radius 20;
		Height 16;
		Scale 0.75;
		
		Inventory.RestrictedTo "Mogdread";
		
		Inventory.PickupSound "LavaCannon/Pickup";
		Weapon.UpSound "LavaCannon/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "LavaCannon/Fire/Fail";
		
		Weapon.AmmoType1 "MoglavaCharge";
		Weapon.AmmoType2 "InfernalCharge";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 10;
		SchismWeapon.SpecialACost 25;
		SchismWeapon.SpecialBCost 25;
		SchismWeapon.SpecialCCost 50;
		SchismWeapon.SpecialDCost 25;
		
		SchismWeapon.SpecialItemA "OverheatModule";
		SchismWeapon.SpecialItemB "EruptionModule";
		SchismWeapon.SpecialItemC "VolcanoModule";
		SchismWeapon.SpecialItemD "MineModule";
		SchismWeapon.PassiveItem "ExhaustModule";
		
		Inventory.PickupMessage "Mogdread's favorite Lava Cannon!";
		Obituary "%o was melted by %k's mighty Lava Cannon!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Moglava Charges to use this ability.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Infernal Charges to use this ability.";
		SchismWeapon.NoSpecialAMessage "\caYou need an Overheat Module to use this ability.";
		SchismWeapon.NoSpecialBMessage "\caYou need an Eruption Module to use this ability.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Volcano Module to use this ability.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Mine Module to use this ability.";

		SchismWeapon.IdleBob 10;
		
		+NOGRAVITY;
		+INVENTORY.RESTRICTABSOLUTELY;
		+WEAPON.ALT_AMMO_OPTIONAL;
	}
	
	action void A_OnAttack() {
		A_ClearBobbing();
		A_Light2();
	}
	
	action state A_User2ReFire() {
		if (GetPlayerInput(MODINPUT_BUTTONS) & BT_USER2) {
			return ResolveState("User2Hold");
		}
		return null;
	}
	
	// Primary Lava Shot:
	action void A_LavaShot() {
		if (!A_SpendAmmo(1)) {
			return;
		}
		A_FireProjectile("LavaCannonChargerProjectile", 0, false);
	}
	
	// Secondary Lava Wall:
	action void A_LavaWall() {
		if (!A_SpendAmmo(2)) {
			return;
		}
		A_FireProjectile("LavaCannonChargerProjectile", 0, false, -80, 0);
		A_FireProjectile("LavaCannonChargerProjectile", 0, false, -40, 0);
		A_FireProjectile("LavaCannonChargerProjectile", 0, false);
		A_FireProjectile("LavaCannonChargerProjectile", 0, false, 40, 0);
		A_FireProjectile("LavaCannonChargerProjectile", 0, false, 80, 0);
	}
	
	
	// Special 1 Overheat:
	action state A_Overheat() {
		if (!A_CheckSpecial(1) || !A_SpendAmmo(3)) {
			return ResolveState("Ready");
		}
		if ( Random(0.0, 1.0) >= 0.95) {
			A_StartSound("LavaCannon/Fire/Overheat");
		}
		A_FireProjectile("LavaCannonOverheatProjectile", 0, false, 0, -20, FPF_NOAUTOAIM, 0);
		return null;
	}
	
	// Special 2 Eruption:
	action state A_Eruption() {
		if (!A_CheckSpecial(2) || !A_CheckAmmo(4)) {
			return ResolveState("Ready");
		}

		ThinkerIterator lavaCannonProjectileIterator = ThinkerIterator.Create("LavaCannonProjectile");
		Array<LavaCannonProjectile> lavaCannonProjectiles;
		LavaCannonProjectile lavaCannonProjectile;
		while (lavaCannonProjectile = LavaCannonProjectile(lavaCannonProjectileIterator.Next())) {
			if (lavaCannonProjectile.target == invoker.owner) {
				lavaCannonProjectiles.Push(lavaCannonProjectile);
			}
		}
		for (int i = 0; i < lavaCannonProjectiles.Size() && i < 50; i++) {
			lavaCannonProjectiles[i].LavaBurst();
		}
		if (lavaCannonProjectiles.Size() > 0) {
			A_SpendAmmo(4);
			A_StartSound("LavaCannon/Fire/Eruption");
		}
		return null;
	}
	
	// Special 3 Volcano:
	action state A_Volcano() {
		if (!A_CheckSpecial(3) || !A_SpendAmmo(5)) {
			return ResolveState("Ready");
		}
		if (Random(0.0, 1.0) >= 0.95) {
			A_StartSound("LavaCannon/Fire/Volcano");
		}
		A_FireProjectile("LavaCannonVolcanoProjectile", 0, false, 0, -20, FPF_NOAUTOAIM, 0);
		return null;
	}
	
	// Special 4 Mine:
	action state A_Mine() {
		if (!A_CheckSpecial(4) || !A_SpendAmmo(6)) {
			return ResolveState("Ready");
		}
		if (Random(0.0, 1.0) >= 0.95) {
			A_StartSound("LavaCannon/Fire/Mine");
		}
		A_FireProjectile("LavaCannonMineProjectile", 0, false, 0, -20, FPF_NOAUTOAIM, 0);
		return null;
	}
	
	// Ultimate Badlands:
	action void A_Badlands() {
		A_StartSound("LavaCannon/Fire/Badlands", CHAN_WEAPON);
		A_FireProjectile("LavaCannonBadlandsProjectile", 0, false, 0, 0, FPF_NOAUTOAIM, -90);
	}
	
	// Passive Magma Exhaust:
	action void A_MagmaExhaust() {
		int passiveLevel = invoker.GetPassiveLevel();
		if (passiveLevel <= 0) {
			return;
		}

		// Magma Drop Rate:
		bool moving = Abs(invoker.owner.vel.x) > 1 || Abs(invoker.owner.vel.y) > 1 || Abs(invoker.owner.vel.z) > 1;
		if (moving && invoker.passiveCooldown > 8) {
			invoker.passiveCooldown = 7;
		}
		if (invoker.passiveCooldown > 0) {
			return;
		}
		invoker.passiveCooldown = 2 * TICRATE;
		if (moving) {
			invoker.passiveCooldown = 8;
		}

		// Drop Magma:
		Actor spawned, projectile;
		[spawned, projectile] = A_FireProjectile("LavaCannonExhaustProjectile", 0, false, 0, 0, FPF_NOAUTOAIM, 90 - invoker.owner.pitch);
		LavaCannonProjectile lavaCannonProjectile = LavaCannonProjectile(projectile);
		if (lavaCannonProjectile) {
			lavaCannonProjectile.poolTics = passiveLevel * 5 * TICRATE;
			lavaCannonProjectile.PoolDamageRadius = passiveLevel * 100;
			lavaCannonProjectile.poolScale = Float(passiveLevel) / 2;
		}
	}
	
	States {
		Ready:
			MLVC ABCDEF 4 {
				A_MagmaExhaust();
				return A_SchismWeaponReady();
			}
			Loop;
			
		Deselect:
			MLVC M 1 A_Lower(12);
			Loop;
			
		Select:
			MLVC M 1 A_Raise(12);
			Loop;
			
		Fire:
			MLVC G 4 {
				A_OnAttack();
				return A_CheckAmmoJump(1, "Ready");
			}
			MLVC HI 4;
			MLVC J 4 A_LavaShot();
			MLVC KL 4 A_Light0();
			goto Ready;
			
		AltFire:
			MLVC M 4 {
				A_OnAttack();
				return A_CheckAmmoJump(2, "Ready");
			}
			MLVC N 4;
			MLVC O 4 A_LavaWall();
			MLVC PQ 4;
			MLVC R 4 A_Light0();
			Goto Ready;
		
		User1:
			MLVC G 4 A_OnAttack();
			MLVC HI 4;
			MLVC J 4  {
				return A_Overheat();
			}
			MLVC K 4;
			MLVC L 4 A_Light0();
			goto Ready;
		
		User2:
			MLVC M 4 A_OnAttack();
			MLVC N 4  {
				return A_Eruption();
			}
			MLVC OPQ 4;
			MLVC R 4 A_Light0();
			goto Ready;
		
		User3:
			MLVC G 4 A_OnAttack();
			MLVC HI 4;
			MLVC J 8  {
				return A_Volcano();
			}
			MLVC K 4;
			MLVC L 4 A_Light0();
			goto Ready;
		
		User4:
			MLVC M 4 A_OnAttack();
			MLVC N 4  {
				return A_Mine();
			}
			MLVC OPQ 4;
			MLVC R 4 A_Light0();
			goto Ready;
		
		Ultimate:
			MLVC M 4 A_OnAttack();
			MLVC N 4  {
				return A_Badlands();
			}
			MLVC OPQ 4;
			MLVC R 4 A_Light0();
			goto Ready;
			
		Spawn:
			MLVC Z -1;
			Stop;
	}
}

class LavaCannonProjectile : MagmaProjectile {
	Default {
		DamageFunction 30;

		SchismProjectile.PoolClass "LavaCannonLavaPool";
		SchismProjectile.PoolTics 10 * TICRATE;
		SchismProjectile.PoolDamage 2;
	}

	void LavaBurst() {
		self.burstTime = 1 * TICRATE;
		bool spawned;
		Actor projectile;
		[spawned, projectile] = self.A_SpawnItemEx("LavaCannonProjectile", 0, 0, 0, Random(-20, 20), Random(-20, 20), Random(20, 30), 0, SXF_TRANSFERPOINTERS);
		projectile.gravity = 4;
	}
	
	States {
		Spawn:
			MLVB ABCDEF 2 bright;
			Loop;
			
		Death:
			MLVB GHIJKLMNO 1 bright;
			MLVB P 1 bright A_QuakeEx(1, 1, 1, 10, 0, 100, "world/quake", 0, 1, 1, 0, 0, 1, 1);
			Goto Pool;
	}
}

class LavaCannonLavaPool : MagmaPool {
	States {
		Spawn:
			MLVD ABCDEFGHIJ 4 {
				return A_EffectUpdate(4);
			}
			Loop;
	}
}

class LavaCannonChargerProjectile : LavaCannonProjectile {
	Default {
		SchismProjectile.PoolClass "LavaCannonLavaChargerPool";
	}

	override void OnPoolDamage(int targets) {
		if (targets > 0 && self.target != null) {
			self.target.A_GiveInventory("InfernalCharge", targets);
		}
	}
}

class LavaCannonLavaChargerPool : LavaCannonLavaPool {
	States {
		Spawn:
			MLVI ABCDEFGHIJ 4 {
				return A_EffectUpdate(4);
			}
			Loop;
	}
}

#include "zscript/schism/weapons/mogdread/lavacannon/overheat.zs"
#include "zscript/schism/weapons/mogdread/lavacannon/eruption.zs"
#include "zscript/schism/weapons/mogdread/lavacannon/volcano.zs"
#include "zscript/schism/weapons/mogdread/lavacannon/moltenmine.zs"
#include "zscript/schism/weapons/mogdread/lavacannon/exhaust.zs"
#include "zscript/schism/weapons/mogdread/lavacannon/badlands.zs"
