class LavaCannonBadlandsProjectile : SchismProjectile {
	EbonCommand ebonCommand;
	
	Default {
		Radius 10;
		Height 10;
		Speed 30;
		DamageFunction 20;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		Projectile;
		
		DamageType "Lava";
		
		SeeSound "LavaCannon/Fire/Badlands";
		DeathSound "";
		
		+SKYEXPLODE;
	}
	
	action void A_CallStorm() {
		int stormSize = 3;
		int cloudSize = 200;
		for (int x = -(stormSize * cloudSize); x <= stormSize * cloudSize; x += cloudSize) {
			for (int y = -(stormSize * cloudSize); y <= stormSize * cloudSize; y += cloudSize) {
				A_SpawnItemEx("LavaCannonBadlandsCloud", x, y, 0, 0, 0, 0, 0, SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
				A_SpawnItemEx("LavaCannonBadlandsCloudFloor", x, y, 0, 0, 0, 0, 0, SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
			}
		}
	}
	
	States {
		Spawn:
		Death:
			MLVB A 1;
			MLVB B 1 A_CallStorm();
			Stop;
	}
}

class LavaCannonBadlandsCloud : SchismProjectile {
	int randomStart;
	
	Default {
		Radius 1;
		Height 1;
		Scale 2;
		Speed 15;
		DamageFunction 2;
		Alpha 0.25;
		Gravity 0;
		Renderstyle "Add";
		
		SeeSound "";
		DeathSound "";
		
		+DONTSPLASH;
		+SPAWNCEILING;
		+CEILINGHUGGER;
		+NODAMAGETHRUST;
		+PAINLESS;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		randomStart = Random(0, TICRATE - 1);
	}
	
	action void A_BadlandsEruption() {
		A_StartSound("LavaCannon/Badlands", CHAN_BODY, CHANF_LOOPING);
		if (invoker.lifetime % TICRATE == invoker.randomStart) {
			if (invoker.bFloorHugger) {
				invoker.A_SpawnItemEx("LavaCannonBadlandsEruptProjectile", 0, 0, 0, 0, 0, 5, 0, SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
			} else {
				invoker.A_SpawnItemEx("LavaCannonBadlandsEruptProjectile", 0, 0, -10, 0, 0, -10, 0, SXF_TRANSFERPOINTERS|SXF_NOCHECKPOSITION);
			}
		}
	}
	
	States {
		Spawn:
			NCLH FGHIJABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDE 4 A_BadlandsEruption();
			NCLH ABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDE 4 A_BadlandsEruption();
		Death:
			NCLH F 4 A_StopSound(CHAN_BODY);
		Death.Fade:
			NCLH GGGGHHHHIIIIJJJJ 1 A_FadeOut(0.1);
			Loop;
	}
}

class LavaCannonBadlandsCloudFloor : LavaCannonBadlandsCloud {
	Default {
		-CEILINGHUGGER;
		+FLOORHUGGER;
	}
}

class LavaCannonBadlandsEruptProjectile : LavaCannonProjectile {
	Default {
		SchismProjectile.PoolTics 15 * TICRATE;
	}
}