class ExhaustModule : WeaponPassive {
	Default {
		Scale 1;
		Inventory.PickupMessage "Your Lava Cannon now leaves a trail of excess lava in your wake!";
		
		Inventory.Icon "MLVCU0";
		Inventory.PickupSound "LavaCannon/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "MoglavaCharge";
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			MLVC U 1 bright;
			Loop;
	}
}

class LavaCannonExhaustProjectile : LavaCannonProjectile {
	Default {
		Scale 1;
		Alpha 0;
		Gravity 1;
		DamageFunction 0;
		Speed 0;

		SchismProjectile.PoolTics 5 * TICRATE;
		
		SeeSound "";
		DeathSound "";

		+FLOORHUGGER;
	}
	
	States {
		Spawn:
		Death:
		Burning:
			0000 P 2 A_PoolUpdate();
			0000 PPP 6;
			Loop;
	}
}