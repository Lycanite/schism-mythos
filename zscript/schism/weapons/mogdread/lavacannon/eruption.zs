class EruptionModule : WeaponUpgrade {
	Default {
		Scale 1;
		Inventory.PickupMessage "Your Lava Cannon can now Erupt a volley of lava into the air!";
		
		Inventory.Icon "MLVCW0";
		Inventory.PickupSound "LavaCannon/Pickup";
		Inventory.RestrictedTo "Mogdread";
		WeaponUpgrade.ExtraAmmoClass "MoglavaCharge";
		
		RenderStyle "Normal";
	}

	States {
		Spawn:
			MLVC W 1 bright;
			Loop;
	}
}

class LavaCannonEruptionProjectile : LavaCannonProjectile {
	Default {
		Speed 10;
		Gravity 0.5;

		SchismProjectile.PoolTics 5 * TICRATE;
		
		SeeSound "LavaCannon/Fire/Eruption";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
	}
}
