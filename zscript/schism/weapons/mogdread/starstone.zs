class StarStone : WeaponUltimate {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You have obtained a Star Stone! With this you may activate a weapon's ultimate ability!";
		
		Inventory.Icon "AMOGV0";
		Inventory.PickupSound "Mogdread/Taunt";
		Inventory.RestrictedTo "Mogdread";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	
	States {
		Spawn:
			AMOG V 1 bright;
			Loop;
	}
}
