class EctoplasmProjectile : SchismProjectile {
	int ectoplasmMin;
	int ectoplasmMax;
	int ectoplasmSpawned;
	int ectoplasmCap;
	
	Default {
		PROJECTILE;
		+RANDOMIZE;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		ectoplasmMin = 1;
		ectoplasmMax = 3;
		ectoplasmCap = 10;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (self.ectoplasmMin > 0 && self.ectoplasmSpawned < self.ectoplasmCap)
		{
			if (specialDamage > 0 && !targetActor.bFriendly && !PlayerPawn(targetActor))
			{
				for (int i = 0; i < random(self.ectoplasmMin, self.ectoplasmMax); i++)
				{
					self.A_SpawnItemEx("Ectoplasm", random(-10, 10), 0, random(0, 10), random(0, 10), random(0, 10), random(0, 10), random(-180, 180), SXF_ISTRACER);
					self.ectoplasmSpawned++;
					Ectoplasm ectoplasm = Ectoplasm(self.tracer);
					if (ectoplasm)
					{
						ectoplasm.target = self.target;
						ectoplasm.tracer = self.target;
						ectoplasm.SetFollowTarget(self.target);
					}
				}
			}
		}
		return specialDamage;
	}
}