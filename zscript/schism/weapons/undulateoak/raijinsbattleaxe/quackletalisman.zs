class QuackleTalisman : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Raijin's Battleaxe now has a fierce Quackle sidekick!";
		
		Inventory.Icon "RAJWE0";
		Inventory.PickupSound "RaijinsBattleaxe/Pickup";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "StormSphere";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			RAJW E 1 bright;
			Loop;
	}
}