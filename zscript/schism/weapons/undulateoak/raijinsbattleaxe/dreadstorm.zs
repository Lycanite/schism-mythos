class DreadTalisman : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Raijin's Battleaxe can now cast Dreadstorm to unleash a terrifying dimensional storm!";
		
		Inventory.Icon "RAJWD0";
		Inventory.PickupSound "RaijinsBattleaxe/Pickup";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "StormSphere";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			RAJW D 1 bright;
			Loop;
	}
}

class RaijinsBattleaxeDreadstormProjectile : EctoplasmProjectile {
	Default {
		Radius 10;
		Height 10;
		Speed 15;
		DamageFunction 20;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		Projectile;
		
		DamageType "Electric";
		
		SeeSound "RaijinsBattleaxe/Dreadstorm/Fire";
		DeathSound "";
		
		+SKYEXPLODE;
	}
	
	action void callDreadstorm() {
		int stormSize = 3;
		int cloudSize = 200;
		for (int x = -(stormSize * cloudSize); x <= stormSize * cloudSize; x += cloudSize)
		{
			for (int y = -(stormSize * cloudSize); y <= stormSize * cloudSize; y += cloudSize)
			{
				A_SpawnItemEx("RaijinsBattleaxeDreadstormCloud", x, y, 0, 0, 0, 0, 0);
			}
		}
	}
	
	States {
		Spawn:
			RAJP ABCDEDCB 6 bright;
			Loop;
			
		Death:
			RAJP F 4 bright callDreadstorm();
			RAJP GHIJK 4 bright;
			Stop;
	}
}

class RaijinsBattleaxeDreadstormCloud : UOakPhaseStrike {
	Default {
		Scale 2;
		Alpha 0.5;
		RenderStyle "Translucent";
		
		SeeSound "";
		DeathSound "";
	}
	
	action void stormCloud() {
		A_StartSound("RaijinsBattleaxe/Dreadstorm/Cloud", CHAN_BODY, CHANF_LOOPING);
		for (int i = 0; i < 3; i++)
		{
			A_SpawnItemEx("RaijinsBattleaxeDreadstormRain", random(-100, 100), random(-100, 100), random(-20, 20), 0, 0, random(-10, -15), 0, SXF_CLIENTSIDE);
		}
		if (random(0, 60) == 60)
		{
			A_SpawnItemEx("UOakPhaseStrike", random(-100, 100), random(-100, 100), 0, 0, 0, 0, 0);
		}
		stormRain();
	}
	
	action void stormRain() {
		BlockThingsIterator iter = BlockThingsIterator.Create(invoker, 100, false);
		while (iter.Next())
		{
			Actor targetActor = iter.thing;
			if (CheckSight(targetActor) && targetActor.bIsMonster && !targetActor.bFriendly)
			{
				if (targetActor.CountInv("SchismSoakDebuff") == 0) {
					SchismSoakDebuff soakDebuff = SchismSoakDebuff(targetActor.GiveInventoryType("SchismSoakDebuff"));
					if (soakDebuff)
					{
						soakDebuff.ApplyEffect(invoker.target, invoker);
					}
				}
			}
		}
	}
	
	States {
		Spawn:
			NCLP FGHIJ 4 bright;
			NCLP ABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDEABCDE 4 bright stormCloud();
		Death:
			NCLP F 4 bright A_StopSound(CHAN_BODY);
			NCLP GHIJ 4 bright;
			Stop;
	}
}

class RaijinsBattleaxeDreadstormRain : Actor {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 12;
		DamageFunction 0;
		Gravity 1;
		
		Alpha 0.5;
		RenderStyle "Translucent";
		
		+RANDOMIZE;
		+CANNOTPUSH;
		+MISSILE;
		+BLOODLESSIMPACT;
		-BLOODSPLATTER;
		+CLIENTSIDEONLY;
	}
	
	States {
		Spawn:
			NRNP A 4;
			Loop;
		Death:
			NRNP BCDEFGH 4;
			Stop;
	}
}
