class UOakLightningStrike : SchismProjectile {
	int lightningTick;
	int lightningTickInterval;
	
	Default {
		Radius 1;
		Height 1;
		Scale 1;
		Speed 15;
		DamageFunction 2;
		Alpha 1;
		Gravity 0;
		Renderstyle "Add";
		
		DamageType "Electric";
		
		SeeSound "DruidBow/Lightning/Strike";
		DeathSound "DruidBow/Lightning/Strike";
		
		+DONTSPLASH;
		+SPAWNCEILING;
		+CEILINGHUGGER;
		+NODAMAGETHRUST;
		+PAINLESS;
		-THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.lightningTick = 0;
		self.lightningTickInterval = 1;
	}
	
	action void fireBolt(String projectileClass) {
		if (invoker.lightningTick-- <= 0)
		{
			invoker.lightningTick = invoker.lightningTickInterval;
			A_SpawnProjectile(projectileClass, 0, 0, 0, CMF_ABSOLUTEPITCH|CMF_TRACKOWNER, 90);
		}
		else
		{
			A_SpawnProjectile(projectileClass.."Weak", 0, 0, 0, CMF_ABSOLUTEPITCH|CMF_TRACKOWNER, 90);
		}
	}
	
	States {
		Spawn:
		Death:
			DBWL MNOPMNOPMNOPMNOPMNOPMNOPQR 4 bright fireBolt("UOakLightningBolt");
			Stop;
	}
}

class UOakLightningBolt : SchismProjectile {
	Default {
		Radius 1;
		Height 1;
		Scale 1;
		Speed 8;
		DamageFunction 1;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		
		DamageType "Electric";
		
		SeeSound "DruidBow/Lightning/Strike";
		DeathSound "DruidBow/Lightning/Strike";
		
		+DONTSPLASH;
		+NODAMAGETHRUST;
		+PAINLESS;
		-THRUGHOST;
	}
	
	action void Explode(int range) {
		SchismAttacks.Explode(invoker, invoker.target, 1, range, 0, 1, false, false, "Electric");
	}
	
	States {
		Spawn:
			DBWL STUVWXYZ 2 bright;
			Loop;
		Death:
			DBWL C 2 bright Explode(100);
			DBWL DEFGHIJKL 1 bright;
			Stop;
	}
}

class UOakLightningBoltWeak : UOakLightningBolt {
	Default {
		DamageFunction 0;
	}
	
	action void Explode(int range) {
		// No explosion.
	}
}


// ==================== Phase ====================
class UOakPhaseStrike : UOakLightningStrike {
	Default {
		SeeSound "RaijinsBattleaxe/Lightning/Strike";
		DeathSound "RaijinsBattleaxe/Lightning/Strike";
	}
	
	States {
		Spawn:
		Death:
			RAJL MNOPMNOPMNOPMNOPMNOPMNOPQR 4 bright  fireBolt("UOakPhaseBolt");
			Stop;
	}
}

class UOakPhaseBolt : UOakLightningBolt {
	Default {
		SeeSound "RaijinsBattleaxe/Lightning/Strike";
		DeathSound "RaijinsBattleaxe/Lightning/Strike";
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		
		if (specialDamage > 0 && targetActor.bIsMonster && !targetActor.bBoss)
		{
			if (!targetActor.bFrightened && targetActor.CountInv("SchismFearDebuff") == 0)
			{
				SchismFearDebuff debuff = SchismFearDebuff(targetActor.GiveInventoryType("SchismFearDebuff"));
				if (debuff)
				{
					debuff.ApplyEffect(self.target, self);
				}
			}
		}
		
		return specialDamage;
	}
	
	States {
		Spawn:
			RAJL STUVWXYZ 2 bright;
			Loop;
		Death:
			RAJL C 2 bright Explode(80);
			RAJL DEFGHIJKL 1 bright;
			Stop;
	}
}

class UOakPhaseBoltWeak : UOakPhaseBolt {
	Default {
		DamageFunction 0;
	}
	
	action void Explode(int range) {
		// No explosion.
	}
}