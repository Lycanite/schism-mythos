class DruidBow : SchismWeapon {
	string weaponMode;
	string weaponModePrev;
	
	Default {
		Radius 20;
		Height 16;
		
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		
		Inventory.PickupSound "DruidBow/Pickup";
		Weapon.UpSound "DruidBow/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "DruidBow/Fire/Fail";
		
		Weapon.AmmoType1 "DruidArrow";
		Weapon.AmmoType2 "Ectoplasm";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 25;
		SchismWeapon.SpecialACost 25;
		SchismWeapon.SpecialBCost 25;
		SchismWeapon.SpecialCCost 25;
		SchismWeapon.SpecialDCost 25;
		
		SchismWeapon.SpecialItemA "";
		SchismWeapon.SpecialItemB "LightningDruidCrest";
		SchismWeapon.SpecialItemC "WaterDruidCrest";
		SchismWeapon.SpecialItemD "FaeDruidCrest";
		SchismWeapon.PassiveItem "QuackleDruidCrest";
		
		Inventory.PickupMessage "The fabled Druid Bow has chosen %o as its new owner!";
		Obituary "%o was shot down by %k's Druid Bow!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Druid Arrows to use this ability.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Ectoplasm for a Phase Charged Shot.";
		SchismWeapon.NoSpecialAMessage "\caYou need a Poison Druid Crest to use this ability.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Lightning Druid Crest to use this ability.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Water Druid Crest to use this ability.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Fae Druid Crest to use this ability.";

		SchismWeapon.IdleBob 20;
		
		+INVENTORY.RESTRICTABSOLUTELY;
		+WEAPON.ALT_USES_BOTH;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		weaponMode = "poison";
		weaponModePrev = "poison";
	}
	
	action void A_OnAttack() {
		A_ClearBobbing();
		A_Light2();
	}
	
	
	action string A_GetWeaponMode(void) {
		return invoker.weaponMode;
	}
	
	action string A_GetWeaponModePrev(void) {
		return invoker.weaponModePrev;
	}
	
	action void A_SetWeaponMode(string weaponMode) {
		invoker.weaponModePrev = invoker.weaponMode;
		invoker.weaponMode = weaponMode;
	}
	
	action void A_ChangeWeaponMode(string newWeaponMode) {
		A_ClearBobbing();
		if (newWeaponMode == "lightning")
		{
			if (!A_CheckSpecial(2, false)) {
				return;
			}
			A_SetWeaponMode("lightning");
			A_Light2();
		}
		else if (newWeaponMode == "water")
		{
			if (!A_CheckSpecial(3, false)) {
				return;
			}
			A_SetWeaponMode("water");
			A_Light0();
		}
		else if (newWeaponMode == "fae")
		{
			if (!A_CheckSpecial(4, false)) {
				return;
			}
			A_SetWeaponMode("fae");
			A_Light2();
		}
		else
		{
			A_SetWeaponMode("poison");
			A_Light0();
		}
	}
	
	action void A_FireArrow(int weaponCharge, bool phaseCharge) {
		A_StopSound(CHAN_WEAPON);
		string arrowNamePrefix = "DruidBow";
		string arrowName = phaseCharge ? "Spore" : "Poison";
		string arrowNameSuffix = "Arrow";
		string arrowNameStrength = "";
		
		// Arrow Cost:
		if (!A_CheckAmmo(1)) {
			return;
		}
		A_SpendAmmo(1);
		
		// Arrow Type:
		if (A_GetWeaponMode() == "lightning")
		{
			if (phaseCharge && !A_SpendAmmo(4)) {
				return;
			}
			arrowName = phaseCharge ? "Dimension" : "Lightning";
		}
		else if (A_GetWeaponMode() == "water")
		{
			if (phaseCharge && !A_SpendAmmo(5)) {
				return;
			}
			arrowName = phaseCharge ? "Ghost" : "Water";
		}
		else if (A_GetWeaponMode() == "fae")
		{
			if (phaseCharge && !A_SpendAmmo(6)) {
				return;
			}
			arrowName = phaseCharge ? "Volant" : "Fae";
		}
		else {
			if (phaseCharge && !A_SpendAmmo(3)) {
				return;
			}
		}
		
		// Arrow Strength:
		if (weaponCharge == 1)
		{
			arrowNameStrength = "Far";
		}
		else if (weaponCharge >= 2)
		{
			arrowNameStrength = "Charged";
		}
		
		// Fire Arrow:
		string arrowFullName = arrowNamePrefix..arrowName..arrowNameSuffix..arrowNameStrength;
		A_FireProjectile(arrowFullName, 0, false);
		
		if (A_GetWeaponMode() == "water" && !phaseCharge)
		{
			if (weaponCharge < 3)
			{
				A_FireProjectile(arrowFullName, 10, false, -5);
				A_FireProjectile(arrowFullName, 20, false, -10);
				A_FireProjectile(arrowFullName, -10, false, 5);
				A_FireProjectile(arrowFullName, -20, false, 10);
			}
			else
			{
				for (int i = 10; i < 360; i += 10) {
					A_FireProjectile(arrowFullName, i, false);
				}
			}
		}
	}
	
	
	// Primary Attack:
	action state A_CheckStandardFire() {
		if (!A_CheckAmmo(1))
		{
			return ResolveState("Ready");
		}
		return null;
	}
	
	
	// Secondary Attack:
	action state A_CheckPhaseCharge() {
		if (!A_CheckAmmo(2))
		{
			return ResolveState("Fire");
		}
		return null;
	}
	
	
	States {
		Spawn:
			DBW1 A -1;
			Stop;
			
		Ready:
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "poison", "ReadyPoison");
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "lightning", "ReadyLightning");
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "water", "ReadyWater");
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "fae", "ReadyFae");
		ReadyPoison:
			DBW2 ABC 4 {
				return A_SchismWeaponReady();
			}
			Loop;
		ReadyLightning:
			DBW3 ABC 2 {
				return A_SchismWeaponReady();
			}
			Loop;
		ReadyWater:
			DBW4 ABC 2 {
				return A_SchismWeaponReady();
			}
			Loop;
		ReadyFae:
			DBW5 ABCB 2 {
				return A_SchismWeaponReady();
			}
			Loop;
			
		Deselect:
			DBW1 B 1 A_Lower(12);
			Loop;
			
		Select:
			DBW1 B 1 A_Raise(12);
			Loop;
			
		Fire:
			DBW1 B 0 {
				A_OnAttack();
				return A_CheckStandardFire();
			}
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "poison", "FirePoison0");
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "lightning", "FireLightning0");
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "water", "FireWater0");
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "fae", "FireFae0");
		Fired:
			DBW1 C 4 {
				return A_SchismWeaponReady();
			}
			Goto Ready;
			
		AltFire:
			DBW1 B 0 {
				A_OnAttack();
				return A_CheckPhaseCharge();
			}
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "poison", "FireSpore0");
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "lightning", "FireDimension0");
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "water", "FireGhost0");
			DBW1 B 0 A_JumpIf(A_GetWeaponMode() == "fae", "FireVolant0");
			Goto Fired;
			
		Reload:
			NCRG A 0 A_JumpIf(A_GetWeaponModePrev() == "poison", "User1");
			NCRG A 0 A_JumpIf(A_GetWeaponModePrev() == "lightning", "User2");
			NCRG A 0 A_JumpIf(A_GetWeaponModePrev() == "water", "User3");
			NCRG A 0 A_JumpIf(A_GetWeaponModePrev() == "fae", "User4");
			Goto Ready;
			
		User1:
			DBW1 C 8 A_ChangeWeaponMode("poison");
			Goto Ready;
			
		User2:
			DBW1 C 8 A_ChangeWeaponMode("lightning");
			Goto Ready;
			
		User3:
			DBW1 C 8 A_ChangeWeaponMode("water");
			Goto Ready;
			
		User4:
			DBW1 C 8 A_ChangeWeaponMode("fae");
			Goto Ready;
			
		// ========== Primary Shots ==========
		// Poison:
		FirePoison0:
			DBW2 D 4 A_StartSound("DruidBow/Ready");
			DBW2 D 0 A_Refire("FirePoison1");
			DBW1 C 4 A_FireArrow(0, false);
			Goto Fired;
		FirePoison1:
			DBW2 E 4;
			DBW2 E 0 A_Refire("FirePoison2");
			DBW1 C 4 A_FireArrow(1, false);
			Goto Fired;
		FirePoison2:
			DBW2 F 4;
			DBW2 F 0 A_Refire("FirePoison3");
			DBW1 C 4 A_FireArrow(2, false);
			Goto Fired;
		FirePoison3:
			DBW2 GHIH 2 A_StartSound("DruidBow/Poison/Ready", CHAN_WEAPON, CHANF_LOOPING);
			DBW2 H 0 A_Refire("FirePoison3");
			DBW1 C 4 A_FireArrow(3, false);
			Goto Fired;
		
		// Lightning:
		FireLightning0:
			DBW3 D 4 A_StartSound("DruidBow/Ready");
			DBW3 D 0 A_Refire("FireLightning1");
			DBW1 C 4 A_FireArrow(0, false);
			Goto Fired;
		FireLightning1:
			DBW3 E 4;
			DBW3 E 0 A_Refire("FireLightning2");
			DBW1 C 4 A_FireArrow(1, false);
			Goto Fired;
		FireLightning2:
			DBW3 F 4;
			DBW3 F 0 A_Refire("FireLightning3");
			DBW1 C 4 A_FireArrow(2, false);
			Goto Fired;
		FireLightning3:
			DBW3 GHI 2 A_StartSound("DruidBow/Lightning/Ready", CHAN_WEAPON, CHANF_LOOPING);
			DBW3 I 0 A_Refire("FireLightning3");
			DBW1 C 4 A_FireArrow(3, false);
			Goto Fired;
		
		// Water:
		FireWater0:
			DBW4 D 4 A_StartSound("DruidBow/Ready");
			DBW4 D 0 A_Refire("FireWater1");
			DBW1 C 4 A_FireArrow(0, false);
			Goto Fired;
		FireWater1:
			DBW4 E 4;
			DBW4 E 0 A_Refire("FireWater2");
			DBW1 C 4 A_FireArrow(1, false);
			Goto Fired;
		FireWater2:
			DBW4 F 4;
			DBW4 F 0 A_Refire("FireWater3");
			DBW1 C 4 A_FireArrow(2, false);
			Goto Fired;
		FireWater3:
			DBW4 GHI 2 A_StartSound("DruidBow/Water/Ready", CHAN_WEAPON, CHANF_LOOPING);
			DBW4 I 0 A_Refire("FireWater3");
			DBW1 C 4 A_FireArrow(3, false);
			Goto Fired;
		
		// Fae:
		FireFae0:
			DBW5 D 4 A_StartSound("DruidBow/Ready");
			DBW5 D 0 A_Refire("FireFae1");
			DBW1 C 4 A_FireArrow(0, false);
			Goto Fired;
		FireFae1:
			DBW5 E 4;
			DBW5 E 0 A_Refire("FireFae2");
			DBW1 C 4 A_FireArrow(1, false);
			Goto Fired;
		FireFae2:
			DBW5 F 4;
			DBW5 F 0 A_Refire("FireFae3");
			DBW1 C 4 A_FireArrow(2, false);
			Goto Fired;
		FireFae3:
			DBW5 GHIH 2 A_StartSound("DruidBow/Fae/Ready", CHAN_WEAPON, CHANF_LOOPING);
			DBW5 H 0 A_Refire("FireFae3");
			DBW1 C 4 A_FireArrow(3, false);
			Goto Fired;
			
		// ========== Secondary Shots ==========
		// Spore:
		FireSpore0:
			DBW6 D 8 A_StartSound("DruidBow/Ready");
			DBW6 D 0 A_Refire("FireSpore1");
			DBW1 C 8 A_FireArrow(0, true);
			Goto Fired;
		FireSpore1:
			DBW6 E 8;
			DBW6 E 0 A_Refire("FireSpore2");
			DBW1 C 8 A_FireArrow(1, true);
			Goto Fired;
		FireSpore2:
			DBW6 F 14;
			DBW6 F 0 A_Refire("FireSpore3");
			DBW1 C 8 A_FireArrow(2, true);
			Goto Fired;
		FireSpore3:
			DBW6 GHIH 2 A_StartSound("DruidBow/Spore/Ready", CHAN_WEAPON, CHANF_LOOPING);
			DBW6 H 0 A_Refire("FireSpore3");
			DBW1 C 8 A_FireArrow(3, true);
			Goto Fired;
		
		// Dimension:
		FireDimension0:
			DBW7 D 8 A_StartSound("DruidBow/Ready");
			DBW7 D 0 A_Refire("FireDimension1");
			DBW1 C 8 A_FireArrow(0, true);
			Goto Fired;
		FireDimension1:
			DBW7 E 8;
			DBW7 E 0 A_Refire("FireDimension2");
			DBW1 C 8 A_FireArrow(1, true);
			Goto Fired;
		FireDimension2:
			DBW7 F 16;
			DBW7 F 0 A_Refire("FireDimension3");
			DBW1 C 8 A_FireArrow(2, true);
			Goto Fired;
		FireDimension3:
			DBW7 GHI 2 A_StartSound("DruidBow/Dimension/Ready", CHAN_WEAPON, CHANF_LOOPING);
			DBW7 I 0 A_Refire("FireDimension3");
			DBW1 C 8 A_FireArrow(3, true);
			Goto Fired;
		
		// Ghost:
		FireGhost0:
			DBW8 D 8 A_StartSound("DruidBow/Ready");
			DBW8 D 0 A_Refire("FireGhost1");
			DBW1 C 8 A_FireArrow(0, true);
			Goto Fired;
		FireGhost1:
			DBW8 E 8;
			DBW8 E 0 A_Refire("FireGhost2");
			DBW1 C 8 A_FireArrow(1, true);
			Goto Fired;
		FireGhost2:
			DBW8 F 16;
			DBW8 F 0 A_Refire("FireGhost3");
			DBW1 C 8 A_FireArrow(2, true);
			Goto Fired;
		FireGhost3:
			DBW8 GHI 2 A_StartSound("DruidBow/Ghost/Ready", CHAN_WEAPON, CHANF_LOOPING);
			DBW8 I 0 A_Refire("FireGhost3");
			DBW1 C 8 A_FireArrow(3, true);
			Goto Fired;
		
		// Volant:
		FireVolant0:
			DBW9 D 8 A_StartSound("DruidBow/Ready");
			DBW9 D 0 A_Refire("FireVolant1");
			DBW1 C 8 A_FireArrow(0, true);
			Goto Fired;
		FireVolant1:
			DBW9 E 8;
			DBW9 E 0 A_Refire("FireVolant2");
			DBW1 C 8 A_FireArrow(1, true);
			Goto Fired;
		FireVolant2:
			DBW9 F 14;
			DBW9 F 0 A_Refire("FireVolant3");
			DBW1 C 8 A_FireArrow(2, true);
			Goto Fired;
		FireVolant3:
			DBW9 GHIH 2 A_StartSound("DruidBow/Volant/Ready", CHAN_WEAPON, CHANF_LOOPING);
			DBW9 H 0 A_Refire("FireVolant3");
			DBW1 C 8 A_FireArrow(3, true);
			Goto Fired;
		
		// Ultimate:
		Ultimate:
			DBW6 D 2 A_StartSound("DruidBow/Spore/Ready");
			DBW6 EFGHI 2;
			DBW1 C 2 {
				A_ChangeWeaponMode("poison");
				A_FireArrow(3, true);
				A_FireArrow(3, true);
				A_FireArrow(3, true);
			}
			DBW7 D 2 A_StartSound("DruidBow/Dimension/Ready");
			DBW7 EFGHI 2;
			DBW1 C 2 {
				A_ChangeWeaponMode("lightning");
				A_FireArrow(3, true);
				A_FireArrow(3, true);
				A_FireArrow(3, true);
			}
			DBW8 D 2 A_StartSound("DruidBow/Ghost/Ready");
			DBW8 EFGHI 2;
			DBW1 C 2 {
				A_ChangeWeaponMode("water");
				A_FireArrow(3, true);
				A_FireArrow(3, true);
				A_FireArrow(3, true);
			}
			DBW9 D 2 A_StartSound("DruidBow/Volant/Ready");
			DBW9 EFGHI 2;
			DBW1 C 2 {
				A_ChangeWeaponMode("fae");
				A_FireArrow(3, true);
				A_FireArrow(3, true);
				A_FireArrow(3, true);
			}
			Goto Fired;
	}
}

#include "zscript/schism/weapons/undulateoak/druidbow/poison.zs"
#include "zscript/schism/weapons/undulateoak/druidbow/lightning.zs"
#include "zscript/schism/weapons/undulateoak/druidbow/water.zs"
#include "zscript/schism/weapons/undulateoak/druidbow/fae.zs"
#include "zscript/schism/weapons/undulateoak/druidbow/spore.zs"
#include "zscript/schism/weapons/undulateoak/druidbow/dimension.zs"
#include "zscript/schism/weapons/undulateoak/druidbow/ghost.zs"
#include "zscript/schism/weapons/undulateoak/druidbow/volant.zs"
#include "zscript/schism/weapons/undulateoak/druidbow/quacklecrest.zs"
