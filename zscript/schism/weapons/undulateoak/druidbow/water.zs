class WaterDruidCrest : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Druid Bow can use Water Arrows to sweep back and drench any threats!";
		
		Inventory.Icon "DBW1E0";
		Inventory.PickupSound "DruidBow/Pickup";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "DruidArrow";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			DBW1 E 1 bright;
			Loop;
	}
}

class DruidBowWaterArrow : EctoplasmProjectile {
	Default {
		Radius 32;
		Height 10;
		Speed 15;
		Gravity 0.25;
		DamageFunction 5;
		Alpha 1;
		Renderstyle "Normal";
		
		SeeSound "DruidBow/Water/Fire";
		DeathSound "DruidBow/Water/Explode";

		SchismProjectile.StatusEffectClass "SchismSoakDebuff";
		SchismProjectile.StatusEffectDuration 20 * 35;
		SchismProjectile.KnockbackStrength 20;
		
		PROJECTILE;
		+RANDOMIZE;
		-NOGRAVITY;
		-ADDITIVEPOISONDAMAGE;
		-ADDITIVEPOISONDURATION;
		+RIPPER;
		-THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		ectoplasmMin = -10;
		ectoplasmMax = 1;
	}
	
	States {
		Spawn:
			DBWW A 6;
			Loop;
			
		Death:
			DBWW CDEFG 4;
			Stop;
	}
}

class DruidBowWaterArrowFar : DruidBowWaterArrow {
	Default {
		Speed 25;
		Gravity 0.1;
		
		SeeSound "DruidBow/Water/FireFar";
	}
}

class DruidBowWaterArrowCharged : DruidBowWaterArrowFar {
	Default {
		SeeSound "DruidBow/Water/FireCharged";
		DeathSound "DruidBow/Water/Explode";

		SchismProjectile.KnockbackStrength 30;
	}
	
	States {
		Spawn:
			DBWW B 2 A_SpawnItemEx("DruidBowWaterArrowTrail",random(-2,-8),random(8,-8),random(8,-8),0,0,frandom(0,0.6),0,SXF_NOCHECKPOSITION|SXF_CLIENTSIDE);
			Loop;
			
		Death:
			DBWW HIJKLMNOPQRSTU 2;
			Stop;
	}
}

class DruidBowWaterArrowTrail : Actor  {
	Default  {
		Scale 1.5;
		RenderStyle "Translucent";
		Alpha 0.5;
		
		+NOINTERACTION
		+FORCEXYBILLBOARD
	}
	
	States {
		Spawn:
			DBWW H 0 ThrustThingZ(0, Random(5, 10), 0, 0);
			DBWW H 0 ThrustThing(Random(0, 255), Random(0, 10), 0, 0);
		Active:
			DBWW HIJKLKJI 1 A_FadeOut(0.05);
			Loop;
	}
}
