class DruidBowGhostArrow : DruidBowWaterArrow {
	Actor teleportedActor;
	
	Default {
		SeeSound "DruidBow/Ghost/Fire";
		DeathSound "DruidBow/Ghost/Explode";
		
		-THRUGHOST;
		-RIPPER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		ectoplasmMin = -10;
		ectoplasmMax = 1;
		knockbackStrength = 0;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0)
		{
			SchismPlayerPawn schismPlayer = SchismPlayerPawn(self.target);
			if (!targetActor.bBoss && schismPlayer && !self.teleportedActor)
			{
				vector3 recallPos = schismPlayer.pos;
				vector3 targetPos = targetActor.pos;
				schismPlayer.StartTeleportRecall(6 * 35, targetPos, targetActor);
				self.teleportedActor = targetActor;
				targetActor.SetOrigin(recallPos, true);
				schismPlayer.SetOrigin(targetPos, true);
				schismPlayer.A_SetAngle(targetActor.angle);
				targetActor.A_SpawnItemEx("OakBlink", 0, 0, 0);
				schismPlayer.A_SpawnItemEx("OakBlink", 0, 0, 0);
			}
		}
		return specialDamage;
	}
	
	States {
		Spawn:
			DBWG A 6;
			Loop;
			
		Death:
			DBWG CDEFG 4;
			Stop;
	}
}

class DruidBowGhostArrowFar : DruidBowGhostArrow {
	Default {
		Speed 25;
		Gravity 0.1;
		
		SeeSound "DruidBow/Ghost/FireFar";
	}
}

class DruidBowGhostArrowCharged : DruidBowGhostArrowFar {
	Default {
		SeeSound "DruidBow/Ghost/FireCharged";
		DeathSound "DruidBow/Ghost/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		knockbackStrength = 30;
	}
	
	States {
		Spawn:
			DBWG B 2 A_SpawnItemEx("DruidBowGhostArrowTrail",random(-2,-8),random(8,-8),random(8,-8),0,0,frandom(0,0.6),0,SXF_NOCHECKPOSITION|SXF_CLIENTSIDE);
			Loop;
			
		Death:
			DBWG HIJKLMNOPQRSTU 2;
			Stop;
	}
}

class DruidBowGhostArrowTrail : Actor  {
	Default  {
		Scale 1.5;
		RenderStyle "Translucent";
		Alpha 0.5;
		
		+NOINTERACTION
		+FORCEXYBILLBOARD
	}
	
	States {
		Spawn:
			DBWG H 0 ThrustThingZ(0, Random(5, 10), 0, 0);
			DBWG H 0 ThrustThing(Random(0, 255), Random(0, 10), 0, 0);
		Active:
			DBWG HIJKLKJI 1 A_FadeOut(0.05);
			Loop;
	}
}