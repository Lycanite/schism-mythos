class DruidBowPoisonArrow : EctoplasmProjectile {	
	Default {
		Radius 10;
		Height 10;
		Speed 25;
		Gravity 0.25;
		DamageFunction 2;
		PoisonDamage 10, 10, 10;
		Alpha 1;
		Renderstyle "Normal";
		
		SeeSound "DruidBow/Poison/Fire";
		DeathSound "DruidBow/Poison/Explode";
		
		PROJECTILE;
		+RANDOMIZE;
		-NOGRAVITY;
		-ADDITIVEPOISONDAMAGE;
		+ADDITIVEPOISONDURATION;
		+RIPPER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		ectoplasmMin = -10;
		ectoplasmMax = 1;
	}
	
	States {
		Spawn:
			DBWP A 6;
			Loop;
			
		Death:
			DBWP CDEFG 4;
			Stop;
	}
}

class DruidBowPoisonArrowFar : DruidBowPoisonArrow {
	Default {
		Speed 25;
		Gravity 0.1;
		
		SeeSound "DruidBow/Poison/FireFar";
	}
}

class DruidBowPoisonArrowCharged : DruidBowPoisonArrowFar {
	int poisonDurarionTics;
	int poisonIntervalTics;
	int poisonDamageAmount;
	int ticksUntilPoison;
	
	Default {		
		SeeSound "DruidBow/Poison/FireCharged";
		DeathSound "DruidBow/Poison/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		detonated = false;
		
		poisonDurarionTics = 350;
		poisonIntervalTics = 10;
		poisonDamageAmount = 10;
		ticksUntilPoison = 10;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (!detonated) {
			PoisonEffects(128);
		}
		else {
			PoisonEffects();
			if (--ticksUntilPoison == 0) {
				ticksUntilPoison = poisonIntervalTics;
				PoisonGas();
			}
		}
	}
	
	virtual void PoisonEffects(int effectSize = 255) {
		self.SpawnParticle("Poison", TICRATE, 1, (Random(-4, 4), 0, Random(-4, 4)), Random(0, 359), Random(-45, 45));
		self.SpawnParticle("PoisonBubble", 2 * TICRATE, 1, (Random(-4, 4), 0, Random(-4, 4)), Random(0, 359), Random(-45, 45));
	}
	
	void PoisonGas() {
		BlockThingsIterator iter = BlockThingsIterator.Create(self, 100, false);
		while (iter.Next()) {
			Actor poisonTarget = iter.thing;
			if (CheckSight(poisonTarget) && poisonTarget.bIsMonster && !poisonTarget.bCorpse && SchismInteraction.CanHarmTarget(self, poisonTarget, false)) {
				if (poisonTarget.PoisonDurationReceived < poisonDurarionTics && poisonTarget.PoisonDamageReceived <= poisonDamageAmount) {
					poisonTarget.PoisonMobj(target, self, poisonDamageAmount, poisonDurarionTics, poisonIntervalTics, "poison");
				}
			}
		}
	}
	
	States {
		Spawn:
			DBWP B 2 A_SpawnItemEx("DruidBowPoisonArrowTrail",random(-2,-8),random(8,-8),random(8,-8),0,0,frandom(0,0.6),0,SXF_NOCHECKPOSITION|SXF_CLIENTSIDE);
			Loop;
			
		Death:
			DBWP M 4 {
				ectoplasmMin = 0;
				detonated = true;
			}
			DBWP NOPQR 4;
			TNT1 A 350;
			Stop;
	}
}

class DruidBowPoisonArrowTrail : Actor  {
	Default  {
		Scale 4;
		RenderStyle "Translucent";
		Alpha 0.5;
		
		+NOINTERACTION
		+FORCEXYBILLBOARD
	}
	
	States {
		Spawn:
			DBWP H 0 ThrustThingZ(0, Random(5, 10), 0, 0);
			DBWP H 0 ThrustThing(Random(0, 255), Random(0, 10), 0, 0);
		Active:
			DBWP HIJKL 1 A_FadeOut(0.05);
			Loop;
	}
}