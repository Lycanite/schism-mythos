class DruidBowVolantArrow : DruidBowFaeArrow {
	Default {
		SeeSound "DruidBow/Volant/Fire";
		DeathSound "DruidBow/Volant/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		ectoplasmMin = 10;
		ectoplasmMax = 20;
		ectoplasmCap = 200;
		healingAmount = 10;
	}
	
	override int SpecialMissileHit(Actor targetActor) {
		int hitResult = super.SpecialMissileHit(targetActor);
		
		// Buff Allies:
		Actor source = self;
		if (self.target)
		{
			source = self.target;
		}
		if (SchismInteraction.canHelpTarget(source, targetActor, false) && targetActor.CountInv("SchismHasteBuff") == 0)
		{
			targetActor.GiveInventoryType("SchismHasteBuff");
		}
		
		// Buff Shooter:
		if (self.target && self.target.CountInv("SchismHasteBuff") == 0)
		{
			self.target.GiveInventoryType("SchismHasteBuff");
		}
		
		return hitResult;
	}
	
	States {
		Spawn:
			DBWV A 1 A_SeekerMissile(0, 60, SMF_LOOK|SMF_PRECISE);
			Loop;
			
		Death:
			DBWV CDEFG 4;
			Stop;
	}
}

class DruidBowVolantArrowFar : DruidBowVolantArrow {
	Default {
		Speed 25;
		Gravity 0.1;
		
		SeeSound "DruidBow/Volant/FireFar";
	}
}

class DruidBowVolantArrowCharged : DruidBowVolantArrowFar {
	int healingIntervalTics;
	int healingGasAmount;
	int ticksUntilHeal;
	
	Default {		
		SeeSound "DruidBow/Volant/FireCharged";
		DeathSound "DruidBow/Volant/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		healingAmount = 25;
		
		detonated = false;
		
		healingIntervalTics = 5;
		healingGasAmount = 1;
		ticksUntilHeal = 10;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen())
		{
			return;
		}
		if (!detonated)
		{
			HealingEffects(128);
		}
		else
		{
			HealingEffects();
			if (--ticksUntilHeal == 0)
			{
				ticksUntilHeal = healingIntervalTics;
				HealingGas();
			}
		}
	}
	
	virtual void HealingEffects(int effectSize = 255) {
		int offsetMaxX = 0;
		int offsetMaxY = 0;
		int offsetMaxZ = 0;
		if (effectSize == 255)
		{
			offsetMaxX = random(-100, 100);
			offsetMaxY = random(-100, 100);
			offsetMaxZ = random(-100, 100);
		}
		//int randomEffect = random(255 - effectSize, 255);
		A_SpawnItemEx("DruidBowVolantArrowTrail", offsetMaxX, offsetMaxY, offsetMaxZ, 0, 0, 0, 0, SXF_CLIENTSIDE);
	}
	
	void HealingGas() {
		BlockThingsIterator iter = BlockThingsIterator.Create(self, 100, false);
		while (iter.Next())
		{
			Actor effectTarget = iter.thing;
			if (CheckSight(effectTarget) && SchismInteraction.CanHelpTarget(self.target, effectTarget))
			{
				effectTarget.A_DamageSelf(-self.healingGasAmount, "None", DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
				if (!effectTarget.bBoss && !effectTarget.bFrightened && effectTarget.CountInv("SchismHasteBuff") == 0)
				{
					effectTarget.GiveInventoryType("SchismHasteBuff");
				}
			}
		}
	}
	
	States {
		Spawn:
			DBWV B 2 {
				A_SpawnItemEx("DruidBowVolantArrowTrail",random(-2,-8),random(8,-8),random(8,-8),frandom(0, 2),frandom(0, 2),frandom(0, 2),0,SXF_NOCHECKPOSITION|SXF_CLIENTSIDE);
				A_SeekerMissile(0, 60, SMF_LOOK|SMF_PRECISE);
			}
			Loop;
			
		Death:
			DBWV M 4 {
				ectoplasmMin = 0;
				detonated = true;
			}
			DBWV NOPQR 4;
			TNT1 A 350;
			Stop;
	}
}

class DruidBowVolantArrowTrail : Actor  {
	Default  {
		Scale 1;
		RenderStyle "Add";
		Alpha 0.8;
		
		+NOINTERACTION
		+FORCEXYBILLBOARD
	}
	
	States {
		Spawn:
			DBWV G 0 ThrustThingZ(0, Random(5, 10), 0, 0);
			DBWV G 0 ThrustThing(Random(0, 255), Random(0, 10), 0, 0);
		Active:
			DBWV GFEDCDEF 1 A_FadeOut(0.05);
			Loop;
	}
}