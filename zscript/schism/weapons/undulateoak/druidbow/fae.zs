class FaeDruidCrest : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Druid Bow can use Fae Arrows to heal allies and extract extra Ectoplasm from foes!";
		
		Inventory.Icon "DBW1F0";
		Inventory.PickupSound "DruidBow/Pickup";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "DruidArrow";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			DBW1 F 1 bright;
			Loop;
	}
}

class DruidBowFaeArrow : EctoplasmProjectile {
	int healingAmount;
	
	Default {
		Radius 16;
		Height 10;
		Speed 15;
		Gravity 0.25;
		DamageFunction 20;
		Alpha 1;
		Renderstyle "Normal";
		
		SeeSound "DruidBow/Fae/Fire";
		DeathSound "DruidBow/Fae/Explode";
		
		PROJECTILE;
		+RANDOMIZE;
		-NOGRAVITY;
		-ADDITIVEPOISONDAMAGE;
		-ADDITIVEPOISONDURATION;
		-RIPPER;
		+SEEKERMISSILE;
		+SCREENSEEKER;
		-THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		ectoplasmMin = 10;
		ectoplasmMax = 20;
		ectoplasmCap = 200;
		healingAmount = 10;
	}
	
	override int SpecialMissileHit(Actor targetActor) {
		int hitResult = super.SpecialMissileHit(targetActor);
		
		// Heal Allies:
		Actor source = self;
		if (self.target)
		{
			source = self.target;
		}
		if (SchismInteraction.canHelpTarget(source, targetActor, false))
		{
			targetActor.A_DamageSelf(-self.healingAmount, "None", DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
		}
		
		return hitResult;
	}
	
	States {
		Spawn:
			DBWF A 1 A_SeekerMissile(0, 60, SMF_LOOK|SMF_PRECISE);
			Loop;
			
		Death:
			DBWF CDEFG 4;
			Stop;
	}
}

class DruidBowFaeArrowFar : DruidBowFaeArrow {
	Default {
		Speed 25;
		Gravity 0.1;
		
		SeeSound "DruidBow/Fae/FireFar";
	}
}

class DruidBowFaeArrowCharged : DruidBowFaeArrowFar {
	int healingIntervalTics;
	int healingGasAmount;
	int ticksUntilHeal;
	
	Default {		
		SeeSound "DruidBow/Fae/FireCharged";
		DeathSound "DruidBow/Fae/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		healingAmount = 25;
		
		detonated = false;
		
		healingIntervalTics = 5;
		healingGasAmount = 1;
		ticksUntilHeal = 10;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen())
		{
			return;
		}
		if (!detonated)
		{
			HealingEffects(128);
		}
		else
		{
			HealingEffects();
			if (--ticksUntilHeal == 0)
			{
				ticksUntilHeal = healingIntervalTics;
				HealingGas();
			}
		}
	}
	
	virtual void HealingEffects(int effectSize = 255) {
		int offsetMaxX = 0;
		int offsetMaxY = 0;
		int offsetMaxZ = 0;
		if (effectSize == 255)
		{
			offsetMaxX = random(-100, 100);
			offsetMaxY = random(-100, 100);
			offsetMaxZ = random(-100, 100);
		}
		//int randomEffect = random(255 - effectSize, 255);
		A_SpawnItemEx("DruidBowFaeArrowTrail", offsetMaxX, offsetMaxY, offsetMaxZ, 0, 0, 0, 0, SXF_CLIENTSIDE);
	}
	
	void HealingGas() {
		BlockThingsIterator iter = BlockThingsIterator.Create(self, 100, false);
		while (iter.Next())
		{
			Actor healingTarget = iter.thing;
			if (CheckSight(healingTarget) &&(healingTarget.bFriendly || PlayerPawn(healingTarget)))
			{
				healingTarget.A_DamageSelf(-self.healingGasAmount, "None", DMSS_FOILINVUL|DMSS_NOFACTOR|DMSS_NOPROTECT);
			}
		}
	}
	
	States {
		Spawn:
			DBWF B 2 {
				A_SpawnItemEx("DruidBowFaeArrowTrail",random(-2,-8),random(8,-8),random(8,-8),frandom(0, 2),frandom(0, 2),frandom(0, 2),0,SXF_NOCHECKPOSITION|SXF_CLIENTSIDE);
				A_SeekerMissile(0, 60, SMF_LOOK|SMF_PRECISE);
			}
			Loop;
			
		Death:
			DBWF M 4 {
				ectoplasmMin = 0;
				detonated = true;
			}
			DBWF NOPQR 4;
			TNT1 A 350;
			Stop;
	}
}

class DruidBowFaeArrowTrail : Actor  {
	Default  {
		Scale 1;
		RenderStyle "Add";
		Alpha 0.8;
		
		+NOINTERACTION
		+FORCEXYBILLBOARD
	}
	
	States {
		Spawn:
			DBWF G 0 ThrustThingZ(0, Random(5, 10), 0, 0);
			DBWF G 0 ThrustThing(Random(0, 255), Random(0, 10), 0, 0);
		Active:
			DBWF GFEDCDEF 1 A_FadeOut(0.05);
			Loop;
	}
}
