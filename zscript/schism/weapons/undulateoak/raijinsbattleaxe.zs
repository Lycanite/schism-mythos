class RaijinsBattleaxe : SchismWeapon {
	bool mirroredAttack;	
	int axeSpeed;	
	Array<RaijinsBattleaxeEctoProjectile> ectoProjectiles;
	
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		
		Inventory.PickupSound "RaijinsBattleaxe/Pickup";
		Weapon.UpSound "RaijinsBattleaxe/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "RaijinsBattleaxe/Fire/Fail";
		
		Weapon.AmmoType1 "StormSphere";
		Weapon.AmmoType2 "Ectoplasm";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 2;
		SchismWeapon.SpecialACost 10;
		SchismWeapon.SpecialBCost 25;
		SchismWeapon.SpecialCCost 10;
		SchismWeapon.SpecialDCost 200;
		
		SchismWeapon.SpecialItemA "PhantomTalisman";
		SchismWeapon.SpecialItemB "EctoTalisman";
		SchismWeapon.SpecialItemC "TwisterTalisman";
		SchismWeapon.SpecialItemD "DreadTalisman";
		SchismWeapon.PassiveItem "QuackleTalisman";
		
		Inventory.PickupMessage "You found the legendary Raijin's Battleaxe, you now weild the awesome power of an elder god!";
		Obituary "%o cleaved asunder %k's Raijin's Battleaxe!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Storm Spheres to use this ability.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Ectoplasm to use this ability.";
		SchismWeapon.NoSpecialAMessage "\caYou need a Phantom Talisman to use this ability.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Ecto Talisman to use this ability.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Twister Talisman to use this ability.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Dread Talisman to use this ability.";

		SchismWeapon.IdleBob 20;
		
		+FLOATBOB;
		+NOGRAVITY;
		+INVENTORY.RESTRICTABSOLUTELY;
		+WEAPON.ALT_AMMO_OPTIONAL;
		+WEAPON.ALT_USES_BOTH;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		mirroredAttack = false;		
		axeSpeed = 0;
	}
	
	action void A_OnAttack() {
		A_ClearBobbing();
		A_Light2();
	}
	
	
	// Primary Attack:
	action void A_StartLightningStrike() {
		invoker.axeSpeed = 0;
	}
	
	action void A_FireLightningStrike() {
		if (A_CheckAmmo(1, 0, false)) {
			A_SpendAmmo(1);
			A_FireProjectile("RaijinsBattleaxeProjectile", 0, false);
			A_FireProjectile("RaijinsBattleaxeProjectile", 15, false, -20);
			A_FireProjectile("RaijinsBattleaxeProjectile", -15, false, 20);
		}
		int meleeDamage = 20;
		if (CountInv("PowerStrength")) {
			meleeDamage = 40;
		}
		A_CustomPunch(meleeDamage, true, CPF_PULLIN|CPF_NOTURN, "BulletPuff", 96, 0, 0, "ArmorBonus", "RaijinsBattleaxe/Projectile");
	}
	
	action State A_RefireLightningStrike() {
		A_OverlayFlags(0, PSPF_FLIP|PSPF_MIRROR, !invoker.mirroredAttack);
		invoker.mirroredAttack = !invoker.mirroredAttack;
		invoker.axeSpeed++;
		if (!(GetPlayerInput(MODINPUT_BUTTONS) & BT_ATTACK)) {
			return null;
		}
		if (invoker.axeSpeed >= 6) {
			return ResolveState("Hold2");
		}
		else if (invoker.axeSpeed >= 2) {
			return ResolveState("Hold1");
		}
		return ResolveState("Hold");
	}
	
	
	// Secondary Attack:
	action state A_StaticPhase(bool useAmmo = false) {
		A_StartSound("RaijinsBattleaxe/StaticPhase", CHAN_WEAPON, CHANF_LOOPING);
		if (useAmmo) {
			A_SpendAmmo(2);
			int projectileRange = 75;
			for (int i = 0; i < 360; i += 20) {
				//Actor projectile = A_SpawnProjectile("RaijinsBattleAxeStaticPhaseProjectile", invoker.height / 2, 0, i, CMF_AIMDIRECTION);
				Actor projectile = A_FireProjectile("RaijinsBattleAxeStaticPhaseProjectile", i, false, 0, invoker.height / 2);
				RaijinsBattleAxeStaticPhaseProjectile staticProjectile = RaijinsBattleAxeStaticPhaseProjectile(projectile);
				if (staticProjectile)
				{
					staticProjectile.setFollowTarget(invoker.owner, projectileRange * cos(i), projectileRange * sin(i), 0);
				}
			}
		}
		A_GiveInventory("RaijinsBattleAxeStaticPhaseGiver", 1);
		
		if (!GetPlayerInput(MODINPUT_BUTTONS) & BT_ALTATTACK) {
			return ResolveState("AltFinish");
		}
		
		return null;
	}
	
	action void A_StaticPhaseStop() {
		A_StopSound(CHAN_WEAPON);
		A_TakeInventory("RaijinsBattleAxeStaticPhase", 1);
	}
	
	
	// Special 1 Phantom Strike:
	action void A_FirePhantomStrike() {
		A_SpendAmmo(3);
		A_FireProjectile("RaijinsBattleaxePhantomProjectile", 0, false);
		A_FireProjectile("RaijinsBattleaxePhantomProjectile", 0, false, 0, 10, 0, -5);
		A_FireProjectile("RaijinsBattleaxePhantomProjectile", 0, false, 0, 20, 0, -10);
		A_FireProjectile("RaijinsBattleaxePhantomProjectile", 0, false, 0, -10, 0, 5);
		A_FireProjectile("RaijinsBattleaxePhantomProjectile", 0, false, 0, -20, 0, 10);
	}
	
	
	// Special 2 Ecto Strike:
	action void A_FireEctoStrike() {
		A_SpendAmmo(4);
		Actor projectile = A_FireProjectile("RaijinsBattleaxeEctoProjectile", 0, false, 0, invoker.height / 2);
		RaijinsBattleaxeEctoProjectile ectoProjectile = RaijinsBattleaxeEctoProjectile(projectile);
		if (ectoProjectile) {
			ectoProjectile.raijinsBattleaxe = invoker;
			invoker.ectoProjectiles.Push(ectoProjectile);
		}
	}
	
	
	// Special 3 Reality Twister:
	action void A_FireRealityTwister() {
		A_SpendAmmo(5);
		A_StartSound("RaijinsBattleaxe/Twister", CHAN_WEAPON);
		for (int i = 0; i < 360; i += 20) {
			Actor projectile = A_FireProjectile("RaijinsBattleaxeTwisterProjectile", i, false);
			RaijinsBattleaxeTwisterProjectile twisterProjectile = RaijinsBattleaxeTwisterProjectile(projectile);
			if (twisterProjectile) {
				twisterProjectile.setFollowTarget(invoker.owner, i);
			}
		}
	}
	
	
	// Special 4 Dreadstorm: TODO Replace
	action void A_FireDreadstorm() {
		A_SpendAmmo(6);
		A_StartSound("RaijinsBattleaxe/Dreadstorm/Fire", CHAN_BODY);
		A_SpawnItemEx("RaijinsBattleaxeDreadstormProjectile", 0, 0, invoker.height / 2, 0, 0, 10);
	}
	
	
	// Ultimate Dreadstorm:
	action void A_Dreadstorm() {
		A_StartSound("RaijinsBattleaxe/Dreadstorm/Fire", CHAN_BODY);
		A_FireProjectile("RaijinsBattleaxeDreadstormProjectile", 0, false, 0, 0, FPF_NOAUTOAIM, -90);
	}
	
	
	States {
		Ready:
			RAJI ABCDCB 6 {
				return A_SchismWeaponReady();
			}
			Loop;
			
		Deselect:
			RAJI A 1 A_Lower(12);
			Loop;
			
		Select:
			RAJI A 1 A_Raise(12);
			Loop;
			
		Fire:
			RAJI E 0 {
				A_OnAttack();
				A_OverlayFlags(0, PSPF_FLIP|PSPF_MIRROR, invoker.mirroredAttack);
				A_StartLightningStrike();
			}
		Hold:
			RAJI E 4 A_Light2();
			RAJI F 4;
			RAJI G 4 A_FireLightningStrike();
			RAJI HI 4;
			RAJI I 4 A_Light0();
			RAJI J 0 {
				return A_RefireLightningStrike();
			}
			Goto Ready;
		Hold1:
			RAJI E 3 A_Light2();
			RAJI F 3;
			RAJI G 3 A_FireLightningStrike();
			RAJI HI 3;
			RAJI I 3 A_Light0();
			RAJI J 0 {
				return A_RefireLightningStrike();
			}
			Goto Ready;
		Hold2:
			RAJI E 2 A_Light2();
			RAJI F 2;
			RAJI G 2 A_FireLightningStrike();
			RAJI HI 2;
			RAJI I 2 A_Light0();
			RAJI J 0 {
				return A_RefireLightningStrike();
			}
			Goto Ready;
			
		AltFire:
			RAJI K 0 {
				return A_CheckAmmoJump(2, "Ready");
			}
			RAJI K 4 A_Light2();
		AltHold:
			RAJI L 4 A_StaticPhase(true);
			RAJI MN 4 A_StaticPhase();
			RAJI N 0 A_Refire();
		AltFinish:
			RAJI K 2 {
				A_StaticPhaseStop();
				A_Light1();
			}
			Goto Ready;
			
		User1:
			RAJI O 4 {
				return A_CheckSpecialJump(1, "Ready");
			}
			RAJI P 4 A_OnAttack();
			RAJI Q 4;
			RAJI R 8 A_FirePhantomStrike();
			RAJI S 4 A_Light0();
			Goto Ready;
			
		User2:
			RAJI E 4 {
				return A_CheckSpecialJump(2, "Ready");
			}
			RAJI F 4 A_OnAttack();
			RAJI G 4;
			RAJI H 4 A_FireEctoStrike();
			RAJI I 4 A_Light0();
			RAJI J 4;
			Goto Ready;
			
		User3:
			RAJI T 4 {
				return A_CheckSpecialJump(3, "Ready");
			}
			RAJI U 4 A_OnAttack();
			RAJI V 4;
			RAJI W 8 A_FireRealityTwister();
			RAJI V 4 A_Light0();
			RAJI UT 4;
			Goto Ready;
			
		User4:
			RAJI T 4 {
				return A_CheckSpecialJump(4, "Ready");
			}
			RAJI U 4 A_OnAttack();
			RAJI V 4 A_Light2();
			RAJI W 8 A_FireDreadstorm();
			RAJI V 4 A_Light0();
			RAJI UT 4;
			Goto Ready;
			
		Ultimate:
			RAJI T 4;
			RAJI U 4 A_OnAttack();
			RAJI V 4 A_Light2();
			RAJI W 8 A_Dreadstorm();
			RAJI V 4 A_Light0();
			RAJI UT 4;
			Goto Ready;
			
		Spawn:
			RAJI Z -1;
			Stop;
	}
}

class RaijinsBattleaxeProjectile : EctoplasmProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 8;
		DamageFunction 5;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		
		DamageType "Electric";
		
		SeeSound "RaijinsBattleaxe/Projectile";
		DeathSound "RaijinsBattleaxe/Projectile/Explode";
		
		+CANNOTPUSH;
		+NODAMAGETHRUST;
		-THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		ectoplasmMin = 1;
		ectoplasmMax = 3;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		
		// Strike Enemies:
		if (!targetActor.bFriendly && target) {
			self.A_SpawnItemEx("UOakPhaseStrike", 0, 0);
		}
		
		// Taunt:
		if (specialDamage > 0 && !targetActor.bFriendly && !PlayerPawn(targetActor)) {
			targetActor.target = target;
		}
		
		return specialDamage;
	}
	
	States {
		Spawn:
			RAJP ABCDEF 4 bright;
			
		Death:
			RAJP G 4 bright {
				SchismAttacks.Explode(invoker, invoker.target, 10, 100, 0, 1, false, false, "Electric");
			}
			RAJP HIJK 4 bright;
			Stop;
	}
}

class RaijinsBattleaxeStaticPhaseProjectile : FollowProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 0.5;
		Speed 0;
		DamageFunction 1;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		
		DamageType "Electric";
		
		SeeSound "";
		DeathSound "";
		
		+CANNOTPUSH;
		+NODAMAGETHRUST;
		-THRUGHOST;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
	}
	
	States {
		Spawn:
			RAJL C 4 bright;
			
		Death:
			RAJL D 4 bright {
				SchismAttacks.Explode(invoker, invoker.target, 1, 10);
			}
			RAJL EFGHIJKL 4 bright;
			Stop;
	}
}

class RaijinsBattleaxeStaticPhaseGiver : PowerupGiver {
	Default {
		Scale 1;
		Inventory.MaxAmount 0;
		Powerup.Duration -6000;
		Powerup.Type "RaijinsBattleaxeStaticPhase";
		
		Inventory.Icon "RAJIZ0";
		Inventory.PickupSound "";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		
		+INVENTORY.RESTRICTABSOLUTELY;
		+INVENTORY.AUTOACTIVATE;
	}
}

class RaijinsBattleaxeStaticPhase : PowerInvulnerable {
	override void DoEffect() {
		super.DoEffect();
		
		owner.A_ChangeVelocity(0, 0, 0, CVF_RELATIVE|CVF_REPLACE);
	}
}

#include "zscript/schism/weapons/undulateoak/raijinsbattleaxe/phantomstrike.zs"
#include "zscript/schism/weapons/undulateoak/raijinsbattleaxe/ectostrike.zs"
#include "zscript/schism/weapons/undulateoak/raijinsbattleaxe/realitytwister.zs"
#include "zscript/schism/weapons/undulateoak/raijinsbattleaxe/dreadstorm.zs"
#include "zscript/schism/weapons/undulateoak/raijinsbattleaxe/quackletalisman.zs"
