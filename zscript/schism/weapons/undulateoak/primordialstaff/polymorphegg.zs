class PolymorphEgg : WeaponUpgrade {
    Default {
        Scale 0.5;
        Inventory.PickupMessage "Your Primordial Staff can now fire special eggs that polymorph lesser creatures into feeble chickens!";
        
        Inventory.Icon "WCCKY0";
        Inventory.PickupSound "PrimordialStaff/Pickup";
        Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
        WeaponUpgrade.ExtraAmmoClass "LifeSeed";
        
        RenderStyle "Normal";
        
        +FLOATBOB;
    }

    States {
        Spawn:
            WCCK Y 1 bright;
            Loop;
    }
}

class PrimordialStaffPolymorphProjectile : MorphProjectile {
    Default {
        Radius 16;
        Height 10;
        Scale 0.25;
        Speed 12;
        DamageFunction 15;
        Gravity 0.125;
        Alpha 1;
        Renderstyle "Normal";
        Projectile;
        BounceType "Grenade";
        BounceCount 5;
        
        SeeSound "PrimordialStaff/Polymorph";
        DeathSound "PrimordialStaff/Polymorph/Explode";
        
        MorphProjectile.PlayerClass "";
        MorphProjectile.MonsterClass "Chicken";
        MorphProjectile.Duration 10 * 35;
        MorphProjectile.MorphStyle
            MRF_FULLHEALTH|
            MRF_UNDOBYTOMEOFPOWER|
            MRF_UNDOBYCHAOSDEVICE|
            MRF_FAILNOTELEFRAG|
            !MRF_WHENINVULNERABLE|
            MRF_LOSEACTUALWEAPON;
        
        +RANDOMIZE;
        -NOGRAVITY;
        +CANBOUNCEWATER;
        +CANNOTPUSH;
        +NODAMAGETHRUST;
    }
    
    override int DoSpecialDamage(Actor target, int damage, name damageType) {
        int damage = super.DoSpecialDamage(target, damage, damageType);
        Actor source = self;
        if (self.target) {
            source = self.target;
        }
        if (damage > 0 && !SchismInteraction.CanHarmTarget(source, target, false)) {
            return 0;
        }
		if (PlayerPawn(target)) {
			return 0;
		}
		if (target.bBoss) {
			return 0;
		}
        return damage;
    }
    
    override int SpecialMissileHit(Actor target) {
		if (target.bBoss) {
			return 0;
		}
		if (!SchismInteraction.CanHarmTarget(self, target, false)) {
			return 0;
		}
        return super.SpecialMissileHit(target);
    }
    
    /*override void InitEffect() {
        if (owner) {
            transformHealth = owner.Health;
        }
        super.InitEffect();
        if (owner) {
            owner.Health = transformHealth;
        }        
    }
    
    override void EndEffect() {
        super.EndEffect();
        if (owner) {
            owner.Health = transformHealth;
        }
    }*/
    
    States {
        Spawn:
            WCEG PQRS 4;
            Loop;
            
        Death:
            WCEG T 4 bright {
                Gravity = 0;
                SchismAttacks.Explode(invoker, invoker.target, 2, 40);
            }
            WCEG UVWX 4 bright;
            Stop;
    }
}
