class ThornsEgg : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Primordial Staff can now launch eggs loaded with deadly thorns!";
		
		Inventory.Icon "WCCKZ0";
		Inventory.PickupSound "Cockatrice/See";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		WeaponUpgrade.ExtraAmmoClass "LifeSeed";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			WCCK Z 1 bright;
			Loop;
	}
}