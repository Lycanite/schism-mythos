class CockatriceMorph : PowerupGiver {
	Default {
		Scale 1;
		Inventory.MaxAmount 0;
		Powerup.Type "PowerCockatrice";
		Powerup.Duration 525;
		
		Inventory.Icon "WCCKY0";
		Inventory.PickupSound "Cockatrice/See";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		
		+INVENTORY.RESTRICTABSOLUTELY;
		+INVENTORY.AUTOACTIVATE;
	}

	States {
		Spawn:
			WCCT H 1 bright;
			Loop;
	}
}

class PowerCockatrice : PowerMorph {
	int transformHealth;
	
	Default {
		PowerMorph.PlayerClass "UndulateOakCockatrice";
		PowerMorph.MorphStyle(
			MRF_FULLHEALTH|
			MRF_ADDSTAMINA|
			MRF_FAILNOTELEFRAG|
			MRF_WHENINVULNERABLE|
			MRF_UNDOBYDEATH|
			MRF_UNDOBYDEATHFORCED|
			MRF_UNDOBYDEATHSAVES|
			MRF_UNDOALWAYS|
			MRF_TRANSFERTRANSLATION
		);
	}
	
	override void InitEffect() {
		if (owner)
		{
			transformHealth = owner.Health;
		}
		super.InitEffect();		
	}
	
	override void EndEffect() {
		super.EndEffect();
		if (owner)
		{
			owner.Health = transformHealth;
		}
	}
}

class WeaponCockatrice : Weapon {
	int primaryCost;
	int secondaryCost;
	int eggBombCost;
	int eggnadoCost;
	int cockatriceCost;
	int polymorphCost;
	
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Weapon.AmmoType "LifeSeed";
		Weapon.AmmoUse 0;
		Weapon.AmmoGive 0;
		Weapon.AmmoType2 "Ectoplasm";
		Weapon.AmmoUse2 0;
		Weapon.AmmoGive2 0;
		Weapon.SlotPriority 0;
		
		Weapon.UpSound "Cockatrice/See";
		Weapon.ReadySound "";
		Obituary "%o was devoured by %k's Cockatrice!";
		
		+WEAPON.AMMO_OPTIONAL;
		+WEAPON.ALT_AMMO_OPTIONAL;
		+WEAPON.MELEEWEAPON;
		-WEAPON.DONTBOB;
		+WEAPON.CHEATNOTWEAPON;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		
		primaryCost = 1;
		secondaryCost = 2;
		eggBombCost = 10;
		eggnadoCost = 10;
		polymorphCost = 20;
		cockatriceCost = 50;
	}
	
	
	// Primary Cockatrice Attack:
	action void CockatriceAttack(void) {
		A_CustomPunch(20, true, CPF_PULLIN, "BulletPuff", 96);
		if (CountInv("LifeSeed") < 1) {
			return;
		}
		A_TakeInventory("LifeSeed", 1);
		A_StartSound("Cockatrice/Fire", CHAN_WEAPON);
		for (int i = 0; i < 3; i++)
		{
			A_FireProjectile("PrimordialStaffProjectileStrong", random(-40, 40), false, random(-5, 5), random(-10, 10));
		}
	}
	
	
	// Special 1 Egg Bomb:
	action state CheckEggBomb() {
		if (CountInv("BombEgg") < 1) {
			A_StartSound("Cockatrice/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need a Bomb Egg to use this ability.");
			return ResolveState("Ready");
		}
		if (CountInv("LifeSeed") < invoker.primaryCost)
		{
			A_StartSound("PrimordialStaff/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Life Seeds to fire an Egg Bomb.", 2);
			return ResolveState("Ready");
		}
		if (CountInv("Ectoplasm") < invoker.eggBombCost)
		{
			A_StartSound("Cockatrice/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Ectoplasm to fire an Egg Bomb.", 2);
			return ResolveState("Ready");
		}
		return null;
	}
	
	
	// Special 2 Eggnado:
	action state CheckTornadoEgg() {
		if (CountInv("TornadoEgg") < 1) {
			A_StartSound("PrimordialStaff/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need a Tornado Egg to use this ability.");
			return ResolveState("Ready");
		}
		if (CountInv("LifeSeed") < invoker.primaryCost)
		{
			A_StartSound("PrimordialStaff/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Life Seeds to fire an Eggnado.", 2);
			return ResolveState("Ready");
		}
		if (CountInv("Ectoplasm") < invoker.eggnadoCost)
		{
			A_StartSound("PrimordialStaff/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Ectoplasm to fire an Eggnado.", 2);
			return ResolveState("Ready");
		}
		return null;
	}
	
	action void FireTornadoEgg() {
		A_TakeInventory("LifeSeed", invoker.primaryCost);
		A_TakeInventory("Ectoplasm", invoker.eggnadoCost);
		for (int i = 0; i < 360; i += 72)
		{
			A_FireProjectile("PrimordialStaffEggnadoProjectile", i, false);
		}
	}
	
	
	// Special 3 Polymorph:
	action state CheckPolymorph() {
		if (CountInv("PolymorphEgg") < 1) {
			A_StartSound("Cockatrice/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need a Polymorph Egg to use this ability.");
			return ResolveState("Ready");
		}
		if (CountInv("LifeSeed") < invoker.primaryCost)
		{
			A_StartSound("PrimordialStaff/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Life Seeds to fire Polymorph Eggs.", 2);
			return ResolveState("Ready");
		}
		if (CountInv("Ectoplasm") < invoker.polymorphCost)
		{
			A_StartSound("Cockatrice/Fire/Fail", CHAN_WEAPON);
			A_Print("\caYou need more Ectoplasm to fire Polymorph Eggs.", 2);
			return ResolveState("Ready");
		}
		return null;
	}
	
	action void FirePolymorph() {
		A_TakeInventory("LifeSeed", invoker.primaryCost);
		A_TakeInventory("Ectoplasm", invoker.polymorphCost);
		for (int i = 0; i < 360; i += 20)
		{
			A_FireProjectile("PrimordialStaffPolymorphProjectile", i, false);
		}
	}
	
	
	// Special 4 Dismount:
	action void CockatriceDismount(void) {
		A_TakeInventory("PowerCockatrice", 1);
	}
	
	
	States {
		Ready:
			WCCT ABCD 6 A_WeaponReady(WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3);
		Mounted:
			WCCT ABCD 6 A_WeaponReady(WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
			Loop;
			
		Deselect:
			WCCT A 1 A_Lower(12);
			Loop;
			
		Select:
			WCCT A 1 A_Raise(12);
			Loop;
			
		Fire:
			WCCT EF 2 CockatriceAttack();
			WCCT G 2 A_ReFire();
			Goto Mounted;
			
		AltFire:
			WCCT E 4 {
				A_StartSound("Cockatrice/Melee");
				A_Recoil(-20);
				A_CustomPunch(60, true, CPF_NOTURN, "BulletPuff", 96);
			}
			WCCT FFF 4 {
				A_Recoil(-20);
				A_CustomPunch(60, true, CPF_NOTURN, "BulletPuff", 96);
			}
			WCCT GH 4 A_CustomPunch(60, true, CPF_NOTURN, "BulletPuff", 96);
			WCCT I 4 A_ReFire();
			Goto Mounted;
			
		User1:
			WCCT E 4 {
				return CheckEggBomb();
			}
			WCCT F 4 {
				A_StartSound("Cockatrice/Fire/Bomb");
			}
			WCCT G 4 {
				A_TakeInventory("LifeSeed", invoker.primaryCost);
				A_TakeInventory("Ectoplasm", invoker.eggBombCost);
				A_FireProjectile("PrimordialStaffBomb");
				A_FireProjectile("PrimordialStaffBomb", 20);
				A_FireProjectile("PrimordialStaffBomb", 40);
				A_FireProjectile("PrimordialStaffBomb", -20);
				A_FireProjectile("PrimordialStaffBomb", -40);
			}
			WCCT H 6 A_Light0();
			WCCT I 6;
			Goto Ready;
			
		User2:
			WCCT E 4 {
				return CheckTornadoEgg();
			}
			WCCT F 4 {
				A_StartSound("Cockatrice/Fire/Tornado");
			}
			WCCT G 4 FireTornadoEgg();
			WCCT H 6 A_Light0();
			WCCT I 6;
			Goto Ready;
			
		User3:
			WCCT E 4 {
				return CheckPolymorph();
			}
			WCCT F 4 {
				A_StartSound("Cockatrice/Fire/Polymorph");
			}
			WCCT K 4 {
				FirePolymorph();
			}
			WCCT H 6 A_Light0();
			WCCT I 6;
			Goto Ready;
		
		User4:
			WCCT A 0 CockatriceDismount();
			Goto Mounted;
	}
}
