class DimensionalFragment : WeaponUltimate {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You have obtained a Dimensional Fragment! With this you may activate a weapon's ultimate ability!";
		
		Inventory.Icon "AUOKV0";
		Inventory.PickupSound "UndulateOak/Taunt";
		Inventory.RestrictedTo "UndulateOak", "UndulateOakCockatrice";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	
	States {
		Spawn:
			AUOK V 1 bright;
			Loop;
	}
}
