class VessSpell : DualHand {	
	Default {
		Weapon.AmmoType "BloodDrupe";
		Weapon.AmmoType2 "Anima";
		
		Inventory.PickupSound "Vess/Taunt";
		Inventory.RestrictedTo "Vess";
		
		+FLOATBOB;
		+NOGRAVITY;
		+INVENTORY.RESTRICTABSOLUTELY;
	}
}
