class WickedCavort : VessSpell {
    int damageBoost;
	Actor lastHit;

	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.PickupSound "WickedCavort/Pickup";
		Weapon.UpSound "WickedCavort/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "WickedCavort/Fire/Fail";
		
		Weapon.AmmoType1 "LustDrupe";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 1;
		SchismWeapon.SpecialACost 10;
		SchismWeapon.SpecialBCost 10;
		SchismWeapon.SpecialCCost 25;
		SchismWeapon.SpecialDCost 25;
		
		SchismWeapon.SpecialItemA "GreedyWaltzScroll";
		SchismWeapon.SpecialItemB "GluttonChasseScroll";
		SchismWeapon.SpecialItemC "SlothsPirouetteScroll";
		SchismWeapon.SpecialItemD "EnviousImpetusScroll";
		SchismWeapon.PassiveItem "LustfulFouetScroll";
		
		Inventory.PickupMessage "The wicked dancefloor calls, the cavort to burn enemy souls is yours!";
		Obituary "%o had their soul burnt by %k's Wicked Cavort!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Lust Drupes to cast this spell.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Anima to cast this spell.";
		SchismWeapon.NoSpecialAMessage "\caYou need a Greedy Waltz Scroll to cast this spell.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Glutton Chasse Scroll to cast this spell.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Sloth's Pirouette Scroll to cast this spell.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Envious Impetus Scroll to cast this spell.";

		SchismWeapon.IdleBob 30;
		DualHand.MirrorOffHand true;
	}

	action state A_OffHandReady() {
		if (invoker.damageBoost > 0) {
			return invoker.ResolveState("OffHand.Boosted");
		}
		return null;
	}
	
	States {
		Spawn:
			VESW B 4 bright;
			Loop;
			
		MainHand.Select:
			VSH1 A 1 A_Raise(12);
			Loop;
			
		MainHand.Deselect:
			VSH1 A 1 A_Lower(12);
			Loop;
			
		MainHand.Ready:
			VSH1 AABBCCDDEEFF 3 {
				return A_SchismWeaponReady();
			}
			Loop;
			
		OffHand.Select:
		OffHand.Deselect:
		OffHand.Ready:
			VSH3 G 0 A_OffHandReady();
			VSH3 GHIJKL 5;
			Loop;
		OffHand.Boosted:
			VSH4 GHIJKL 5;
			Goto OffHand.Ready;
		
		OffHand.Ultimate:
			VSH3 MNON 3;
			Loop;
	}
}

#include "zscript/schism/weapons/vess/wickedcavort/wrathcross.zs"
#include "zscript/schism/weapons/vess/wickedcavort/pridedosado.zs"
#include "zscript/schism/weapons/vess/wickedcavort/greedywaltz.zs"
#include "zscript/schism/weapons/vess/wickedcavort/gluttonchasse.zs"
#include "zscript/schism/weapons/vess/wickedcavort/slothspirouette.zs"
#include "zscript/schism/weapons/vess/wickedcavort/enviousimpetus.zs"
#include "zscript/schism/weapons/vess/wickedcavort/lustfulfouet.zs"
#include "zscript/schism/weapons/vess/wickedcavort/acediaronde.zs"