extend class MaladyQuiver {
	action void A_MaladyDart() {
		Actor projectile = A_FireProjectile("MaladyDartProjectile", 0, true);
		MaladyDartProjectile maladyDartProjectile = MaladyDartProjectile(projectile);
		if (maladyDartProjectile) {
			maladyDartProjectile.maladyQuiver = invoker;
		}
	}

    States {
		MainHand.Fire:
			VSH1 W 2 {
                A_ClearBobbing();
                A_Light2();
            }
			VSH1 X 2;
			VSH1 Y 2 A_MaladyDart();
            VSH1 Z 2;
            VSH3 QRLK 2;
			VSH3 C 0 A_MirrorHands();
			VSH3 C 0 A_ReFire("Fire");
			VSH3 C 0 A_Light0();
			Goto Ready;

        OffHand.Fire:
			VSH5 ABCDEFGH 2;
			Loop;
    }
}

class MaladyDartProjectile : NoxiousDartProjectile {
	MaladyQuiver maladyQuiver;

	Default {
        Scale 0.75;
		Radius 2;
		Height 2;
		Speed 80;
		FastSpeed 80;
		DamageFunction GetDamage();

		SchismProjectile.StatusEffectDuration 20 * 35;
		SchismProjectile.StatusEffectStrength 6;
		
		SeeSound "MaladyQuiver/Fire";
		DeathSound "MaladyQuiver/Explode";
	}

	virtual int GetDamage() {
        return 4 + Min(32, (self.lifetime / TICRATE * 32));
	}
	
	override int DoSpecialDamage(Actor target, int damage, name damageType) {
		// Contagion:
		if (self.maladyQuiver) {
			int passiveLevel = self.maladyQuiver.GetPassiveLevel();
			if (passiveLevel >= 1) {
				self.statusEffectSpread = "SchismPoisonEffect";
			}
	
		}
		return super.DoSpecialDamage(target, damage, damageType);
	}
	
	override void OnInflictEffect(Actor target, Inventory statusEffect) {
		// Contagion:
		if (self.maladyQuiver) {
			int passiveLevel = self.maladyQuiver.GetPassiveLevel();
			SchismPoisonEffect poisonEffect = SchismPoisonEffect(statusEffect);
			if (!poisonEffect) {
				return;
			}
			if (passiveLevel >= 2) {
				poisonEffect.gasLevel = 1;
			}
			if (passiveLevel >= 3) {
				poisonEffect.gasLevel = 2;
			}
		}
	}
}