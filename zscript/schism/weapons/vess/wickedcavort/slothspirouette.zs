class SlothsPirouetteScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You can now trap your enemies into the Sloths Pirouette.";
		
		Inventory.Icon "VESWK0";
		Inventory.PickupSound "WickedCavort/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "LustDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW K 1 bright;
			Loop;
	}
}

extend class WickedCavort {
	action state A_SlothsPirouette() {
		if (!A_CheckSpecial(3) || !A_SpendAmmo(5)) {
			return ResolveState("Ready");
		}

        A_StartSound("WickedCavort/Fire/SlothsPirouette", CHAN_WEAPON);
        Actor projectileActor = A_FireProjectile("SlothsPirouetteProjectile");
		SchismProjectile projectile = SchismProjectile(projectileActor);
		if (projectile) {
			projectile.statusEffectDuration = (5 + (invoker.GetPassiveLevel() * 5)) * TICRATE;
		}
		return null;
	}

    States {
		MainHand.User3:
			VSH1 Q 2 {
                A_ClearBobbing();
                A_Light2();
            }
			VSH1 R 2;
			VSH1 S 2 A_SlothsPirouette();
            VSH1 T 2;
            VSH1 UVR 2;
			VSH1 R 0 A_Light0();
			Goto Ready;

        OffHand.User3:
			VSH3 PQR 2;
			Loop;
    }
}

class SlothsPirouetteProjectile : WrathCrossProjectile {
	Actor twirlActor;

	Default {
		Radius 5;
		Height 7;
		Scale 0.5;
		Speed 30;
		Gravity 0.25;
		DamageFunction Random(10, 20);
		Alpha 1.0;
		RenderStyle "Add";

		BounceType "Hexen";
		BounceFactor 1;
		WallBounceFactor 1;
		BounceCount 3;

		SchismProjectile.LifetimeMax 0;

		SchismProjectile.ParticleName "Nether";
		SchismProjectile.ParticleColor 0xFFDD0000;
		SchismProjectile.StatusEffectClass "SlothsPirouetteEffect";
		SchismProjectile.StatusEffectDuration 5 * TICRATE;
		SchismProjectile.StatusEffectStrength 1;
		
		SeeSound "WickedCavort/Fire/SlothsPirouette";
		DeathSound "WickedCavort/Explode/PrideDosado";
		Obituary "%o was spun one too many times by %k's Sloth's Pirouette!";
		
		-NOGRAVITY;
	}

	override int SpecialMissileHit(Actor target) {
		if (self.twirlActor && self.twirlActor == target) {
			return 1;
		}
		return super.SpecialMissileHit(target);
	}
	
	States {
		Spawn:
			VWC5 ABCDEFGHIJ 4;
			Loop;
			
		Death:
			VWC5 GHIJ 4;
			Stop;
	}
}

class SlothsPirouetteEffect : SchismTwirlEffect {
	bool firedNext;

	Default {
		SchismStatusEffect.ParticleName "Nether";
		SchismStatusEffect.ParticleColor 0xFFDD0000;
	}
	
	override void DoEffect() {
		super.DoEffect();
		if (!self.firedNext && self.effectInflictor && self.effectTics % TICRATE == 0) {
			bool spawned;
			Actor projectileActor;
			[spawned, projectileActor] = self.owner.A_SpawnItemEx("SlothsPirouetteProjectile", 0, 0, 0, Random(-10, 10), Random(-10, 10), Random(0, 5));
			SlothsPirouetteProjectile projectile = SlothsPirouetteProjectile(projectileActor);
			if (projectile) {
				projectile.target = self.effectInflictor;
				projectile.twirlActor = self.owner;
			}
			self.firedNext = true;
		}
	}
}