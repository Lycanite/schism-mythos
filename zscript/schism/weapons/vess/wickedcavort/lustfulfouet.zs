class LustfulFouetScroll : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You can now perform Lustful Fouets to enhance your Wicked Cavort performance.";
		
		Inventory.Icon "VESWM0";
		Inventory.PickupSound "WickedCavort/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "LustDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW M 1 bright;
			Loop;
	}
}