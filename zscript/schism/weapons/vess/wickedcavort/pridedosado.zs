extend class WickedCavort {
	action state A_PrideDosado() {
		if (!A_SpendAmmo(2)) {
			return ResolveState("Ready");
		}

        A_StartSound("WickedCavort/Fire/PrideDosado", CHAN_WEAPON);
        Actor projectileActor = A_FireProjectile("PrideDosadoProjectile");
		SchismProjectile projectile = SchismProjectile(projectileActor);
		if (projectile) {
			projectile.statusEffectDuration = (5 + (invoker.GetPassiveLevel() * 5)) * TICRATE;
		}
		return null;
	}

    States {
		MainHand.AltFire:
			VSH1 Q 2 {
                A_ClearBobbing();
                A_Light2();
            }
			VSH1 R 2;
			VSH1 S 2 A_PrideDosado();
            VSH1 T 2;
            VSH1 UVR 2;
			VSH1 R 0 A_ReFire("AltFire");
			VSH1 R 0 A_Light0();
			Goto Ready;

        OffHand.AltFire:
			VSH3 PQR 2;
			Loop;
    }
}

class PrideDosadoProjectile : WrathCrossProjectile {
	Default {
		Radius 5;
		Height 7;
		Scale 0.5;
		Speed 10;
		Gravity 0.25;
		DamageFunction Random(10, 20);
		Alpha 1.0;
		RenderStyle "Add";
		
		SeeSound "WickedCavort/Fire/PrideDosado";
		DeathSound "WickedCavort/Explode/PrideDosado";
		Obituary "%o had their soul flayed by %k's Pride Dosado!";

		SchismProjectile.LifetimeMax 0;

		SchismProjectile.ParticleName "Nether";
		SchismProjectile.ParticleInterval 1;
		SchismProjectile.ParticleDuration 3 * TICRATE;

		SchismProjectile.PoolTics 5 * TICRATE;
		SchismProjectile.PoolDamage 2;
		SchismProjectile.PoolDamageRadius 140;
		SchismProjectile.PoolEffectTime 2 * TICRATE;
		SchismProjectile.PoolEffectClass "SchismFloatEffect";
		SchismProjectile.PoolClass "PrideDosadoPool";
		
		-NOGRAVITY;
	}
	
	override Inventory InflictEffect(Class<Inventory> effectName, Actor targetActor, int effectDuration, Class<SchismStatusEffect> effectSpread, int effectStrength) {
		super.InflictEffect(effectName, targetActor, effectDuration, effectSpread, effectStrength);
        SchismStatusEffect effect = SchismStatusEffect(SchismStatusEffects.InflictEffect("SchismBleedDebuff", self.target, self, targetActor, effectDuration, effectSpread, 2));
        if (effect) {
            effect.effectDamage = 1;
            effect.effectDamageType = "Nether";
            effect.particleName = "Nether";
        }
		return effect;
	}

	override void OnPoolDamage(int targets) {
		if (targets > 0 && self.target != null) {
			self.target.A_GiveInventory("Anima", targets);
		}
	}
	
	States {
		Spawn:
			VWC2 ABCDEFGHIJ 4;
			Loop;
			
		Death:
			VWC2 E 4 {
                invoker.bFloorHugger = true;
                invoker.particleCount = 20;
            }
        Pool:
			VWC2 F 4 A_PoolUpdate(0.1);
			VWC2 GH 4;
			Loop;
	}
}

class PrideDosadoPool : ProjectilePool {
	Default {
		Radius 16;
		Height 1;
		Scale 1;
		Gravity 0.25;
		Alpha 1;
		Renderstyle "Add";

		ProjectilePool.ScaleBase 1;
		ProjectilePool.RemainingTics 2 * TICRATE;

		ProjectilePool.particleName "Nether";
		ProjectilePool.particleCount 1;
		ProjectilePool.ParticleInterval 10;
		ProjectilePool.particleOffsetRandom 60;
		ProjectilePool.particleDuration TICRATE;
		ProjectilePool.particleSpeed 2;
		ProjectilePool.particlePitchMin -100;
		ProjectilePool.particlePitchMax -80;
	}
	
	States {
		Spawn:
			VWC2 EFGHIJ 4 {
				return A_EffectUpdate(4);
			}
			Loop;
	}
}