class HyperbloodScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You can now stimulate your Blood Globe to explode with anima refreshing any of your enemy's ailments!";
		
		Inventory.Icon "VESWE0";
		Inventory.PickupSound "BloodGlobe/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "BloodDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW E 1 bright;
			Loop;
	}
}

extend class BloodGlobe {
	action void A_Hyperblood() {
		int level = invoker.AnimaLevel(invoker.specialBCost);
		if (!A_CheckSpecial(2) || !A_SpendAmmo(4) || !invoker.sphere) {
			return;
		}
		A_StartSound("BloodGlobe/Fire/Hyperblood", CHAN_WEAPON);
		invoker.sphereTarget.locked = true;
		invoker.sphere.Hyperblood(level);
	}

	States {
		MainHand.User2:
			VSH1 W 0 A_ClearBobbing();
			VSH1 WX 4;
			VSH1 Y 4 A_Hyperblood();
			VSH1 ZW 4 A_Light0();
			Goto Ready;
	}
}

extend class BloodGlobeSphere {
	int hyperbloodLevel;

	virtual void Hyperblood(int level) {
		if (!self.bloodGlobe) {
			return;
		}
		self.specialAction = "Hyperblood";
		self.hyperbloodLevel = level;
		
		float scale = 0.1 + (float(level) / 8);
		self.A_SetScale(scale);
		self.SetState(self.ResolveState("Hyperblood"));

		int damage = level * 16 * (1 + self.bloodGlobe.GetPassiveLevel());
		double range = level * 100;
		Array<Actor> actorsHit;
		SchismAttacks.Explode(self, self.bloodGlobe.owner, damage, range, 0, 1, false, false, "Fae", actorsHit);
		self.A_QuakeEx(level, level, level, 10, 0, range * 2, "world/quake", 0, 1, 1, 0, 0, 1, 1);

		// Particles:
		for (int i = 0; i < level * 100; i++) {
			self.SpawnParticle("Anima", Round((float(level) / 4) * TICRATE), 8,
				(Random(-50 * scale, 50 * scale), Random(-50 * scale, 50 * scale), Random(-50 * scale, 50 * scale)),
				Random(0, 360), Random(-90, 90), scale
			);
		}

		// Renew Detrimental Effects:
		ThinkerIterator statusEffectIterator = ThinkerIterator.Create("SchismStatusEffect");
		SchismStatusEffect statusEffect;
		while (statusEffect = SchismStatusEffect(statusEffectIterator.Next())) {
			if (!statusEffect.detrimental) {
				continue;
			}
			for (int i = 0; i < actorsHit.Size(); i++) {
				Actor target = actorsHit[0];
				if (target && statusEffect.owner == target) {
					statusEffect.RefreshEffect();
				}
			}
		}
	}

	States {
		Hyperblood:
			VBLO PPPPQQQQRRRRQQQQPPPP 1 A_BloodGlobeUpdate(false);
			VBLO P 0 A_EndSpecial();
			Goto Spawn;
	}
}