class BlooddrinkerScroll : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You can now fire slicing blades from your Blood Globe to bleed your enemies to heal yourself and your allies!";
		
		Inventory.Icon "VESWF0";
		Inventory.PickupSound "BloodGlobe/Pickup";
		Inventory.RestrictedTo "Vess";
		WeaponUpgrade.ExtraAmmoClass "BloodDrupe";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			VESW F 1 bright;
			Loop;
	}
}

extend class BloodGlobe {
	action void A_Blooddrinker() {
		int level = invoker.AnimaLevel(invoker.specialCCost);
		if (!A_CheckSpecial(3) || !A_SpendAmmo(5) || !invoker.sphere) {
			return;
		}
		invoker.sphere.Blooddrinker(level);
	}

	States {
		MainHand.User3:
			VSH1 K 0 A_ClearBobbing();
			VSH1 KL 4;
			VSH1 M 4 A_Blooddrinker();
			VSH1 NO 4 A_Light0();
			Goto Ready;
	}
}

extend class BloodGlobeSphere {
	int blooddrinkerLevel;

	virtual void Blooddrinker(int level) {
		if (!self.bloodGlobe) {
			return;
		}
		self.specialAction = "Blooddrinker";
		self.blooddrinkerLevel = level;
		self.SetState(self.ResolveState("Blooddrinker"));
	}

	action void A_Blooddrinker() {
		if (!invoker.bloodGlobe) {
			return;
		}

		for (int i = 0; i < invoker.blooddrinkerLevel; i++) {
			Actor projectile = A_SpawnProjectile("BloodGlobeBlooddrinkerProjectile", 0, 0, Random(0, 360), CMF_AIMDIRECTION|CMF_TRACKOWNER, Random(-90, 90));
			if (projectile && invoker.bloodGlobe) {
				projectile.target = invoker.bloodGlobe.owner;
				projectile.A_SetScale(invoker.scale.x);
			}
		}
	}

	States {
		Blooddrinker:
			VBLO MMMMNNNNOOOOMMMMNNNNOOOO 1 A_Blooddrinker();
			VBLO O 0 A_EndSpecial();
			Goto Spawn;
	}
}

class BloodGlobeBlooddrinkerProjectile : BloodBelchProjectile {
	Default {
		DamageFunction 2;
		Alpha 1;

		SchismProjectile.KnockbackStrength 1;
		SchismProjectile.StatusEffectClass "SchismBleedDebuff";
		SchismProjectile.StatusEffectDuration 10 * TICRATE;
		SchismProjectile.StatusEffectStrength 2;

		SchismProjectile.ParticleInterval 4;
		
		SeeSound "BloodGlobe/Fire/Blooddrinker";

		+RIPPER;
	}
	
	States {
		Spawn:
			NPBS ABCDEFGH 2;
			Loop;
	}
}