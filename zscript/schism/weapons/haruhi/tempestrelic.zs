class TempestRelic : WeaponUltimate {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You have obtained a Tempest Relic! With this you may activate a weapon's ultimate ability!";
		
		Inventory.Icon "AHARV0";
		Inventory.PickupSound "Haruhi/Taunt";
		Inventory.RestrictedTo "Haruhi";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	
	States {
		Spawn:
			AHAR V 1 bright;
			Loop;
	}
}
