class BlizzardFractal : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Glacial Nodachi can now cast Blizzard!";
		
		Inventory.Icon "GNODW0";
		Inventory.PickupSound "GlacialNodachi/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "GlacialCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			GNOD W 1 bright;
			Loop;
	}
}

class GlacialNodachiBlizzardProjectile : GlacialNodachiProjectile {
	Default {
		Speed 12;
		DamageFunction 2;
		BounceType "Grenade";
		BounceCount 12;
		
		SeeSound "GlacialNodachi/Projectile/Blizzard";
		DeathSound "GlacialNodachi/Projectile/Blizzard/Explode";
		
		+RIPPER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
	}
	
	States {
		Spawn:
			GNP3 ABCDEFG 2;
			GNP3 HIJKLMNOPQRSTUVWXYZ[]\ 2;
			GNP3 ABCDEFGHIJKLMNOPQRSTUVWXYZ[]\ 2;
			
		Death:
			Stop;
	}
}
