class IcicleFractal : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Glacial Nodachi can now cast Icicle Valley!";
		
		Inventory.Icon "GNODV0";
		Inventory.PickupSound "GlacialNodachi/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "GlacialCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			GNOD V 1 bright;
			Loop;
	}
}

class GlacialNodachiIcicleSeedProjectile : GlacialNodachiProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 0.5;
		Speed 28;
		DamageFunction 10;
		
		SeeSound "GlacialNodachi/Projectile/Icicle";
		DeathSound "GlacialNodachi/Projectile/Icicle/Explode";
		
		+RIPPER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
		self.inflictChill = false;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0 && !targetActor.bBoss) {
			targetActor.AddZ(10);
		}
		return specialDamage;
	}
	
	action void A_SpawnIcicle() {
		GlacialNodachiIcicleProjectile projectileActor = GlacialNodachiIcicleProjectile(A_SpawnProjectile("GlacialNodachiIcicleProjectile", 0, 0, 0, CMF_AIMDIRECTION|CMF_TRACKOWNER, 0));
		if (projectileActor) {
			projectileActor.target = target;
		}
	}
	
	States {
		Spawn:
			GNP3 JKLJKLJKLJKL 4 A_SpawnIcicle();
			
		Death:
			Stop;
	}
}

class GlacialNodachiIcicleProjectile : SchismProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 1;
		Speed 0;
		DamageFunction 10;
		Alpha 0.75;
		Renderstyle "Translucent";
		
		SeeSound "GlacialNodachi/Projectile/Icicle";
		DeathSound "GlacialNodachi/Projectile/Icicle/Explode";
		
		+DONTSPLASH;
		+NODAMAGETHRUST;
		+PAINLESS;
		+FORCERADIUSDMG;
		+FLOORHUGGER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
	}
	
	States {
		Spawn:
		Death:
			GNP2 A 2 {
				A_StartSound("GlacialNodachi/Projectile/Icicle");
				SchismAttacks.Explode(invoker, invoker.target, 2, 100, 0, 1, false, false, "Ice");
			}
			GNP2 AABBB 2 {SchismAttacks.Explode(invoker, invoker.target, 2, 100, 0, 1, false, false, "Ice");}
			GNP2 CCCCCCCC 4 {SchismAttacks.Explode(invoker, invoker.target, 2, 150, 0, 1, false, false, "Ice");}
			GNP2 CCCCCCBB 4 {SchismAttacks.Explode(invoker, invoker.target, 2, 100, 0, 1, false, false, "Ice");}
			GNP2 D 4 {
				A_Scream();
				A_IceGuyDie();
				SchismAttacks.Explode(invoker, invoker.target, 2, 10);
			}
			GNP2 EFGH 4 {
				A_IceGuyDie();
				SchismAttacks.Explode(invoker, invoker.target, 2, 10, 0, 1, false, false, "Ice");
			}
			Stop;
	}
}
