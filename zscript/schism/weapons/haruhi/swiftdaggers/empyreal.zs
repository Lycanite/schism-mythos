class EmpyrealSash : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Swift Daggers can now cast Empyreal Rending!";
		
		Inventory.Icon "SWDGV0";
		Inventory.PickupSound "SwiftDaggers/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "WhirlwindCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			SWDG V 1 bright;
			Loop;
	}
}

class SwiftDaggersEmpyrealProjectile : SwiftDaggersProjectile {
	Default {
		Scale 2;
		
		SeeSound "SwiftDaggers/Projectile/Empyreal";
		DeathSound "SwiftDaggers/Projectile/Empyreal/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
	}
}
