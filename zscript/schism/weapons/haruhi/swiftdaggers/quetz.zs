class QuetzSash : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Swift Daggers can now cast Quetz Wingbeat!";
		
		Inventory.Icon "SWDGX0";
		Inventory.PickupSound "SwiftDaggers/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "WhirlwindCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			SWDG X 1 bright;
			Loop;
	}
}

class SwiftDaggersQuetzProjectile : SwiftDaggersProjectile {
	Default {
		Radius 32;
		Scale 1;
		DamageFunction 2;
		
		SeeSound "SwiftDaggers/Projectile/Quetz";
		DeathSound "SwiftDaggers/Projectile/Quetz/Explode";
		
		+RIPPER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
		self.pitch = 0;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0)
		{
			if (!targetActor.bBoss)
			{
				targetActor.Thrust(20, self.angle);
			}
		}
		return specialDamage;
	}
	
	States {
		Spawn:
			SDP1 ABCDEFGHIJ 2;
			
		Death:
			SDP1 K 2 {
				SchismAttacks.Explode(invoker, invoker.target, 10, 100, 0, 1, false, false, "Air");
			}
			SDP1 L 2;
			Stop;
	}
}
