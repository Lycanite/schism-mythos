class TornadoSash : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Swift Daggers can now cast Tornado!";
		
		Inventory.Icon "SWDGY0";
		Inventory.PickupSound "SwiftDaggers/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "WhirlwindCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			SWDG Y 1 bright;
			Loop;
	}
}

class SwiftDaggersTornado : SchismAttackActor {
	Default {
		Radius 32;
		Height 10;
		Scale 3;
		Speed 20;
		DamageFunction 0;
		Gravity 0.125;
		Alpha 0.8;
		Renderstyle "Translucent";

		SchismAttackActor.Lifespan 5 * TICRATE;
		SchismAttackActor.GenerateSpecialAmmo false;
		
		SeeSound "SwiftDaggers/Tornado";
		DeathSound "SwiftDaggers/Tornado";
		
		+RANDOMIZE;
		-NOGRAVITY;
	}
	
	action state A_TornadoUpdate() {
		if (!target) {
			return invoker.ResolveState("Death");
		}

		A_StartSound(invoker.seeSound, CHAN_BODY, CHANF_LOOPING);
		A_Wander();
		SchismAttacks.Explode(invoker, invoker.target, 0, 200, 4, 8);
		
		for (int i = 0; i < 4; i++) {
			int offsetXY = random(-10, 10);
			int offsetZ = random(0, 40);
			Actor projectileActor = A_SpawnProjectile("SwiftDaggersTornadoProjectile", offsetZ, offsetXY, Random(-180, 180), CMF_AIMDIRECTION|CMF_TRACKOWNER, Random(0, -35));
			if (projectileActor) {
				projectileActor.target = target;
			}
		}
		
		if (invoker.lifespan <= 0) {
			return invoker.ResolveState("Death");
		}
		return null;
	}
	
	States {
		Spawn:
			SWDP WXYZ 4 bright;
			
		See:
			SWDP TUV 4 bright {
				return A_TornadoUpdate();
			}
			Loop;
			
		Death:
			SWDP Z 4 bright A_StopSound(CHAN_BODY);
			SWDP YXW 4 bright;
			Stop;
	}
}

class SwiftDaggersTornadoProjectile : SwiftDaggersProjectile {
	Default {
		Scale 2;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
	}
}
