class MagnetBlade : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Celestial Kunai can now use Magnet Blade!";
		
		Inventory.Icon "CKNIV0";
		Inventory.PickupSound "CelestialKunai/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "CelestialCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			CKNI V 1 bright;
			Loop;
	}
}

class CelestialKunaiMagnetProjectile : SchismStickyProjectile {
	CelestialKunai celestialKunai;
	
	Default {
		Radius 16;
		Height 10;
		Scale 0.25;
		Speed 60;
		DamageFunction 40;
		Gravity 0;
		Alpha 1;
		Renderstyle "Normal";
		
		DamageType "Gravity";
		
		SeeSound "CelestialKunai/Projectile/Magnet";
		DeathSound "CelestialKunai/Projectile/Magnet/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
		self.expireTime = 3 * 35;
	}
	
	action void MagnetUpdate() {
		if (invoker.celestialKunai)
		{
			for (int i = 0; i < invoker.celestialKunai.magnetProjectiles.Size(); i++)
			{
				CelestialKunaiMagnetProjectile targetProjectile = invoker.celestialKunai.magnetProjectiles[i];
				if (targetProjectile == invoker || !targetProjectile || invoker.Distance3D(targetProjectile) > 500)
				{
					continue;
				}
				
				double thrustAmount = 5;
				Actor magnetTarget = targetProjectile.followTarget;
				if (magnetTarget)
				{
					magnetTarget.Thrust(thrustAmount, magnetTarget.AngleTo(invoker));
					if (magnetTarget.pos.z < invoker.pos.z - 50)
					{
						magnetTarget.A_ChangeVelocity(magnetTarget.vel.x, magnetTarget.vel.y, 0, CVF_REPLACE);
						magnetTarget.AddZ(thrustAmount * 4, false);
					}
					else if (magnetTarget.pos.z > invoker.pos.z + 50) {
						magnetTarget.AddZ(-thrustAmount, false);
					}
				}
			}
		}
	}
	
	States {
		Spawn:
			CKNP FGHIJKLM 2;
			Loop;
			
		Death:
			CKNP G 4 {
				A_SetRenderStyle(1.0, STYLE_Add);
				MagnetUpdate();
				return AttachUpdate();
			}
			Loop;
			
		Detonate:
			CKNP N 0 {
				A_SetRenderStyle(1.0, STYLE_Add);
				A_SetScale(1.0);
			}
			CKNP N 2 Detonate();
			CKNP OPQRSTUVWXY 2;
			Stop;
	}
}
