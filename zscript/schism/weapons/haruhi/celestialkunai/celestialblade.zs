class CelestialKunaiProjectile : SchismProjectile {
	float pullForce;
	float pullRange;
	
	CelestialKunai celestialKunai;
	
	Default {
		Radius 16;
		Height 10;
		Scale 0.25;
		Speed 100;
		DamageFunction 16;
		Gravity 0;
		Alpha 1;
		Renderstyle "Normal";
		
		DamageType "Gravity";
		
		SeeSound "CelestialKunai/Projectile";
		DeathSound "CelestialKunai/Projectile/Explode";
		
		+RIPPER;
		+EXPLODEONWATER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.pullForce = 20;
		self.pullRange = 100;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0 && self.celestialKunai && targetActor)
		{
			self.celestialKunai.lastHitActor = targetActor;
		}
		return specialDamage;
	}
	
	States {
		Spawn:
			CKNP ABCDE 2;
			Loop;
			
		Death:
			CKNP N 0 {
				A_SetRenderStyle(1.0, STYLE_Add);
				A_SetScale(1.0);
			}
			CKNP N 2 {
				SchismAttacks.Explode(invoker, invoker.target, 4, invoker.pullRange, -invoker.pullForce, 1, false, false, "Gravity");
			}
			CKNP OPQRSTUVWXY 2;
			Stop;
	}
}

class CelestialKunaiGraplingProjectile : SchismStickyProjectile {
	float grapleDistance;
	float grapleDistanceOrig;
	CelestialKunai celestialKunai;
	
	Default {
		Radius 16;
		Height 10;
		Scale 0.25;
		Speed 100;
		DamageFunction 40;
		Gravity 0;
		Alpha 1;
		Renderstyle "Normal";
		
		DamageType "Gravity";
		
		SeeSound "CelestialKunai/Projectile/Grapling";
		DeathSound "CelestialKunai/Projectile/Grapling/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.expireTime = 60 * 60 * 24 * 35;
		self.grapleDistance = 0;
		self.grapleDistanceOrig = 200;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0)
		{
			// Grapling Stick
		}
		return specialDamage;
	}
	
	action void GraplingUpdate() {
		if (!invoker.target || !invoker.celestialKunai)
		{
			invoker.Detach();
			return;
		}
		
		double distance = invoker.target.Distance3D(invoker);
		double xyDistance = invoker.target.Distance2D(invoker);
		double thrustAmount = -5;
		
		// Initial Graple:
		if (invoker.grapleDistance <= 0)
		{
			invoker.grapleDistance = distance;
			invoker.grapleDistanceOrig = distance;
		}
		
		// Controls:
		PlayerPawn playerTarget = PlayerPawn(invoker.target);
		if (playerTarget)
		{
			// Swinging:
			double swingForce = 0.5;
			if (playerTarget.GetPlayerInput(MODINPUT_BUTTONS) & BT_FORWARD)
			{
				invoker.target.A_ChangeVelocity(swingForce, 0, 0, CVF_RELATIVE);
			}
			else if (playerTarget.GetPlayerInput(MODINPUT_BUTTONS) & BT_BACK)
			{
				invoker.target.A_ChangeVelocity(-swingForce, 0, 0, CVF_RELATIVE);
			}
			if (playerTarget.GetPlayerInput(MODINPUT_BUTTONS) & BT_MOVELEFT)
			{
				invoker.target.A_ChangeVelocity(0, swingForce, 0, CVF_RELATIVE);
			}
			else if (playerTarget.GetPlayerInput(MODINPUT_BUTTONS) & BT_MOVERIGHT)
			{
				invoker.target.A_ChangeVelocity( 0, -swingForce,0, CVF_RELATIVE);
			}
			
			// Distance Reeling:
			if (playerTarget.GetPlayerInput(MODINPUT_BUTTONS) & BT_JUMP)
			{
				invoker.grapleDistance = max(50, invoker.grapleDistance - 10);
			}
			else if (playerTarget.GetPlayerInput(MODINPUT_BUTTONS) & BT_CROUCH)
			{
				invoker.grapleDistance = min(invoker.grapleDistanceOrig + 200, invoker.grapleDistance + 10);
			}
		}
		
		// Horizontal:
		if (xyDistance > invoker.grapleDistance)
		{
			double sideThrustScale =(xyDistance / invoker.grapleDistance) - 1;
			invoker.target.Thrust(thrustAmount * sideThrustScale, invoker.AngleTo(invoker.target));
		}
		
		// Vertical:
		if (distance > invoker.grapleDistance)
		{
			double zDistance = invoker.pos.z - invoker.target.pos.z;
			if (zDistance < -20 && invoker.pos.z - 20 > invoker.target.GetZAt(0, 0, 0))
			{
				invoker.target.AddZ(thrustAmount, false);
			}
			else if (zDistance > 20)
			{
				if (invoker.target.vel.z < 0)
				{
					invoker.target.A_ChangeVelocity(invoker.target.vel.x, invoker.target.vel.y, 0, CVF_REPLACE);
				}
				double upThrustScale =(zDistance / invoker.grapleDistance) - 1;
				double upVel = -thrustAmount * upThrustScale * 4;
				invoker.target.AddZ(max(0, upVel), false);
			}
		}
	}
	
	States {
		Spawn:
			CKNP FGHIJKLM 2;
			Loop;
			
		Death:
			CKNP G 1 {
				GraplingUpdate();
				return AttachUpdate();
			}
			Loop;
			
		Detonate:
			CKNP H 2 Detonate();
			CKNP IJ 2;
			Stop;
	}
}