class BlackHoleBlade : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Celestial Kunai can now use Black Hole Blade!";
		
		Inventory.Icon "CKNIY0";
		Inventory.PickupSound "CelestialKunai/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "CelestialCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			CKNI Y 1 bright;
			Loop;
	}
}

class CelestialKunaiBlackHoleProjectile : CelestialKunaiProjectile {
	Default {
		Scale 0.5;
		Height 80;
		Speed 4;
		DamageFunction 40;
		
		SeeSound "CelestialKunai/Projectile/BlackHole";
		DeathSound "CelestialKunai/Projectile/BlackHole/Explode";
		
		+SKYEXPLODE;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
		self.pullForce = 30;
		self.pullRange = 800;
	}
	
	States {
		Spawn:
			CKNP ABCDE 8;
		Death:
			CKNP A 0 A_Scream();
			CKNP ABCDE 4;
			CKNP ABCDE 2;
			CKNP ABCDE 1;
			CKNB A 0 {
				A_SetSpeed(0);
				A_Stop();
				A_SetRenderStyle(0.6, STYLE_Translucent);
				A_SetScale(4);
			}
			CKNB ABCDEFGHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKHIJKLMNOP 2 {
				SchismAttacks.Explode(invoker, invoker.target, 4, invoker.pullRange, -invoker.pullForce, 1, false, false, "Gravity");
			}
			Stop;
	}
}
