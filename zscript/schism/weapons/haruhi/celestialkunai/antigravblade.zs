class AntigravBlade : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Celestial Kunai can now use Antigrav Blade!";
		
		Inventory.Icon "CKNIW0";
		Inventory.PickupSound "CelestialKunai/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "CelestialCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			CKNI W 1 bright;
			Loop;
	}
}

class CelestialKunaiAntigravProjectile : CelestialKunaiProjectile {
	int effectIntervalTics;
	int ticksUntilEffect;
	
	Default {
		Scale 0.5;
		Speed 40;
		DamageFunction 40;
		Gravity 0.1;
		
		RenderStyle "Add";
		
		SeeSound "CelestialKunai/Projectile/Antigrav";
		DeathSound "CelestialKunai/Projectile/Antigrav/Explode";
		
		-NOGRAVITY;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
		
		detonated = false;
		
		effectIntervalTics = 5;
		ticksUntilEffect = 10;
	}
	
	override void Tick() {
		super.Tick();
		if (level.isFrozen() || self.isFrozen()) {
			return;
		}
		if (!detonated) {
			VisualAreaEffects(128);
		}
		else {
			VisualAreaEffects();
			if (--ticksUntilEffect == 0) {
				ticksUntilEffect = effectIntervalTics;
				ApplyAreaEffect();
			}
		}
	}
	
	virtual void VisualAreaEffects(int effectSize = 255) {
		int offsetMaxX = 0;
		int offsetMaxY = 0;
		int offsetMaxZ = 0;
		if (effectSize == 255) {
			offsetMaxX = random(-100, 100);
			offsetMaxY = random(-100, 100);
			offsetMaxZ = random(-100, 100);
		}
		self.SpawnParticle(
			"Gravity", 3 * TICRATE, 2,
			(offsetMaxX, offsetMaxY, offsetMaxZ),
			0, 0, 1.7
		);
	}
	
	void ApplyAreaEffect() {
		BlockThingsIterator iter = BlockThingsIterator.Create(self, 100, false);
		while (iter.Next()) {
			Actor effectTarget = iter.thing;
			if (CheckSight(effectTarget)) {
				if (SchismInteraction.CanHarmTarget(self.target, effectTarget) && !effectTarget.bBoss) {
					InflictEffect("SchismFloatEffect", effectTarget, 3 * 35);
				}
				else if (SchismInteraction.CanHelpTarget(self.target, effectTarget) && !effectTarget.bBoss) {
					//InflictEffect("SchismJumpBuff", effectTarget, 3 * 35);
				}
			}
		}
	}
	
	States {
		Spawn:
			CKNP FGHIJKLM 2;
			Loop;
			
		Death:
			CKNP N 0 {
				A_SetScale(1.5);
			}
			CKNP N 2 {
				SchismAttacks.Explode(invoker, invoker.target, 4, invoker.pullRange, -invoker.pullForce, 1, false, false, "Gravity");
				invoker.detonated = true;
			}
			CKNP OPQRSTUVWXY 2;
			TNT1 A 350;
			Stop;
	}
}
