class SpatialBlade : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Celestial Kunai can now use Spatial Blade!";
		
		Inventory.Icon "CKNIX0";
		Inventory.PickupSound "CelestialKunai/Pickup";
		Inventory.RestrictedTo "Haruhi";
		WeaponUpgrade.ExtraAmmoClass "CelestialCharge";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}
	
	States {
		Spawn:
			CKNI X 1 bright;
			Loop;
	}
}

class CelestialKunaiSpatialProjectile : CelestialKunaiProjectile {
	Default {
		Scale 0.5;
		Speed 60;
		DamageFunction 20;
		
		SeeSound "CelestialKunai/Projectile/Spatial";
		DeathSound "CelestialKunai/Projectile/Spatial/Explode";
		
		-RIPPER;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.generateSpecialAmmo = false;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0 && SchismInteraction.CanHarmTarget(self.target, targetActor)) {
			InflictEffect("CelestialKunaiSpatialEffect", targetActor, 10 * 35);
		}
		return specialDamage;
	}
	
	States {
		Spawn:
			CKNP FGHIJKLM 2;
			Loop;
			
		Death:
			CKNP N 0 {
				A_SetRenderStyle(1.0, STYLE_Add);
				A_SetScale(1.5);
			}
			CKNP N 2 {
				SchismAttacks.Explode(invoker, invoker.target, 4, invoker.pullRange, -invoker.pullForce, 1, false, false, "Gravity");
			}
			CKNP OPQRSTUVWXY 2;
			Stop;
	}
}

class CelestialKunaiSpatialEffect : SchismStatusEffect {
	Default {
		SchismStatusEffect.ParticleName "Gravity";
	}
}