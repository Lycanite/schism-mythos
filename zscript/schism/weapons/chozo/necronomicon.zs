class Necronomicon : DualHand {
	string weaponMode;
	string weaponModePrev;
	
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.RestrictedTo "Chozo";
		
		Inventory.PickupSound "Necronomicon/Pickup";
		Weapon.UpSound "Necronomicon/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "Necronomicon/Fire/Fail";
		
		Weapon.AmmoType1 "NecroticRune";
		Weapon.AmmoType2 "SoulEssence";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 25;
		SchismWeapon.SpecialACost 25;
		SchismWeapon.SpecialBCost 25;
		SchismWeapon.SpecialCCost 25;
		SchismWeapon.SpecialDCost 25;
		
		SchismWeapon.SpecialItemA "";
		SchismWeapon.SpecialItemB "NecronomiconBalancePage";
		SchismWeapon.SpecialItemC "NecronomiconSoulsPage";
		SchismWeapon.SpecialItemD "NecronomiconFrostPage";
		SchismWeapon.PassiveItem "NecronomiconMemoirPage";
		
		Inventory.PickupMessage "Behold the power of the Necronomicon!";
		Obituary "%o was purged by %k's Necronomicon!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Necrotic Runes to cast this spell.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Soul Essence to cast this spell.";
		SchismWeapon.NoSpecialAMessage "\caYou need a Bones Page to cast this spell.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Balance Page to cast this spell.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Souls Page to cast this spell.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Frost Page to cast this spell.";

		SchismWeapon.IdleBob 20;
		
		+INVENTORY.RESTRICTABSOLUTELY;
		+WEAPON.ALT_USES_BOTH;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		weaponMode = "bones";
		weaponModePrev = "bones";
	}
	
	action void A_OnAttack() {
		A_ClearBobbing();
		A_Light2();
	}
	
	action string A_GetWeaponMode(void) {
		return invoker.weaponMode;
	}
	
	action string A_GetWeaponModePrev(void) {
		return invoker.weaponModePrev;
	}
	
	action void A_SetWeaponMode(string weaponMode) {
		A_StartSound("Necronomicon/Up");
		invoker.weaponModePrev = invoker.weaponMode;
		invoker.weaponMode = weaponMode;
	}
	
	action void A_ChangeWeaponMode(string newWeaponMode) {
		A_ClearBobbing();
		A_SetOffHandState("OffHand.Change");
		if (newWeaponMode == "balance") {
			if (!A_CheckSpecial(2, false)) {
				return;
			}
			A_SetWeaponMode("balance");
			A_Light2();
		}
		else if (newWeaponMode == "souls") {
			if (!A_CheckSpecial(3, false)) {
				return;
			}
			A_SetWeaponMode("souls");
			A_Light2();
		}
		else if (newWeaponMode == "frost") {
			if (!A_CheckSpecial(4, false)) {
				return;
			}
			A_SetWeaponMode("frost");
			A_Light0();
		}
		else {
			A_SetWeaponMode("bones");
			A_Light0();
		}
	}
	
	// Primary Spells:
	action state A_BoneSplint(bool useAmmo) {
		A_MemoirSpirits();
		if (useAmmo && !A_SpendAmmo(1)) {
			return ResolveState("Ready");
		}
		A_FireProjectile("NecronomiconBoneSplintProjectile", 0, false);
		return null;
	}
	
	action state A_Soulfire() {
		A_MemoirSpirits();
		if (!A_SpendAmmo(1)) {
			return ResolveState("Ready");
		}
		A_FireProjectile("NecronomiconSoulfireProjectile", 0, false);
		return null;
	}
	
	action state A_ChainsOfIce() {
		A_MemoirSpirits();
		if (!A_SpendAmmo(1)) {
			return ResolveState("Ready");
		}
		A_FireProjectile("NecronomiconChainsOfIceProjectile", 0, false);
		return null;
	}
	
	action state A_NetherLightning(bool useAmmo) {
		A_MemoirSpirits();
		if (useAmmo && !A_SpendAmmo(1)) {
			return ResolveState("Ready");
		}
		Actor projectileActor = A_FireProjectile("NecronomiconNetherLightningProjectile", 0, false);
		NecronomiconNetherLightningProjectile clusterActor = NecronomiconNetherLightningProjectile(projectileActor);
		if (clusterActor) {
			clusterActor.SetSourceTarget(invoker);
		}
		return null;
	}
	
	// Secondary Spells:
	action void A_BonePit() {
		A_MemoirSpirits();
		if (!A_CheckAmmo(1) || !A_CheckAmmo(3)) {
			return;
		}
		A_SpendAmmo(1);
		A_SpendAmmo(3);
		
		A_StartSound("Necronomicon/Fire/BonePit");
		A_SpawnItemEx("NecronomiconBonePitProjectile", 80);
		A_SpawnItemEx("NecronomiconBonePitProjectile", -80);
		A_SpawnItemEx("NecronomiconBonePitProjectile", 0, 80);
		A_SpawnItemEx("NecronomiconBonePitProjectile", 0, -80);
		A_SpawnItemEx("NecronomiconBonePitProjectile", 60, 60);
		A_SpawnItemEx("NecronomiconBonePitProjectile", -60, 60);
		A_SpawnItemEx("NecronomiconBonePitProjectile", 60, -60);
		A_SpawnItemEx("NecronomiconBonePitProjectile", -60, -60);
	}
	
	action void A_Doomfire() {
		A_MemoirSpirits();
		if (!A_CheckAmmo(1) || !A_CheckAmmo(4)) {
			return;
		}
		A_SpendAmmo(1);
		A_SpendAmmo(4);
		
		A_StartSound("Necronomicon/Fire/Doomfire", CHAN_WEAPON);
		Actor projectile = A_FireProjectile("NecronomiconDoomfireSource", 0, false);
		FollowProjectile followProjectile = FollowProjectile(projectile);
		if (followProjectile) {
			followProjectile.setFollowTarget(invoker.owner, 0, 0, 60);
		}
	}
	
	action void A_SummonMinion(string minionClass, double xOffset, double yOffset, double zOffset, double xVel, double yVel, double zVel, double angle) {
		A_SpawnItemEx(minionClass, xOffset, yOffset, zOffset, xVel, yVel, zVel, angle, SXF_SETMASTER|SXF_NOCHECKPOSITION|SXF_ISTRACER);
		if (invoker.tracer && invoker.target) {
			invoker.tracer.target = invoker.target;
		}
	}
	
	action void A_FuryOfTheFrostDrake() {
		A_MemoirSpirits();
		if (!A_CheckAmmo(1) || !A_CheckAmmo(5)) {
			return;
		}
		A_SpendAmmo(1);
		A_SpendAmmo(5);
		
		A_StartSound("Necronomicon/Fire/FuryOfTheFrostDrake", CHAN_WEAPON);
		Actor projectile = A_FireProjectile("NecronomiconFuryOfTheFrostDrakeSource", 0, false);
		FollowProjectile followProjectile = FollowProjectile(projectile);
		if (followProjectile) {
			followProjectile.setFollowTarget(invoker.owner, 0, 0, 60);
		}
	}
	
	action void A_AetherLightning() {
		A_MemoirSpirits();
		if (!A_CheckAmmo(1) || !A_CheckAmmo(6)) {
			return;
		}
		A_SpendAmmo(1);
		A_SpendAmmo(6);
		
		Actor projectileActor = A_FireProjectile("NecronomiconAetherLightningProjectile", 0, false);
		NecronomiconNetherLightningProjectile clusterActor = NecronomiconNetherLightningProjectile(projectileActor);
		if (clusterActor) {
			clusterActor.SetSourceTarget(invoker);
		}
	}
	
	// Ultimate Final Page: TODO Implement!
	action void A_UltimateAbility() {
		A_StartSound("Necronomicon/Fire/Final", CHAN_WEAPON);
		A_FireProjectile("NecronomiconFinalProjectile", 0, false, 0, 0, FPF_NOAUTOAIM, -90);
	}

	// Passive Memoir Spirits:
	action void A_MemoirSpirits() {
		int passiveLevel = invoker.GetPassiveLevel();
		if (passiveLevel <= 0 || --invoker.passiveCooldown > 0) {
			return;
		}
		invoker.passiveCooldown = (10 - (passiveLevel * 2)) * TICRATE;
		for (int i = 0; i < passiveLevel; i++) {
			A_FireProjectile("NecronomiconMemoirSpiritProjectile", Random(-180, 180), false, 0, 0, 0, -invoker.owner.pitch);
		}
	}
	
	States {
		Spawn:
			NCRM X -1;
			Stop;
			
		// ========== Main Hand ==========
		MainHand.Select:
			NCRG A 1 A_Raise(12);
			Loop;
			
		MainHand.Deselect:
			NCRG A 1 A_Lower(12);
			Loop;
			
		MainHand.Ready:
			NCRG AABBCCBB 4 {
				A_MemoirSpirits();
				return A_SchismWeaponReady();
			}
			Loop;
			
		MainHand.Fire:
			NCRG A 0 A_OnAttack();
			NCRG A 0 A_JumpIf(A_GetWeaponMode() == "bones", "MainHand.Fire.Bones");
			NCRG A 0 A_JumpIf(A_GetWeaponMode() == "balance", "MainHand.Fire.Balance");
			NCRG A 0 A_JumpIf(A_GetWeaponMode() == "souls", "MainHand.Fire.Souls");
			NCRG A 0 A_JumpIf(A_GetWeaponMode() == "frost", "MainHand.Fire.Frost");
			Goto Ready;
			
		MainHand.AltFire:
			NCRG A 0 A_OnAttack();
			NCRG A 0 A_JumpIf(A_GetWeaponMode() == "bones", "MainHand.AltFire.Bones");
			NCRG A 0 A_JumpIf(A_GetWeaponMode() == "balance", "MainHand.AltFire.Balance");
			NCRG A 0 A_JumpIf(A_GetWeaponMode() == "souls", "MainHand.AltFire.Souls");
			NCRG A 0 A_JumpIf(A_GetWeaponMode() == "frost", "MainHand.AltFire.Frost");
			Goto Ready;
			
		Reload:
			NCRG A 0 A_JumpIf(A_GetWeaponModePrev() == "bones", "User1");
			NCRG A 0 A_JumpIf(A_GetWeaponModePrev() == "souls", "User2");
			NCRG A 0 A_JumpIf(A_GetWeaponModePrev() == "frost", "User3");
			NCRG A 0 A_JumpIf(A_GetWeaponModePrev() == "balance", "User4");
			Goto Ready;
			
		MainHand.User1:
			NCRG J 4 A_ChangeWeaponMode("bones");
			NCRG K 4;
			Goto Ready;
			
		MainHand.User2:
			NCRG J 4 A_ChangeWeaponMode("balance");
			NCRG K 4;
			Goto Ready;
			
		MainHand.User3:
			NCRG J 4 A_ChangeWeaponMode("souls");
			NCRG K 4;
			Goto Ready;
			
		MainHand.User4:
			NCRG J 4 A_ChangeWeaponMode("frost");
			NCRG K 4;
			Goto Ready;
			
		// ========== Primary Spells ==========
		// Bones:
		MainHand.Fire.Bones:
			NCRG DE 2;
		MainHand.ReFire.Bones:
			NCRG F 2 {
				return A_BoneSplint(true);
			}
			NCRG GH 2 A_BoneSplint(false);
			NCRG H 0 A_Refire("MainHand.ReFire.Bones");
			Goto Ready;
		
		// Balance:
		MainHand.Fire.Balance:
			NCRG DE 4;
		MainHand.ReFire.Balance:
			NCRG F 2 {
				return A_NetherLightning(true);
			}
			NCRG FGGHH 2 A_NetherLightning(false);
			NCRG H 0 A_Refire("MainHand.ReFire.Balance");
			Goto Ready;
		
		// Souls:
		MainHand.Fire.Souls:
			NCRG DE 4;
		MainHand.ReFire.Souls:
			NCRG F 4 {
				return A_Soulfire();
			}
			NCRG GH 4 A_SchismWeaponReady();
			NCRG H 0 A_Refire("MainHand.ReFire.Souls");
			Goto Ready;
		
		// Frost:
		MainHand.Fire.Frost:
			NCRG DE 4;
		MainHand.ReFire.Frost:
			NCRG F 4 {
				return A_ChainsOfIce();
			}
			NCRG GH 4 A_SchismWeaponReady();
			NCRG H 0 A_Refire("MainHand.ReFire.Frost");
			Goto Ready;
			
		// ========== Secondary Spells ==========
		// Bones:
		MainHand.AltFire.Bones:
			NCRG DE 4;
		MainHand.AltReFire.Bones:
			NCRG F 4 {
				return A_BonePit();
			}
			NCRG GH 4 A_SchismWeaponReady();
			NCRG H 0 A_Refire("MainHand.AltReFire.Bones");
			Goto Ready;
		
		// Balance:
		MainHand.AltFire.Balance:
			NCRG DE 4;
		MainHand.AltReFire.Balance:
			NCRG F 4 {
				return A_AetherLightning();
			}
			NCRG GH 4 A_SchismWeaponReady();
			NCRG H 0 A_Refire("MainHand.AltReFire.Balance");
			Goto Ready;
		
		// Souls:
		MainHand.AltFire.Souls:
			NCRG DE 4;
		MainHand.AltReFire.Souls:
			NCRG F 4 {
				return A_Doomfire();
			}
			NCRG GH 4 A_SchismWeaponReady();
			NCRG H 0 A_Refire("MainHand.AltReFire.Souls");
			Goto Ready;
		
		// Frost:
		MainHand.AltFire.Frost:
			NCRG DE 4;
		MainHand.AltReFire.Frost:
			NCRG F 4 {
				return A_FuryOfTheFrostDrake();
			}
			NCRG GH 4 A_SchismWeaponReady();
			NCRG H 0 A_Refire("MainHand.AltReFire.Frost");
			Goto Ready;
		
		// Ultimate:
		MainHand.Ultimate:
			NCRG DE 4;
			NCRG F 4 A_UltimateAbility();
			Goto Ready;
			
			
		// ========== Off Hand ==========
		OffHand.Select:
			NCRM CB 4;
		OffHand.Select.Loop:
			NCRM A 4;
			Loop;
			
		OffHand.Deselect:
			NCRM AB 4;
		OffHand.Deselect.Loop:
			NCRM C 4;
			Loop;
			
		OffHand.Ready:
		OffHand.Fire:
		OffHand.AltFire:
		OffHand.User1:
		OffHand.User2:
		OffHand.User3:
		OffHand.User4:
		OffHand.Reload:
			NCRM D 0 A_JumpIf(A_GetWeaponMode() == "bones", "OffHand.Ready.Bones");
			NCRM D 0 A_JumpIf(A_GetWeaponMode() == "balance", "OffHand.Ready.Balance");
			NCRM D 0 A_JumpIf(A_GetWeaponMode() == "souls", "OffHand.Ready.Souls");
			NCRM D 0 A_JumpIf(A_GetWeaponMode() == "frost", "OffHand.Ready.Frost");
		OffHand.Ready.Bones:
			NCRM DEF 8;
			Loop;
		OffHand.Ready.Souls:
			NCRM IJK 8;
			Loop;
		OffHand.Ready.Frost:
			NCRM NOP 8;
			Loop;
		OffHand.Ready.Balance:
			NCRM STU 8;
			Loop;
			
		OffHand.Change:
			NCRM G 0 A_JumpIf(A_GetWeaponModePrev() == "bones", "OffHand.Change.Bones");
			NCRM G 0 A_JumpIf(A_GetWeaponModePrev() == "balance", "OffHand.Change.Balance");
			NCRM G 0 A_JumpIf(A_GetWeaponModePrev() == "souls", "OffHand.Change.Souls");
			NCRM G 0 A_JumpIf(A_GetWeaponModePrev() == "frost", "OffHand.Change.Frost");
		OffHand.Change.Bones:
			NCRM GH 4;
			Goto OffHand.Ready;
		OffHand.Change.Souls:
			NCRM LM 4;
			Goto OffHand.Ready;
		OffHand.Change.Frost:
			NCRM QR 4;
			Goto OffHand.Ready;
		OffHand.Change.Balance:
			NCRM VW 4;
			Goto OffHand.Ready;
			
		OffHand.Ultimate:
			NCRM D 0 A_OverlayFlags(10, PSPF_FLIP|PSPF_MIRROR, true);
			NCRM DEF 4;
			Loop;
	}
}

#include "zscript/schism/weapons/chozo/necronomicon/bones.zs"
#include "zscript/schism/weapons/chozo/necronomicon/balance.zs"
#include "zscript/schism/weapons/chozo/necronomicon/souls.zs"
#include "zscript/schism/weapons/chozo/necronomicon/frost.zs"
#include "zscript/schism/weapons/chozo/necronomicon/memoir.zs"
#include "zscript/schism/weapons/chozo/necronomicon/final.zs"
