class CorpseExplosionSigil : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Grim Scythe can now cast Corpse Explosion at the cost of Soul Essence!";
		
		Inventory.Icon "GSCIB0";
		Inventory.PickupSound "GrimScythe/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "GrimRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			GSCI B 1 bright;
			Loop;
	}
}

class GrimScytheCorpseExplosionProjectile : SoulEssenceProjectile {
	Default {
		Radius 16;
		Height 10;
		Scale 3;
		Speed 20;
		FastSpeed 20;
		DamageFunction 4;
		Alpha 0.75;
		RenderStyle "Add";
		
		SeeSound "GrimScythe/Fire/Corpse";
		DeathSound "GrimScythe/Explode/Corpse";
		
		PROJECTILE
		+RANDOMIZE;
		+RIPPER;
		+FLOORHUGGER;
	}
	
	action void A_CorpseExplosion() {
		A_QuakeEx(2, 2, 2, 10, 0, 200, "world/quake", 0, 1, 1, 0, 0, 1, 1);
		A_StartSound("GrimScythe/Explode/Corpse");
		SchismAttacks.Explode(invoker, invoker.target, 60, 300);
		for (int i = 0; i < 360; i += 20) {
			int randomGore = random(0, 255);
			if (randomGore <= 60) {
				A_SpawnItemEx("SchismGoreHeart", 0, 0, 0, random(-6, 6), random(-6, 6), random(10, 40), i, SXF_CLIENTSIDE);
			}
			else {
				A_SpawnItemEx("SchismGoreBase", 0, 0, 0, random(-6, 6), random(-6, 6), random(10, 40), i, SXF_CLIENTSIDE);
			}
		}
	}
	
	States {
		Spawn:
			FXGR K 4;
			Goto Death;
			
		Death:
			FXGR L 4 A_CorpseExplosion();
			FXGR MNOPQ 4;
			Stop;
	}
}
