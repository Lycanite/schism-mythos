class DreadValleySigil : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Grim Scythe can now form a Dread Valley at the cost of Soul Essence!";
		
		Inventory.Icon "GSCIE0";
		Inventory.PickupSound "GrimScythe/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "GrimRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			GSCI E 1 bright;
			Loop;
	}
}

class GrimScytheDreadValley : GrimScytheDreadWall {
	Default {
		Scale 0.5;
	}
}