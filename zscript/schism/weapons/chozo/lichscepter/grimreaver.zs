class LichScepterReaverEffigy : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Lich Scepter can now cast Grim Reaver at the cost of Soul Essence!";
		
		Inventory.Icon "LSCPW0";
		Inventory.PickupSound "LichScepter/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "DeathRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			LSCP W 1 bright;
			Loop;
	}
}
