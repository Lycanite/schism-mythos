class LichScepterGhoulEffigy : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Lich Scepter can raise a toxic Ghouls to poison enemies at the cost of Soul Essence!";
		
		Inventory.Icon "LSCPX0";
		Inventory.PickupSound "LichScepter/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "DeathRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			LSCP X 1 bright;
			Loop;
	}
}
