class LichScepterDraugrEffigy : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Lich Scepter can raise a group of Draugrs at the cost of Soul Essence!";
		
		Inventory.Icon "LSCPY0";
		Inventory.PickupSound "LichScepter/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "DeathRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			LSCP Y 1 bright;
			Loop;
	}
}
