class LichScepterHarrowEffigy : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "Your Lich Scepter will now raise your foes to serve you in undeath!";
		
		Inventory.Icon "LSCPU0";
		Inventory.PickupSound "LichScepter/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "DeathRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			LSCP U 1 bright;
			Loop;
	}
}
