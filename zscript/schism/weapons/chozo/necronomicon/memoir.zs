class NecronomiconMemoirPage : WeaponPassive {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You found a Page of Memoirs by the sprits bound to the Necronomicon!";
		
		Inventory.Icon "NCRM]0";
		Inventory.PickupSound "Necronomicon/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "NecroticRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			NCRM ] 1 bright;
			Loop;
	}
}

class NecronomiconMemoirSpiritProjectile : SchismProjectile {
	Default {
		Radius 20;
		Height 10;
		Scale 1.5;
		Speed 20;
		FastSpeed 20;
		DamageFunction 10;
		Alpha 0.75;
		RenderStyle "Translucent";
		
		SeeSound "Necronomicon/Fire/MemoirSpirit";
		DeathSound "Necronomicon/Explode/MemoirSpirit";
		
		+NODAMAGETHRUST;
		+SEEKERMISSILE;
		+SCREENSEEKER;
	}

	action state A_MemoirSpiritUpdate() {
		// Seek new Tracer:
		if (!invoker.tracer && invoker.lifetime % TICRATE == 0) {
			invoker.tracer = invoker.FindNewTarget();
		}

		// Home on Tracer:
		A_SeekerMissile(0, 20, SMF_LOOK|SMF_PRECISE);
		if (invoker.lifetime > 20 * TICRATE) {
			return ResolveState("Death");
		}
		
		return null;
	}
	
	States {
		Spawn:
			NCR5 AB 4 {
				return A_MemoirSpiritUpdate();
			}
			Loop;
			
		Death:
			NCR5 CDEFGH 4;
			Stop;
	}
}
