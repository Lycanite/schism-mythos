class NecronomiconSoulsPage : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You found the Necronomicon Page of Souls!";
		
		Inventory.Icon "NCRMY0";
		Inventory.PickupSound "Necronomicon/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "NecroticRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			NCRM Z 1 bright;
			Loop;
	}
}

class NecronomiconSoulfireProjectile : SoulEssenceProjectile {
	Default {
		Radius 5;
		Height 7;
		Scale 1.5;
		Speed 60;
		DamageFunction 60;
		Alpha 1;
		RenderStyle "Normal";
		
		DamageType "Fire";
		
		SoulEssenceProjectile.SoulEssenceMin 0;
		SoulEssenceProjectile.SoulEssenceMin 0;
		
		SeeSound "Necronomicon/Fire/Soulfire";
		DeathSound "Necronomicon/Explode/Soulfire";

		SchismProjectile.ParticleName "Fire";
		
		+NODAMAGETHRUST;
	}
	
	States {
		Spawn:
			NCR2 ABC 2 bright;
			Loop;
			
		Death:
			NCR2 D 4 bright {
				SchismAttacks.Explode(invoker, invoker.target, 20, 200, 0, 1, false, false, "Fire");
			}
			NCR2 EFG 4 bright;
			Stop;
	}
}

class NecronomiconDoomfireSource : FollowProjectile {
	int remainingTics;
	
	Default {
		Scale 1;
		Radius 16;
		Height 10;
		Scale 2;
		Speed 0;
		DamageFunction 1;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		
		DamageType "Fire";

		SchismProjectile.ParticleName "Nether";
		
		SeeSound "";
		DeathSound "";
		
		-SOLID;
		+NOBLOCKMAP;
		+RIPPER;
		+CANNOTPUSH;
		+NODAMAGETHRUST;
		-THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.remainingTics = 20;
	}
	
	override void Tick() {
		super.Tick();
		if (self.followTarget) {
			self.angle = self.followTarget.angle;
			self.followTarget = null;
			self.Thrust(10, self.angle);
			self.AddZ(4);
		}
	}
	
	action state A_Doomfire() {
		if (invoker.remainingTics-- <= 0) {
			return invoker.ResolveState("Death");
		}
		
		for (int i = 0; i < 10; i++) {
			NecronomiconDoomfireProjectile projectileActor = NecronomiconDoomfireProjectile(A_SpawnProjectile("NecronomiconDoomfireProjectile", 0, 0, Random(-45, 45), CMF_AIMDIRECTION|CMF_TRACKOWNER, Random(-10, 25)));
			if (projectileActor) {
				projectileActor.target = invoker.target;
			}
		}
		
		return null;
	}
	
	States {
		Spawn:
			NCR2 ABC 4 bright {
				return A_Doomfire();
			}
			Loop;
			
		Death:
			NCR2 DEFG 4 bright;
			Stop;
	}
}

class NecronomiconDoomfireProjectile : NecronomiconSoulfireProjectile {
	Default {
		Scale 1;
		DamageFunction 5;
		Alpha 0.5;

		SchismProjectile.ParticleName "Fire";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		soulEssenceMin = 0;
		soulEssenceMax = 0;
	}
	
	override void SpawnParticles() {
		super.SpawnParticles();
		for (int i = 0; i < 3; i++) {
			self.SpawnParticle("Nether", TICRATE, 1, (FRandom(-10, 10), 0, FRandom(-4, 4)), Random(0, 359), Random(-45, 45));
		}
	}
}
