class NecronomiconFrostPage : WeaponUpgrade {
	Default {
		Scale 0.5;
		Inventory.PickupMessage "You found the Necronomicon Page of Frost!";
		
		Inventory.Icon "NCRMZ0";
		Inventory.PickupSound "Necronomicon/Pickup";
		Inventory.RestrictedTo "Chozo";
		WeaponUpgrade.ExtraAmmoClass "NecroticRune";
		
		RenderStyle "Normal";
		
		+FLOATBOB;
	}

	States {
		Spawn:
			NCRM [ 1 bright;
			Loop;
	}
}

class NecronomiconChainsOfIceProjectile : SoulEssenceProjectile {
	Default {
		Radius 5;
		Height 7;
		Scale 1.5;
		Speed 60;
		DamageFunction 60;
		Alpha 1;
		RenderStyle "Translucent";
		
		DamageType "Ice";
		
		SoulEssenceProjectile.SoulEssenceMin 0;
		SoulEssenceProjectile.SoulEssenceMin 0;
		
		SeeSound "Necronomicon/Fire/ChainsOfIce";
		DeathSound "Necronomicon/Explode/ChainsOfIce";
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0) {
			InflictEffect("SchismChillDebuff", targetActor, 10 * TICRATE);
		}
		return specialDamage;
	}
	
	override void SpawnParticles() {
		for (int i = 0; i < 2; i++) {
			self.SpawnParticle("Frost", TICRATE, 1, (FRandom(-20, 20), 0, FRandom(-4, 4)), Random(0, 359), Random(-45, 45));
			self.SpawnParticle("Snowflake", 2 * TICRATE, 3, (FRandom(-20, 20), 0, FRandom(-4, 4)), Random(0, 359), Random(-45, 45));
		}
	}
	
	States {
		Spawn:
			NCR3 A 2 bright;
			Loop;
			
		Death:
			NCR3 B 4 bright {
				SchismAttacks.Explode(invoker, invoker.target, 20, 200, 0, 1, false, false, "Frost");
			}
			NCR3 CDEF 4 bright;
			Stop;
	}
}

class NecronomiconFuryOfTheFrostDrakeSource : FollowProjectile {
	int remainingTics;
	
	Default {
		Scale 1;
		Radius 16;
		Height 10;
		Scale 2;
		Speed 0;
		DamageFunction 1;
		Gravity 0;
		Alpha 1;
		Renderstyle "Add";
		
		DamageType "Ice";

		SchismProjectile.ParticleName "Frost";
		
		SeeSound "";
		DeathSound "";
		
		-SOLID;
		+NOBLOCKMAP;
		+RIPPER;
		+CANNOTPUSH;
		+NODAMAGETHRUST;
		-THRUGHOST;
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.remainingTics = 20;
	}
	
	override void Tick() {
		super.Tick();
		if (self.followTarget) {
			self.angle = self.followTarget.angle;
			self.followTarget = null;
			self.Thrust(10, self.angle);
			self.AddZ(4);
		}
	}
	
	action state A_FrostBreath() {
		if (invoker.remainingTics-- <= 0) {
			return ResolveState("Death");
		}
		
		for (int i = 0; i < 10; i++) {
			NecronomiconFuryOfTheFrostDrakeProjectile projectileActor = NecronomiconFuryOfTheFrostDrakeProjectile(A_SpawnProjectile("NecronomiconFuryOfTheFrostDrakeProjectile", 0, 0, Random(-45, 45), CMF_AIMDIRECTION|CMF_TRACKOWNER, Random(-10, 25)));
			if (projectileActor) {
				projectileActor.target = target;
			}
		}
		
		return null;
	}
	
	States {
		Spawn:
			FWYV ABCDCB 4 bright {
				return A_FrostBreath();
			}
			Loop;
			
		Death:
			FWYV F 2 bright A_FadeOut(0.1);
			Loop;
	}
}

class NecronomiconFuryOfTheFrostDrakeProjectile : NecronomiconChainsOfIceProjectile {
	Default {
		Scale 1;
		DamageFunction 5;
		Alpha 0.5;

		SchismProjectile.ParticleName "Frost";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		soulEssenceMin = 0;
		soulEssenceMax = 0;
	}
	
	override void SpawnParticles() {
		super.SpawnParticles();
		for (int i = 0; i < 3; i++) {
			self.SpawnParticle("Snowflake", TICRATE, 1, (FRandom(-10, 10), 0, FRandom(-4, 4)), Random(0, 359), Random(-45, 45));
		}
	}
}
