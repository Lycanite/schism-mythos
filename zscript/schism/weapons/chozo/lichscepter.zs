class LichScepter : SchismWeapon {
	Default {
		Radius 20;
		Height 16;
		Scale 0.5;
		
		Inventory.RestrictedTo "Chozo";
		
		Inventory.PickupSound "LichScepter/Pickup";
		Weapon.UpSound "LichScepter/Up";
		Weapon.ReadySound "";
		SchismWeapon.FailSound "LichScepter/Fire/Fail";
		
		Weapon.AmmoType1 "DeathRune";
		Weapon.AmmoType2 "SoulEssence";
		
		SchismWeapon.PrimaryCost 1;
		SchismWeapon.SecondaryCost 2;
		SchismWeapon.SpecialACost 20;
		SchismWeapon.SpecialBCost 20;
		SchismWeapon.SpecialCCost 20;
		SchismWeapon.SpecialDCost 20;
		
		SchismWeapon.SpecialItemA "LichScepterOssifexEffigy";
		SchismWeapon.SpecialItemB "LichScepterReaverEffigy";
		SchismWeapon.SpecialItemC "LichScepterGhoulEffigy";
		SchismWeapon.SpecialItemD "LichScepterDraugrEffigy";
		SchismWeapon.PassiveItem "LichScepterHarrowEffigy";
		
		Inventory.PickupMessage "You found the the pinacle of undeath magic, the Lich Scepter!";
		Obituary "%o was undone by %k's Lich Scepter!";
		SchismWeapon.InsufficientAmmoAMessage "\caNot enough Necrotic Runes to cast this spell.";
		SchismWeapon.InsufficientAmmoBMessage "\caNot enough Soul Essence to cast this spell.";
		SchismWeapon.NoSpecialAMessage "\caYou need an Ossifex Effigy to cast this spell.";
		SchismWeapon.NoSpecialBMessage "\caYou need a Reaver Effigy to cast this spell.";
		SchismWeapon.NoSpecialCMessage "\caYou need a Ghost Effigy to cast this spell.";
		SchismWeapon.NoSpecialDMessage "\caYou need a Draugr Effigy to cast this spell.";

		SchismWeapon.IdleBob 20;
		
		+INVENTORY.RESTRICTABSOLUTELY;
		+WEAPON.AMMO_OPTIONAL;
		+WEAPON.ALT_AMMO_OPTIONAL;
		+WEAPON.NOALERT;
	}
	
	// Primary Death Nova:
	action void A_DeathNova() {
		if (!A_SpendAmmo(1)) {
			return;
		}
		A_StartSound("LichScepter/Fire", CHAN_WEAPON);
		LichScepterProjectile lichScepterProjectile = LichScepterProjectile(A_FireProjectile("LichScepterProjectile", 0, false, 0, 0, 0, 0));
		if (lichScepterProjectile) {
			lichScepterProjectile.summonMaster = invoker.owner;
			lichScepterProjectile.lichScepter = invoker;
		}
		lichScepterProjectile = LichScepterProjectile(A_FireProjectile("LichScepterProjectile", 0, false, 0, 0, 0, 0));
		if (lichScepterProjectile) {
			lichScepterProjectile.summonMaster = invoker.owner;
			lichScepterProjectile.lichScepter = invoker;
			lichScepterProjectile.weaveDistance = 4;
			lichScepterProjectile.scale = lichScepterProjectile.scale * 0.75;
		}
		lichScepterProjectile = LichScepterProjectile(A_FireProjectile("LichScepterProjectile", 0, false, 0, 0, 0, 0));
		if (lichScepterProjectile) {
			lichScepterProjectile.summonMaster = invoker.owner;
			lichScepterProjectile.lichScepter = invoker;
			lichScepterProjectile.weaveDistance = -4;
			lichScepterProjectile.scale = lichScepterProjectile.scale * 0.75;
		}
	}
	
	// Secondary Chill of The Lich:
	action void A_ChillOfTheLich() {
		if (!A_SpendAmmo(2)) {
			return;
		}
		A_StartSound("LichScepter/Fire", CHAN_WEAPON);
		for (int i = 0; i <= 2; i++) {
			LichScepterProjectile lichScepterProjectile = LichScepterProjectile(A_FireProjectile("LichScepterProjectileChill", Random(-30, 30), false, Random(-5, 5), Random(-20, 20)));
			if (lichScepterProjectile) {
				lichScepterProjectile.summonMaster = invoker.owner;
				lichScepterProjectile.lichScepter = invoker;
			}
		}
	}
	
	// Special 1 Raise Ossifex:
	action void A_RaiseOssifex() {
		A_SpendAmmo(3);
		A_StartSound("LichScepter/Fire/Ossifex");
		
		bool spawned;
		Actor minion;
		[spawned, minion] = invoker.owner.A_SpawnItemEx("OssifexKeppel", invoker.owner.radius + 24, 0, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION);
		if (minion) {
			minion.master = invoker.owner;
			for (int i = 0; i < 20; i += 20) {
				invoker.owner.A_SpawnItemEx("SchismGoreBone", invoker.owner.radius + 24, 0, 10, Random(-6, 6), Random(-6, 6), Random(10, 40), i, SXF_CLIENTSIDE);
			}
		}
	}
	
	// Special 2 Grim Reaver:
	action void A_GrimReaver() {
		A_SpendAmmo(4);
		A_StartSound("LichScepter/Fire/Reaver");
		
		bool spawned;
		Actor minion;
		[spawned, minion] = invoker.owner.A_SpawnItemEx("GrimreaverKeppel", invoker.owner.radius + 24, 0, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION);
		if (minion) {
			minion.master = invoker.owner;
			GrimReaver grimReaver = GrimReaver(minion);
			if (grimReaver) {
				grimReaver.lichScepter = invoker;
			}
			for (int i = 0; i < 20; i += 20) {
				invoker.owner.A_SpawnItemEx("SchismGoreBone", invoker.owner.radius + 24, 0, 10, Random(-6, 6), Random(-6, 6), Random(10, 40), i, SXF_CLIENTSIDE);
			}
		}
	}
	
	// Special 3 Raise Ghoul: TODO
	action void A_RaiseGhoul() {
		A_SpendAmmo(5);
		A_StartSound("LichScepter/Fire/Ghoul");
		
		bool spawned;
		Actor minion;
		[spawned, minion] = invoker.owner.A_SpawnItemEx("GhoulKeppel", invoker.owner.radius + 24, 0, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION);
		if (minion) {
			minion.master = invoker.owner;
			for (int i = 0; i < 20; i += 20) {
				invoker.owner.A_SpawnItemEx("SchismGoreBone", invoker.owner.radius + 24, 0, 10, Random(-6, 6), Random(-6, 6), Random(10, 40), i, SXF_CLIENTSIDE);
			}
		}
	}
	
	// Special 4 Raise Draugr:
	action void A_RaiseDraugr() {
		A_SpendAmmo(6);
		A_StartSound("LichScepter/Fire/Draugr");
		
		bool spawned;
		Actor minion;
		[spawned, minion] = invoker.owner.A_SpawnItemEx("DraugrKeppel", invoker.owner.radius + 24, 40, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION);
		if (minion) {
			minion.master = invoker.owner;
		}
		[spawned, minion] = invoker.owner.A_SpawnItemEx("DraugrKeppel", invoker.owner.radius + 24, -40, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION);
		if (minion) {
			minion.master = invoker.owner;
		}
		[spawned, minion] = invoker.owner.A_SpawnItemEx("DraugrKeppel", invoker.owner.radius - 24, 0, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION);
		if (minion) {
			minion.master = invoker.owner;
		}
		[spawned, minion] = invoker.owner.A_SpawnItemEx("DraugrKeppel", 0, invoker.owner.radius + 24, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION);
		if (minion) {
			minion.master = invoker.owner;
		}
		[spawned, minion] = invoker.owner.A_SpawnItemEx("DraugrKeppel", 0, invoker.owner.radius - 24, 0, 0, 0, 0, 0, SXF_NOCHECKPOSITION);
		if (minion) {
			minion.master = invoker.owner;
		}
		for (int i = 0; i < 20; i += 20) {
			invoker.owner.A_SpawnItemEx("SchismGoreBone", invoker.owner.radius + 24, 0, 10, Random(-6, 6), Random(-6, 6), Random(10, 40), i, SXF_CLIENTSIDE);
		}
	}
	
	// Ultimate Army of The Dead:
	action void A_ArmyOfTheDead() {
		A_StartSound("LichScepter/Fire/Army");
		A_FireProjectile("LichScepterArmyProjectile", 0, false, 0, 0, FPF_NOAUTOAIM, -90);
	}
	
	States {
		Spawn:
			LSCP Z -1;
			Stop;
			
		Ready:
			LSCP AAAABBBBCCCCBBBB 2 {
				return A_SchismWeaponReady();
			}
			Loop;
			
		Deselect:
			LSCP A 1 A_Lower(12);
			Loop;
			
		Select:
			LSCP A 1 A_Raise(12);
			Loop;
			
		Fire:
			LSCP D 4 A_ClearBobbing();
			LSCP E 4;
		Hold:
			LSCP F 4;
			LSCP G 4 A_DeathNova();
			LSCP H 4;
			LSCP E 4 A_ReFire("Hold");
			goto Ready;
			
		AltFire:
			LSCP I 8 A_ClearBobbing();
		AltHold:
			LSCP J 4 A_ChillOfTheLich();
			LSCP K 4;
			LSCP I 4 A_ReFire("AltHold");
			goto Ready;
		
		User1:
			LSCP D 2 A_ClearBobbing();
			LSCP D 0 {
				return A_CheckSpecialJump(1, "Ready");
			}
			LSCP EF 2;
			LSCP G 2 A_RaiseOssifex();
			LSCP H 2 A_Light1();
			LSCP F 2 A_Light0();
			LSCP D 2;
			goto Ready;
		
		User2:
			LSCP D 2 A_ClearBobbing();
			LSCP D 0 {
				return A_CheckSpecialJump(2, "Ready");
			}
			LSCP EF 2;
			LSCP G 2 A_GrimReaver();
			LSCP H 2 A_Light1();
			LSCP F 2 A_Light0();
			LSCP D 2;
			goto Ready;
		
		User3:
			LSCP I 4 A_ClearBobbing();
			LSCP D 0 {
				return A_CheckSpecialJump(3, "Ready");
			}
			LSCP J 4 A_RaiseGhoul();
			LSCP K 4 A_Light0();
			goto Ready;
		
		User4:
			LSCP I 4 A_ClearBobbing();
			LSCP D 0 {
				return A_CheckSpecialJump(4, "Ready");
			}
			LSCP J 4 A_RaiseDraugr();
			LSCP K 4 A_Light0();
			goto Ready;
		
		Ultimate:
			LSCP I 4 A_ClearBobbing();
			LSCP J 4 A_ArmyOfTheDead();
			LSCP K 4 A_Light0();
			goto Ready;
	}
}

class LichScepterProjectile : SoulEssenceProjectile {
	LichScepter lichScepter;
	float weaveDistance;
	int remainingTics;
	
	Default {
		Radius 5;
		Height 7;
		Scale 1.5;
		Speed 20;
		DamageFunction 40;
		DamageType "Nether";
		Alpha 0.5;
		RenderStyle "Translucent";

		SchismProjectile.ParticleName "Nether";
		SchismProjectile.SummonFlags SXF_NOCHECKPOSITION;
		
		SoulEssenceProjectile.SoulEssenceMin 1;
		SoulEssenceProjectile.SoulEssenceMin 3;
		
		SeeSound "LichScepter/Fire";
		DeathSound "LichScepter/Explode";
	}
	
	override void BeginPlay() {
		super.BeginPlay();
		self.remainingTics = 9;
	}

	override void OnKill(Actor victim) {
		super.OnKill(victim);

		// Death Harrow Passive:
		if (self.lichScepter && victim) {
			int passiveLevel = self.lichScepter.GetPassiveLevel();
			bool raiseUndead = passiveLevel >= 2 || (passiveLevel > 0 && Random(0, 2) == 0);
			if (raiseUndead) {
				if (victim.SpawnHealth() >= 300) {
					self.SummonMinion((0, 0, 0), (0, 0, 0), Random(0, 1) ? "OssifexKeppel" : "GrimReaverKeppel");
				} else {
					self.SummonMinion((0, 0, 0), (0, 0, 0), "DraugrKeppel");
				}
				victim.A_Burst("SchismGoreBone");
			}
		}
	}
	
	action state A_Update() {
		if (--invoker.remainingTics <= 0) {
			A_ChangeVelocity(0, 0, 0, CVF_REPLACE);
			return ResolveState("Death");
		}
		
		A_StartSound("LichScepter/Projectile", CHAN_AUTO, 1.0, true);
		if (invoker.weaveDistance > 0) {
			invoker.weaveDistance += 0.5;
		}
		else if (invoker.weaveDistance < 0) {
			invoker.weaveDistance -= 0.25;
		}
		A_Weave(10, 0, invoker.weaveDistance, 0);
		
		return null;
	}
	
	States {
		Spawn:
			LSP1 ABCDEF 4 bright { return A_Update(); }
			Loop;
			
		Death:
			LSP1 G 4 bright A_StopSound(CHAN_AUTO);
			LSP1 H 4 bright A_StartSound("LichScepter/Explode");
			LSP1 IJK 4 bright;
		DeathFade:
			LSP1 L 4 bright A_FadeOut(0.1);
			Loop;
	}
}

class LichScepterProjectileChill : LichScepterProjectile {
	Default {
		Radius 10;
		Height 12;
		Scale 1;
		Speed 40;
		DamageFunction 40;
		DamageType "Ice";
		Alpha 1;
		RenderStyle "Add";

		SchismProjectile.ParticleName "Frost";
		
		SeeSound "LichScepter/Fire/Chill";
		DeathSound "LichScepter/Explode/Chill";

		+RIPPER;
	}
	
	override int DoSpecialDamage(Actor targetActor, int damage, Name damageType) {
		int specialDamage = super.DoSpecialDamage(targetActor, damage, damageType);
		if (specialDamage > 0) {
			InflictEffect("SchismChillDebuff", targetActor, 10 * TICRATE);
		}
		return specialDamage;
	}
	
	States {
		Spawn:
			LSP2 AB 4 bright;
			Loop;
			
		Death:
			LSP2 CDEFGH 4 bright;
			Stop;
	}
}

#include "zscript/schism/weapons/chozo/lichscepter/raiseossifex.zs"
#include "zscript/schism/weapons/chozo/lichscepter/grimreaver.zs"
#include "zscript/schism/weapons/chozo/lichscepter/raiseghoul.zs"
#include "zscript/schism/weapons/chozo/lichscepter/raisedraugr.zs"
#include "zscript/schism/weapons/chozo/lichscepter/deathharrow.zs"
#include "zscript/schism/weapons/chozo/lichscepter/armyofthedead.zs"
