#!/bin/bash
set -e

for file in *.png; do
    convert $file -channel RGB -negate $file
done
