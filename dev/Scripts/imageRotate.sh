#!/bin/bash
set -e

for file in *.png; do
    convert $file -rotate $1 $file
done
