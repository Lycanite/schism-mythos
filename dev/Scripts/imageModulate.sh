#!/bin/bash
set -e

for file in *.png; do
    convert $file -modulate $1,$2,$3 $file
done