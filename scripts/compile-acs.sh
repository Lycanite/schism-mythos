#!/bin/bash
set -e

for file in acs/*.acs; do
    acc $file
done
for file in acs/maps/*.acs; do
    acc $file
done