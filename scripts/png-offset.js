const extract = require("png-chunks-extract");
const encode = require("png-chunks-encode");
const path = require("path");
const fs = require("fs");
const process = require("process");

/**
 * Converts an Uint8Array to an integer.
 * @param {Uint8Array} u8 The Uint8Array to convert.
 * @returns The integer value of the Uint8Array.
 */
function u8ToInt(u8) {
    return new DataView(u8.buffer).getInt32();
}

/**
 * Converts an integer to an Uint8Array.
 * @param {number} i The integer to convert.
 * @returns The Uint8Array value of the integer.
 */
function intToU8(i) {
    return Uint8Array.of(
        (i&0xff000000)>>24,
        (i&0x00ff0000)>>16,
        (i&0x0000ff00)>> 8,
        (i&0x000000ff)>> 0);
}

/**
 * Applies Doom Sprite offset metadata to a png image.
 * @param {string} fullPath The full path of the png file.
 * @param {string} mode The mode, can be manual for a specific xy offset, monster/object for centered horizontally and 4 pixels off the ground vertically, floating to have it float 29 off the ground or projectile for full centered.
 * @param {number} x The x offset to set when in manual mode.
 * @param {number} y The y offset to set when in manual mode.
 */
function pngOffset(fullPath, mode, x, y) {
    // Read:
    const buffer = fs.readFileSync(fullPath);
    const chunks = extract(buffer);
    const widthU8 = new Uint8Array(chunks.find(chunk => chunk.name == "IHDR").data.slice(0, 4));
    const heightU8 = new Uint8Array(chunks.find(chunk => chunk.name == "IHDR").data.slice(4, 8));
    const dimensions = {
        width: u8ToInt(widthU8),
        height: u8ToInt(heightU8),
    };

    // Offsets
    const offsets = {
        x,
        y,
    };
    if (mode === "monster" || mode === "object") {
        offsets.x = Math.round(dimensions.width / 2) + x;
        offsets.y = dimensions.height - 4 + y;
    }
    else if (mode === "floating") {
        offsets.x = Math.round(dimensions.width / 2) + x;
        offsets.y = dimensions.height + 28 + y;
    }
    else if (mode === "projectile") {
        offsets.x = Math.round(dimensions.width / 2) + x;
        offsets.y = Math.round(dimensions.height / 2) + y;
    }

    // Grab:
    let grAb = chunks.find(chunk => chunk.name == "grAb");
    if (!grAb) {
        grAb = {
            name: "grAb",
            data: [
                0, 0, 0, 0,
                0, 0, 0, 0,
            ],
        }
        chunks.splice(1, 0, grAb);
    }

    // Write:
    grAb.data = [
        ...intToU8(offsets.x),
        ...intToU8(offsets.y),
    ];
    fs.writeFileSync(fullPath, Buffer.from(encode(chunks)));
    console.log(`Offset metadata set! File: ${fullPath} Dimensions: ${dimensions.width} x ${dimensions.height} Offset: ${offsets.x} x ${offsets.y}`);
}

// Path Check:
if (!process.argv[2]) {
    throw new Error("Missing filepath. Useage: 'pngOffset filepath x y'. Offset may also be 'monster', 'object', 'floating' or 'projectile' in place of x and y for auto offsets.");
}

// Offset PNG:
const modes = ["monster", "object", "floating", "projectile"];
let mode = "manual";
let x = 0;
let y = 0;
if (modes.includes(process.argv[3])) {
    mode = process.argv[3];
    x = process.argv[4] ? parseInt(process.argv[4].replaceAll("_", "-")) : 0;
    y = process.argv[5] ? parseInt(process.argv[5].replaceAll("_", "-")) : 0;
    console.log(`Mode offsets: ${x} x ${y}`);
}
else {
    if (parseInt(process.argv[3].replaceAll("_", "-")) === NaN || parseInt(process.argv[4].replaceAll("_", "-")) === NaN) {
        throw new Error("Missing or invalid offsets. Provide either x and y offsets or the mode 'monster', 'object', 'floating' or 'projectile'.");
    }
    x = parseInt(process.argv[3].replaceAll("_", "-"));
    y = parseInt(process.argv[4].replaceAll("_", "-"));
}

// Apply To Files:
function applyToFiles(relativePath) {
    const rootPath = __dirname.replace("/scripts", "");
    const fullPath = path.join(rootPath, relativePath.replace(rootPath, ""));
    if (fullPath.endsWith(".png")) {
        pngOffset(fullPath, mode, x, y);
    } else {
        const filenames = fs.readdirSync(fullPath);
        for (const filename of filenames) {
            const filePath = path.join(fullPath, filename);
            if (filename.endsWith(".png")) {
                pngOffset(filePath, mode, x, y);
            }
            if (fs.lstatSync(filePath).isDirectory()) {
                applyToFiles(path.join(relativePath, filename));
            }
        }
    }
}
applyToFiles(process.argv[2]);